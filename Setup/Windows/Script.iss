[Setup]
AppId={{C3115557-FB6B-4951-A91C-1B85EB95G5FA}
AppName=STWFA
AppVersion=1.0.0.2
;AppVerName=STWFA 1.0.0.2
AppPublisher=http://jonoie.com/STWFA
AppPublisherURL=http://jonoie.com/STWFA
AppSupportURL=http://jonoie.com/STWFA
AppUpdatesURL=http://jonoie.com/STWFA
DefaultDirName={pf}\STWFA
DisableProgramGroupPage=yes
OutputDir=Output
OutputBaseFilename=STWFASetup
Compression=lzma
SolidCompression=yes
PrivilegesRequired=admin
UninstallDisplayIcon={app}\SavingTheWorldFromAngriness.exe
SetupIconFile=installer.ico
InfoBeforeFile=info.txt

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Dirs]
Name: "{app}"; Permissions: everyone-full;
Name: "{app}\Content"; Permissions: everyone-full;
Name: "{app}\lib"; Permissions: everyone-full;
Name: "{app}\Saves"; Permissions: everyone-full;

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"

[Files]
Source: "compiler:..\Files\SavingTheWorldFromAngriness.exe"; DestDir: "{app}"; Flags: ignoreversion; Permissions: everyone-full;
Source: "compiler:..\Files\SavingTheWorldFromAngriness.exe.config"; DestDir: "{app}"; Flags: ignoreversion; Permissions: everyone-full;
Source: "compiler:..\Files\Content\*"; DestDir: "{app}\Content"; Flags: ignoreversion recursesubdirs createallsubdirs; Permissions: everyone-full;
Source: "compiler:..\Files\lib\*"; DestDir: "{app}\lib"; Flags: ignoreversion recursesubdirs createallsubdirs; Permissions: everyone-full;

[Icons]
Name: "{commonprograms}\STWFA"; Filename: "{app}\SavingTheWorldFromAngriness.exe";
Name: "{commondesktop}\STWFA"; Filename: "{app}\SavingTheWorldFromAngriness.exe"; Tasks: desktopicon;

[Run]
Filename: "{app}\SavingTheWorldFromAngriness.exe"; Description: "{cm:LaunchProgram,STWFA}"; Flags: nowait postinstall shellexec skipifsilent runasoriginaluser