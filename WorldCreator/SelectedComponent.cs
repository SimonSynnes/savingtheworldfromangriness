﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace WorldCreator
{
    public class SelectedComponent
    {
        public Rectangle rectangle;
        public string argument;
        public string className;

        public SelectedComponent(Rectangle rectangle, string argument, string className)
        {
            this.rectangle = rectangle;
            this.argument = argument;
            this.className = className;
        }

        public string GenerateCode(Vector2 position)
        {
            string returnValue = argument;

            string componentName = className + SavingTheWorldFromAngriness.Global.GameConstants.RNDM.Next(1000, 9999).ToString();

            returnValue = returnValue.Replace("|", ",");
            returnValue = String.Format(returnValue, String.Format("new Rectangle({0}, {1}, {2}, {3})", (int)position.X, (int)position.Y, rectangle.Width, rectangle.Height));
            returnValue = className + " " + componentName + " = new " + className + "(" + returnValue + ");\nAddGameComponent(" + componentName + ");";

            return returnValue;
        }
    }
}
