﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldCreator
{
    public class Component
    {
        public string className;
        public List<string> constructorNames;
        public List<Type> constructorTypes;

        public Component(string className, List<string> constructorNames, List<Type> constructorTypes)
        {
            this.className = className;
            this.constructorNames = constructorNames;
            this.constructorTypes = constructorTypes;
        }
    }
}
