﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorldCreator.WinForms
{
    public partial class frmExport : Form
    {
        public frmExport(string WorldCode)
        {
            InitializeComponent();

            txtWorld.Text = WorldCode;
        }
    }
}
