﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorldCreator.WinForms
{
    public partial class FrmNewWorld : Form
    {
        public string RETURN_TILEX { get; set; }
        public string RETURN_TILEY { get; set; }

        public FrmNewWorld()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            RETURN_TILEX = txtTileX.Text;
            RETURN_TILEY = txtTileY.Text;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
    }
}
