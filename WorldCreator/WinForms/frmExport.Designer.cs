﻿namespace WorldCreator.WinForms
{
    partial class frmExport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtWorld = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtWorld
            // 
            this.txtWorld.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtWorld.Location = new System.Drawing.Point(0, 0);
            this.txtWorld.Multiline = true;
            this.txtWorld.Name = "txtWorld";
            this.txtWorld.Size = new System.Drawing.Size(841, 486);
            this.txtWorld.TabIndex = 0;
            // 
            // frmExport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 486);
            this.Controls.Add(this.txtWorld);
            this.Name = "frmExport";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtWorld;
    }
}