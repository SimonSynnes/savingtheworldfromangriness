﻿namespace WorldCreator.WinForms
{
    partial class frmImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtWorld = new System.Windows.Forms.TextBox();
            this.btnImport = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtWorld
            // 
            this.txtWorld.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtWorld.Location = new System.Drawing.Point(0, 0);
            this.txtWorld.MaxLength = 100000;
            this.txtWorld.Multiline = true;
            this.txtWorld.Name = "txtWorld";
            this.txtWorld.Size = new System.Drawing.Size(1073, 474);
            this.txtWorld.TabIndex = 0;
            // 
            // btnImport
            // 
            this.btnImport.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnImport.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImport.Location = new System.Drawing.Point(0, 409);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(1073, 65);
            this.btnImport.TabIndex = 1;
            this.btnImport.Text = "import";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // frmImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1073, 474);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.txtWorld);
            this.Name = "frmImport";
            this.ShowIcon = false;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtWorld;
        private System.Windows.Forms.Button btnImport;
    }
}