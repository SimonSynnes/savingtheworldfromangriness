﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorldCreator.WinForms
{
    public partial class frmComponent : Form
    {
        public SelectedComponent SELECTED_COMPONENT { get; set; }
        private string className;

        public frmComponent(Component component)
        {
            InitializeComponent();

            SELECTED_COMPONENT = null;
            AddGameComponentComponents(component);
        }

        public void AddGameComponentComponents(Component component)
        {
            this.className = component.className;
            this.Text = component.className;

            int yPos = 10;

            int count = 0;
            foreach (Type type in component.constructorTypes)
            {
                Label label = new Label();
                label.Text = "(" + type.Name.ToString() + ") " + component.constructorNames[count] + " ";
                label.Location = new Point(20, yPos + 3);
                label.AutoSize = true;
                Controls.Add(label);

                TextBox textBox = new TextBox();
                textBox.Location = new Point(label.Location.X + label.Width + 5, yPos);
                textBox.Width = 300;
                Controls.Add(textBox);

                if (component.constructorNames[count] == "game")
                {
                    textBox.Text = "Game";
                }
                else
                if (component.constructorNames[count] == "worldRenderer")
                {
                    textBox.Text = "worldRenderer";
                }
                else
                if (component.constructorNames[count] == "basicMap")
                {
                    textBox.Text = "this";
                }
                else
                if (component.constructorNames[count] == "componentRectangle")
                {
                    textBox.Text = "new Rectangle(0, 0, 0, 0)";
                }
                else
                if (component.constructorNames[count] == "player")
                {
                    textBox.Text = "player";
                }
                else
                {
                    if (type == typeof(String))
                    {
                        textBox.Text = "\"\"";
                    }
                    else
                    if (type == typeof(Microsoft.Xna.Framework.Rectangle))
                    {
                        textBox.Text = "new Rectangle(0, 0, 0, 0)";
                    }
                    else
                    {
                        textBox.Text = "1";
                    }
                }

                count++;
                yPos += 40;
                Height += 50;
                btnOK.Location = new Point(btnOK.Location.X, btnOK.Location.Y + 50);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string argument = "";
            string componentRectangle = "";

            int count = 0;
            foreach (Control control in Controls)
            {
                if (control.GetType() != typeof(TextBox))
                {
                    continue;
                }

                if (count != 3)
                {
                    argument += ((TextBox)control).Text + "|";
                }
                else
                {
                    argument += "{0}" + "|";
                    componentRectangle = ((TextBox)control).Text;
                }

                count++;
            }

            argument = argument.Remove(argument.Length - 1);

            int width = Convert.ToInt32(componentRectangle.Split(',')[2].Trim());
            int height = Convert.ToInt32(componentRectangle.Split(',')[3].Trim().Replace(")", ""));

            SELECTED_COMPONENT = new SelectedComponent(new Microsoft.Xna.Framework.Rectangle(0, 0, width, height), argument, className);

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
    }
}
