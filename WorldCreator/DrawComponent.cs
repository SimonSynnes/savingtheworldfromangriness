﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.World.Components;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace WorldCreator
{
    public class DrawComponent
    {
        public string className;
        public Rectangle position;
        public string code;

        public DrawComponent(string className, Rectangle position, string code)
        {
            this.className = className;
            this.position = position;
            this.code = code;
        }
    }
}
