﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Diagnostics;
using System.Threading;
using System.Reflection;
using System.Windows.Threading;

namespace WorldCreator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Thread gameWindowThread;
        private bool finishedLoading;

        private int worldWidth;
        private int worldHeight;

        private List<Component> components;

        private bool dialogOpen;

        public MainWindow()
        {
            InitializeComponent();

            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);

            finishedLoading = false;
            gameWindowThread = null;
            dialogOpen = false;

            worldWidth = 0;
            worldHeight = 0;

            components = new List<Component>();

            Type[] typelist = SavingTheWorldFromAngriness.Program.GetComponentsClasses();
            for (int i = 0; i < typelist.Length; i++)
            {
                if (typelist[i].Name == "BasicComponent" || typelist[i].Name == "MovingEntity")
                {
                    continue;
                }

                string className = typelist[i].Name;
                List<string> constructorNames = new List<string>();
                List<Type> constructorTypes = new List<Type>();

                ConstructorInfo[] ci = typelist[i].GetConstructors();
                ParameterInfo[] parameters = ci[0].GetParameters();
                foreach (ParameterInfo pi in parameters)
                {
                    constructorNames.Add(pi.Name);
                    constructorTypes.Add(pi.ParameterType);
                }

                components.Add(new Component(className, constructorNames, constructorTypes));
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                Global.DockClasses.GAME.Exit();
            }
            catch
            {

            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                gameWindowThread = new Thread(delegate ()
                {
                    System.Drawing.Rectangle screenBounds = System.Windows.Forms.Screen.PrimaryScreen.Bounds;


                    Global.DockClasses.GAME = new Game1(screenBounds.Width, screenBounds.Height);

                    Global.DockClasses.GAME.Activated += GAME_Activated;
                    new Docking.AttachGame(Global.DockClasses.GAME, gameDock, screenBounds.Width, screenBounds.Height);
                });

                gameWindowThread.SetApartmentState(ApartmentState.STA);
                gameWindowThread.Start();
            }
            catch
            {
                System.Windows.MessageBox.Show("Problem starting game instance.", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(0);
            }

            finishedLoading = true;


        }

        private void GAME_Activated(object sender, EventArgs e)
        {
            Global.DockClasses.GAME.Activated -= GAME_Activated;

            Global.DockClasses.GAME.eventLoadGame += GAME_eventLoadGame;
        }

        private void GAME_eventLoadGame(object sender, EventArgs e)
        {
            gridBrowse.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, (Action)(() =>
            {
                LoadBrowseTile();
            }));

            Global.DockClasses.GAME.ClearMap();
            OpenNewWorld();
        }

        private void OpenNewWorld()
        {
            gridBrowse.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, (Action)(() =>
            {
                WinForms.FrmNewWorld newWorld = new WinForms.FrmNewWorld();

                var result = newWorld.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    Global.DockClasses.GAME.CreateWorldTiles(Convert.ToInt32(newWorld.RETURN_TILEX), Convert.ToInt32(newWorld.RETURN_TILEY));

                    worldWidth = Convert.ToInt32(newWorld.RETURN_TILEX);
                    worldHeight = Convert.ToInt32(newWorld.RETURN_TILEY);
                }
            }));
        }

        private void LoadBrowseTile()
        {
            gridBrowse.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, (Action)(() =>
            {
                List<Button> tileImages = new List<Button>();
                BitmapImage tileSheetBitmap = Global.DockClasses.GAME.getTileSheetBitmap();
                Object[][] tilesArray = Global.DockClasses.GAME.getTileData();

                for (int i = 0; i < tilesArray.Length; i++)
                {
                    Microsoft.Xna.Framework.Vector2 tilePosition = (Microsoft.Xna.Framework.Vector2)tilesArray[i][0];
                    Microsoft.Xna.Framework.Vector2 tileSize = (Microsoft.Xna.Framework.Vector2)tilesArray[i][1];
                    SavingTheWorldFromAngriness.World.Tiles.Tile.Type tileType = (SavingTheWorldFromAngriness.World.Tiles.Tile.Type)tilesArray[i][2];
                    string tileName = tilesArray[i][3].ToString();

                    CroppedBitmap tileBitmap = new CroppedBitmap(tileSheetBitmap,
                        new Int32Rect((int)tilePosition.X, (int)tilePosition.Y,
                        (int)tileSize.X, (int)tileSize.Y));

                    Button btn = new Button();
                    btn.Content = tileType.ToString();
                    btn.Width = 64;
                    btn.Height = 64;
                    btn.VerticalAlignment = VerticalAlignment.Center;
                    btn.HorizontalAlignment = HorizontalAlignment.Center;
                    btn.FontSize = 12f;
                    btn.Tag = new object[] { tileName, tileType };
                    btn.FontWeight = FontWeights.Bold;
                    btn.Foreground = Brushes.Yellow;
                    ResetBrowsePanelSelection();
                    btn.Click += Btn_Click;

                    var brush = new ImageBrush();
                    brush.ImageSource = tileBitmap;
                    btn.Background = brush;

                    Style style = this.FindResource("tileButton") as Style;
                    btn.Style = style;

                    browsePanel.Children.Add(btn);
                }
            }));
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            ResetBrowsePanelSelection();

            Button button = (Button)sender;
            button.BorderThickness = new Thickness(3);
            button.BorderBrush = Brushes.Black;

            object[] arguments = (object[])button.Tag;
            Global.DockClasses.GAME.selectedTile = arguments[0].ToString();
            Global.DockClasses.GAME.selectedTileType = (SavingTheWorldFromAngriness.World.Tiles.Tile.Type)arguments[1];
        }

        public void ResetBrowsePanelSelection()
        {
            foreach (Button button in browsePanel.Children)
            {
                button.BorderThickness = new Thickness(2);
                button.BorderBrush = Brushes.LightGray;
            }
        }

        private void btnTiles_Click(object sender, RoutedEventArgs e)
        {
            Global.DockClasses.GAME.drawLightBulbs = false;
            Global.DockClasses.GAME.addComponents = false;
            browsePanel.Children.Clear();
            LoadBrowseTile();
        }

        private void LoadBrowseLight()
        {
            StackPanel stackPanel = new StackPanel();

            Label lblInfo = new Label();
            lblInfo.Content = "ADD / REMOVE LIGHTS \n\nTo add: click on a location on the grid to add a new light.\nTo remove: click on an existing light and then press 'delete' to remove a light.";
            lblInfo.Foreground = Brushes.Black;
            lblInfo.Margin = new Thickness(0, 0, 0, 25);

            Label lblRadius = new Label();
            lblRadius.Content = "radius";
            lblRadius.Foreground = Brushes.Black;
            lblRadius.Margin = new Thickness(0, 0, 0, 5);

            TextBox txtRadius = new TextBox();
            txtRadius.Margin = new Thickness(0, 0, 0, 10);
            txtRadius.Text = "200";
            txtRadius.TextChanged += TxtRadius_TextChanged;

            Label lblColor = new Label();
            lblColor.Content = "color";
            lblColor.Foreground = Brushes.Black;
            lblColor.Margin = new Thickness(0, 0, 0, 5);

            Xceed.Wpf.Toolkit.ColorPicker colorPicker = new Xceed.Wpf.Toolkit.ColorPicker();
            colorPicker.Margin = new Thickness(0, 0, 0, 10);
            colorPicker.SelectedColor = Colors.White;
            colorPicker.SelectedColorChanged += ColorPicker_SelectedColorChanged;

            Label lblPower = new Label();
            lblPower.Content = "power";
            lblPower.Foreground = Brushes.Black;
            lblPower.Margin = new Thickness(0, 0, 0, 5);

            TextBox txtPower = new TextBox();
            txtPower.Margin = new Thickness(0, 0, 0, 10);
            txtPower.Text = "1.25";
            txtPower.TextChanged += TxtPower_TextChanged;

            Label lblDarkness = new Label();
            lblDarkness.Content = "darkness";
            lblDarkness.Foreground = Brushes.Black;
            lblDarkness.Margin = new Thickness(0, 0, 0, 5);

            TextBox txtDarkness = new TextBox();
            txtDarkness.Margin = new Thickness(0, 0, 0, 10);
            txtDarkness.Text = "0.65";
            txtDarkness.TextChanged += TxtDarkness_TextChanged;

            stackPanel.Children.Add(lblInfo);
            stackPanel.Children.Add(lblRadius);
            stackPanel.Children.Add(txtRadius);
            stackPanel.Children.Add(lblColor);
            stackPanel.Children.Add(colorPicker);
            stackPanel.Children.Add(lblPower);
            stackPanel.Children.Add(txtPower);
            stackPanel.Children.Add(lblDarkness);
            stackPanel.Children.Add(txtDarkness);

            browsePanel.Children.Add(stackPanel);
        }

        private void TxtDarkness_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txtBox = (TextBox)sender;

            try
            {
                Global.DockClasses.GAME.setDarkness(float.Parse(txtBox.Text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat));
            }
            catch
            {

            }
        }

        private void ColorPicker_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            Xceed.Wpf.Toolkit.ColorPicker colorPicker = (Xceed.Wpf.Toolkit.ColorPicker)sender;

            Color? color = colorPicker.SelectedColor;

            Global.DockClasses.GAME.newLightColor = new Microsoft.Xna.Framework.Color(color.Value.R, color.Value.G, color.Value.B);
        }

        private void TxtPower_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txtBox = (TextBox)sender;

            try
            {
                Global.DockClasses.GAME.newLightPower = float.Parse(txtBox.Text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            }
            catch
            {

            }
        }
        
        private void TxtRadius_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txtBox = (TextBox)sender;

            try
            {
                Global.DockClasses.GAME.newLightRadius = float.Parse(txtBox.Text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            }
            catch
            {

            }
        }

        private void btnLights_Click(object sender, RoutedEventArgs e)
        {
            browsePanel.Children.Clear();
            LoadBrowseLight();

            Global.DockClasses.GAME.drawLightBulbs = true;
            Global.DockClasses.GAME.addComponents = false;
        }

        private void MenuItemExit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Global.DockClasses.GAME.Exit();
            }
            catch
            {

            }

            this.Close();
        }

        private void MenuItemNew_Click(object sender, RoutedEventArgs e)
        {
            Global.DockClasses.GAME.ClearMap();
            OpenNewWorld();
        }

        private void MenuItemImport_Click(object sender, RoutedEventArgs e)
        {
            Global.DockClasses.GAME.isLocked = true;
            WinForms.frmImport frmImport = new WinForms.frmImport();

            var result = frmImport.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                string worldCode = frmImport.RETURN_MAP;

                Global.DockClasses.GAME.LoadMapData(worldCode);
            }

            Global.DockClasses.GAME.isLocked = false;
        }

        private void MenuItemExport_Click(object sender, RoutedEventArgs e)
        {
            Global.DockClasses.GAME.isLocked = true;
            string WorldCode = Global.DockClasses.GAME.CreateWorldTextFile();

            WinForms.frmExport frmExport = new WinForms.frmExport(WorldCode);
            frmExport.ShowDialog();
            Global.DockClasses.GAME.isLocked = false;
        }

        private void toolGrid_MouseEnter(object sender, MouseEventArgs e)
        {
            if (finishedLoading)
            {
                try
                {
                    if (Global.DockClasses.GAME != null)
                    {
                        Global.DockClasses.GAME.isMouseInToolGrid = true;
                    }
                }
                catch
                {

                }
            }
        }

        private void toolGrid_MouseLeave(object sender, MouseEventArgs e)
        {
            if (finishedLoading)
            {
                try
                {
                    if (Global.DockClasses.GAME != null)
                    {
                        Global.DockClasses.GAME.isMouseInToolGrid = false;
                    }
                }
                catch
                {

                }
            }
        }

        private void btnComponents_Click(object sender, RoutedEventArgs e)
        {
            browsePanel.Children.Clear();
            LoadComponentsList();
        }

        private void LoadComponentsList()
        {
            StackPanel stackPanel = new StackPanel();

            foreach (Component component in components)
            {
                Button btnComponent = new Button();
                btnComponent.Content = component.className;
                btnComponent.Width = gridBrowse.ActualWidth - 5;
                btnComponent.Height = 40;
                btnComponent.HorizontalAlignment = HorizontalAlignment.Center;
                btnComponent.Margin = new Thickness(0, 0, 0, 5);
                btnComponent.Background = Brushes.DarkGray;
                btnComponent.Foreground = Brushes.Black;
                btnComponent.Tag = component;
                btnComponent.Click += BtnComponent_Click;

                stackPanel.Children.Add(btnComponent);
            }

            browsePanel.Children.Add(stackPanel);

            Global.DockClasses.GAME.drawLightBulbs = false;
            Global.DockClasses.GAME.addComponents = true;
        }

        private void BtnComponent_Click(object sender, RoutedEventArgs e)
        {
            ResetComponentsSelection();
            dialogOpen = true;
            Global.DockClasses.GAME.isLocked = true;

            Button btnComponent = (Button)sender;
            Component component = (Component)btnComponent.Tag;

            gridBrowse.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, (Action)(() =>
            {
                WinForms.frmComponent newComponent = new WinForms.frmComponent(component);

                var result = newComponent.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    Global.DockClasses.GAME.selectedComponent = newComponent.SELECTED_COMPONENT;

                    btnComponent.Background = Brushes.DarkOrange;
                }
            }));

            dialogOpen = false;
            Global.DockClasses.GAME.isLocked = false;
        }

        public void ResetComponentsSelection()
        {
            StackPanel stackPanel = (StackPanel)browsePanel.Children[0];

            foreach (Button btn in stackPanel.Children)
            {
                btn.Background = Brushes.DarkGray;
            }
        }

        private void MenuItemComponents_Click(object sender, RoutedEventArgs e)
        {
            Global.DockClasses.GAME.isLocked = true;

            string ComponentsCode = "        protected override void LoadContent()" + Environment.NewLine;
            ComponentsCode += "        {" + Environment.NewLine;
            ComponentsCode += "	base.LoadContent();" + Environment.NewLine + Environment.NewLine;

            foreach (DrawComponent drawComponent in Global.DockClasses.GAME.drawComponents)
            {
                ComponentsCode += "	" + drawComponent.code + Environment.NewLine;
            }

            ComponentsCode += "        }" + Environment.NewLine;

            WinForms.frmExport frmExport = new WinForms.frmExport(ComponentsCode);
            frmExport.ShowDialog();
            Global.DockClasses.GAME.isLocked = false;
        }

        private void Window_MouseEnter(object sender, MouseEventArgs e)
        {
            if (finishedLoading && !dialogOpen)
            {
                try
                {
                    if (Global.DockClasses.GAME != null)
                    {
                        Global.DockClasses.GAME.isLocked = true;
                    }
                }
                catch
                {

                }
            }
        }

        private void Window_MouseLeave(object sender, MouseEventArgs e)
        {
            if (finishedLoading && !dialogOpen)
            {
                try
                {
                    if (Global.DockClasses.GAME != null)
                    {
                        Global.DockClasses.GAME.isLocked = false;
                    }
                }
                catch
                {

                }
            }
        }
    }
}