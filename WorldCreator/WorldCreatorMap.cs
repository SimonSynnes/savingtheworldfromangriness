﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.GUI;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;


namespace WorldCreator
{
    public class WorldCreatorMap : SavingTheWorldFromAngriness.World.Maps.BasicMap
    {
        public WorldCreatorMap(Game game, SavingTheWorldFromAngriness.World.WorldRenderer worldRenderer)
            : base(game, worldRenderer)
        {

        }

        public override void Update(GameTime gameTime)
        {

        }

        public override void Draw(GameTime gameTime)
        {

        }

        public override void DrawBehindShadows()
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            foreach (GameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).Draw(spriteBatch);
            }

            spriteBatch.End();
        }
    }
}
