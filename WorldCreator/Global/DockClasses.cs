﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MonoGame;
using FormHost = System.Windows.Forms.Integration.WindowsFormsHost;
using System.Windows.Controls;

namespace WorldCreator.Global
{
    class DockClasses
    {
        public static FormHost MONOGAME_HOST { get; set; }
        public static DockPanel GAME_DOCK { get; set; }
        public static Game1 GAME { get; set; }
    }
}
