﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Panel = System.Windows.Controls.DockPanel;
using FormPanel = System.Windows.Forms.Panel;
using FormHost = System.Windows.Forms.Integration.WindowsFormsHost;
using PanelGame = Microsoft.Xna.Framework.Game;

namespace WorldCreator.Docking
{
    class AttachGame
    {
        private PanelGame game;
        private IntPtr gameWindowHandle;

        private Panel dockPanel;
        private FormPanel hostPanel;

        private int gameWidth;
        private int gameHeight;

        public AttachGame(PanelGame game, Panel dockPanel, int gameWidth, int gameHeight)
        {
            this.game = game;
            this.dockPanel = dockPanel;
            this.gameWidth = gameWidth;
            this.gameHeight = gameHeight;

            game.Activated += Game_Activated;
            game.Run();
        }

        private void Game_Activated(object sender, EventArgs e)
        {
            AttachGameWindowToPanel(game.Window.Handle);

            // Remove event handler
            game.Activated -= Game_Activated;
        }

        private void AttachGameWindowToPanel(IntPtr gameWindowHandle)
        {
            dockPanel.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, (Action)(() =>
            {
                FormHost gameHost = new FormHost();
                FormPanel gameHostPanel = new FormPanel();

                this.gameWindowHandle = gameWindowHandle;
                this.hostPanel = gameHostPanel;

                gameHost.Child = gameHostPanel;
                gameHostPanel.SizeChanged += new EventHandler(hostPanel_SizeChanged);

                UserLibraryMethods.SetParent(gameWindowHandle, gameHostPanel.Handle);

                int style = UserLibraryMethods.GetWindowLong(gameWindowHandle, UserLibraryMethodConstants.GWL_STYLE);
                style = style & ~UserLibraryMethodConstants.WS_CAPTION & ~UserLibraryMethodConstants.WS_THICKFRAME
                    & ~UserLibraryMethodConstants.SWP_NOZORDER & ~UserLibraryMethodConstants.SWP_NOACTIVATE;

                UserLibraryMethods.SetWindowLong(gameWindowHandle, UserLibraryMethodConstants.GWL_STYLE, style);
                UserLibraryMethods.MoveWindow(gameWindowHandle, 0, 0, gameWidth, gameHeight, true);

                UserLibraryMethods.SetFocus(gameWindowHandle);
                UserLibraryMethods.ShowWindow(gameWindowHandle, 0);

                gameHost.UpdateLayout();

                dockPanel.Children.Clear();
                dockPanel.Children.Add(gameHost);

                Global.DockClasses.MONOGAME_HOST = gameHost;
                Global.DockClasses.GAME_DOCK = dockPanel;
            }));
        }

        private void hostPanel_SizeChanged(object sender, EventArgs e)
        {
            UserLibraryMethods.MoveWindow(gameWindowHandle, 0, 0, gameWidth, gameHeight, true);
        }
    }
}