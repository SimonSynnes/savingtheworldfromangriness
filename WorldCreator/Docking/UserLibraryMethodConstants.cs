﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldCreator.Docking
{
    class UserLibraryMethodConstants
    {
        #region Window Style Constants (SetWindowLong)
        public const int SWP_NOZORDER = 0x0004;
        public const int SWP_NOACTIVATE = 0x0010;
        public const int GWL_STYLE = -16;
        public const int WS_CAPTION = 0x00C00000;
        public const int WS_THICKFRAME = 0x00040000;
        public const int FRAMECHANGED = 0x0020;
        public const UInt32 WM_CLOSE = 0x0010;
        #endregion
    }
}
