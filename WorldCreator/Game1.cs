﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using System.IO;
using SavingTheWorldFromAngriness.World;
using SavingTheWorldFromAngriness.World.Tiles;
using SavingTheWorldFromAngriness.World.Components;

namespace WorldCreator
{
    public class Game1 : Game
    {
        public event EventHandler eventLoadGame;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        WorldRenderer worldRenderer;
        Camera camera;

        private int gameWidthX;
        private int gameHeightY;

        double elapsedTime;

        public string selectedTile;
        public Tile.Type selectedTileType;
        
        public bool drawLightBulbs;

        Texture2D rectangle;
        public bool addComponents;
        public SelectedComponent selectedComponent;
        public List<DrawComponent> drawComponents;
        DrawComponent selectedDrawComponent;
        private SavingTheWorldFromAngriness.World.Maps.BasicMap basicMap;

        Texture2D textureLightBulb;
        private Vector2 selectedLightBulbPosition;
        private Lighting.PointLight selectedLight;

        public Color newLightColor;
        public float newLightRadius;
        public float newLightPower;

        int mousePositionX;
        int mousePositionY;

        SpriteFont spriteFont;

        public bool isMouseInToolGrid;

        public bool isLeftMouseDown;

        public bool isLocked;

        public Game1(int gameWidth, int gameHeight)
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = gameWidth;
            graphics.PreferredBackBufferHeight = gameHeight;
            graphics.PreferMultiSampling = false;
            graphics.GraphicsProfile = GraphicsProfile.HiDef;

            this.IsMouseVisible = true;
            this.isLeftMouseDown = false;

            isLocked = false;

            drawComponents = new List<DrawComponent>();

            selectedDrawComponent = null;
            addComponents = false;
            worldRenderer = null;

            gameWidthX = 0;
            gameHeightY = 0;

            selectedTile = "001";
            selectedTileType = Tile.Type.floor;

            selectedComponent = null;

            drawLightBulbs = false;
            selectedLightBulbPosition = new Vector2(0, 0);
            selectedLight = null;

            newLightColor = Color.White;
            newLightRadius = 200;
            newLightPower = 1.25f;

            mousePositionX = 0;
            mousePositionY = 0;

            isMouseInToolGrid = false;
        }

        public string CreateWorldTextFile()
        {
            string WorldCode = "";

            int count = 0;
            for (int y = 0; y < gameHeightY; y++)
            {
                WorldCode += "|";

                for (int i = 0; i < gameWidthX; i++)
                {
                    if (worldRenderer.lowerLevel[count] != null)
                    {
                        // FLOOR..
                        if (worldRenderer.lowerLevel[count].name == "000")
                        {
                            WorldCode += "xxx,";
                        }
                        else
                        {
                            WorldCode += worldRenderer.lowerLevel[count].name + ",";
                        }
                    }
                    else
                    if (worldRenderer.lightingManager.isBlockInList(count))
                    {
                        // WALL
                        WorldCode += worldRenderer.lightingManager.getBlock(count).Texture.Tag.ToString() + ",";
                    }
                    else
                    {
                        WorldCode += "xxx,";
                    }


                    count++;
                }

                WorldCode += Environment.NewLine;
            }

            foreach (Lighting.PointLight light in worldRenderer.lightingManager.lights)
            {
                // X, Y, radius, R, G, B, power
                WorldCode += "Light:" +
                    light.Position.X.ToString() + "#" +
                    light.Position.Y.ToString() + "#" +
                    light.Radius.ToString() + "#" +
                    light.Color.R.ToString() + "#" +
                    light.Color.G.ToString() + "#" +
                    light.Color.B.ToString() + "#" +
                    light.Power.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture); 
                WorldCode += Environment.NewLine;
            }

            WorldCode += "Darkness:" + worldRenderer.worldDarkness.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);

            return WorldCode;
        }

        public void LoadMapData(string map)
        {
            ClearMap();

            worldRenderer.LoadMap(map, "");

            gameWidthX = worldRenderer.map_width / SavingTheWorldFromAngriness.Global.GameConstants.TILE_SIZE;
            gameHeightY = worldRenderer.map_height / SavingTheWorldFromAngriness.Global.GameConstants.TILE_SIZE;
        }

        public void setDarkness(float value)
        {
            worldRenderer.worldDarkness = value;
        }

        public void setCameraZoom(float zoom)
        {
            camera.zoom = zoom;
        }

        public void ClearMap()
        {
            worldRenderer.ClearMap();
            selectedComponent = null;
            drawComponents.Clear();

            gameWidthX = 0;
            gameHeightY = 0;
        }

        public System.Windows.Media.Imaging.BitmapImage getTileSheetBitmap()
        {
            Texture2D tileSheetTexture = worldRenderer.tileManager.getTileSetTexture();

            MemoryStream memoryStream = new MemoryStream();

            tileSheetTexture.SaveAsPng(memoryStream, tileSheetTexture.Width, tileSheetTexture.Height);

            System.Windows.Media.Imaging.BitmapImage bitmap = new System.Windows.Media.Imaging.BitmapImage();
            bitmap.BeginInit();
            bitmap.StreamSource = memoryStream;
            bitmap.EndInit();

            return bitmap;
        }

        public Object[][] getTileData()
        {
            return TileTranslate.getTilesArray();
        }

        protected override void Initialize()
        {
            worldRenderer = new WorldRenderer(this);
            worldRenderer.isWorldCreator = true;

            Components.Add(worldRenderer);
            
            base.Initialize();
        }

        protected override void Update(GameTime gameTime)
        {
            // TODO: Add your update logic here

            if (isLocked)
            {
                return;
            }

            KeyboardState ks = Keyboard.GetState();
            MouseState ms = Mouse.GetState();

            mousePositionX = Convert.ToInt32((camera.matrixTranslation().X - ms.Position.X) * -1);
            mousePositionY = Convert.ToInt32((camera.matrixTranslation().Y - ms.Position.Y) * -1);

            if (ks.IsKeyDown(Keys.A))
                camera.position += new Vector2(5, 0);
            if (ks.IsKeyDown(Keys.D))
                camera.position -= new Vector2(5, 0);
            if (ks.IsKeyDown(Keys.W))
                camera.position += new Vector2(0, 5);
            if (ks.IsKeyDown(Keys.S))
                camera.position -= new Vector2(0, 5);

            elapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
            if (elapsedTime >= 10)
            {
                if (ms.LeftButton == ButtonState.Pressed & !isMouseInToolGrid)
                {
                    Vector2 tilePosition;
                    int index = getTileIndex(ms.Position.X, ms.Position.Y, out tilePosition);

                    if (index != -1)
                    {
                        if (!drawLightBulbs && !addComponents)
                        {
                            if (selectedTileType == Tile.Type.floor)
                            {
                                if (worldRenderer.lowerLevel[index] != null)
                                {
                                    if (worldRenderer.lowerLevel[index].name != selectedTile)
                                    {
                                        Tile tile = worldRenderer.tileManager.CreateNewTile(selectedTile, tilePosition);

                                        worldRenderer.lowerLevel[index] = tile;
                                        worldRenderer.lightingManager.RemoveVisualBlock(index);
                                    }
                                }
                                else
                                {
                                    Tile tile = worldRenderer.tileManager.CreateNewTile(selectedTile, tilePosition);

                                    worldRenderer.lowerLevel[index] = tile;
                                    worldRenderer.lightingManager.RemoveVisualBlock(index);
                                }
                            }

                            if (selectedTileType == Tile.Type.wall)
                            {
                                if (!worldRenderer.lightingManager.isBlockInList(index, selectedTile))
                                {
                                    worldRenderer.lightingManager.RemoveVisualBlock(index);

                                    Tile tile = worldRenderer.tileManager.CreateNewTile(selectedTile, tilePosition);

                                    Texture2D tileTexture = tile.Crop(new Rectangle((int)tile.crop.X, (int)tile.crop.Y, SavingTheWorldFromAngriness.Global.GameConstants.TILE_SIZE, SavingTheWorldFromAngriness.Global.GameConstants.TILE_SIZE));
                                    tileTexture.Tag = selectedTile;

                                    worldRenderer.lightingManager.AddVisualBlock(tileTexture, tilePosition, 0, tileTexture, index);
                                    worldRenderer.lowerLevel[index] = null;
                                }
                            }
                        }
                        else
                        {
                            if (drawLightBulbs)
                            {
                                selectedLightBulbPosition = new Vector2(ms.Position.X, ms.Position.Y);

                                float x_pos = (camera.matrixTranslation().X - selectedLightBulbPosition.X) * -1;
                                float y_pos = (camera.matrixTranslation().Y - selectedLightBulbPosition.Y) * -1;
                                Rectangle cursorRectangle = new Rectangle((int)x_pos, (int)y_pos, 2, 2);

                                bool isLightSelected = false;

                                foreach (Lighting.PointLight light in worldRenderer.lightingManager.lights)
                                {
                                    Rectangle lightRectangle = new Rectangle((int)light.Position.X - (textureLightBulb.Width / 2), (int)light.Position.Y - (textureLightBulb.Height / 2), textureLightBulb.Width, textureLightBulb.Height);

                                    // selectedLightBulbPosition
                                    if (lightRectangle.Intersects(cursorRectangle))
                                    {
                                        selectedLight = light;
                                        isLightSelected = true;
                                    }
                                }

                                if (!isLightSelected)
                                {
                                    worldRenderer.lightingManager.AddLight(new Vector2(x_pos, y_pos), newLightRadius, newLightColor, newLightPower);
                                }
                            }
                        }
                    }
                }

                if (addComponents)
                {
                    if (selectedComponent != null)
                    {
                        if (ms.LeftButton == ButtonState.Pressed & !isMouseInToolGrid)
                        {
                            isLeftMouseDown = true;
                        }
                        else
                        {
                            if (isLeftMouseDown)
                            {
                                bool isComponentSelected = false;
                                selectedLightBulbPosition = new Vector2(ms.Position.X, ms.Position.Y);
                                float x_pos = (camera.matrixTranslation().X - selectedLightBulbPosition.X) * -1;
                                float y_pos = (camera.matrixTranslation().Y - selectedLightBulbPosition.Y) * -1;

                                Rectangle cursorRectangle = new Rectangle((int)x_pos, (int)y_pos, 2, 2);
                                foreach (DrawComponent component in drawComponents)
                                {
                                    if (cursorRectangle.Intersects(component.position))
                                    {
                                        selectedDrawComponent = component;
                                        isComponentSelected = true;
                                    }
                                }

                                if (!isComponentSelected)
                                {
                                    Rectangle componentRectangle = new Rectangle((int)x_pos, (int)y_pos,
                                        selectedComponent.rectangle.Width, selectedComponent.rectangle.Height);

                                    if (componentRectangle.Width != 0)
                                    {
                                        drawComponents.Add(new DrawComponent(selectedComponent.className,
                                            componentRectangle, selectedComponent.GenerateCode(new Vector2(x_pos, y_pos))));
                                    }
                                    else
                                    {
                                        drawComponents.Add(new DrawComponent(selectedComponent.className, new Rectangle((int)x_pos, (int)y_pos,
                                        50, 50), selectedComponent.GenerateCode(new Vector2(x_pos, y_pos))));
                                    }
                                }
                            }

                            isLeftMouseDown = false;
                        }
                    }
                }

                if (drawLightBulbs && !addComponents)
                {
                    if (ks.IsKeyDown(Keys.Delete))
                    {
                        if (selectedLight != null)
                        {
                            lock (worldRenderer.lightingManager.lights)
                            {
                                worldRenderer.lightingManager.lights.Remove(selectedLight);
                                selectedLight = null;
                            }
                        }
                    }
                }

                if (!drawLightBulbs && addComponents)
                {
                    if (ks.IsKeyDown(Keys.Delete))
                    {
                        if (selectedDrawComponent != null)
                        {
                            lock (drawComponents)
                            {
                                drawComponents.Remove(selectedDrawComponent);
                                selectedDrawComponent = null;
                            }
                        }
                    }
                }

                elapsedTime = 0;
            }

            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            camera = worldRenderer.getCamera();

            eventLoadGame.Invoke(null, null);

            textureLightBulb = Content.Load<Texture2D>("Textures/Lightbulb");

            spriteFont = Content.Load<SpriteFont>("Fonts/DebugFont");

            rectangle = Content.Load<Texture2D>("Textures/rectangle");
            
            basicMap = new WorldCreatorMap(this, worldRenderer);
            Components.Add(basicMap);
            basicMap.AddPlayerComponent();
            basicMap.SetCamera(camera);
            basicMap.InitializeSpriteBatch();
        }

        public void CreateWorldTiles(int width, int height)
        {
            gameWidthX = width;
            gameHeightY = height;
            worldRenderer.LoadMapWorldCreator(width, height);
        }

        public int getTileIndex(int mousePositionX, int mousePositionY, out Vector2 tilePosition)
        {
            tilePosition = new Vector2(0, 0);

            int count = 0;
            for (int y = 0; y < gameHeightY; y++)
            {
                for (int i = 0; i < gameWidthX; i++)
                {
                    Rectangle tileSize = new Rectangle(i * SavingTheWorldFromAngriness.Global.GameConstants.TILE_SIZE, y * SavingTheWorldFromAngriness.Global.GameConstants.TILE_SIZE, SavingTheWorldFromAngriness.Global.GameConstants.TILE_SIZE, SavingTheWorldFromAngriness.Global.GameConstants.TILE_SIZE);

                    float x_pos = (camera.matrixTranslation().X - mousePositionX) * -1;
                    float y_pos = (camera.matrixTranslation().Y - mousePositionY) * -1;

                    Rectangle cursorSize = new Rectangle(
                        (int)x_pos + (SavingTheWorldFromAngriness.Global.GameConstants.TILE_SIZE / 2),
                        (int)y_pos + (SavingTheWorldFromAngriness.Global.GameConstants.TILE_SIZE / 2), 1, 1);

                    tilePosition = new Vector2(i * SavingTheWorldFromAngriness.Global.GameConstants.TILE_SIZE, y * SavingTheWorldFromAngriness.Global.GameConstants.TILE_SIZE);

                    if (tileSize.Intersects(cursorSize))
                    {
                        return count;
                    }

                    count++;
                }
            }

            return -1;
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Yellow);

            // TODO: Add your drawing code here
            worldRenderer.Draw();

            if (drawLightBulbs && !addComponents)
            {
                spriteBatch.Begin(SpriteSortMode.Deferred,
                    BlendState.Additive,
                    SamplerState.AnisotropicClamp,
                    DepthStencilState.Default,
                    RasterizerState.CullCounterClockwise,
                    null,
                    camera.TransformMatrix);


                foreach (Lighting.PointLight light in worldRenderer.lightingManager.lights)
                {
                    if (light == selectedLight)
                    {
                        spriteBatch.Draw(textureLightBulb, light.Position - new Vector2(
                            textureLightBulb.Width / 2, textureLightBulb.Height / 2), Color.Red);
                    }
                    else
                    {
                        spriteBatch.Draw(textureLightBulb, light.Position - new Vector2(
                            textureLightBulb.Width / 2, textureLightBulb.Height / 2), Color.White);
                    }
                }

                spriteBatch.End();
            }

            if (addComponents && !drawLightBulbs)
            {
                spriteBatch.Begin(SpriteSortMode.Deferred,
                    BlendState.NonPremultiplied,
                    SamplerState.AnisotropicClamp,
                    DepthStencilState.Default,
                    RasterizerState.CullCounterClockwise,
                    null,
                    camera.TransformMatrix);

                foreach (DrawComponent drawComponent in drawComponents)
                {
                    if (drawComponent == selectedDrawComponent)
                    {
                        spriteBatch.Draw(rectangle, drawComponent.position, Color.Red);
                    }
                    else
                    {
                        spriteBatch.Draw(rectangle, drawComponent.position, Color.Black);
                    }

                    spriteBatch.DrawString(spriteFont, drawComponent.className, new Vector2(drawComponent.position.X, drawComponent.position.Y), Color.White);
                }

                spriteBatch.End();
            }

            spriteBatch.Begin();
            spriteBatch.DrawString(spriteFont, "MOUSE POSITION: \nX:" + mousePositionX.ToString() + "\nY:" + mousePositionY.ToString(), new Vector2(10, 10), Color.Yellow);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
