﻿using System;
using System.Linq;
using SavingTheWorldFromAngriness.World;
using GeonBit.UI;
using GeonBit.UI.Entities;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using SavingTheWorldFromAngriness.GUI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SavingTheWorldFromAngriness.Weapons;
using Microsoft.Xna.Framework.Audio;
using SavingTheWorldFromAngriness.Global.Sounds;

namespace SavingTheWorldFromAngriness
{
    class MainMenu : DrawableGameComponent
    {
        #region SpriteBatch, WorldRenderer, Panel
        private SpriteBatch spriteBatch;
        private WorldRenderer worldRenderer;
        private Panel mainPanel;
        #endregion

        #region URL
        private Texture2D urlTexture;
        private Vector2 urlPosition;
        #endregion

        #region Version
        private SpriteFont versionFont;
        private Vector2 versionPosition;
        private string versionText;
        #endregion

        #region Sound
        private SoundEffect hoverButtonSound;
        private SoundEffect buttonClickSound;
        #endregion

        public MainMenu(Game game, WorldRenderer worldRenderer) : base (game)
        {
            #region Worldrenderer

            this.worldRenderer = worldRenderer;
            worldRenderer.SetBackground(new Backgrounds.BlurredMovingClouds(this.Game, 33, 1), new Color(182, 208, 255));
            #endregion

        }

        protected override void LoadContent()
        {
            base.LoadContent();

            #region Worldrender, Spritebatch, Window

            worldRenderer.ClearMap();

            spriteBatch = new SpriteBatch(GraphicsDevice);
            worldRenderer.RenderBackgroundTexture(spriteBatch);

            int windowWidth = GraphicsDevice.Viewport.Width;
            int windowHeight = GraphicsDevice.Viewport.Height;
            #endregion

            #region URL link

            urlTexture = Game.Content.Load<Texture2D>("Textures/Intro/jonoieLink");
            urlPosition = new Vector2((windowWidth - urlTexture.Width) - 10, (windowHeight - urlTexture.Height) - 8);
            #endregion

            #region Version

            versionFont = Game.Content.Load<SpriteFont>("Fonts/CoinFont");

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;

            Vector2 fontText = versionFont.MeasureString(version);
            versionText = version;

            versionPosition = new Vector2(15, windowHeight - fontText.Y + 2);
            #endregion

            #region Logo

            Image logo = new Image(Game.Content.Load<Texture2D>("Textures/Intro/titleColor"), new Vector2(739, 267), ImageDrawMode.Stretch, Anchor.Center, new Vector2(0, -260));

            if (windowHeight < 900)
            {
                logo = new Image(Game.Content.Load<Texture2D>("Textures/Intro/titleColor"), new Vector2(739, 267), ImageDrawMode.Stretch, Anchor.Center, new Vector2(0, -190));
            }

            UserInterface.Active.AddEntity(logo);
            #endregion

            #region Panel
            CreateMainPanel();
            UserInterface.Active.AddEntity(mainPanel);
            #endregion

            #region Sounds
            Song song = Game.Content.Load<Song>("Sounds/Intro/wind");
            SoundManager.PlayBackgroundMusic(song);


            buttonClickSound = Game.Content.Load<SoundEffect>("Sounds/Misc/buttonClickSound");
            hoverButtonSound = Game.Content.Load<SoundEffect>("Sounds/Misc/buttonHoverSound");
            #endregion
        }

        private void CreateMainPanel()
        {
            #region Window
            byte activeOpacity = 190;
            byte inActiveOpacity = 165;

            int windowHeight = GraphicsDevice.Viewport.Height;
            mainPanel = new Panel(size: new Vector2(500, 450), skin: PanelSkin.None, anchor: Anchor.Center, offset: new Vector2(0, 160));

            if (windowHeight < 900)
            {
                mainPanel = new Panel(size: new Vector2(500, 450), skin: PanelSkin.None, anchor: Anchor.Center, offset: new Vector2(0, 160));
            }

            #endregion

            #region Buttons

            var btnPlay = new Button("Play", ButtonSkin.Alternative);
            var btnLoad = new Button("Load Game", ButtonSkin.Alternative);
            var btnOptions = new Button("Options", ButtonSkin.Alternative);
            var btnExit = new Button("Exit Game", ButtonSkin.Alternative);
            #endregion

            #region Play Button

            btnPlay.FillColor = Color.Black;
            btnPlay.Opacity = inActiveOpacity;
            PlaySounds(btnPlay);

            btnPlay.OnMouseEnter += (Entity entity) =>
            {
                btnPlay.Opacity = activeOpacity;
                btnLoad.Opacity = inActiveOpacity;
                btnOptions.Opacity = inActiveOpacity;
                btnExit.Opacity = inActiveOpacity;
            };

            btnPlay.OnMouseLeave += (Entity entity) =>
            {
                btnPlay.Opacity = inActiveOpacity;
            };

            btnPlay.OnClick += (Entity entity) =>
            {
                SoundManager.StopBackgroundMusic();

                UserInterface.Active.RemoveEntity(mainPanel);
                Game.Components.Remove(this);

                if (Game1.DEBUG_MODE == true)
                {
                    worldRenderer.LoadMap("Test", "testChambers");
                }
                else
                {
                    worldRenderer.LoadMap("World1", "Home");
                }
            };
            #endregion

            #region Load Button

            btnLoad.FillColor = Color.Black;
            btnLoad.Opacity = inActiveOpacity;
            PlaySounds(btnLoad);

            #region btnLoad.OnMouseEnter

            btnLoad.OnMouseEnter += (Entity entity) =>
            {
                btnPlay.Opacity = inActiveOpacity;
                btnLoad.Opacity = activeOpacity;
                btnOptions.Opacity = inActiveOpacity;
                btnExit.Opacity = inActiveOpacity;
            };

            #endregion

            #region btnLoad.OnMouseLeave
            btnLoad.OnMouseLeave += (Entity entity) =>
            {
                btnLoad.Opacity = inActiveOpacity;
            };
            #endregion

            #region btnLoad.OnMouseClick

            btnLoad.OnClick += (Entity entity) =>
            {
                Panel saveBiggerPanel = new Panel(new Vector2(600, 575));
                UserInterface.Active.AddEntity(saveBiggerPanel);
                Panel savePanel = new Panel(new Vector2(600, 560));
                saveBiggerPanel.AddChild(savePanel);
                savePanel.Skin = PanelSkin.None;
                savePanel.AddChild(new Header("Load game"));
                savePanel.AddChild(new LineSpace());

                string directory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Saves");
                Directory.CreateDirectory(directory);
                foreach (String file in Directory.GetFiles(directory, "*.save*", SearchOption.AllDirectories).OrderByDescending(d => new FileInfo(d).LastWriteTime.TimeOfDay))
                {
                    Button button = new Button(Path.GetFileNameWithoutExtension(file));
                    savePanel.AddChild(button);
                    PlaySounds(button);

                    button.OnClick += (Entity entity2) =>
                    {
                        Panel buttonPanel = new Panel(size: new Vector2(500, 300), skin: PanelSkin.Golden, anchor: Anchor.Center, offset: null);
                        UserInterface.Active.AddEntity(buttonPanel);
                        Button loadButton = new Button("Load game");
                        Button deleteButton = new Button("Delete Save");
                        buttonPanel.AddChild(loadButton);
                        buttonPanel.AddChild(deleteButton);
                        Button cancelButton = new Button("Cancel", ButtonSkin.Default, Anchor.BottomCenter, new Vector2(200, 55));
                        buttonPanel.AddChild(cancelButton);

                        PlaySounds(cancelButton);
                        PlaySounds(loadButton);
                        PlaySounds(deleteButton);

                        cancelButton.OnClick += (Entity entity6) =>
                        {
                            UserInterface.Active.RemoveEntity(buttonPanel);
                        };

                        loadButton.OnClick += (Entity entity3) =>
                        {
                            string[] lines = File.ReadAllLines(file);
                            Global.Player.PLAYER_NAME = SaveGame.Base64Decode(lines[0]);
                            Global.Player.HEALTH = int.Parse(SaveGame.Base64Decode(lines[1]));
                            Global.Player.AMMO = int.Parse(SaveGame.Base64Decode(lines[2]));
                            Global.Player.AMMO_IN_MAGAZINE = int.Parse(SaveGame.Base64Decode(lines[3]));
                            Global.Player.CURRENT_WORLD = SaveGame.Base64Decode(lines[4]);
                            Global.Player.CURRENT_MAP = SaveGame.Base64Decode(lines[5]);
                            Global.Player.COINS = int.Parse(SaveGame.Base64Decode(lines[6]));
                            if (lines[7].Length != 0)
                            {
                                Global.Player.INVENTORY = SaveGame.Base64Decode(lines[7]).Split(',').ToList();
                            }
                            else
                            {
                                Global.Player.INVENTORY.Clear();
                            }
                            byte[] bytes = Convert.FromBase64String(lines[8]);
                            using (MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length))
                            {
                                ms.Write(bytes, 0, bytes.Length);
                                ms.Position = 0;
                                Global.Player.WEAPON = (Weapon)new BinaryFormatter().Deserialize(ms);
                            }

                            UserInterface.Active.RemoveEntity(buttonPanel);
                            UserInterface.Active.RemoveEntity(saveBiggerPanel);
                            Game.Components.Remove(this);
                            worldRenderer.LoadMap(Global.Player.CURRENT_WORLD, Global.Player.CURRENT_MAP);
                        };

                        deleteButton.OnClick += (Entity entity4) =>
                        {
                            File.Delete(file);
                            UserInterface.Active.RemoveEntity(buttonPanel);
                            savePanel.RemoveChild(button);
                        };
                    };
                }

                if (Directory.GetFiles(directory).Length == 0)
                {
                    Paragraph emptyMessage = new Paragraph("Nothing has been saved yet! :(", Anchor.Center, Color.AntiqueWhite,
                        scale: 1f);
                    emptyMessage.TextStyle = FontStyle.Italic;
                    savePanel.AddChild(emptyMessage);
                }

                Button btn = new Button("OK", ButtonSkin.Default, Anchor.BottomCenter, new Vector2(150, 55), new Vector2(-152, 0));
                saveBiggerPanel.AddChild(btn);
                PlaySounds(btn);
                btn.OnClick += (Entity entity6) =>
                {
                    UserInterface.Active.RemoveEntity(saveBiggerPanel);
                };
            };
            #endregion

            #endregion

            #region Option Button
            PlaySounds(btnOptions);

            btnOptions.FillColor = Color.Black;
            btnOptions.Opacity = inActiveOpacity;

            btnOptions.OnMouseEnter += (Entity entity) =>
            {
                btnPlay.Opacity = inActiveOpacity;
                btnLoad.Opacity = inActiveOpacity;
                btnOptions.Opacity = activeOpacity;
                btnExit.Opacity = inActiveOpacity;
            };

            btnOptions.OnMouseLeave += (Entity entity) =>
            {
                btnOptions.Opacity = inActiveOpacity;
            };

            btnOptions.OnClick += (Entity entity) =>
            {
                Button okBtn = Item.OptionsPanel();
                PlaySounds(okBtn);
            };

            #endregion

            #region Exit Button
            PlaySounds(btnExit);
            btnExit.FillColor = Color.Black;
            btnExit.Opacity = inActiveOpacity;

            btnExit.OnMouseEnter += (Entity entity) =>
            {
                btnPlay.Opacity = inActiveOpacity;
                btnLoad.Opacity = inActiveOpacity;
                btnOptions.Opacity = inActiveOpacity;
                btnExit.Opacity = activeOpacity;
            };

            btnExit.OnMouseLeave += (Entity entity) =>
            {
                btnExit.Opacity = inActiveOpacity;
            };

            btnExit.OnClick += (Entity entity) =>
            {
                Game.Exit();
            };
            #endregion

            #region Panel addChild
            mainPanel.AddChild(btnPlay);
            mainPanel.AddChild(btnLoad);
            mainPanel.AddChild(btnOptions);
            mainPanel.AddChild(btnExit);
            #endregion
        
        }
        private void PlaySounds(Button btn)
        {
            #region OnMouseEnter

            btn.OnMouseEnter += (Entity entity) =>
            {
                SoundManager.PlaySoundEffect(hoverButtonSound);
            };
            #endregion

            #region OnClick

            btn.OnClick += (Entity entity) =>
            {
                 SoundManager.PlaySoundEffect(buttonClickSound);
            };
            #endregion
        }
        public override void Draw(GameTime gameTime)
        {
            UserInterface.Active.Draw(spriteBatch);
            UserInterface.Active.DrawMainRenderTarget(spriteBatch);

            spriteBatch.Begin();
            spriteBatch.Draw(urlTexture, urlPosition, Color.White);
            spriteBatch.DrawString(versionFont, versionText, versionPosition, new Color(17, 17, 17) * 0.75f);
            spriteBatch.End();

            base.Draw(gameTime);

        }
    }
}
