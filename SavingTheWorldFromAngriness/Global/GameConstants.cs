﻿using GeonBit.UI.Entities;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavingTheWorldFromAngriness.Global
{
    public static class GameConstants
    {
        public const int TILE_SIZE = 32; // Tiles are 64x64
        public static Random RNDM = new Random();
        public static int ENEMY_ATTACKSPEED = 150;
    }
}
