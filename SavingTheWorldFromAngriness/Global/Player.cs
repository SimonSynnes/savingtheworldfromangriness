﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Weapons;
using SavingTheWorldFromAngriness.World.Maps;

namespace SavingTheWorldFromAngriness.Global
{
    public static class Player
    {
        public static int HEALTH = 100;
        public static double COINS = 0;
        public static List<string> INVENTORY = new List<string>();
        public static Weapon WEAPON = new BeginnerGun();
        public static String CURRENT_WORLD = "";
        public static String CURRENT_MAP = "";
        public static double AMMO = 0;
        public static double AMMO_IN_MAGAZINE = 0;
        public static string PLAYER_NAME = "";
        public static Type LAST_MAP = null;
    }
}