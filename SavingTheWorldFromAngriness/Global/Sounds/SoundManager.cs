﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.Global.Sounds
{
    public static class SoundManager
    {
        public static float VOLUME_EFFECT = 0.7f; // 0 to 1
        public static float VOLUME_SONG = 1;

        public static Dictionary<SoundEffect, SoundEffectInstance> 
        CURRENTLY_LOOPING_SOUND_EFFECTS = new Dictionary<SoundEffect, SoundEffectInstance>();

        public static void PlaySoundEffect(SoundEffect soundEffect)
        {
            soundEffect.Play(VOLUME_EFFECT, 0, 0);
        }

        public static void PlayRepeatingSoundEffect(SoundEffect soundEffect)
        {
            SoundEffectInstance soundEffectInstance = soundEffect.CreateInstance();
            soundEffectInstance.Volume = VOLUME_EFFECT;
            soundEffectInstance.IsLooped = true;
            soundEffectInstance.Play();

            CURRENTLY_LOOPING_SOUND_EFFECTS.Add(soundEffect, soundEffectInstance);
        }

        public static void StopRepeatingSoundEffect(SoundEffect soundEffect)
        {
            CURRENTLY_LOOPING_SOUND_EFFECTS[soundEffect].IsLooped = false;
            CURRENTLY_LOOPING_SOUND_EFFECTS[soundEffect].Pause();
            CURRENTLY_LOOPING_SOUND_EFFECTS[soundEffect].Stop();
            CURRENTLY_LOOPING_SOUND_EFFECTS[soundEffect].Dispose();
            CURRENTLY_LOOPING_SOUND_EFFECTS.Remove(soundEffect);
        }

        public static void ClearRepeatingSoundEffects()
        {
            foreach (KeyValuePair<SoundEffect, SoundEffectInstance> soundEffectInstance in CURRENTLY_LOOPING_SOUND_EFFECTS)
            {
                soundEffectInstance.Value.IsLooped = false;
                soundEffectInstance.Value.Pause();
                soundEffectInstance.Value.Stop();
                soundEffectInstance.Value.Dispose();
            }

            CURRENTLY_LOOPING_SOUND_EFFECTS.Clear();
        }

        public static void PlayBackgroundMusic(Song song)
        {
            MediaPlayer.Play(song);
            MediaPlayer.Volume = VOLUME_SONG;
            MediaPlayer.IsRepeating = true;
        }

        public static void StopBackgroundMusic()
        {
            MediaPlayer.Stop();
        }

        public static void PauseBackgroundMusic()
        {
            MediaPlayer.Pause();
        }

        public static void ResumeBackgroundMusic()
        {
            MediaPlayer.Resume();
        }

        public static void PlaySoundEffect(SoundEffect soundEffect, float varRelativeVolume = 1.0f)
        {
            soundEffect.Play((VOLUME_EFFECT * varRelativeVolume), 0, 0);
        }

        public static void PlayRepeatingSoundEffect(SoundEffect soundEffect, float varRelativeVolume = 1.0f)
        {
            SoundEffectInstance soundEffectInstance = soundEffect.CreateInstance();
            soundEffectInstance.Volume = (VOLUME_EFFECT * varRelativeVolume);
            soundEffectInstance.IsLooped = true;
            soundEffectInstance.Play();

            CURRENTLY_LOOPING_SOUND_EFFECTS.Add(soundEffect, soundEffectInstance);
        }
    }
}
