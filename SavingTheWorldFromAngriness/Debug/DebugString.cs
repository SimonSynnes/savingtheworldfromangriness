﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace SavingTheWorldFromAngriness.Debug
{
    public class DebugString  : DrawableGameComponent
    {
        SpriteBatch spriteBatch;
        SpriteFont spriteFont;

        string debugString;

        public DebugString(Game game)
            : base(game)
        {
            debugString = "";
        }

        public override void Update(GameTime gameTime)
        {
            debugString = "GAMECOMPONENTS: " + Game.Components.Count.ToString();

            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteFont = Game.Content.Load<SpriteFont>("Fonts/DebugFont");

            base.LoadContent();
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            int windowHeight = GraphicsDevice.Viewport.Height;
            int margin = 20;

            spriteBatch.DrawString(spriteFont, debugString, new Vector2(33, windowHeight - 66 - (margin)), Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
