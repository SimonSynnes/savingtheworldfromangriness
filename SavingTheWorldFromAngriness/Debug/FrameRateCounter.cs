﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavingTheWorldFromAngriness.Debug
{
    public class FrameRateCounter : DrawableGameComponent
    {
        SpriteBatch spriteBatch;
        SpriteFont spriteFont;

        int frameRate;
        int frameCounter;
        TimeSpan elapsedTime;

        public FrameRateCounter(Game game)
            : base(game)
        {
            frameRate = 0;
            frameCounter = 0;
            elapsedTime = TimeSpan.Zero;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteFont = Game.Content.Load<SpriteFont>("Fonts/DebugFont");    

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            elapsedTime += gameTime.ElapsedGameTime;

            if (elapsedTime > TimeSpan.FromSeconds(1))
            {
                elapsedTime -= TimeSpan.FromSeconds(1);
                frameRate = frameCounter;
                frameCounter = 0;
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            frameCounter++;

            string fps = string.Format("fps: {0}", frameRate);

            spriteBatch.Begin();

            int windowHeight = GraphicsDevice.Viewport.Height;
            int margin = 20;

            spriteBatch.DrawString(spriteFont, fps, new Vector2(33, windowHeight - 33 - (margin)), Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
