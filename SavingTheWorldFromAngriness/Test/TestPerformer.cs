﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;

using SavingTheWorldFromAngriness.World;
using SavingTheWorldFromAngriness.World.Tiles;
using SavingTheWorldFromAngriness.World.Maps;

namespace SavingTheWorldFromAngriness.Test
{
    class TestPerformer
    {
        public static void PerformTests()
        {
            Console.Title = "STWFA TESTS";

            Console.WriteLine("Running tests...");
            Console.WriteLine("");

            WallOptimizationTest();

            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("Testing complete!");
        }

        private static void WallOptimizationTest()
        {
            string mapCode = "|021,021,021,021,021,021,021,021,021,021,\n|021,011,011,011,011,011,011,011,011,021,\n|021,011,011,011,011,011,011,011,011,021,\n|021,011,011,011,011,011,011,011,011,021,\n|021,011,011,011,011,011,011,011,011,021,\n|021,011,011,011,011,011,011,011,011,021,\n|021,011,011,011,011,011,011,011,011,021,\n|021,011,011,011,011,011,011,011,011,021,\n|021,011,011,011,011,011,011,011,011,021,\n|021,021,021,021,021,021,021,021,021,021,";

            try
            {
                Game game = new Game1();
                TileManager tileManager = new TileManager(game);

                List<Tile> walls = WallOptimization.optimizeWalls(mapCode, tileManager);

                if (walls.Count == 4)
                {
                    ReturnTrue("Wall Optimization");
                }
                else
                {
                    ReturnFalse("Wall Optimization");
                }
            }
            catch
            {
                ReturnFalse("Wall Optimization");
            }
        }

        private static void ReturnFalse(string title)
        {
            ConsoleColor defaultColor = Console.ForegroundColor;

            Console.Write(title + ": ");

            Console.ForegroundColor = ConsoleColor.Red;

            Console.Write("FAILED");

            Console.ForegroundColor = defaultColor;
        }

        private static void ReturnTrue(string title)
        {
            ConsoleColor defaultColor = Console.ForegroundColor;

            Console.Write(title + ": ");

            Console.ForegroundColor = ConsoleColor.Green;

            Console.Write("PASSED");

            Console.ForegroundColor = defaultColor;
        }
    }
}
