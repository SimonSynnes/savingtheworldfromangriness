﻿using SavingTheWorldFromAngriness.Weapons;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SavingTheWorldFromAngriness
{
    public class SaveGame
    {
        private String name;
        private int health;
        private double ammo;
        private double ammoInMagazine;
        private String world;
        private String map;
        private List<string> inventory;
        private double coins;
        private String fileName;
        private Weapons.Weapon weapon;

        public SaveGame(String name, int health, double ammo, double ammoInMagazine, String world, String map, double coins,
            List<String> inventory, Weapons.Weapon weapon)
        {
            this.name = name;
            this.health = health;
            this.ammo = ammo;
            this.world = world;
            this.map = map;
            this.ammoInMagazine = ammoInMagazine;
            this.coins = coins;
            this.inventory = inventory;
            this.weapon = weapon;
        }

        // Create a string array with the lines of text
        public void Save()
        {
            string directory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Saves");
            string fileName = $@"{DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss")}.save";
            string[] lines = { Base64Encode(name), Base64Encode(health.ToString()), Base64Encode(ammo.ToString()), Base64Encode(ammoInMagazine.ToString()),
                Base64Encode(world), Base64Encode(map), Base64Encode(coins.ToString()),
            Base64Encode(string.Join(",", inventory)), ObjectToString(weapon) };
            File.WriteAllLines(Path.Combine(directory, fileName), lines);
            
            this.fileName = fileName;
        }

        public string getFileName()
        {
            return this.fileName;
        }

        public static string ObjectToString(Object obj)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                new BinaryFormatter().Serialize(ms, obj);
                return Convert.ToBase64String(ms.ToArray());
            }
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
