﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RoyT.XNA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lighting
{
    public class Visual
    {
        /// <summary>
        /// The texture, or color of the object
        /// </summary>
        public Texture2D Texture{ get; set; }

        /// <summary>
        /// The glow, or light emitted by the object
        /// can be null
        /// </summary>
        public Texture2D Glow { get; set; }

        public int Index { get; set; }

        /// <summary>
        /// The position, scale and rotation of the object
        /// </summary>
        public Pose2D Pose { get; set; }

        public Rectangle rectangle { get; set; }

        public Visual(Texture2D texture, Pose2D pose, int index, Rectangle rectangle)
            :this(texture, pose, null, index, rectangle) { }

        public Visual(Texture2D texture, Vector2 position, float rotation, int index, Rectangle rectangle)
            : this(texture, new Pose2D(position, rotation), index, rectangle) { }

        public Visual(Texture2D texture, Vector2 position, float rotation, Texture2D glow, int index, Rectangle rectangle)
            : this(texture, new Pose2D(position, rotation), glow, index, rectangle) { }

        public Visual(Texture2D texture, Pose2D pose, Texture2D glow, int index, Rectangle rectangle)
        {
            this.Texture = texture;
            this.Pose = pose;
            this.Glow = glow;
            this.Index = index;
            this.rectangle = rectangle;
        }                
    }
}
