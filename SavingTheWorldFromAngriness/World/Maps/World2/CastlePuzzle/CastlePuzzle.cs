﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.World.Components.World2;
using Microsoft.Xna.Framework.Media;
using SavingTheWorldFromAngriness.Backgrounds;
using Microsoft.Xna.Framework.Audio;
using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.GUI;

namespace SavingTheWorldFromAngriness.World.Maps.World2.CastlePuzzle
{
    class CastlePuzzle : BasicMap
    {
        #region UI
        private Texture2D playerAvatarTexture;
        private SoundEffect promptSoundEffect1;
        private bool stopMapLogic;
        #endregion

        #region button
        private SoundEffect btnClicked;
        private SoundEffect btnUnclicked;
        private FloorButton floorBtn;
        private BasicComponent stoneWallBlock;
        private Texture2D stoneWall;
        private Rectangle buttonRectangle;
        private bool soundPlayed;
        private bool steppedOnButton;
        private bool doorHidden;
        private Vector2 tileSize;
        bool isButtonVisible;
        #endregion

        #region Crate
        private BasicComponent crate7;
        private Texture2D crateTexture;
        private Vector2 crate7Position { get; set; }
        #endregion

        private LevelDoor exitCastlePuzzle;
        private Rectangle wallCollisionRectangle;

        public CastlePuzzle(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer)
        {
            #region Button
            this.steppedOnButton = false;
            this.soundPlayed = false;
            this.doorHidden = true;
            this.buttonRectangle = new Rectangle((worldRenderer.map_width / 2), (worldRenderer.map_height / 2), 32, 32);
            this.tileSize = new Vector2(32, 32);
            this.isButtonVisible = false;
            #endregion
            this.crate7Position = new Vector2(64,96);
        }

        public override void Initialize()
        {
            base.Initialize();
            player.setPlayerPosition(new Vector2(80, 86));

            SetCameraToPlayerPosition();

            stopMapLogic = true;
            DialogPrompt beginningMessage = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME,
                playerAvatarTexture, "Hmm... Where do I go from here?",
                promptSoundEffect1);
            Game.Components.Add(beginningMessage);

            beginningMessage.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
            {
                stopMapLogic = false;
                Game.Components.Remove(beginningMessage);
            };
        }

        public override void DrawBehindShadows()
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                SamplerState.LinearWrap,
                null,
                null,
                null,
                camera.TransformMatrix);

            foreach (GameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).Draw(spriteBatch);
            }

            spriteBatch.End();

            player.Draw(spriteBatch, camera);
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            #region UI
            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");
            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");
            #endregion

            #region Enter Room
            LevelDoor enterCastlePuzzle = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(48, 16, 64, 32), "World2", "CastleBanquetHall");
            AddGameComponent(enterCastlePuzzle);
            #endregion

            #region Hidden-door
            stoneWall = Game.Content.Load<Texture2D>("Textures/World2/CastlePuzzle/twoBlockStoneWall");
            stoneWallBlock = new BasicComponent(this.Game, worldRenderer, this, new Rectangle(272, 16, 64, 32), stoneWall);

            AddGameComponent(stoneWallBlock);

            //stops the player from walking through the component
            wallCollisionRectangle = new Rectangle(272, 16, 64, 32);
            worldRenderer.addMiscCollision(wallCollisionRectangle);
            #endregion

            #region Floor button
            floorBtn = new FloorButton(this.Game, worldRenderer, this,
                new Rectangle(buttonRectangle.X, buttonRectangle.Y, (int)tileSize.X, (int)tileSize.Y));
            AddGameComponent(floorBtn);
            #endregion

            #region Sounds
            Song backgroundMusic = Game.Content.Load<Song>("Sounds/Ominous");
            PlayBackgroundMusic(backgroundMusic);

            btnClicked = Game.Content.Load<SoundEffect>("Sounds/World2/stoneButtonClicked");
            btnUnclicked = Game.Content.Load<SoundEffect>("Sounds/World2/stoneButtonUnclicked");
            #endregion

            #region Background
            worldRenderer.SetBackground(new BasicBackground(this.Game), Color.TransparentBlack);
            #endregion

            #region Crate
            crateTexture = Game.Content.Load<Texture2D>("Textures/World2/CastlePuzzle/crate1");

            BasicComponent crate2 = new BasicComponent(this.Game, worldRenderer, this,
                new Rectangle(buttonRectangle.X - 32, buttonRectangle.Y, (int)tileSize.X, (int)tileSize.Y), crateTexture);
            AddGameComponent(crate2);

            BasicComponent crate3 = new BasicComponent(this.Game, worldRenderer, this,
                new Rectangle(buttonRectangle.X - 96, buttonRectangle.Y, (int)tileSize.X, (int)tileSize.Y), crateTexture);
            AddGameComponent(crate3);

            BasicComponent crate4 = new BasicComponent(this.Game, worldRenderer, this,
                new Rectangle(buttonRectangle.X + 32, buttonRectangle.Y, (int)tileSize.X, (int)tileSize.Y), crateTexture);
            AddGameComponent(crate4);

            BasicComponent crate5 = new BasicComponent(this.Game, worldRenderer, this,
                new Rectangle(buttonRectangle.X, buttonRectangle.Y + 32, (int)tileSize.X, (int)tileSize.Y), crateTexture);
            AddGameComponent(crate5);

            BasicComponent crate6 = new BasicComponent(this.Game, worldRenderer, this,
                new Rectangle(buttonRectangle.X, buttonRectangle.Y - 32, (int)tileSize.X, (int)tileSize.Y), crateTexture);
            AddGameComponent(crate6);

            crate7 = new BasicComponent(this.Game, worldRenderer, this, 
                new Rectangle(buttonRectangle.X, buttonRectangle.Y, (int)tileSize.X, (int)tileSize.Y), crateTexture);
            AddGameComponent(crate7);

            #endregion
        }

        public override void Update(GameTime gameTime)
        {
            if (stopMapLogic)
            {
                SetCameraToPlayerPosition();
                return;
            }

            base.Update(gameTime);

            #region Floor button

            if (floorBtn.IsPlayerOnRectangle == true && soundPlayed == false && isButtonVisible)
            {
                SoundManager.PlaySoundEffect(btnClicked);
                soundPlayed = true;
                steppedOnButton = true;
                doorHidden = false;

            }
            if (steppedOnButton == true && floorBtn.IsPlayerOnRectangle == false && isButtonVisible)
            {
                SoundManager.PlaySoundEffect(btnUnclicked);
                soundPlayed = false;
                steppedOnButton = false;
            }
            if (steppedOnButton)
            {                
                // This stair-door will reveal it self when some puzzel is solved. 
                exitCastlePuzzle = new LevelDoor(this.Game, worldRenderer, this,
                    new Rectangle(272, 16, 64, 32), "World2", "CastleBasement");
                AddGameComponent(exitCastlePuzzle);
            }
            #endregion

            #region Hidden door
            if (doorHidden == false)
            {
                RemoveGameComponent(stoneWallBlock);
                worldRenderer.removeMiscCollision(wallCollisionRectangle);

            }
            #endregion

            //if the player walks on the crate, it will move to another location.
            if (player.getPlayerRectangle().Intersects(buttonRectangle) && !isButtonVisible)
            {
                isButtonVisible = true;
                RemoveGameComponent(crate7);

                crate7 = new BasicComponent(this.Game, worldRenderer, this,
                    new Rectangle(buttonRectangle.X + 64, buttonRectangle.Y, (int)tileSize.X, (int)tileSize.Y), crateTexture);
                AddGameComponent(crate7);
            }
        }

        public Rectangle GetCrateRectangle()
        {
            return new Rectangle((int)crate7Position.X + (Global.GameConstants.TILE_SIZE / 2),
                (int)crate7Position.Y + (Global.GameConstants.TILE_SIZE / 2),
                crateTexture.Width, crateTexture.Height);
        }
    }
}
