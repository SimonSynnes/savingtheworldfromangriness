﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global;
using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.World.Maps;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.World.Maps.World2.ShrineSanctuaryRoom1
{
    class ShrineSanctuaryRoom1 : BasicMap
    {
        public ShrineSanctuaryRoom1(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();

            worldRenderer.SetBackground(new Backgrounds.BasicBackground(Game), new Color(17, 17, 17));
        }

        public override void RemoveAllGameComponents()
        {
            SoundManager.ClearRepeatingSoundEffects();

            foreach (IGameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).RemoveAllGameComponents();
                Game.Components.Remove(gameComponent);
            }

            Game.Components.Remove(player);

            PlayerUI.ClearUI();
            Game.Components.Remove(PlayerUI);

            if (!isPlayerDead)
            {
                Global.Player.LAST_MAP = this.GetType();
            }
        }

        public override void OnPlayerDeath()
        {
            base.OnPlayerDeath();

            worldRenderer.LoadMap("World2", "ShrineSanctuaryRoom1");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            LevelDoor LevelDoor2787 = new LevelDoor(Game, worldRenderer, this, new Rectangle(587, 19, 100, 32), "World2", "ShrineSanctuaryHall2");
            AddGameComponent(LevelDoor2787);
            LevelDoor LevelDoor6703 = new LevelDoor(Game, worldRenderer, this, new Rectangle(19, 242, 32, 100), "World2", "ShrineSanctuaryRoom2");
            AddGameComponent(LevelDoor6703);

            if (Global.Player.LAST_MAP == typeof(World2.ShrineSanctuaryHall2.ShrineSanctuaryHall2))
            {
                player.setPlayerPosition(new Vector2(615, 46));
            }
            else
            if (Global.Player.LAST_MAP == typeof(World2.ShrineSanctuaryRoom2.ShrineSanctuaryRoom2))
            {
                player.setPlayerPosition(new Vector2(49, 270));
            }
            else
            {
                player.setPlayerPosition(new Vector2(615, 46));
                Song backgroundSong = Game.Content.Load<Song>("Sounds/World2/Shrine/silentLight");
                SoundManager.PlayBackgroundMusic(backgroundSong);
            }

            Enemy Enemy3927 = new Enemy(Game, worldRenderer, this, new Rectangle(107, 358, 0, 0), player, 256, 80, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 3, 3);
            AddGameComponent(Enemy3927);
            Enemy Enemy9523 = new Enemy(Game, worldRenderer, this, new Rectangle(308, 363, 0, 0), player, 256, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 3, 3);
            AddGameComponent(Enemy9523);
            Enemy Enemy1735 = new Enemy(Game, worldRenderer, this, new Rectangle(477, 366, 0, 0), player, 256, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 3, 3);
            AddGameComponent(Enemy1735);
            Enemy Enemy3558 = new Enemy(Game, worldRenderer, this, new Rectangle(619, 366, 0, 0), player, 256, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 3, 3);
            AddGameComponent(Enemy3558);
            Enemy Enemy9977 = new Enemy(Game, worldRenderer, this, new Rectangle(104, 180, 0, 0), player, 256, 80, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 3, 0);
            AddGameComponent(Enemy9977);
            Enemy Enemy4872 = new Enemy(Game, worldRenderer, this, new Rectangle(308, 175, 0, 0), player, 256, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 3, 0);
            AddGameComponent(Enemy4872);

            /*   Enemy Enemy8391 = new Enemy(Game, worldRenderer, this, new Rectangle(504, 170, 0, 0), player, 250, 200, 99, 25, 3, 0);
               AddGameComponent(Enemy8391);
               Enemy Enemy7859 = new Enemy(Game, worldRenderer, this, new Rectangle(662, 164, 0, 0), player, 250, 200, 99, 25, 3, 0);
               AddGameComponent(Enemy7859);*/

            HealthPack HealthPack3442 = new HealthPack(Game, worldRenderer, this, new Rectangle(28, 107, 0, 0));
            AddGameComponent(HealthPack3442);
            HealthPack HealthPack8421 = new HealthPack(Game, worldRenderer, this, new Rectangle(28, 195, 0, 0));
            AddGameComponent(HealthPack8421);
            HealthPack HealthPack2959 = new HealthPack(Game, worldRenderer, this, new Rectangle(25, 304, 0, 0));
            AddGameComponent(HealthPack2959);
            HealthPack HealthPack4990 = new HealthPack(Game, worldRenderer, this, new Rectangle(34, 435, 0, 0));
            AddGameComponent(HealthPack4990);
            Ammo Ammo8070 = new Ammo(Game, worldRenderer, this, new Rectangle(310, 31, 0, 0));
            AddGameComponent(Ammo8070);
            Ammo Ammo4345 = new Ammo(Game, worldRenderer, this, new Rectangle(405, 42, 0, 0));
            AddGameComponent(Ammo4345);
            Ammo Ammo3718 = new Ammo(Game, worldRenderer, this, new Rectangle(524, 45, 0, 0));
            AddGameComponent(Ammo3718);
            Ammo Ammo1142 = new Ammo(Game, worldRenderer, this, new Rectangle(660, 47, 0, 0));
            AddGameComponent(Ammo1142);
            Ammo Ammo4973 = new Ammo(Game, worldRenderer, this, new Rectangle(57, 151, 0, 0));
            AddGameComponent(Ammo4973);
            Ammo Ammo8600 = new Ammo(Game, worldRenderer, this, new Rectangle(62, 249, 0, 0));
            AddGameComponent(Ammo8600);
            Ammo Ammo6532 = new Ammo(Game, worldRenderer, this, new Rectangle(50, 343, 0, 0));
            AddGameComponent(Ammo6532);
            Ammo Ammo6409 = new Ammo(Game, worldRenderer, this, new Rectangle(54, 444, 0, 0));
            AddGameComponent(Ammo6409);
            HealthPack HealthPack4453 = new HealthPack(Game, worldRenderer, this, new Rectangle(439, 272, 0, 0));
            AddGameComponent(HealthPack4453);
            HealthPack HealthPack1973 = new HealthPack(Game, worldRenderer, this, new Rectangle(678, 280, 0, 0));
            AddGameComponent(HealthPack1973);
            HealthPack HealthPack3368 = new HealthPack(Game, worldRenderer, this, new Rectangle(424, 463, 0, 0));
            AddGameComponent(HealthPack3368);
            HealthPack HealthPack1752 = new HealthPack(Game, worldRenderer, this, new Rectangle(243, 477, 0, 0));
            AddGameComponent(HealthPack1752);
            HealthPack HealthPack2750 = new HealthPack(Game, worldRenderer, this, new Rectangle(263, 115, 0, 0));
            AddGameComponent(HealthPack2750);
            HealthPack HealthPack2058 = new HealthPack(Game, worldRenderer, this, new Rectangle(584, 81, 0, 0));
            AddGameComponent(HealthPack2058);
        }

    }
}