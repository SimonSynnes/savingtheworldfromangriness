﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using SavingTheWorldFromAngriness.Backgrounds;
using SavingTheWorldFromAngriness.World.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavingTheWorldFromAngriness.World.Maps.World2
{
    class Disco : BasicMap
    {
        public Disco(Game game, WorldRenderer worldRenderer) : base(game, worldRenderer)
        { }

            public override void Initialize()
        {
            base.Initialize();

            player.setPlayerPosition(new Vector2(695, 720));
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            #region Song
            Song backgroundMusic = Game.Content.Load<Song>("Sounds/World2/gametime");
            PlayBackgroundMusic(backgroundMusic);
            #endregion

            #region Leveldoor
            LevelDoor levelDoorMap = new LevelDoor(this.Game, worldRenderer,
                this, new Rectangle(515, 15, 50, 50), "World2", "BlackMap1");
            AddGameComponent(levelDoorMap);
            #endregion

            #region Enemies
            Enemy enemy1 = new Enemy(this.Game, worldRenderer, this, new Rectangle(684, 124, 0, 10), player, 438, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(enemy1);

            Enemy enemy2 = new Enemy(this.Game, worldRenderer, this, new Rectangle(524, 227, 0, 10), player, 438, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(enemy2);

            Enemy enemy3 = new Enemy(this.Game, worldRenderer, this, new Rectangle(317, 739, 0, 10), player, 438, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(enemy3);

            Enemy enemy4 = new Enemy(this.Game, worldRenderer, this, new Rectangle(71, 704, 0, 10), player, 438, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(enemy4);
            Enemy Enemy6569 = new Enemy(Game, worldRenderer, this, new Rectangle(262, 214, 0, 0), player, 438, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(Enemy6569);
            Enemy Enemy2088 = new Enemy(Game, worldRenderer, this, new Rectangle(499, 456, 0, 0), player, 438, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(Enemy2088);
            Enemy Enemy6243 = new Enemy(Game, worldRenderer, this, new Rectangle(233, 353, 0, 0), player, 438, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(Enemy6243);
            #endregion

            #region Ammo
            Ammo Ammo2313 = new Ammo(Game, worldRenderer, this, new Rectangle(660, 636, 0, 0));
            AddGameComponent(Ammo2313);
            Ammo Ammo7002 = new Ammo(Game, worldRenderer, this, new Rectangle(673, 497, 0, 0));
            AddGameComponent(Ammo7002);
            Ammo Ammo8892 = new Ammo(Game, worldRenderer, this, new Rectangle(544, 569, 0, 0));
            AddGameComponent(Ammo8892);
            Ammo Ammo2622 = new Ammo(Game, worldRenderer, this, new Rectangle(666, 398, 0, 0));
            AddGameComponent(Ammo2622);
            Ammo Ammo3468 = new Ammo(Game, worldRenderer, this, new Rectangle(42, 312, 0, 0));
            AddGameComponent(Ammo3468);
            Ammo Ammo9514 = new Ammo(Game, worldRenderer, this, new Rectangle(45, 480, 0, 0));
            AddGameComponent(Ammo9514);
            #endregion

            #region HP
            HealthPack HealthPack6117 = new HealthPack(Game, worldRenderer, this, new Rectangle(655, 680, 0, 0));
            AddGameComponent(HealthPack6117);
            #endregion

            #region Background
            //worldRenderer.SetBackground(new MovingLava(Game, 40, 1), Color.Red);
            worldRenderer.SetBackground(new Backgrounds.BasicBackground(this.Game), Color.Black);
            #endregion

        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

        }

        public override void RemoveAllGameComponents()
        {
            worldRenderer.SetBackground(new MovingClouds(this.Game, 40, 1), Color.CornflowerBlue);
            base.RemoveAllGameComponents();
        }

         public override void OnPlayerDeath()
         {
             worldRenderer.LoadMap("World2", "Disco");
            base.OnPlayerDeath();
         }

    }
}
