﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;
using SavingTheWorldFromAngriness.World.Components;

namespace SavingTheWorldFromAngriness.World.Maps.World2
{
    class DarkTeleportRoom : BasicMap
    {
        public DarkTeleportRoom(Game game, WorldRenderer worldRenderer) : base(game, worldRenderer)
        {
        }

        public override void Initialize()
        {
            base.Initialize();

            #region Background 
            worldRenderer.SetBackground(new Backgrounds.BasicBackground(this.Game), Color.Black);
            #endregion

            player.setPlayerPosition(new Vector2(368, 720));
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            #region Teleports
            Teleport Teleport5229 = new Teleport(Game, worldRenderer, this, new Rectangle(485, 595, 0, 0), 59, 64);
            AddGameComponent(Teleport5229);

            Teleport Teleport8052 = new Teleport(Game, worldRenderer, this, new Rectangle(203, 42, 0, 0), 698, 63);
            AddGameComponent(Teleport8052);

            Teleport Teleport4679 = new Teleport(Game, worldRenderer, this, new Rectangle(494, 43, 0, 0), 371, 70);
            AddGameComponent(Teleport4679);

            Teleport Teleport3171 = new Teleport(Game, worldRenderer, this, new Rectangle(40, 681, 0, 0), 643, 396);
            AddGameComponent(Teleport3171);
            #endregion

            #region Enemies

            Enemy Enemy4055 = new Enemy(Game, worldRenderer, this, new Rectangle(372, 182, 0, 0), player, 201, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(Enemy4055);

            Enemy Enemy2500 = new Enemy(Game, worldRenderer, this, new Rectangle(79, 391, 0, 0), player, 201, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(Enemy2500);
            #endregion

            #region Ammo
            Ammo Ammo4002 = new Ammo(Game, worldRenderer, this, new Rectangle(380, 691, 0, 0));
            AddGameComponent(Ammo4002);
            #endregion

            #region HP
            HealthPack HealthPack6117 = new HealthPack(Game, worldRenderer, this, new Rectangle(522, 172, 0, 0));
            AddGameComponent(HealthPack6117);
            #endregion

            #region Torchlight
            TorchLight TorchLight7008 = new TorchLight(Game, worldRenderer, this, new Rectangle(113, 285, 0, 0), 5, 300);
            AddGameComponent(TorchLight7008);
            TorchLight TorchLight1740 = new TorchLight(Game, worldRenderer, this, new Rectangle(546, 285, 0, 0), 5, 300);
            AddGameComponent(TorchLight1740);
            #endregion

            #region LevelDoor
            LevelDoor LevelDoor7970 = new LevelDoor(Game, worldRenderer, this, new Rectangle(365, 30, 50, 50), "World2", "ShrineEntrance");
            AddGameComponent(LevelDoor7970);
            #endregion

            #region Song
            Song backgroundMusic = Game.Content.Load<Song>("Sounds/World2/Map9");
            PlayBackgroundMusic(backgroundMusic);

            #endregion

        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void OnPlayerDeath()
        {
            worldRenderer.LoadMap("World2", "DarkTeleportRoom");
            base.OnPlayerDeath();
        }
    }
}