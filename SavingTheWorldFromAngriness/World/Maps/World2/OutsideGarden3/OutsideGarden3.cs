﻿using Microsoft.Xna.Framework;
using SavingTheWorldFromAngriness.World.Components;
using Microsoft.Xna.Framework.Media;
using System;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;
using SavingTheWorldFromAngriness.GUI;
using GeonBit.UI.Entities;

namespace SavingTheWorldFromAngriness.World.Maps.World2.OutsideGarden3
{
    #region Tree Class
    class Tree
    {
        public Vector2 position;
        public Rectangle rectangle;
        public Tree(Vector2 treePosition)
        {
            this.position = treePosition;
            this.rectangle = new Rectangle(
                (int)treePosition.X, (int)treePosition.Y + 40, 32, 20);
        }
        public Rectangle GetTreeRectangle()
        {
            return new Rectangle(
                rectangle.X + (Global.GameConstants.TILE_SIZE / 2),
                rectangle.Y + (Global.GameConstants.TILE_SIZE / 2),
                rectangle.Width, rectangle.Height);
        }
    }
    #endregion

    class OutsideGarden3 : BasicMap
    {
        #region Tree
        private List<Tree> trees;
        private Texture2D treeTexture;
        #endregion

        #region UI
        private Texture2D playerAvatarTexture;
        private Texture2D guardAvatarTexture;
        private SoundEffect promptSoundEffect2;
        private SoundEffect promptSoundEffect1;
        #endregion

        private bool stopMapLogic;

        public OutsideGarden3(Game game, WorldRenderer worldRenderer) 
            : base(game, worldRenderer)
        {
        }

        public override void Initialize()
        {
            #region Tree
            trees = new List<Tree>();
            #endregion

            #region Background
            worldRenderer.SetBackground(new Backgrounds.MovingClouds(this.Game, 40, 1), Color.CornflowerBlue);
            #endregion

            base.Initialize();

            #region Player start position
            if (Global.Player.LAST_MAP == typeof(World2.OutsideSnow.OutsideSnow))
            {
                player.setPlayerPosition(new Vector2((5 * 32), (9 * 32) + 16));
            }
            else
            {
                player.setPlayerPosition(new Vector2(16 + (16 * 32), (16 + (9 * 32))));

            }
            #endregion

            SetCameraToPlayerPosition();

            stopMapLogic = true;
            DialogPrompt beginningMessage = new DialogPrompt(this.Game, "Guard",
                guardAvatarTexture, "Stop right there!\n\nThis portal is for the King's use only!",
                promptSoundEffect2);
            Game.Components.Add(beginningMessage);

            beginningMessage.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
            {
                DialogPrompt prompt2 = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME,
                    playerAvatarTexture, "...", promptSoundEffect1);
                Game.Components.Add(prompt2);

                prompt2.getOKButton().OnClick += (Entity entity2) =>
                {
                    stopMapLogic = false;
                    Game.Components.Remove(prompt2);
                };

                Game.Components.Remove(beginningMessage);
            };
        }

        public override void Update(GameTime gameTime)
        {
            if (stopMapLogic)
            {
                SetCameraToPlayerPosition();
                return;
            }

            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            #region Sounds
            Song backgroundMusic = Game.Content.Load<Song>("Sounds/World2/OutsideGarden/OutsideGarden");
            PlayBackgroundMusic(backgroundMusic);
            #endregion

            #region LevelDoors
            Texture2D portal = Game.Content.Load<Texture2D>("Textures/World1/CastleOutside/portal");

            LevelDoor EnterDoor = new LevelDoor(Game, worldRenderer, this, 
                new Rectangle((16+(17*32)), (9*32)-16, 32, 64), "World2", "OutsideGarden2");
            AddGameComponent(EnterDoor);
            LevelDoor ExitDoor = new LevelDoor(Game, worldRenderer, this, 
                new Rectangle((3*32), (8*32)-16, 127, 127), "World2", "OutsideSnow", portal);
            AddGameComponent(ExitDoor);
            #endregion

            #region Trees

            treeTexture = Game.Content.Load<Texture2D>("Textures/World1/Coconut/tree");

            //trees at the top
            trees.Add(new Tree(new Vector2(144, 45)));
            trees.Add(new Tree(new Vector2(144 + (6 * 32), 45)));
            trees.Add(new Tree(new Vector2(144 + (11 * 32), 45)));

            //trees at the bottom
            trees.Add(new Tree(new Vector2(144, 45+(13*32))));
            trees.Add(new Tree(new Vector2(144 + (6 * 32), 45 + (13 * 32))));
            trees.Add(new Tree(new Vector2(144 + (11 * 32), 45 + (13 * 32))));

            //trees above the portal
            trees.Add(new Tree(new Vector2(68, 45 + (4 * 32))));
            trees.Add(new Tree(new Vector2(100, 45+(4*32))));
            trees.Add(new Tree(new Vector2(100 + (1 * 32), 45 + (4 * 32))));
            trees.Add(new Tree(new Vector2(100 + (2 * 32), 45 + (4 * 32))));
            trees.Add(new Tree(new Vector2(100 + (3 * 32), 45 + (4 * 32))));
            trees.Add(new Tree(new Vector2(100 + (6 * 32), 45 + (4 * 32))));
            trees.Add(new Tree(new Vector2(100 + (9 * 32), 45 + (4 * 32))));
            trees.Add(new Tree(new Vector2(100 + (12 * 32), 45 + (4 * 32))));

            //trees below the poral
            trees.Add(new Tree(new Vector2(100, 45 + (9 * 32))));
            trees.Add(new Tree(new Vector2(100 + (1 * 32), 45 + (9 * 32))));
            trees.Add(new Tree(new Vector2(100 + (2 * 32), 45 + (9 * 32))));
            trees.Add(new Tree(new Vector2(100 + (3 * 32), 45 + (9 * 32))));
            trees.Add(new Tree(new Vector2(100 + (6 * 32), 45 + (9 * 32))));
            trees.Add(new Tree(new Vector2(100 + (9 * 32), 45 + (9 * 32))));
            trees.Add(new Tree(new Vector2(100 + (12 * 32), 45 + (9 * 32))));

            //trees vertical left of portal
            trees.Add(new Tree(new Vector2(68, 35 + (5 * 32))));
            trees.Add(new Tree(new Vector2(68, 35 + (6 * 32))));
            trees.Add(new Tree(new Vector2(68, 35 + (7 * 32))));
            trees.Add(new Tree(new Vector2(68, 35 + (8 * 32))));
            trees.Add(new Tree(new Vector2(68, 45 + (9 * 32))));


            foreach (Tree tree in trees)
            {
                worldRenderer.addMiscCollision(
                    new Rectangle(tree.GetTreeRectangle().X+6, tree.GetTreeRectangle().Y,
                    tree.GetTreeRectangle().Width-18, tree.GetTreeRectangle().Height-5));
            }
            #endregion

            #region Enemies
            int hp = 343;
            int aggro = 50;
            int attackSpeed = Global.GameConstants.ENEMY_ATTACKSPEED;
            int attackPower = 10;
            int walkingSpeed = 3;
            int lookingRight = 2;


            Enemy Enemy1 = new Enemy(Game, worldRenderer, this, new Rectangle(300, 275, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingRight);
            AddGameComponent(Enemy1);
            Ammo Ammo5235 = new Ammo(Game, worldRenderer, this, new Rectangle(512, 232, 0, 0));
            #endregion
            Ammo Ammo5236 = new Ammo(Game, worldRenderer, this, new Rectangle(512, 232, 0, 0));
            AddGameComponent(Ammo5236);

            #region UI
            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");
            guardAvatarTexture = Game.Content.Load<Texture2D>("Textures/guardAvatarTexture");
            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");
            promptSoundEffect2 = Game.Content.Load<SoundEffect>("Sounds/World1/blip2");
            #endregion

        }
        public override void DrawBehindShadows()
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                SamplerState.LinearWrap, null, null, null, camera.TransformMatrix);

            foreach (GameComponent gameComponent in gameComponents)
            {
                if (gameComponent.GetType() != typeof(Enemy))
                {
                    ((BasicComponent)gameComponent).Draw(spriteBatch);
                }
            }
            spriteBatch.End();


            player.Draw(spriteBatch, camera);

            spriteBatch.Begin(SpriteSortMode.Immediate,
    BlendState.NonPremultiplied,
    SamplerState.LinearWrap, null, null, null, camera.TransformMatrix);

            foreach (GameComponent gameComponent in gameComponents)
            {
                if (gameComponent.GetType() == typeof(Enemy))
                {
                    ((BasicComponent)gameComponent).Draw(spriteBatch);
                }
            }
            spriteBatch.End();
            //   base.DrawBehindShadows();
        

        #region SpriteBatch Trees
        spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            foreach (Tree tree in trees)
            {
                spriteBatch.Draw(treeTexture, tree.position, Color.White);
            }
            spriteBatch.End();
            #endregion

        }
        public override void OnPlayerDeath()
        {
            base.OnPlayerDeath();

            worldRenderer.LoadMap("World2", "OutsideGarden3");
        }
    }
}
