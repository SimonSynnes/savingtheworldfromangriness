﻿using System;
using System.Collections.Generic;
using SavingTheWorldFromAngriness.Global.Sounds;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.World.Maps.World2.Skiing
{
    class Skiing : BasicMap
    {

        private bool isMoving;

        private Texture2D skiWithPlayerTexture;
        private Texture2D emptySkiTexture;
        private Texture2D gameTipTexture;
        private Texture2D roadTexture;
        private Texture2D obstacleTexture;
        private bool iskeyDown;
        private bool isPlayingSound;
        private bool atEndOfMap;
        private List<Vector2> obstacles;
        private Vector2 emptySkiPosition;
        private SoundEffect crashSoundEffect;
        private SoundEffect turningSoundEffect;
        private SoundEffect downhillSoundEffect;

        private int finishLine;

        public Skiing(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer)
        {
            this.isMoving = false;
            this.iskeyDown = false;
            this.isPlayingSound = false;
            this.atEndOfMap = false;
            this.finishLine = 0;
        }

        public override void Initialize()
        {
            base.Initialize();

            player.setPlayerPosition(new Vector2(100, 100));

            obstacles = new List<Vector2>();
            FillList();

            worldRenderer.SetBackground(new Backgrounds.Snow(this.Game), Color.Snow);
        }

        protected override void LoadContent()
        {
            skiWithPlayerTexture = Game.Content.Load<Texture2D>("Textures/World2/Skiing/playerOnSkis");

            base.LoadContent();

            roadTexture = Game.Content.Load<Texture2D>("Textures/World2/Driving/road");
            obstacleTexture = Game.Content.Load<Texture2D>("Textures/World2/Skiing/obstacle");
            emptySkiTexture = Game.Content.Load<Texture2D>("Textures/World2/Skiing/smallSkis");

            turningSoundEffect = Game.Content.Load<SoundEffect>("Sounds/World2/Skiing/skiing");
            downhillSoundEffect = Game.Content.Load<SoundEffect>("Sounds/World2/Skiing/downhillSoundEffect");
            crashSoundEffect = Game.Content.Load<SoundEffect>("Sounds/Misc/impact");
            gameTipTexture = Game.Content.Load<Texture2D>("Textures/World2/Driving/GameTip");



            emptySkiPosition = new Vector2(worldRenderer.map_width / 2 - (emptySkiTexture.Width / 2),
                worldRenderer.map_height / 2 - (emptySkiTexture.Height / 2));

            Rectangle emptySkiRectangle =
                new Rectangle(
                    (int)emptySkiPosition.X + 5,
                    (int)emptySkiPosition.Y + 5,
                    emptySkiTexture.Width - 10,
                    emptySkiTexture.Height - 10);

            worldRenderer.addMiscCollision(emptySkiRectangle);
        }

        public override void Update(GameTime gameTime)
        {
            if (Global.GameState.IS_GAME_PAUSED)
            {
                return;
            }

            KeyboardState ks = Keyboard.GetState();

            if (isMoving)
            {
                Vector2 playerCurrentPosition = player.getPlayerPosition();
                Vector2 playerPosition = new Vector2(playerCurrentPosition.X, playerCurrentPosition.Y - 20);

                if (playerPosition.Y < finishLine)
                {
                    worldRenderer.LoadMap("World2", "MilitaryBase");

                    isMoving = false;
                }

                if (playerPosition.X <= -175 || playerPosition.X >= 190 * 3)
                {
                    if (playerPosition.X <= -175)
                    {
                        playerPosition.X = -175;
                    }
                    else
                    {
                        playerPosition.X = 190 * 3;
                    }

                    atEndOfMap = true;
                }
                else
                {
                    atEndOfMap = false;
                }

                foreach (Vector2 obstacle in obstacles)
                {
                    Rectangle obstacleRectangle = new Rectangle((int)obstacle.X + 41, (int)obstacle.Y, 218, 117);

                    Vector2 skiPosition = player.getPlayerPosition();

                    Rectangle skiRectangle = new Rectangle((int)skiPosition.X, (int)skiPosition.Y, skiWithPlayerTexture.Width,
                        skiWithPlayerTexture.Height - 130);

                    if (obstacleRectangle.Intersects(skiRectangle))
                    {
                        Global.Sounds.SoundManager.PlaySoundEffect(crashSoundEffect);
                        worldRenderer.LoadMap("World2", "Skiing");

                        isMoving = false;
                    }
                }

                if ((ks.IsKeyDown(Keys.A) || ks.IsKeyDown(Keys.D)) && !atEndOfMap)
                {
                    if (!isPlayingSound)
                    {
                        isPlayingSound = true;

                        if (!SoundManager.CURRENTLY_LOOPING_SOUND_EFFECTS.ContainsKey(turningSoundEffect))
                        {
                            SoundManager.PlayRepeatingSoundEffect(turningSoundEffect);
                        }
                    }
                }
                else
                {
                    if (SoundManager.CURRENTLY_LOOPING_SOUND_EFFECTS.ContainsKey(turningSoundEffect))
                    {
                        isPlayingSound = false;
                        SoundManager.StopRepeatingSoundEffect(turningSoundEffect);
                    }
                }

                player.setPlayerPosition(playerPosition);
            }
            else
            {
                SoundManager.ClearRepeatingSoundEffects();

                if (ks.IsKeyDown(Keys.Enter))
                {
                    iskeyDown = true;
                }
                else
                {
                    // Check if enter key has been pressed and then released
                    // (in order to only call functions once)
                    if (iskeyDown)
                    {
                        Rectangle emptySkiRectangle =
                            new Rectangle(
                                (int)emptySkiPosition.X + (Global.GameConstants.TILE_SIZE / 2) - 45,
                                (int)emptySkiPosition.Y + (Global.GameConstants.TILE_SIZE / 2) - 45,
                                emptySkiTexture.Width + 30,
                                emptySkiTexture.Height + 30);

                        if (emptySkiRectangle.Intersects(player.getPlayerRectangle()))
                        {
                            isMoving = true;

                            Song backgroundSong = Game.Content.Load<Song>("Sounds/World2/Skiing/snowStorm");
                            SoundManager.PlayBackgroundMusic(backgroundSong);

                            worldRenderer.SetBackground(new Backgrounds.MovingClouds(this.Game, 33, 15, new Vector2(0, -1)), Color.CornflowerBlue);
                            player.SetWalkingSpeed(15);
                            Vector2 playerPosition = player.getPlayerPosition();
                            player.setPlayerPosition(new Vector2(playerPosition.X, playerPosition.Y - 185));

                            SoundManager.PlayRepeatingSoundEffect(downhillSoundEffect, 0.8f);
                        }
                    }

                    iskeyDown = false;
                }
            }

            base.Update(gameTime);
        }

        public void FillList()
        {
            int[][] map = new int[][]
            {

             #region Int Arrays for the map
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 1, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 1, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 1 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 1, 0, 1 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 1 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 1, 1, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 1, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 1 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 1, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 0, 0, 0 },
            new int[] { 1, 0, 1 },
            new int[] { 1, 0, 1 },
            new int[] { 1, 0, 1 },
            new int[] { 1, 0, 1 },
            new int[] { 1, 0, 1 }
            #endregion

            };

            int height = 199;
            int onePos = -200;
            int twoPos = -200 + 270;
            int threePos = -200 + 270 * 2;

            int y = 0;
            for (int i = 0; i < map.Length; i++)
            {
                if (map[i][0] == 1)
                {
                    obstacles.Add(new Vector2(onePos, y));
                }

                if (map[i][1] == 1)
                {
                    obstacles.Add(new Vector2(twoPos, y));
                }

                if (map[i][2] == 1)
                {
                    obstacles.Add(new Vector2(threePos, y));
                }

                y -= height;
            }

            finishLine = y - 400;
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                SamplerState.LinearWrap,
                null,
                null,
                null,
                camera.TransformMatrix);

            int windowWidth = GraphicsDevice.Viewport.Width;
            int windowHeight = GraphicsDevice.Viewport.Height;

            if (isMoving)
            {
                spriteBatch.Draw(roadTexture, new Rectangle(-200, -Convert.ToInt32(camera.position.Y), 800 + 40, windowHeight), Color.AliceBlue);

                foreach (Vector2 obstacle in obstacles)
                {
                    spriteBatch.Draw(obstacleTexture, new Rectangle((int)obstacle.X, (int)obstacle.Y, 300, 199), Color.White);
                }

                spriteBatch.Draw(skiWithPlayerTexture, player.getPlayerPosition(), Color.White);
            }
            else
            {
                spriteBatch.Draw(gameTipTexture, new Vector2(worldRenderer.map_width, 0), Color.White);
                spriteBatch.Draw(emptySkiTexture, emptySkiPosition, Color.White);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
