﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global;
using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.World.Maps;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.World.Maps.World2.ShrineSanctuaryHall2
{
    class ShrineSanctuaryHall2 : BasicMap
    {
        public ShrineSanctuaryHall2(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();

            worldRenderer.SetBackground(new Backgrounds.BasicBackground(Game), new Color(17, 17, 17));
        }

        public override void RemoveAllGameComponents()
        {
            SoundManager.ClearRepeatingSoundEffects();

            foreach (IGameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).RemoveAllGameComponents();
                Game.Components.Remove(gameComponent);
            }

            Game.Components.Remove(player);

            PlayerUI.ClearUI();
            Game.Components.Remove(PlayerUI);

            if (!isPlayerDead)
            {
                Global.Player.LAST_MAP = this.GetType();
            }
        }

        public override void OnPlayerDeath()
        {
            base.OnPlayerDeath();

            worldRenderer.LoadMap("World2", "ShrineSanctuaryHall2");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            LevelDoor LevelDoor1417 = new LevelDoor(Game, worldRenderer, this, new Rectangle(1354, 236, 100, 32), "World2", "ShrineSanctuaryRoom1");
            AddGameComponent(LevelDoor1417);
            LevelDoor LevelDoor9219 = new LevelDoor(Game, worldRenderer, this, new Rectangle(18, 87, 32, 100), "World2", "ShrineSanctuaryHall1");
            AddGameComponent(LevelDoor9219);

            Enemy Enemy6633 = new Enemy(Game, worldRenderer, this, new Rectangle(667, 120, 0, 0), player, 244, 150, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 2, 1);
            AddGameComponent(Enemy6633);
            Enemy Enemy2505 = new Enemy(Game, worldRenderer, this, new Rectangle(852, 120, 0, 0), player, 244, 150, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 2, 2);
            AddGameComponent(Enemy2505);
            Enemy Enemy7373 = new Enemy(Game, worldRenderer, this, new Rectangle(750, 194, 0, 0), player, 244, 150, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 2, 0);
            AddGameComponent(Enemy7373);
            Enemy Enemy1766 = new Enemy(Game, worldRenderer, this, new Rectangle(750, 50, 0, 0), player, 244, 150, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 2, 3);
            AddGameComponent(Enemy1766);

            if (Global.Player.LAST_MAP == typeof(World2.ShrineSanctuaryHall1.ShrineSanctuaryHall1))
            {
                player.setPlayerPosition(new Vector2(42, 111));
            }
            else
            if (Global.Player.LAST_MAP == typeof(World2.ShrineSanctuaryRoom1.ShrineSanctuaryRoom1))
            {
                player.setPlayerPosition(new Vector2(1367, 216));
            }
            else
            {
                player.setPlayerPosition(new Vector2(42, 111));
                Song backgroundSong = Game.Content.Load<Song>("Sounds/World2/Shrine/silentLight");
                SoundManager.PlayBackgroundMusic(backgroundSong);
            }

            Ammo Ammo2373 = new Ammo(Game, worldRenderer, this, new Rectangle(67, 56, 0, 0));
            AddGameComponent(Ammo2373);
            Ammo Ammo1501 = new Ammo(Game, worldRenderer, this, new Rectangle(66, 169, 0, 0));
            AddGameComponent(Ammo1501);
            Ammo Ammo5438 = new Ammo(Game, worldRenderer, this, new Rectangle(1488, 163, 0, 0));
            AddGameComponent(Ammo5438);
            Ammo Ammo3354 = new Ammo(Game, worldRenderer, this, new Rectangle(1401, 199, 0, 0));
            AddGameComponent(Ammo3354);
        }
    }
}