﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using SavingTheWorldFromAngriness.World.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavingTheWorldFromAngriness.World.Maps.World2
{
    class DarkAlley : BasicMap
    {
        public DarkAlley(Game game, WorldRenderer worldRenderer) : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();

            #region Background 
            worldRenderer.SetBackground(new Backgrounds.BasicBackground(this.Game), Color.Black);
            #endregion

            player.setPlayerPosition(new Vector2(35, 93));
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            #region Enemies
            Enemy Enemy2522 = new Enemy(Game, worldRenderer, this, new Rectangle(354, 255, 0, 0), player, 559, 70, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(Enemy2522);
            Enemy Enemy9286 = new Enemy(Game, worldRenderer, this, new Rectangle(315, 584, 0, 0), player, 559, 70, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 1);
            AddGameComponent(Enemy9286);
            Enemy Enemy2537 = new Enemy(Game, worldRenderer, this, new Rectangle(507, 385, 0, 0), player, 559, 70, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(Enemy2537);
            Enemy Enemy3196 = new Enemy(Game, worldRenderer, this, new Rectangle(225, 696, 0, 0), player, 559, 70, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(Enemy3196);

            #endregion

            #region Ammo
            Ammo Ammo5117 = new Ammo(Game, worldRenderer, this, new Rectangle(133, 43, 0, 0));
            AddGameComponent(Ammo5117);
            Ammo Ammo2783 = new Ammo(Game, worldRenderer, this, new Rectangle(268, 59, 0, 0));
            AddGameComponent(Ammo2783);
            #endregion

            #region HP
            HealthPack HealthPack8340 = new HealthPack(Game, worldRenderer, this, new Rectangle(46, 603, 0, 0));
            AddGameComponent(HealthPack8340);
            HealthPack HealthPack1665 = new HealthPack(Game, worldRenderer, this, new Rectangle(655, 36, 0, 0));
            AddGameComponent(HealthPack1665);
            #endregion

            #region Leveldoor
            LevelDoor LevelDoor1905 = new LevelDoor(Game, worldRenderer, this, 
                new Rectangle(655, 697, 50, 50), "World2", "PreBossBridge");
            AddGameComponent(LevelDoor1905);
            #endregion

            #region Teleport
            Teleport Teleport5008 = new Teleport(Game, worldRenderer, this, new Rectangle(486, 683, 0, 0), 682, 704);
            AddGameComponent(Teleport5008);
            #endregion

            #region MiscCollision
            //Prevents player from walking off map
            Rectangle OffMapBlock = new Rectangle(656, 780, 2 * 32, 2);
            worldRenderer.addMiscCollision(OffMapBlock);
            #endregion

            #region Song
            Song backgroundMusic = Game.Content.Load<Song>("Sounds/World2/Map10");

            PlayBackgroundMusic(backgroundMusic);

            #endregion
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void OnPlayerDeath()
        {
            worldRenderer.LoadMap("World2", "DarkAlley");
            base.OnPlayerDeath();
        }
    }
}