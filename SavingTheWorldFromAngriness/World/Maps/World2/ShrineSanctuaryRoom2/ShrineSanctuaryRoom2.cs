﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global;
using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.World.Maps;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
namespace SavingTheWorldFromAngriness.World.Maps.World2.ShrineSanctuaryRoom2
{
    class ShrineSanctuaryRoom2 : BasicMap
    {
        public ShrineSanctuaryRoom2(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();

            worldRenderer.SetBackground(new Backgrounds.BasicBackground(Game), new Color(17, 17, 17));
        }

        public override void RemoveAllGameComponents()
        {
            SoundManager.ClearRepeatingSoundEffects();

            foreach (IGameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).RemoveAllGameComponents();
                Game.Components.Remove(gameComponent);
            }

            Game.Components.Remove(player);

            PlayerUI.ClearUI();
            Game.Components.Remove(PlayerUI);

            if (!isPlayerDead)
            {
                Global.Player.LAST_MAP = this.GetType();
            }
        }

        public override void OnPlayerDeath()
        {
            base.OnPlayerDeath();

            worldRenderer.LoadMap("World2", "ShrineSanctuaryRoom2");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            LevelDoor LevelDoor2852 = new LevelDoor(Game, worldRenderer, this, new Rectangle(19, 230, 32, 100), "World2", "ShrineSanctuaryRoom3");
            AddGameComponent(LevelDoor2852);
            LevelDoor LevelDoor5659 = new LevelDoor(Game, worldRenderer, this, new Rectangle(718, 229, 32, 100), "World2", "ShrineSanctuaryRoom1");
            AddGameComponent(LevelDoor5659);

            if (Global.Player.LAST_MAP == typeof(World2.ShrineSanctuaryRoom1.ShrineSanctuaryRoom1))
            {
                player.setPlayerPosition(new Vector2(680, 253));
            }
            else
            if (Global.Player.LAST_MAP == typeof(World2.ShrineSanctuaryRoom3.ShrineSanctuaryRoom3))
            {
                player.setPlayerPosition(new Vector2(57, 259));
            }
            else
            {
                player.setPlayerPosition(new Vector2(680, 253));
                Song backgroundSong = Game.Content.Load<Song>("Sounds/World2/Shrine/silentLight");
                SoundManager.PlayBackgroundMusic(backgroundSong);
            }

            Enemy Enemy1165 = new Enemy(Game, worldRenderer, this, new Rectangle(243, 84, 0, 0), player, 269, 160, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 1);
            AddGameComponent(Enemy1165);
            Enemy Enemy2622 = new Enemy(Game, worldRenderer, this, new Rectangle(245, 151, 0, 0), player, 269, 160, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 1);
            AddGameComponent(Enemy2622);
            Enemy Enemy9081 = new Enemy(Game, worldRenderer, this, new Rectangle(245, 226, 0, 0), player, 269, 160, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 1);
            AddGameComponent(Enemy9081);
            Enemy Enemy1116 = new Enemy(Game, worldRenderer, this, new Rectangle(238, 295, 0, 0), player, 269, 160, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 1);
            AddGameComponent(Enemy1116);
            Enemy Enemy8348 = new Enemy(Game, worldRenderer, this, new Rectangle(238, 377, 0, 0), player, 269, 160, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 1);
            AddGameComponent(Enemy8348);
            Enemy Enemy6249 = new Enemy(Game, worldRenderer, this, new Rectangle(238, 454, 0, 0), player, 269, 160, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 1);
            AddGameComponent(Enemy6249);
            Enemy Enemy8877 = new Enemy(Game, worldRenderer, this, new Rectangle(378, 88, 0, 0), player, 269, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 2);
            AddGameComponent(Enemy8877);
            Enemy Enemy1543 = new Enemy(Game, worldRenderer, this, new Rectangle(370, 157, 0, 0), player, 269, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 2);
            AddGameComponent(Enemy1543);
            Enemy Enemy7572 = new Enemy(Game, worldRenderer, this, new Rectangle(362, 229, 0, 0), player, 269, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 2);
            AddGameComponent(Enemy7572);
            Enemy Enemy1300 = new Enemy(Game, worldRenderer, this, new Rectangle(360, 303, 0, 0), player, 269, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 2);
            AddGameComponent(Enemy1300);
            Enemy Enemy8854 = new Enemy(Game, worldRenderer, this, new Rectangle(364, 381, 0, 0), player, 269, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 2);
            AddGameComponent(Enemy8854);
            Enemy Enemy6767 = new Enemy(Game, worldRenderer, this, new Rectangle(363, 461, 0, 0), player, 269, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 2);
            AddGameComponent(Enemy6767);


            Ammo Ammo5656 = new Ammo(Game, worldRenderer, this, new Rectangle(645, 39, 0, 0));
            AddGameComponent(Ammo5656);
            Ammo Ammo6335 = new Ammo(Game, worldRenderer, this, new Rectangle(650, 116, 0, 0));
            AddGameComponent(Ammo6335);
            Ammo Ammo1611 = new Ammo(Game, worldRenderer, this, new Rectangle(654, 201, 0, 0));
            AddGameComponent(Ammo1611);
            Ammo Ammo2211 = new Ammo(Game, worldRenderer, this, new Rectangle(647, 280, 0, 0));
            AddGameComponent(Ammo2211);
            Ammo Ammo6333 = new Ammo(Game, worldRenderer, this, new Rectangle(655, 372, 0, 0));
            AddGameComponent(Ammo6333);
            Ammo Ammo1679 = new Ammo(Game, worldRenderer, this, new Rectangle(655, 463, 0, 0));
            AddGameComponent(Ammo1679);
            Ammo Ammo9334 = new Ammo(Game, worldRenderer, this, new Rectangle(654, 537, 0, 0));
            AddGameComponent(Ammo9334);
            Ammo Ammo8779 = new Ammo(Game, worldRenderer, this, new Rectangle(31, 36, 0, 0));
            AddGameComponent(Ammo8779);
            Ammo Ammo3960 = new Ammo(Game, worldRenderer, this, new Rectangle(29, 124, 0, 0));
            AddGameComponent(Ammo3960);
            Ammo Ammo1002 = new Ammo(Game, worldRenderer, this, new Rectangle(37, 231, 0, 0));
            AddGameComponent(Ammo1002);
            Ammo Ammo3475 = new Ammo(Game, worldRenderer, this, new Rectangle(47, 324, 0, 0));
            AddGameComponent(Ammo3475);
            Ammo Ammo3429 = new Ammo(Game, worldRenderer, this, new Rectangle(56, 409, 0, 0));
            AddGameComponent(Ammo3429);
            Ammo Ammo6661 = new Ammo(Game, worldRenderer, this, new Rectangle(52, 495, 0, 0));
            AddGameComponent(Ammo6661);
            Ammo Ammo8285 = new Ammo(Game, worldRenderer, this, new Rectangle(311, 539, 0, 0));
            AddGameComponent(Ammo8285);
            HealthPack HealthPack9684 = new HealthPack(Game, worldRenderer, this, new Rectangle(303, 26, 0, 0));
            AddGameComponent(HealthPack9684);
            HealthPack HealthPack2582 = new HealthPack(Game, worldRenderer, this, new Rectangle(378, 26, 0, 0));
            AddGameComponent(HealthPack2582);
            HealthPack HealthPack6174 = new HealthPack(Game, worldRenderer, this, new Rectangle(274, 549, 0, 0));
            AddGameComponent(HealthPack6174);
            HealthPack HealthPack3574 = new HealthPack(Game, worldRenderer, this, new Rectangle(400, 549, 0, 0));
            AddGameComponent(HealthPack3574);
            HealthPack HealthPack4969 = new HealthPack(Game, worldRenderer, this, new Rectangle(581, 283, 0, 0));
            AddGameComponent(HealthPack4969);
            HealthPack HealthPack6343 = new HealthPack(Game, worldRenderer, this, new Rectangle(528, 219, 0, 0));
            AddGameComponent(HealthPack6343);
            HealthPack HealthPack7374 = new HealthPack(Game, worldRenderer, this, new Rectangle(463, 345, 0, 0));
            AddGameComponent(HealthPack7374);
            HealthPack HealthPack4679 = new HealthPack(Game, worldRenderer, this, new Rectangle(128, 283, 0, 0));
            AddGameComponent(HealthPack4679);
        }
    }
}