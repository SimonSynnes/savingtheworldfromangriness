﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using SavingTheWorldFromAngriness.Backgrounds;

namespace SavingTheWorldFromAngriness.World.Maps.World2.Beginning
{
    class levelRectangle
    {
        public string text;
        public Vector2 position;

        public levelRectangle(string text, Vector2 position)
        {
            this.text = text;
            this.position = position;
        }
    }

    class BeginningWorld2 : BasicMap
    {
        private SpriteFont levelFont;
        private List<levelRectangle> levels;
        private Texture2D levelRectangleTexture;

        public BeginningWorld2(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();

            #region Background
            worldRenderer.SetBackground(new Backgrounds.DirtBackground(Game), Color.CornflowerBlue);
            #endregion

            player.setPlayerPosition(new Vector2(75, 150));
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            player.SetWalkingSpeed(9);
            levelFont = Game.Content.Load<SpriteFont>("Fonts/CoinFont");
            levelRectangleTexture = Game.Content.Load<Texture2D>("Temp/world1levelrectangle");

            Texture2D headerWorld1BackgroundTexture = Game.Content.Load<Texture2D>("Temp/world2header");
            Texture2D footerWorld1BackgroundTexture = Game.Content.Load<Texture2D>("Temp/world1footer");

            BasicComponent world1Background = new BasicComponent(Game, worldRenderer, this,
                new Rectangle(177, 0, headerWorld1BackgroundTexture.Width, headerWorld1BackgroundTexture.Height),
                headerWorld1BackgroundTexture);
            AddGameComponent(world1Background);

            BasicComponent footer1Background = new BasicComponent(Game, worldRenderer, this,
                new Rectangle(40, 830, footerWorld1BackgroundTexture.Width, footerWorld1BackgroundTexture.Height),
                footerWorld1BackgroundTexture);
            AddGameComponent(footer1Background);

            levels = new List<levelRectangle>();
            
            #region rad 1

            levels.Add(new levelRectangle("preBoss", new Vector2(21, 270)));
            LevelDoor homeDoor = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21, 270, levelRectangleTexture.Width + 10,
                levelRectangleTexture.Height), "World2", "PreBossBridge", levelRectangleTexture);
            AddGameComponent(homeDoor);

            levels.Add(new levelRectangle("Skiing", new Vector2(25 + (levelRectangleTexture.Width * 1), 270)));
            LevelDoor homeDoor2 = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21 + (levelRectangleTexture.Width * 1), 270, levelRectangleTexture.Width + 10,
                levelRectangleTexture.Height), "World2", "Skiing", levelRectangleTexture);
            AddGameComponent(homeDoor2);

            levels.Add(new levelRectangle("Base-\nment", new Vector2(21 + (levelRectangleTexture.Width * 2), 270)));
            LevelDoor homeDoor3 = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21 + (levelRectangleTexture.Width * 2), 270, levelRectangleTexture.Width + 40,
                levelRectangleTexture.Height), "World2", "CastleBasement", levelRectangleTexture);
            AddGameComponent(homeDoor3);

            levels.Add(new levelRectangle("Disco", new Vector2(21 + (levelRectangleTexture.Width * 3), 270)));
            LevelDoor disco = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21 + (levelRectangleTexture.Width * 3), 270, levelRectangleTexture.Width,
                levelRectangleTexture.Height), "World2", "Disco", levelRectangleTexture);
            AddGameComponent(disco);

            #endregion

            #region rad 2

            levels.Add(new levelRectangle("Garden\n1", new Vector2(21, 270 + levelRectangleTexture.Height + 5)));
            LevelDoor Garden = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21, 270 + levelRectangleTexture.Height + 5, levelRectangleTexture.Width,
                levelRectangleTexture.Height), "World2", "OutsideGarden", levelRectangleTexture);
            AddGameComponent(Garden);

            levels.Add(new levelRectangle("Garden\n2", new Vector2(21 + (levelRectangleTexture.Width * 1), 270 + levelRectangleTexture.Height + 5)));
            LevelDoor Garden2 = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21 + (levelRectangleTexture.Width * 1), 270 + levelRectangleTexture.Height + 5, levelRectangleTexture.Width,
                levelRectangleTexture.Height), "World2", "OutsideGarden2", levelRectangleTexture);
            AddGameComponent(Garden2);

            levels.Add(new levelRectangle("Garden\n3", new Vector2(21 + (levelRectangleTexture.Width * 2), 270 + levelRectangleTexture.Height + 5)));
            LevelDoor Garden3 = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21 + (levelRectangleTexture.Width * 2), 270 + levelRectangleTexture.Height + 5, levelRectangleTexture.Width,
                levelRectangleTexture.Height), "World2", "OutsideGarden3", levelRectangleTexture);
            AddGameComponent(Garden3);

            levels.Add(new levelRectangle("Snow", new Vector2(21 + (levelRectangleTexture.Width * 3), 270 + levelRectangleTexture.Height + 5)));
            LevelDoor snow = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21 + (levelRectangleTexture.Width * 3), 270 + levelRectangleTexture.Height + 5, levelRectangleTexture.Width,
                levelRectangleTexture.Height), "World2", "OutsideSnow", levelRectangleTexture);
            AddGameComponent(snow);

            #endregion

            #region rad 3

            levels.Add(new levelRectangle("Shrine", new Vector2(21, 270 + levelRectangleTexture.Height * 2 + 5)));
            LevelDoor shrine = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21, 270 + levelRectangleTexture.Height * 2 + 10, levelRectangleTexture.Width,
                levelRectangleTexture.Height), "World2", "ShrineSanctuaryStart", levelRectangleTexture);
            AddGameComponent(shrine);

            levels.Add(new levelRectangle("Castle-\nMoat", new Vector2(21 + (levelRectangleTexture.Width * 1), 270 + levelRectangleTexture.Height * 2 + 5)));
            LevelDoor afterShrine = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21 + (levelRectangleTexture.Width * 1), 270 + levelRectangleTexture.Height * 2 + 10, levelRectangleTexture.Width,
                levelRectangleTexture.Height), "World2", "CastleMoat", levelRectangleTexture);
            AddGameComponent(afterShrine);

            levels.Add(new levelRectangle("MC1", new Vector2(21 + (levelRectangleTexture.Width * 2), 270 + levelRectangleTexture.Height * 2 + 5)));
            LevelDoor blank = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21 + (levelRectangleTexture.Width * 2), 270 + levelRectangleTexture.Height * 2 + 10, levelRectangleTexture.Width,
                levelRectangleTexture.Height), "World2", "Mc1", levelRectangleTexture);
            AddGameComponent(blank);

            levels.Add(new levelRectangle("Military", new Vector2(21 + (levelRectangleTexture.Width * 3), 270 + levelRectangleTexture.Height * 2 + 5)));
            LevelDoor military = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21 + (levelRectangleTexture.Width * 3), 270 + levelRectangleTexture.Height * 2 + 10, levelRectangleTexture.Width,
                levelRectangleTexture.Height), "World2", "MilitaryBase", levelRectangleTexture);
            AddGameComponent(military);

            #endregion

            #region rad 4
            levels.Add(new levelRectangle("Black\n-map", new Vector2(21, 270 + levelRectangleTexture.Height * 3 + 5)));
            LevelDoor blackMap = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21, 270 + levelRectangleTexture.Height * 3 + 10, levelRectangleTexture.Width,
                levelRectangleTexture.Height), "World2", "BlackMap1", levelRectangleTexture);
            AddGameComponent(blackMap);

            levels.Add(new levelRectangle("Kam2", new Vector2(21 + (levelRectangleTexture.Width * 1), 270 + levelRectangleTexture.Height * 3 + 5)));
            LevelDoor kam2 = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21 + (levelRectangleTexture.Width * 1), 270 + levelRectangleTexture.Height * 3 + 10, levelRectangleTexture.Width,
                levelRectangleTexture.Height), "World2", "Kam2", levelRectangleTexture);
            AddGameComponent(kam2);
            #endregion

        }


        public override void DrawBehindShadows()
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            foreach (GameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).Draw(spriteBatch);
            }

            foreach (levelRectangle level in levels)
            {
                Vector2 levelFontSize = (levelFont.MeasureString(level.text));
                spriteBatch.DrawString(levelFont, level.text,
                    new Vector2(level.position.X + (levelRectangleTexture.Width / 2) - (levelFontSize.X / 2),
                    level.position.Y + (levelRectangleTexture.Height / 2) - (levelFontSize.Y / 2)), Color.Black);
            }

            spriteBatch.End();

            player.Draw(spriteBatch, camera);
        }
    }
}
