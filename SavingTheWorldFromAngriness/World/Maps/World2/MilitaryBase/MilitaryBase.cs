﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using SavingTheWorldFromAngriness.World.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.GUI;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace SavingTheWorldFromAngriness.World.Maps.World2
{
    class MilitaryBase : BasicMap
    {
        private bool stopMapLogic;
        private Texture2D playerAvatarTexture;
        private SoundEffect promptSoundEffect1;

        public MilitaryBase(Game game, WorldRenderer worldRenderer) : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();

            #region Background 
            worldRenderer.SetBackground(new Backgrounds.DarkRockBackground(this.Game), Color.Black);
            #endregion

            player.setPlayerPosition(new Vector2(371, 720));

            SetCameraToPlayerPosition();

            stopMapLogic = true;
            DialogPrompt prompt = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME, playerAvatarTexture, 
                "I might just be closing in on the king now. How the tables have turned.", promptSoundEffect1);
            Game.Components.Add(prompt);
            prompt.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
            {
                stopMapLogic = false;
                Game.Components.Remove(prompt);
            };
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            #region Ammo
            Ammo Ammo4372 = new Ammo(Game, worldRenderer, this, new Rectangle(260, 710, 0, 0));
            AddGameComponent(Ammo4372);
            Ammo Ammo1173 = new Ammo(Game, worldRenderer, this, new Rectangle(450, 710, 0, 0));
            AddGameComponent(Ammo1173);
            #endregion

            #region Enemies
            Enemy Enemy7980 = new Enemy(Game, worldRenderer, this, new Rectangle(363, 258, 0, 0), player, 378, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(Enemy7980);
            Enemy Enemy6695 = new Enemy(Game, worldRenderer, this, new Rectangle(622, 279, 0, 0), player, 378, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(Enemy6695);
            Enemy Enemy2678 = new Enemy(Game, worldRenderer, this, new Rectangle(106, 276, 0, 0), player, 378, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(Enemy2678);
            Enemy Enemy7181 = new Enemy(Game, worldRenderer, this, new Rectangle(154, 131, 0, 0), player, 378, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(Enemy7181);
            Enemy Enemy3343 = new Enemy(Game, worldRenderer, this, new Rectangle(606, 135, 0, 0), player, 378, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(Enemy3343);
            #endregion

            #region HP
            HealthPack HealthPack2509 = new HealthPack(Game, worldRenderer, this, new Rectangle(22, 114, 0, 0));
            AddGameComponent(HealthPack2509);
            #endregion

            #region LevelDoor
            Texture2D portal = Game.Content.Load<Texture2D>("Textures/World1/CastleOutside/portal");
            LevelDoor LevelDoor9568 = new LevelDoor(Game, worldRenderer, this, 
                new Rectangle(300, 0, 127, 127), "World2", "Kam2", portal);
            AddGameComponent(LevelDoor9568);
            #endregion

            #region Teleport
            Teleport Teleport1440 = new Teleport(Game, worldRenderer, this, new Rectangle(495, 60, 0, 0), 382, 585);
            AddGameComponent(Teleport1440);
            #endregion

            #region TorchLight

            TorchLight TorchLight6897 = new TorchLight(Game, worldRenderer, this, new Rectangle(688, 39, 0, 0), 3333, 3333);
            AddGameComponent(TorchLight6897);
            TorchLight TorchLight4975 = new TorchLight(Game, worldRenderer, this, new Rectangle(34, 33, 0, 0), 3333, 3333);
            AddGameComponent(TorchLight4975);
            TorchLight TorchLight7611 = new TorchLight(Game, worldRenderer, this, new Rectangle(884, 244, 0, 0), 3333, 3333);
            AddGameComponent(TorchLight7611);
            TorchLight TorchLight9715 = new TorchLight(Game, worldRenderer, this, new Rectangle(-204, 86, 0, 0), 3333, 3333);
            AddGameComponent(TorchLight9715);
            TorchLight TorchLight1664 = new TorchLight(Game, worldRenderer, this, new Rectangle(709, 568, 0, 0), 7, 66);
            AddGameComponent(TorchLight1664);
            TorchLight TorchLight9825 = new TorchLight(Game, worldRenderer, this, new Rectangle(3, 571, 0, 0), 7, 66);
            AddGameComponent(TorchLight9825);
            #endregion

            #region Song
            Song backgroundMusic = Game.Content.Load<Song>("Sounds/World2/battle");

            PlayBackgroundMusic(backgroundMusic);

            #endregion

            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");
            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");

            worldRenderer.addMiscCollision(new Rectangle(350, 790, 3 * 36, 6));
        }

        public override void Update(GameTime gameTime)
        {
            if (stopMapLogic)
            {
                SetCameraToPlayerPosition();

                return;
            }

            base.Update(gameTime);
        }

        public override void OnPlayerDeath()
        {
            worldRenderer.LoadMap("World2", "MilitaryBase");
            base.OnPlayerDeath();
        }
    }
}