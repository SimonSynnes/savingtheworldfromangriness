﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global;
using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.World.Maps;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SavingTheWorldFromAngriness.GUI;

namespace SavingTheWorldFromAngriness.World.Maps.World2.ShrineSanctuaryOutside1
{
    class ShrineSanctuaryOutside1 : BasicMap
    {
        private Texture2D keyTexture;
        private Vector2 keyPosition;
        private SoundEffect keySoundEffect;

        #region UI
        private Texture2D playerAvatarTexture;
        private SoundEffect promptSoundEffect1;
        #endregion

        public ShrineSanctuaryOutside1(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();

            if (Global.Player.LAST_MAP == typeof(World2.ShrineSanctuaryHall1.ShrineSanctuaryHall1))
            {
                player.setPlayerPosition(new Vector2(685, 130));
            }

            worldRenderer.SetBackground(new Backgrounds.BasicBackground(Game), new Color(17, 17, 17));

            if (Global.Player.INVENTORY.Contains("World2:Shrine:Key1"))
            {
                keyPosition = new Vector2(9999, 56499);
            }
            else
            {
                keyPosition = new Vector2(126, 564);
            }
        }

        public override void RemoveAllGameComponents()
        {
            SoundManager.ClearRepeatingSoundEffects();

            foreach (IGameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).RemoveAllGameComponents();
                Game.Components.Remove(gameComponent);
            }

            Game.Components.Remove(player);

            PlayerUI.ClearUI();
            Game.Components.Remove(PlayerUI);

            if (!isPlayerDead)
            {
                Global.Player.LAST_MAP = this.GetType();
            }
        }

        public override void OnPlayerDeath()
        {
            base.OnPlayerDeath();

            worldRenderer.LoadMap("World2", "ShrineSanctuaryOutside1");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (player.getPlayerRectangle().Intersects(
                new Rectangle((int)keyPosition.X + (Global.GameConstants.TILE_SIZE / 2),
                (int)keyPosition.Y + (Global.GameConstants.TILE_SIZE / 2), keyTexture.Width, keyTexture.Height)))
            {
                Global.Player.INVENTORY.Add("World2:Shrine:Key1");
                keyPosition = new Vector2(9999, 56499);
                Global.Sounds.SoundManager.PlaySoundEffect(keySoundEffect);
            }
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            LevelDoor LevelDoor8781 = new LevelDoor(Game, worldRenderer, this, new Rectangle(711, 103, 32, 100), "World2", "ShrineSanctuaryHall1");
            AddGameComponent(LevelDoor8781);

            keyTexture = Game.Content.Load<Texture2D>("Textures/World2/ShrineSanctuary/key");
            keySoundEffect = Game.Content.Load<SoundEffect>("Sounds/World1/pop");
            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");
            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");

            Enemy Enemy4456 = new Enemy(Game, worldRenderer, this, new Rectangle(259, 598, 0, 0), player, 232, 300, Global.GameConstants.ENEMY_ATTACKSPEED, 35, 3, 0);
            AddGameComponent(Enemy4456);
            Enemy Enemy2891 = new Enemy(Game, worldRenderer, this, new Rectangle(681, 551, 0, 0), player, 232, 300, Global.GameConstants.ENEMY_ATTACKSPEED, 35, 3, 1);
            AddGameComponent(Enemy2891);
            Enemy Enemy7693 = new Enemy(Game, worldRenderer, this, new Rectangle(679, 691, 0, 0), player, 232, 300, Global.GameConstants.ENEMY_ATTACKSPEED, 35, 3, 1);
            AddGameComponent(Enemy7693);
            Enemy Enemy5838 = new Enemy(Game, worldRenderer, this, new Rectangle(414, 409, 0, 0), player, 232, 300, Global.GameConstants.ENEMY_ATTACKSPEED, 35, 3, 3);
            AddGameComponent(Enemy5838);

            if (Global.Player.LAST_MAP == typeof(World2.ShrineSanctuaryHall1.ShrineSanctuaryHall1))
            {
                player.setPlayerPosition(new Vector2(685, 130));
            }
            else
            {
                player.setPlayerPosition(new Vector2(685, 130));
                Song backgroundSong = Game.Content.Load<Song>("Sounds/World2/Shrine/silentLight");
                SoundManager.PlayBackgroundMusic(backgroundSong);
            }

            Ammo Ammo8979 = new Ammo(Game, worldRenderer, this, new Rectangle(621, 249, 0, 0));
            AddGameComponent(Ammo8979);
            Ammo Ammo8974 = new Ammo(Game, worldRenderer, this, new Rectangle(621, 230, 0, 0));
            AddGameComponent(Ammo8974);


            HealthPack HealthPack4000 = new HealthPack(Game, worldRenderer, this, new Rectangle(34, 541, 0, 0));
            AddGameComponent(HealthPack4000);
            HealthPack HealthPack7723 = new HealthPack(Game, worldRenderer, this, new Rectangle(95, 564, 0, 0));
            AddGameComponent(HealthPack7723);
            HealthPack HealthPack4903 = new HealthPack(Game, worldRenderer, this, new Rectangle(3179, 407, 0, 0));
            AddGameComponent(HealthPack4903);
        }

        public override void DrawBehindShadows()
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            spriteBatch.Draw(keyTexture, keyPosition, Color.White);

            spriteBatch.End();

            base.DrawBehindShadows();
        }
    }
}