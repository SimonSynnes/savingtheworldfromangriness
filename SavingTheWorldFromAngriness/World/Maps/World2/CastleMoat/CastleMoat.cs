﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using SavingTheWorldFromAngriness.Backgrounds;
using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using System.Collections.Generic;

namespace SavingTheWorldFromAngriness.World.Maps.World2
{
    public class Platform
    {
        public Texture2D texture;
        public Rectangle rect;
        public Vector2 direction;
        public Vector2 leftEnd;
        public Vector2 rightEnd;

        public Platform(Texture2D texture, Rectangle rect, Vector2 direction, Vector2 leftEnd, Vector2 rightEnd)
        {
            this.texture = texture;
            this.rect = rect;
            this.direction = direction;
            this.leftEnd = leftEnd;
            this.rightEnd = rightEnd;
        }

        public Rectangle getPlatformRectangle()
        {
            return this.rect;
        }
    }

    class CastleMoat : BasicMap
    {
        #region Platforms
        private List<Platform> platforms = new List<Platform>();
        private List<Platform> waitingPlatforms = new List<Platform>();

        private int platformLength;
        private int platformWidth;
        private int waitingPlatformWidth;
        private int waitingPlatformLength;
        private Texture2D platformTexture;
        private Texture2D waitingPlatformTexture;
        #endregion

        #region Rectangles
        Rectangle waterRect;
        Rectangle landRect;
        #endregion

        #region Sounds
        private SoundEffect playerWaterSplash;
        #endregion

        #region Time elapsed
        float timeElapsed;
        #endregion

        public CastleMoat(Game game, WorldRenderer worldRenderer) : base(game, worldRenderer)
        {
            #region Platforms
            this.platformWidth = 32;
            this.platformLength = 64;
            this.waitingPlatformWidth = 32;
            this.waitingPlatformLength = 64;
            #endregion

            #region Rectangles
            //Water rectangle
            this.waterRect = new Rectangle(95, -11, 2065, 447);
            //Land rectangle
            this.landRect = new Rectangle(-11, -14, 67, 445);
            #endregion

            #region Time elapsed
            this.timeElapsed = 0f;
            #endregion
        }

        public override void Initialize()
        {
            base.Initialize();

            #region Player start position
            if (Global.Player.LAST_MAP == typeof(World2.OutsideGarden.OutsideGarden))
            {
                player.setPlayerPosition(new Vector2(2320, 232));
            }
            else
            {
                player.setPlayerPosition(new Vector2(32, 274));
            }
            //Uncomment to start with the player at the end of the map:
            //player.setPlayerPosition(new Vector2(2320, 232));
            #endregion

            #region Background 
            worldRenderer.SetBackground(new WaterBackground(this.Game, 1, 0), Color.Black);
            #endregion

        }

        protected override void LoadContent()
        {
            #region Sounds
            Song backgroundMusic = Game.Content.Load<Song>("Sounds/World2/Map3");
            PlayBackgroundMusic(backgroundMusic);

            playerWaterSplash = Game.Content.Load<SoundEffect>("Sounds/World2/playerWaterSplash");
            #endregion

            #region Exit
            LevelDoor levelDoorMap4 = new LevelDoor(this.Game, worldRenderer, 
                this, new Rectangle(2301, 226, 50, 50), "World2", "OutsideGarden");
            AddGameComponent(levelDoorMap4);

            //stops the player from going out of map
            Rectangle exitStop = new Rectangle(2386, 220, 1, 64);
            worldRenderer.addMiscCollision(exitStop);
            #endregion

            #region Platforms

            platformTexture = Game.Content.Load<Texture2D>("Textures/World2/boat");
            waitingPlatformTexture = Game.Content.Load<Texture2D>("Textures/World2/stillPlatform");

            //Moving platforms
            platforms.Add(new Platform(platformTexture, new Rectangle(218, 245, platformLength, platformWidth), new Vector2(1, 0), new Vector2(26 * 3, 0), new Vector2(32 * 15, 0)));
            platforms.Add(new Platform(platformTexture, new Rectangle(700, 245, platformLength, platformWidth), new Vector2(-1, 0), new Vector2(480, 0), new Vector2(1000, 0)));
            platforms.Add(new Platform(platformTexture, new Rectangle(1250, 245, platformLength, platformWidth), new Vector2(1, 0), new Vector2(1001, 0), new Vector2(1500, 0)));
            platforms.Add(new Platform(platformTexture, new Rectangle(1750, 245, platformLength, platformWidth), new Vector2(-1, 0), new Vector2(1501, 0), new Vector2(2000, 0)));
            platforms.Add(new Platform(platformTexture, new Rectangle(2070, 245, platformLength, platformWidth), new Vector2(1, 0), new Vector2(2001, 0), new Vector2(2122, 0)));
            //Waiting platforms
            waitingPlatforms.Add(new Platform(waitingPlatformTexture, new Rectangle(1001, 245, waitingPlatformLength, waitingPlatformWidth), new Vector2(0, 0), new Vector2(0, 0), new Vector2(0, 0)));
            waitingPlatforms.Add(new Platform(waitingPlatformTexture, new Rectangle(480, 245, waitingPlatformLength, waitingPlatformWidth), new Vector2(0, 0), new Vector2(0, 0), new Vector2(0, 0)));
            waitingPlatforms.Add(new Platform(waitingPlatformTexture, new Rectangle(1501, 245, waitingPlatformLength, waitingPlatformWidth), new Vector2(0, 0), new Vector2(0, 0), new Vector2(0, 0)));
            waitingPlatforms.Add(new Platform(waitingPlatformTexture, new Rectangle(2001, 245, waitingPlatformLength, waitingPlatformWidth), new Vector2(0, 0), new Vector2(0, 0), new Vector2(0, 0)));
            waitingPlatforms.Add(new Platform(waitingPlatformTexture, new Rectangle(480, 245, waitingPlatformLength, waitingPlatformWidth), new Vector2(0, 0), new Vector2(0, 0), new Vector2(0, 0)));
            #endregion

            #region Ammo
            Ammo Ammo1 = new Ammo(Game, worldRenderer, this, new Rectangle(2210, 80, 0, 0));
            AddGameComponent(Ammo1);
            Ammo Ammo2 = new Ammo(Game, worldRenderer, this, new Rectangle(2250, 80, 0, 0));
            AddGameComponent(Ammo2);
            #endregion

            base.LoadContent();

        }

        public override void Update(GameTime gameTime)
        {
            #region Time elapsed
            timeElapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;
            #endregion

            #region Platforms
            bool playerOnPlatform = false;
            Vector2 platformMovementSpeed = new Vector2(1, 1);

            foreach (Platform platform in platforms)
            {
                if (platform.leftEnd.X > platform.rect.X || platform.rightEnd.X < platform.rect.X)
                {
                    platform.direction *= -1;
                }

                platform.rect = new Rectangle(platform.rect.X + ((int)platformMovementSpeed.X * (int)platform.direction.X),
                    platform.rect.Y, platform.rect.Width, platform.rect.Height);

                //is the player on the platform?
                if ((platform.getPlatformRectangle().Intersects(player.getPlayerRectangle())) && !playerOnPlatform)
                {
                    playerOnPlatform = true;
                }
            }


            #region Waiting platform
            //is the player on the waiting platform?
            foreach (Platform waitingPlatform in waitingPlatforms)
            {
                if ((waitingPlatform.getPlatformRectangle().Intersects(player.getPlayerRectangle())) && !playerOnPlatform)
                {
                    playerOnPlatform = true;
                }
            }
            #endregion


            if (!playerOnPlatform)
            {
                if (waterRect.Intersects(player.getPlayerRectangle()))
                {
                    if (timeElapsed >= 0.5)
                    {
                        SoundManager.PlaySoundEffect(playerWaterSplash);
                        timeElapsed = 0;
                    }
                    player.setPlayerPosition(new Vector2(32, 274));
                }
            }
            #endregion

            base.Update(gameTime);
        }

        public override void RemoveAllGameComponents()
        {
            worldRenderer.SetBackground(new MovingClouds(this.Game, 40, 1), Color.CornflowerBlue);

            base.RemoveAllGameComponents();
        }

        public override void OnPlayerDeath()
         {
            worldRenderer.LoadMap("World2", "CastleMoat");
            base.OnPlayerDeath();
         }

        public override void DrawBehindShadows()
        {
            spriteBatch.Begin(SpriteSortMode.Deferred,
                BlendState.NonPremultiplied,
                SamplerState.AnisotropicClamp,
                DepthStencilState.Default,
                RasterizerState.CullCounterClockwise,
                null,
                camera.TransformMatrix);

            foreach (Platform platform in platforms)
            {
                spriteBatch.Draw(platformTexture, platform.getPlatformRectangle(), Color.White);
            }
            foreach (Platform waitingPlatform in waitingPlatforms)
            {
                spriteBatch.Draw(waitingPlatformTexture, waitingPlatform.getPlatformRectangle(), Color.White);
            }

            spriteBatch.End();
            base.DrawBehindShadows();
        }
    }
}
