﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global;
using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.World.Components.World2;
using SavingTheWorldFromAngriness.World.Maps;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
namespace SavingTheWorldFromAngriness.World.Maps.World2.ShrineSanctuaryHall1
{
    class ShrineSanctuaryHall1 : BasicMap
    {
        public ShrineSanctuaryHall1(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();

            worldRenderer.SetBackground(new Backgrounds.BasicBackground(Game), new Color(17, 17, 17));
        }

        public override void RemoveAllGameComponents()
        {
            SoundManager.ClearRepeatingSoundEffects();

            foreach (IGameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).RemoveAllGameComponents();
                Game.Components.Remove(gameComponent);
            }

            Game.Components.Remove(player);

            PlayerUI.ClearUI();
            Game.Components.Remove(PlayerUI);

            if (!isPlayerDead)
            {
                Global.Player.LAST_MAP = this.GetType();
            }
        }

        public override void OnPlayerDeath()
        {
            base.OnPlayerDeath();

            worldRenderer.LoadMap("World2", "ShrineSanctuaryHall1");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            Vendor vendor = new Vendor(this.Game, worldRenderer, this, new Rectangle(791, 30, 100, 100));
            AddGameComponent(vendor);

            LevelDoor LevelDoor4143 = new LevelDoor(Game, worldRenderer, this, new Rectangle(539, 878, 100, 32), "World2", "ShrineSanctuaryStart");
            AddGameComponent(LevelDoor4143);
            LevelDoor LevelDoor4377 = new LevelDoor(Game, worldRenderer, this, new Rectangle(19, 153, 32, 100), "World2", "ShrineSanctuaryOutside1");
            AddGameComponent(LevelDoor4377);

            if (Global.Player.LAST_MAP == typeof(World2.ShrineSanctuaryStart.ShrineSanctuaryStart))
            {
                player.setPlayerPosition(new Vector2(560, 848));
            }
            else
            if (Global.Player.LAST_MAP == typeof(World2.ShrineSanctuaryHall2.ShrineSanctuaryHall2))
            {
                player.setPlayerPosition(new Vector2(896, 193));
            }
            else
            if (Global.Player.LAST_MAP == typeof(World2.ShrineSanctuaryOutside1.ShrineSanctuaryOutside1))
            {
                player.setPlayerPosition(new Vector2(44, 173));
            }
            else
            {
                player.setPlayerPosition(new Vector2(560, 848));
                Song backgroundSong = Game.Content.Load<Song>("Sounds/World2/Shrine/silentLight");
                SoundManager.PlayBackgroundMusic(backgroundSong);
            }

            if (Global.Player.INVENTORY.Contains("World2:Shrine:Key1"))
            {
                LevelDoor LevelDoor5338 = new LevelDoor(Game, worldRenderer, this, new Rectangle(940, 164, 32, 100), "World2", "ShrineSanctuaryHall2");
                AddGameComponent(LevelDoor5338);
            }
            else
            {
                LockedLevelDoor LevelDoor5338 = new LockedLevelDoor(Game, worldRenderer, this, new Rectangle(940, 164, 32, 100));
                AddGameComponent(LevelDoor5338);
            }
        }
    }
}