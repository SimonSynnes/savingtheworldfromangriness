﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.GUI;

using Lighting;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace SavingTheWorldFromAngriness.World.Maps.World2.BlackMap3
{
    class BlackMap3 : BasicMap
    {
        private PointLight playerLight;

        public BlackMap3(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();

            Global.GameState.IS_RENDERER_MAZE = true;
        }

        public override void Update(GameTime gameTime)
        {
            playerLight.Position = player.getPlayerPosition();

            base.Update(gameTime);
        }

        public override void RemoveAllGameComponents()
        {
            Global.GameState.IS_RENDERER_MAZE = false;

            //SoundManager.StopBackgroundMusic();
            SoundManager.ClearRepeatingSoundEffects();

            foreach (IGameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).RemoveAllGameComponents();
                Game.Components.Remove(gameComponent);
            }

            Game.Components.Remove(player);

            PlayerUI.ClearUI();
            Game.Components.Remove(PlayerUI);

            if (!isPlayerDead)
            {
                Global.Player.LAST_MAP = this.GetType();
            }
        }

        protected override void LoadContent()
        {
            worldRenderer.SetBackground(new Backgrounds.BasicBackground(this.Game), Color.Black);

            playerLight = new PointLight(worldRenderer.lightingManager.lightEffect, new Vector2(0, 0), 1200f, Color.White, 1f);
            worldRenderer.lightingManager.AddLight(playerLight);

            LevelDoor LevelDoor1370 = new LevelDoor(Game, worldRenderer, this, new Rectangle(1788, 1248, 45, 150), "World2", "BlackMap4");
            AddGameComponent(LevelDoor1370);

            base.LoadContent();

            if (Global.Player.LAST_MAP == typeof(World2.BlackMap2.BlackMap2))
            {
                player.setPlayerPosition(new Vector2(338, 330));
            }
            else
            {
                player.setPlayerPosition(new Vector2(338, 330));
                Song backgroundSong = Game.Content.Load<Song>("Sounds/World1/Ink/heathen");
                SoundManager.PlayBackgroundMusic(backgroundSong);
            }

            Enemy Enemy4129 = new Enemy(Game, worldRenderer, this, new Rectangle(536, 1163, 0, 0), player, 483, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 12, 4, 3);
            AddGameComponent(Enemy4129);
            Enemy Enemy8923 = new Enemy(Game, worldRenderer, this, new Rectangle(373, 1026, 0, 0), player, 483, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 12, 4, 3);
            AddGameComponent(Enemy8923);
            Enemy Enemy9096 = new Enemy(Game, worldRenderer, this, new Rectangle(1215, 637, 0, 0), player, 483, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 12, 4, 3);
            AddGameComponent(Enemy9096);
            Enemy Enemy2751 = new Enemy(Game, worldRenderer, this, new Rectangle(1252, 1029, 0, 0), player, 483, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 12, 4, 3);
            AddGameComponent(Enemy2751);
            Enemy Enemy8447 = new Enemy(Game, worldRenderer, this, new Rectangle(1090, 1349, 0, 0), player, 483, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 12, 4, 3);
            AddGameComponent(Enemy8447);
            Enemy Enemy1177 = new Enemy(Game, worldRenderer, this, new Rectangle(1360, 1494, 0, 0), player, 483, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 12, 4, 3);
            AddGameComponent(Enemy1177);
            Enemy Enemy7439 = new Enemy(Game, worldRenderer, this, new Rectangle(1573, 1378, 0, 0), player, 483, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 12, 4, 3);
            AddGameComponent(Enemy7439);
            Enemy Enemy1056 = new Enemy(Game, worldRenderer, this, new Rectangle(1402, 1234, 0, 0), player, 483, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 12, 4, 3);
            AddGameComponent(Enemy1056);
            Enemy Enemy7230 = new Enemy(Game, worldRenderer, this, new Rectangle(1604, 1086, 0, 0), player, 483, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 12, 4, 3);
            AddGameComponent(Enemy7230);
            Enemy Enemy6099 = new Enemy(Game, worldRenderer, this, new Rectangle(1326, 909, 0, 0), player, 483, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 12, 4, 3);
            AddGameComponent(Enemy6099);
            Enemy Enemy6164 = new Enemy(Game, worldRenderer, this, new Rectangle(1258, 750, 0, 0), player, 483, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 12, 4, 3);
            AddGameComponent(Enemy6164);
            Enemy Enemy7299 = new Enemy(Game, worldRenderer, this, new Rectangle(1205, 1201, 0, 0), player, 483, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 12, 4, 3);
            AddGameComponent(Enemy7299);
            Enemy Enemy8457 = new Enemy(Game, worldRenderer, this, new Rectangle(1463, 1038, 0, 0), player, 483, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 12, 4, 3);
            AddGameComponent(Enemy8457);
            Enemy Enemy6481 = new Enemy(Game, worldRenderer, this, new Rectangle(404, 689, 0, 0), player, 483, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy6481);

            Ammo Ammo1542 = new Ammo(Game, worldRenderer, this, new Rectangle(460, 360, 0, 0));
            AddGameComponent(Ammo1542);
            Ammo Ammo9163 = new Ammo(Game, worldRenderer, this, new Rectangle(410, 386, 0, 0));
            AddGameComponent(Ammo9163);
            Ammo Ammo7852 = new Ammo(Game, worldRenderer, this, new Rectangle(420, 448, 0, 0));
            AddGameComponent(Ammo7852);
            Ammo Ammo1548 = new Ammo(Game, worldRenderer, this, new Rectangle(445, 491, 0, 0));
            AddGameComponent(Ammo1548);
            Ammo Ammo4702 = new Ammo(Game, worldRenderer, this, new Rectangle(439, 420, 0, 0));
            AddGameComponent(Ammo4702);
            Ammo Ammo6097 = new Ammo(Game, worldRenderer, this, new Rectangle(430, 280, 0, 0));
            AddGameComponent(Ammo6097);
            Ammo Ammo2765 = new Ammo(Game, worldRenderer, this, new Rectangle(425, 329, 0, 0));
            AddGameComponent(Ammo2765);
            Ammo Ammo2766 = new Ammo(Game, worldRenderer, this, new Rectangle(420, 250, 0, 0));
            AddGameComponent(Ammo2766);
            Ammo Ammo2767 = new Ammo(Game, worldRenderer, this, new Rectangle(445, 270, 0, 0));
            AddGameComponent(Ammo2767);
            Ammo Ammo2768 = new Ammo(Game, worldRenderer, this, new Rectangle(490, 250, 0, 0));
            AddGameComponent(Ammo2768);
            Ammo Ammo2769 = new Ammo(Game, worldRenderer, this, new Rectangle(530, 250, 0, 0));
            AddGameComponent(Ammo2769);
            Ammo Ammo2770 = new Ammo(Game, worldRenderer, this, new Rectangle(590, 250, 0, 0));
            AddGameComponent(Ammo2770);
            player.SetWalkingSpeed(2);

            Vendor Vendor1687 = new Vendor(Game, worldRenderer, this, new Rectangle(744, 159, 100, 100));
            AddGameComponent(Vendor1687);
        }

        public override void OnPlayerDeath()
        {
            worldRenderer.LoadMap("World2", "BlackMap3");
            base.OnPlayerDeath();
        }
    }
}
