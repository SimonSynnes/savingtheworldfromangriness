﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.GUI;

using Lighting;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace SavingTheWorldFromAngriness.World.Maps.World2.BlackMap1
{
    class BlackMap1 : BasicMap
    {
        private PointLight playerLight;

        private bool locked;

        #region UI
        private Texture2D playerAvatarTexture;
        private SoundEffect promptSoundEffect1;
        #endregion

        public BlackMap1(Game game, WorldRenderer worldRenderer) 
            : base(game, worldRenderer)
        {
            Global.Player.AMMO = 0;
            Global.Player.AMMO_IN_MAGAZINE = 0;
            this.locked = true;
        }

        public override void Initialize()
        {
            base.Initialize();

            player.setPlayerPosition(new Vector2(518, 2949));

            Global.GameState.IS_RENDERER_MAZE = true;
        }

        public override void Update(GameTime gameTime)
        {
            if (locked)
            {
                return;
            }

            playerLight.Position = player.getPlayerPosition();

            base.Update(gameTime);
        }

        public override void RemoveAllGameComponents()
        {
            Global.GameState.IS_RENDERER_MAZE = false;

            //SoundManager.StopBackgroundMusic();
            SoundManager.ClearRepeatingSoundEffects();

            foreach (IGameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).RemoveAllGameComponents();
                Game.Components.Remove(gameComponent);
            }

            Game.Components.Remove(player);

            PlayerUI.ClearUI();
            Game.Components.Remove(PlayerUI);

            if (!isPlayerDead)
            {
                Global.Player.LAST_MAP = this.GetType();
            }
        }

        protected override void LoadContent()
        {
            #region UI

            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");
            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");

            DialogPrompt prompt = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME, playerAvatarTexture,
                "Looks like someone has been here...", promptSoundEffect1);
            Game.Components.Add(prompt);
            prompt.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
            {
                Game.Components.Remove(prompt);
                locked = false;
            };
            #endregion

            Song backgroundSong = Game.Content.Load<Song>("Sounds/World1/Ink/heathen");
            SoundManager.PlayBackgroundMusic(backgroundSong);

            worldRenderer.SetBackground(new Backgrounds.BasicBackground(this.Game), Color.Black);

            playerLight = new PointLight(worldRenderer.lightingManager.lightEffect, new Vector2(0, 0), 1200f, Color.DarkRed, 0.8f);
            worldRenderer.lightingManager.AddLight(playerLight);

            LevelDoor LevelDoor5272 = new LevelDoor(Game, worldRenderer, this, new Rectangle(880, 272, 64, 32), "World2", "BlackMap2");
            AddGameComponent(LevelDoor5272);

            base.LoadContent();

            player.SetWalkingSpeed(2);

            player.AddSplat(new Vector2(289, 2938));
            player.AddSplat(new Vector2(290, 2935));
            player.AddSplat(new Vector2(293, 3005));
            player.AddSplat(new Vector2(306, 3011));
            player.AddSplat(new Vector2(323, 3003));
            player.AddSplat(new Vector2(358, 3022));
            player.AddSplat(new Vector2(386, 3022));
            player.AddSplat(new Vector2(406, 3022));
            player.AddSplat(new Vector2(430, 3024));
            player.AddSplat(new Vector2(490, 3025));
            player.AddSplat(new Vector2(521, 3023));
            player.AddSplat(new Vector2(570, 3022));
            player.AddSplat(new Vector2(585, 3025));
            player.AddSplat(new Vector2(648, 3024));
            player.AddSplat(new Vector2(693, 2960));
            player.AddSplat(new Vector2(695, 2931));
            player.AddSplat(new Vector2(694, 2917));
            player.AddSplat(new Vector2(696, 2901));
            player.AddSplat(new Vector2(653, 2982));
            player.AddSplat(new Vector2(455, 2965));
            player.AddSplat(new Vector2(392, 2854));
            player.AddSplat(new Vector2(600, 2901));
            player.AddSplat(new Vector2(484, 2916));
            player.AddSplat(new Vector2(405, 2928));
            player.AddSplat(new Vector2(432, 2978));
            player.AddSplat(new Vector2(511, 3025));
            player.AddSplat(new Vector2(359, 2947));
            player.AddSplat(new Vector2(368, 2953));
            player.AddSplat(new Vector2(553, 2992));
            player.AddSplat(new Vector2(475, 3024));
            player.AddSplat(new Vector2(395, 3009));
            player.AddSplat(new Vector2(450, 3022));
            player.AddSplat(new Vector2(485, 3010));
            player.AddSplat(new Vector2(530, 3024));
            player.AddSplat(new Vector2(537, 3024));
            player.AddSplat(new Vector2(388, 2665));
            player.AddSplat(new Vector2(243, 2458));
            player.AddSplat(new Vector2(266, 2515));
            player.AddSplat(new Vector2(193, 2353));
            player.AddSplat(new Vector2(98, 2320));
            player.AddSplat(new Vector2(104, 2132));
            player.AddSplat(new Vector2(119, 2073));
            player.AddSplat(new Vector2(156, 1791));
            player.AddSplat(new Vector2(97, 1689));
            player.AddSplat(new Vector2(159, 1594));
            player.AddSplat(new Vector2(149, 1690));
            player.AddSplat(new Vector2(242, 1595));
            player.AddSplat(new Vector2(156, 1400));
            player.AddSplat(new Vector2(341, 1194));
            player.AddSplat(new Vector2(328, 1112));
            player.AddSplat(new Vector2(340, 920));
            player.AddSplat(new Vector2(438, 823));
            player.AddSplat(new Vector2(503, 691));
            player.AddSplat(new Vector2(503, 711));
            player.AddSplat(new Vector2(694, 725));
            player.AddSplat(new Vector2(695, 687));
            player.AddSplat(new Vector2(642, 627));
            player.AddSplat(new Vector2(811, 658));
            player.AddSplat(new Vector2(789, 407));
            player.AddSplat(new Vector2(769, 345));
            player.AddSplat(new Vector2(822, 305));
            player.AddSplat(new Vector2(808, 241));
            player.AddSplat(new Vector2(738, 438));
            player.AddSplat(new Vector2(630, 669));
            player.AddSplat(new Vector2(664, 536));
            player.AddSplat(new Vector2(354, 1071));
            player.AddSplat(new Vector2(287, 975));
            player.AddSplat(new Vector2(314, 1304));
            player.AddSplat(new Vector2(215, 1400));
            player.AddSplat(new Vector2(155, 1496));
            player.AddSplat(new Vector2(156, 1497));
            player.AddSplat(new Vector2(230, 2031));
            player.AddSplat(new Vector2(161, 2228));
            player.AddSplat(new Vector2(280, 2513));
            player.AddSplat(new Vector2(319, 2607));
            player.AddSplat(new Vector2(604, 3023));
            player.AddSplat(new Vector2(602, 3022));
            player.AddSplat(new Vector2(513, 3009));
            player.AddSplat(new Vector2(368, 2854));
            player.AddSplat(new Vector2(384, 2896));
            player.AddSplat(new Vector2(366, 2746));
            player.AddSplat(new Vector2(439, 2922));
            player.AddSplat(new Vector2(399, 2865));
            player.AddSplat(new Vector2(420, 2854));
            player.AddSplat(new Vector2(531, 2853));
            player.AddSplat(new Vector2(398, 2786));
            player.AddSplat(new Vector2(469, 2863));
            player.AddSplat(new Vector2(484, 2909));
            player.AddSplat(new Vector2(386, 2712));
            player.AddSplat(new Vector2(525, 2961));
            player.AddSplat(new Vector2(634, 2981));
            player.AddSplat(new Vector2(631, 2981));
            player.AddSplat(new Vector2(604, 2983));
            player.AddSplat(new Vector2(590, 2976));
            player.AddSplat(new Vector2(629, 2953));
            player.AddSplat(new Vector2(458, 2898));
            player.AddSplat(new Vector2(638, 2822));
            player.AddSplat(new Vector2(532, 2851));
            player.AddSplat(new Vector2(461, 2822));
            player.AddSplat(new Vector2(491, 2791));
            player.AddSplat(new Vector2(347, 2626));
            player.AddSplat(new Vector2(352, 2614));
            player.AddSplat(new Vector2(311, 2523));
            player.AddSplat(new Vector2(383, 2739));
            player.AddSplat(new Vector2(439, 2645));
            player.AddSplat(new Vector2(612, 2681));
            player.AddSplat(new Vector2(568, 2782));
            player.AddSplat(new Vector2(440, 2633));
            player.AddSplat(new Vector2(384, 2533));

        }
    }
}
