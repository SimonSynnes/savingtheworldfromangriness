﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using SavingTheWorldFromAngriness.Backgrounds;
using SavingTheWorldFromAngriness.GUI;
using Microsoft.Xna.Framework.Audio;

namespace SavingTheWorldFromAngriness.World.Maps.World2
{
    class ShrineEntrance : BasicMap
    {
        private Texture2D playerAvatarTexture;
        private SoundEffect promptSoundEffect1;

        public ShrineEntrance(Game game, WorldRenderer worldRenderer) 
            : base(game, worldRenderer)
        {

        }

        public override void Initialize()
        {
            base.Initialize();

            player.setPlayerPosition(new Vector2(75, 75));
        }

        protected override void LoadContent()
        {
            #region Misc
            //worldRenderer.SetBackground(new MovingLava(this.Game, 40, 1), Color.Black);
           worldRenderer.SetBackground(new Backgrounds.BasicBackground(this.Game), Color.Black);

            Song backgroundMusic = Game.Content.Load<Song>("Sounds/World2/battle");
            PlayBackgroundMusic(backgroundMusic);

            #endregion

            base.LoadContent();

            #region LevelDoor
            LevelDoor levelDoorMap2 = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(574, 700, 50, 50), "World2", "ShrineSanctuaryStart");
            AddGameComponent(levelDoorMap2);
            #endregion

            #region Enemies
            Enemy enemy = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(7 * 7, 20 * 33, 0, 10), player, 211, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 2);
            AddGameComponent(enemy);

            Enemy enemy2 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(7 * 7, 20 * 30, 0, 10), player, 211, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 2);
            AddGameComponent(enemy2);

            Enemy enemy3 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(7 * 7, 20 * 27, 0, 10), player, 211, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 2);
            AddGameComponent(enemy3);

            Enemy enemy4 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(7 * 7, 20 * 24, 0, 10), player, 211, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 2);
            AddGameComponent(enemy4);

            Enemy enemy5 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(7 * 7, 20 * 21, 0, 10), player, 211, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 2);
            AddGameComponent(enemy5);

            Enemy enemy6 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(26 * 26, 20 * 33, 0, 10), player, 211, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 1);
            AddGameComponent(enemy6);

            Enemy enemy7 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(26 * 26, 20 * 30, 0, 10), player, 211, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 1);
            AddGameComponent(enemy7);

            Enemy enemy8 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(26 * 26, 20 * 27, 0, 10), player, 211, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 1);
            AddGameComponent(enemy8);

            Enemy enemy9 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(26 * 26, 20 * 24, 0, 10), player, 211, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 1);
            AddGameComponent(enemy9);

            Enemy enemy10 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(26 * 26, 20 * 21, 0, 10), player, 211, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 1);
            AddGameComponent(enemy10);
            #endregion

            #region Healthpacks
            HealthPack HealthPack6117 = new HealthPack(Game, worldRenderer, this, new Rectangle(300, 70, 0, 0));
            AddGameComponent(HealthPack6117);
            HealthPack HealthPack6118 = new HealthPack(Game, worldRenderer, this, new Rectangle(320, 70, 0, 0));
            AddGameComponent(HealthPack6118);
            HealthPack HealthPack6119 = new HealthPack(Game, worldRenderer, this, new Rectangle(340, 70, 0, 0));
            AddGameComponent(HealthPack6119);
            #endregion

            #region Ammo
            Ammo ammo1 = new Ammo(this.Game, worldRenderer, this, new Rectangle(162, 70, 0, 0));
            Ammo ammo2 = new Ammo(this.Game, worldRenderer, this, new Rectangle(207, 67, 0, 0));
            Ammo ammo3 = new Ammo(this.Game, worldRenderer, this, new Rectangle(214, 108, 0, 0));
            AddGameComponent(ammo1);
            AddGameComponent(ammo2);
            AddGameComponent(ammo3);
            #endregion

            #region Vendor
            Vendor vendor = new Vendor(this.Game, worldRenderer, this, new Rectangle(670, 70, 100, 100));
            AddGameComponent(vendor);
            #endregion
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void RemoveAllGameComponents()
        {
            worldRenderer.SetBackground(new MovingClouds(this.Game, 40, 1), Color.CornflowerBlue);

            base.RemoveAllGameComponents();
        }

        public override void OnPlayerDeath()
        {
            worldRenderer.LoadMap("World2", "ShrineEntrance");

            base.OnPlayerDeath();
        }
    }
}