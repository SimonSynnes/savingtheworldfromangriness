﻿using Microsoft.Xna.Framework;
using SavingTheWorldFromAngriness.World.Components;
using Microsoft.Xna.Framework.Media;
using SavingTheWorldFromAngriness.Backgrounds;
using SavingTheWorldFromAngriness.GUI;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace SavingTheWorldFromAngriness.World.Maps.World2.OutsideGarden
{
    class OutsideGarden : BasicMap
    {
        #region Water Rectangles
        private Rectangle waterTopLeftRectangle;
        private Rectangle waterMiddleLeftRectangle;
        private Rectangle waterBottomLeftRectangle;
        private Rectangle waterTopRightRectangle;
        private Rectangle waterMiddleRightRectangle;
        private Rectangle waterBottomRightRectangle;
        #endregion

        #region UI
        private Texture2D playerAvatarTexture;
        private SoundEffect promptSoundEffect1;
        #endregion

        private bool stopMapLogic;

        public OutsideGarden(Game game, WorldRenderer worldRenderer)
        : base(game, worldRenderer)
        {
            #region Water Rectangles
            this.waterTopLeftRectangle = new Rectangle(16, 111, 8 * 32, 4 * 32);
            this.waterTopRightRectangle = new Rectangle(656, 111, 8 * 32, 4 * 32);
            this.waterMiddleLeftRectangle = new Rectangle(16, 400, 8 * 32, 4 * 32);
            this.waterMiddleRightRectangle = new Rectangle(656, 400, 8 * 32, 4 * 32);
            this.waterBottomLeftRectangle = new Rectangle(16, 688, 8 * 32, 4 * 32);
            this.waterBottomRightRectangle = new Rectangle(656, 688, 8 * 32, 4 * 32);
            #endregion
        }

        public override void Initialize()
        {
            base.Initialize();
            player.setPlayerPosition(new Vector2(445, 856));

            #region Background
            worldRenderer.SetBackground(new Backgrounds.MovingClouds(this.Game, 40, 1), Color.CornflowerBlue);
            #endregion

            SetCameraToPlayerPosition();

            stopMapLogic = true;
            DialogPrompt prompt = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME, playerAvatarTexture,
                "I've nearly escaped!", promptSoundEffect1);
            Game.Components.Add(prompt);
            prompt.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
            {
                stopMapLogic = false;
                Game.Components.Remove(prompt);
            };
        }

        public override void Update(GameTime gameTime)
        {
            if (stopMapLogic)
            {
                SetCameraToPlayerPosition();
                return;
            }

            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            #region UI

            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");
            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");
            #endregion

            base.LoadContent();
            #region Sounds
            Song backgroundMusic = Game.Content.Load<Song>("Sounds/World2/OutsideGarden/OutsideGarden");
            PlayBackgroundMusic(backgroundMusic);
            #endregion

            #region Healthpacks
            HealthPack HealthPack1606 = new HealthPack(Game, worldRenderer, this, new Rectangle(17, 838, 0, 0));
            AddGameComponent(HealthPack1606);
            HealthPack HealthPack2944 = new HealthPack(Game, worldRenderer, this, new Rectangle(872, 830, 0, 0));
            AddGameComponent(HealthPack2944);
            HealthPack HealthPack6057 = new HealthPack(Game, worldRenderer, this, new Rectangle(20, 360, 0, 0));
            AddGameComponent(HealthPack6057);
            #endregion

            #region LevelDoors
            LevelDoor EnterDoor = new LevelDoor(Game, worldRenderer, this, 
                new Rectangle(431, 880, 64, 32), "World2", "CastleMoat");
            AddGameComponent(EnterDoor);
            LevelDoor ExitDoor = new LevelDoor(Game, worldRenderer, this, 
                new Rectangle(434, 14, 64, 32), "World2", "OutsideGarden2");
            AddGameComponent(ExitDoor);
            #endregion

            #region Enemies
            int hp = 311;
            int aggro = 170;
            int attackSpeed = Global.GameConstants.ENEMY_ATTACKSPEED;
            int attackPower = 10;
            int walkingSpeed = 3;
            int lookingRight = 2;
            int lookingLeft = 1;
            int lookingDown = 0;

            Enemy Enemy9853 = new Enemy(Game, worldRenderer, this, new Rectangle(600, 119, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingLeft);
            AddGameComponent(Enemy9853);
            Enemy Enemy5768 = new Enemy(Game, worldRenderer, this, new Rectangle(600, 175, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingLeft);
            AddGameComponent(Enemy5768);
            Enemy Enemy9797 = new Enemy(Game, worldRenderer, this, new Rectangle(330, 19, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingDown);
            AddGameComponent(Enemy9797);
            Enemy Enemy6667 = new Enemy(Game, worldRenderer, this, new Rectangle(538, 22, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingDown);
            AddGameComponent(Enemy6667);
            Enemy Enemy9519 = new Enemy(Game, worldRenderer, this, new Rectangle(280, 117, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingRight);
            AddGameComponent(Enemy9519);
            Enemy Enemy8723 = new Enemy(Game, worldRenderer, this, new Rectangle(280, 173, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingRight);
            AddGameComponent(Enemy8723);
            Enemy Enemy7620 = new Enemy(Game, worldRenderer, this, new Rectangle(280, 407, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingRight);
            AddGameComponent(Enemy7620);
            Enemy Enemy2185 = new Enemy(Game, worldRenderer, this, new Rectangle(280, 459, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingRight);
            AddGameComponent(Enemy2185);
            Enemy Enemy7717 = new Enemy(Game, worldRenderer, this, new Rectangle(600, 401, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingLeft);
            AddGameComponent(Enemy7717);
            Enemy Enemy9119 = new Enemy(Game, worldRenderer, this, new Rectangle(600, 453, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingLeft);
            AddGameComponent(Enemy9119);
            Enemy Enemy6561 = new Enemy(Game, worldRenderer, this, new Rectangle(280, 694, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingRight);
            AddGameComponent(Enemy6561);
            Enemy Enemy4307 = new Enemy(Game, worldRenderer, this, new Rectangle(600, 694, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingLeft);
            AddGameComponent(Enemy4307);
            Enemy EnemyMiddle = new Enemy(Game, worldRenderer, this, new Rectangle(442, 350, 0, 0), player, hp, aggro+20, attackSpeed, attackPower, walkingSpeed, lookingDown);
            AddGameComponent(EnemyMiddle);
            #endregion

            #region Ammo
            Ammo Ammo5475 = new Ammo(Game, worldRenderer, this, new Rectangle(23, 649, 0, 0));
            AddGameComponent(Ammo5475);
            Ammo Ammo4684 = new Ammo(Game, worldRenderer, this, new Rectangle(23, 524, 0, 0));
            AddGameComponent(Ammo4684);
            Ammo Ammo3671 = new Ammo(Game, worldRenderer, this, new Rectangle(852, 362, 0, 0));
            AddGameComponent(Ammo3671);
            Ammo Ammo5471 = new Ammo(Game, worldRenderer, this, new Rectangle(854, 233, 0, 0));
            AddGameComponent(Ammo5471);
            Ammo Ammo5472 = new Ammo(Game, worldRenderer, this, new Rectangle(425, 856, 0, 0));
            AddGameComponent(Ammo5472);
            Ammo Ammo5473 = new Ammo(Game, worldRenderer, this, new Rectangle(479, 856, 0, 0));
            AddGameComponent(Ammo5473);
            #endregion

            #region Collisions at Water Rectangles
            worldRenderer.addMiscCollision(waterTopLeftRectangle);
            worldRenderer.addMiscCollision(waterTopRightRectangle);
            worldRenderer.addMiscCollision(waterMiddleLeftRectangle);
            worldRenderer.addMiscCollision(waterMiddleRightRectangle);
            worldRenderer.addMiscCollision(waterBottomLeftRectangle);
            worldRenderer.addMiscCollision(waterBottomRightRectangle);


            #endregion
        }
        public override void OnPlayerDeath()
        {
            worldRenderer.LoadMap("World2", "OutsideGarden");
            base.OnPlayerDeath();
        }
    }
}
