﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using SavingTheWorldFromAngriness.Backgrounds;
using SavingTheWorldFromAngriness.World.Components;
using Microsoft.Xna.Framework.Audio;
using SavingTheWorldFromAngriness.GUI;

namespace SavingTheWorldFromAngriness.World.Maps.World2
{
    class CastleBanquetHall : BasicMap
    {
        private Texture2D playerAvatarTexture;
        private SoundEffect promptSoundEffect1;

        private bool stopMapLogic;

        public CastleBanquetHall(Game game, WorldRenderer worldRenderer) : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();

            #region Background 
            worldRenderer.SetBackground(new Backgrounds.BasicBackground(this.Game), Color.Black);
            #endregion

            player.setPlayerPosition(new Vector2(545, 1140));

            SetCameraToPlayerPosition();

            stopMapLogic = true;
            DialogPrompt prompt = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME, playerAvatarTexture, "I can barely see the guards in here. Surely there must be an exit nearby...", promptSoundEffect1);
            Game.Components.Add(prompt);
            prompt.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
            {
                stopMapLogic = false;
                Game.Components.Remove(prompt);
            };
        }

        protected override void LoadContent()
        {
            Song backgroundMusic = Game.Content.Load<Song>("Sounds/World2/Map2");
            PlayBackgroundMusic(backgroundMusic);

            LevelDoor levelDoorMap3 = new LevelDoor(this.Game, worldRenderer,
                this, new Rectangle(295, 28, 50, 50), "World2", "CastlePuzzle");
            AddGameComponent(levelDoorMap3);

            base.LoadContent();

            Enemy enemy = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(533, 952, 0, 10), player, 182, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(enemy);

            Enemy enemy3 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(380, 310, 0, 10), player, 182, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(enemy3);

            Enemy enemy4 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(113, 467, 0, 10), player, 182, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(enemy4);

            Enemy enemy5 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(109, 105, 0, 10), player, 182, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(enemy5);

            Enemy enemy6 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(414, 23, 0, 10), player, 182, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(enemy6);

            HealthPack HealthPack6117 = new HealthPack(Game, worldRenderer, this, new Rectangle(244, 759, 0, 0));
            AddGameComponent(HealthPack6117);
            HealthPack HealthPack6118 = new HealthPack(Game, worldRenderer, this, new Rectangle(54, 340, 0, 0));
            AddGameComponent(HealthPack6118);
            HealthPack HealthPack6119 = new HealthPack(Game, worldRenderer, this, new Rectangle(262, 414, 0, 0));
            AddGameComponent(HealthPack6119);


            Ammo ammo1 = new Ammo(this.Game, worldRenderer, this, new Rectangle(411, 1135, 0, 0));
            Ammo ammo2 = new Ammo(this.Game, worldRenderer, this, new Rectangle(318, 1134, 0, 0));
            Ammo ammo3 = new Ammo(this.Game, worldRenderer, this, new Rectangle(233, 1135, 0, 0));
            Ammo ammo4 = new Ammo(this.Game, worldRenderer, this, new Rectangle(133, 1136, 0, 0));

            AddGameComponent(ammo1);
            AddGameComponent(ammo2);
            AddGameComponent(ammo3);
            AddGameComponent(ammo4);

            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");
            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");

            //     worldRenderer.SetBackground(new MovingLava(this.Game, 40, 1), Color.Black);
            worldRenderer.SetBackground(new Backgrounds.BasicBackground(this.Game), Color.Black);
        }

        public override void Update(GameTime gameTime)
        {
            if (stopMapLogic)
            {
                SetCameraToPlayerPosition();
                return;
            }

            base.Update(gameTime);
        }

        public override void RemoveAllGameComponents()
        {
            worldRenderer.SetBackground(new MovingClouds(this.Game, 40, 1), Color.CornflowerBlue);

            base.RemoveAllGameComponents();
        }

        public override void OnPlayerDeath()
         {
             worldRenderer.LoadMap("World2", "CastleBanquetHall");
            base.OnPlayerDeath();
         }
         
    }
}
