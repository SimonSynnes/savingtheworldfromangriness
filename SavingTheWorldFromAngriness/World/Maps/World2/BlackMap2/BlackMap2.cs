﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.GUI;

using Lighting;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace SavingTheWorldFromAngriness.World.Maps.World2.BlackMap2
{
    class BlackMap2 : BasicMap
    {
        private PointLight playerLight;

        public BlackMap2(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();


            Global.GameState.IS_RENDERER_MAZE = true;
        }

        public override void Update(GameTime gameTime)
        {
            playerLight.Position = player.getPlayerPosition();

            base.Update(gameTime);
        }

        public override void RemoveAllGameComponents()
        {
            Global.GameState.IS_RENDERER_MAZE = false;

            //SoundManager.StopBackgroundMusic();
            SoundManager.ClearRepeatingSoundEffects();

            foreach (IGameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).RemoveAllGameComponents();
                Game.Components.Remove(gameComponent);
            }

            Game.Components.Remove(player);

            PlayerUI.ClearUI();
            Game.Components.Remove(PlayerUI);

            if (!isPlayerDead)
            {
                Global.Player.LAST_MAP = this.GetType();
            }
        }

        protected override void LoadContent()
        {
            worldRenderer.SetBackground(new Backgrounds.BasicBackground(this.Game), Color.Black);

            playerLight = new PointLight(worldRenderer.lightingManager.lightEffect, new Vector2(0, 0), 700f, Color.DarkRed, 1f);
            worldRenderer.lightingManager.AddLight(playerLight);

            LevelDoor LevelDoor2038 = new LevelDoor(Game, worldRenderer, this, new Rectangle(24, 642, 32, 100), "World2", "BlackMap3");
            AddGameComponent(LevelDoor2038);

            base.LoadContent();

            if (Global.Player.LAST_MAP == typeof(World2.BlackMap1.BlackMap1))
            {
                player.setPlayerPosition(new Vector2(72, 2545));
            }
            else
            {
                player.setPlayerPosition(new Vector2(72, 2545));
                Song backgroundSong = Game.Content.Load<Song>("Sounds/World1/Ink/heathen");
                SoundManager.PlayBackgroundMusic(backgroundSong);
            }


            Enemy Enemy6070 = new Enemy(Game, worldRenderer, this, new Rectangle(153, 2214, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy6070);
            Enemy Enemy9337 = new Enemy(Game, worldRenderer, this, new Rectangle(153, 2282, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy9337);
            Enemy Enemy2570 = new Enemy(Game, worldRenderer, this, new Rectangle(539, 2221, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy2570);
            Enemy Enemy1813 = new Enemy(Game, worldRenderer, this, new Rectangle(543, 2285, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy1813);
            Enemy Enemy5893 = new Enemy(Game, worldRenderer, this, new Rectangle(145, 1648, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy5893);
            Enemy Enemy3723 = new Enemy(Game, worldRenderer, this, new Rectangle(146, 1703, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy3723);
            Enemy Enemy6155 = new Enemy(Game, worldRenderer, this, new Rectangle(532, 1658, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy6155);
            Enemy Enemy1892 = new Enemy(Game, worldRenderer, this, new Rectangle(534, 1711, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy1892);
            Enemy Enemy4660 = new Enemy(Game, worldRenderer, this, new Rectangle(135, 1207, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy4660);
            Enemy Enemy7528 = new Enemy(Game, worldRenderer, this, new Rectangle(250, 1283, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy7528);
            Enemy Enemy5575 = new Enemy(Game, worldRenderer, this, new Rectangle(352, 1220, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy5575);
            Enemy Enemy8657 = new Enemy(Game, worldRenderer, this, new Rectangle(486, 1291, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy8657);
            Enemy Enemy5483 = new Enemy(Game, worldRenderer, this, new Rectangle(516, 1211, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy5483);
            Enemy Enemy4509 = new Enemy(Game, worldRenderer, this, new Rectangle(333, 1688, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy4509);
            Enemy Enemy9634 = new Enemy(Game, worldRenderer, this, new Rectangle(348, 2276, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy9634);
            Enemy Enemy3957 = new Enemy(Game, worldRenderer, this, new Rectangle(438, 2221, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy3957);
            Enemy Enemy9630 = new Enemy(Game, worldRenderer, this, new Rectangle(332, 2207, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy9630);
            Enemy Enemy7957 = new Enemy(Game, worldRenderer, this, new Rectangle(519, 74, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy7957);
            Enemy Enemy1937 = new Enemy(Game, worldRenderer, this, new Rectangle(358, 140, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy1937);
            Enemy Enemy2081 = new Enemy(Game, worldRenderer, this, new Rectangle(224, 84, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy2081);
            Enemy Enemy6293 = new Enemy(Game, worldRenderer, this, new Rectangle(48, 147, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy6293);
            Enemy Enemy9399 = new Enemy(Game, worldRenderer, this, new Rectangle(332, 651, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy9399);
            Enemy Enemy6218 = new Enemy(Game, worldRenderer, this, new Rectangle(176, 706, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy6218);
            Enemy Enemy2562 = new Enemy(Game, worldRenderer, this, new Rectangle(464, 726, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy2562);
            Enemy Enemy9174 = new Enemy(Game, worldRenderer, this, new Rectangle(96, 366, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 1, 1);
            AddGameComponent(Enemy9174);
            Enemy Enemy4819 = new Enemy(Game, worldRenderer, this, new Rectangle(323, 436, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 1, 1);
            AddGameComponent(Enemy4819);
            Enemy Enemy6312 = new Enemy(Game, worldRenderer, this, new Rectangle(454, 381, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 1, 1);
            AddGameComponent(Enemy6312);
            Enemy Enemy6209 = new Enemy(Game, worldRenderer, this, new Rectangle(466, 932, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 1, 1);
            AddGameComponent(Enemy6209);
            Enemy Enemy3292 = new Enemy(Game, worldRenderer, this, new Rectangle(256, 997, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 1, 1);
            AddGameComponent(Enemy3292);
            Enemy Enemy6642 = new Enemy(Game, worldRenderer, this, new Rectangle(115, 951, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 1, 1);
            AddGameComponent(Enemy6642);
            Enemy Enemy3506 = new Enemy(Game, worldRenderer, this, new Rectangle(470, 1463, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 1, 1);
            AddGameComponent(Enemy3506);
            Enemy Enemy7289 = new Enemy(Game, worldRenderer, this, new Rectangle(150, 1494, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 1, 1);
            AddGameComponent(Enemy7289);
            Enemy Enemy4858 = new Enemy(Game, worldRenderer, this, new Rectangle(602, 2025, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 1, 1);
            AddGameComponent(Enemy4858);
            Enemy Enemy2386 = new Enemy(Game, worldRenderer, this, new Rectangle(363, 1976, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 1, 1);
            AddGameComponent(Enemy2386);
            Enemy Enemy7390 = new Enemy(Game, worldRenderer, this, new Rectangle(243, 1940, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 1, 1);
            AddGameComponent(Enemy7390);
            Enemy Enemy2688 = new Enemy(Game, worldRenderer, this, new Rectangle(94, 2000, 0, 0), player, 460, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 1, 1);
            AddGameComponent(Enemy2688);
            Ammo Ammo8470 = new Ammo(Game, worldRenderer, this, new Rectangle(27, 2454, 0, 0));
            AddGameComponent(Ammo8470);
            Ammo Ammo5255 = new Ammo(Game, worldRenderer, this, new Rectangle(136, 2464, 0, 0));
            AddGameComponent(Ammo5255);
            Ammo Ammo8261 = new Ammo(Game, worldRenderer, this, new Rectangle(247, 2459, 0, 0));
            AddGameComponent(Ammo8261);
            Ammo Ammo4524 = new Ammo(Game, worldRenderer, this, new Rectangle(353, 2460, 0, 0));
            AddGameComponent(Ammo4524);
            Ammo Ammo7895 = new Ammo(Game, worldRenderer, this, new Rectangle(454, 2463, 0, 0));
            AddGameComponent(Ammo7895);
            Ammo Ammo1577 = new Ammo(Game, worldRenderer, this, new Rectangle(568, 2465, 0, 0));
            AddGameComponent(Ammo1577);
            Ammo Ammo8381 = new Ammo(Game, worldRenderer, this, new Rectangle(688, 2464, 0, 0));
            AddGameComponent(Ammo8381);
            Ammo Ammo1662 = new Ammo(Game, worldRenderer, this, new Rectangle(679, 2544, 0, 0));
            AddGameComponent(Ammo1662);
            Ammo Ammo1663 = new Ammo(Game, worldRenderer, this, new Rectangle(679, 2545, 0, 0));
            AddGameComponent(Ammo1663);
            Ammo Ammo1667 = new Ammo(Game, worldRenderer, this, new Rectangle(679, 2546, 0, 0));
            AddGameComponent(Ammo1667);
            Ammo Ammo1664 = new Ammo(Game, worldRenderer, this, new Rectangle(679, 2547, 0, 0));
            AddGameComponent(Ammo1664);
            Ammo Ammo1665 = new Ammo(Game, worldRenderer, this, new Rectangle(679, 2550, 0, 0));
            AddGameComponent(Ammo1665);
            Ammo Ammo1666 = new Ammo(Game, worldRenderer, this, new Rectangle(679, 2552, 0, 0));
            AddGameComponent(Ammo1666);
            Ammo Ammo1670 = new Ammo(Game, worldRenderer, this, new Rectangle(679, 2555, 0, 0));
            AddGameComponent(Ammo1670);
            Ammo Ammo1675 = new Ammo(Game, worldRenderer, this, new Rectangle(679, 2556, 0, 0));
            AddGameComponent(Ammo1675);
            Ammo Ammo6147 = new Ammo(Game, worldRenderer, this, new Rectangle(584, 2541, 0, 0));
            AddGameComponent(Ammo6147);
            Ammo Ammo6546 = new Ammo(Game, worldRenderer, this, new Rectangle(478, 2547, 0, 0));
            AddGameComponent(Ammo6546);
            Ammo Ammo1473 = new Ammo(Game, worldRenderer, this, new Rectangle(339, 2548, 0, 0));
            AddGameComponent(Ammo1473);
            Ammo Ammo6140 = new Ammo(Game, worldRenderer, this, new Rectangle(268, 2552, 0, 0));
            AddGameComponent(Ammo6140);
            Ammo Ammo2169 = new Ammo(Game, worldRenderer, this, new Rectangle(68, 2552, 0, 0));
            AddGameComponent(Ammo2169);
            Ammo Ammo8196 = new Ammo(Game, worldRenderer, this, new Rectangle(50, 2635, 0, 0));
            AddGameComponent(Ammo8196);
            Ammo Ammo7686 = new Ammo(Game, worldRenderer, this, new Rectangle(188, 2643, 0, 0));
            AddGameComponent(Ammo7686);
            Ammo Ammo2638 = new Ammo(Game, worldRenderer, this, new Rectangle(458, 2640, 0, 0));
            AddGameComponent(Ammo2638);
            Ammo Ammo1648 = new Ammo(Game, worldRenderer, this, new Rectangle(656, 2653, 0, 0));
            AddGameComponent(Ammo1648);
            Ammo Ammo8031 = new Ammo(Game, worldRenderer, this, new Rectangle(559, 2590, 0, 0));
            AddGameComponent(Ammo8031);
            Ammo Ammo6502 = new Ammo(Game, worldRenderer, this, new Rectangle(298, 2632, 0, 0));
            AddGameComponent(Ammo6502);
            Ammo Ammo7457 = new Ammo(Game, worldRenderer, this, new Rectangle(167, 2549, 0, 0));
            AddGameComponent(Ammo7457);

            player.SetWalkingSpeed(2);

            Vendor Vendor1687 = new Vendor(Game, worldRenderer, this, new Rectangle(20, 1431, 100, 100));
            AddGameComponent(Vendor1687);
        }

        public override void OnPlayerDeath()
        {
            worldRenderer.LoadMap("World2", "BlackMap2");
            base.OnPlayerDeath();
        }
    }
}
