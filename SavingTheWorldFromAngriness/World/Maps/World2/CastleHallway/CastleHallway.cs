﻿using Lighting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SavingTheWorldFromAngriness.World.Components;
using Microsoft.Xna.Framework.Media;
using SavingTheWorldFromAngriness.Backgrounds;
using SavingTheWorldFromAngriness.GUI;
using Microsoft.Xna.Framework.Audio;

namespace SavingTheWorldFromAngriness.World.Maps.World2.CastleHallway
{
    class CastleHallway : BasicMap
    {
        private PointLight playerLight;
        private Texture2D playerAvatarTexture;
        private SoundEffect promptSoundEffect1;

        public CastleHallway(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();

            player.setPlayerPosition(new Vector2(144, 44));
            Ammo ammo = new Ammo(this.Game, worldRenderer, this, new Rectangle(154, 44, 0, 0));
            AddGameComponent(ammo);
        }

        public override void Update(GameTime gameTime)
        {
            playerLight.Position = player.getPlayerPosition();
            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            #region Light that follows the player
            playerLight = new PointLight(worldRenderer.lightingManager.lightEffect,
                new Vector2(0, 0), 300f, Color.White, 0.5f);
            worldRenderer.lightingManager.AddLight(playerLight);
            #endregion

            #region Torches in the beginning area.
            int torchStrength = 15;
            int torchSpeed = 150;

            //TorchLight torchLightUpLeft = new TorchLight(Game, worldRenderer, this, new Rectangle(32, 56, 0, 0), torchStrength, torchSpeed);
            //AddGameComponent(torchLightUpLeft);
            //TorchLight torchLightUpRight = new TorchLight(Game, worldRenderer, this, new Rectangle(240, 56, 0, 0), torchStrength, torchSpeed);
            //AddGameComponent(torchLightUpRight);
            TorchLight torchLightDownLeft = new TorchLight(Game, worldRenderer, this, new Rectangle(32, 164, 0, 0), torchStrength, torchSpeed);
            AddGameComponent(torchLightDownLeft);
            TorchLight torchLightDownRight = new TorchLight(Game, worldRenderer, this, new Rectangle(240, 164, 0, 0), torchStrength, torchSpeed);
            AddGameComponent(torchLightDownRight);
            #endregion

            #region Enter
            Texture2D stairs = Game.Content.Load<Texture2D>("Textures/RedCarpetStairs");

            LevelDoor exitCastleHallway = new LevelDoor(this.Game, worldRenderer, this, new Rectangle(112, 1298, 64, 32), "World2", "CastleBanquetHall", stairs);
            AddGameComponent(exitCastleHallway);
            #endregion

            #region Enemies
            int hp = 173;
            int aggro = 140;
            int attackSpeed = Global.GameConstants.ENEMY_ATTACKSPEED;
            int attackPower = 10;
            int walkingSpeed = 2;

            Enemy enemy = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(32, 600, 0, 10), player,
                hp, aggro, attackSpeed, attackPower, walkingSpeed, 2);
            AddGameComponent(enemy);

            Enemy enemy2 = new Enemy(this.Game, worldRenderer,
                 this, new Rectangle(225, 1085, 0, 10), player,
                 hp, aggro, attackSpeed, attackPower, walkingSpeed, 1);
            AddGameComponent(enemy2);

            Enemy enemy3 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(25, 955, 0, 10), player,
                hp, aggro, attackSpeed, attackPower, walkingSpeed, 2);
            AddGameComponent(enemy3);

            Enemy enemy4 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(225, 1210, 0, 10), player,
                hp, aggro, attackSpeed, attackPower, walkingSpeed, 1);
            AddGameComponent(enemy4);

            Enemy enemy5 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(25, 1210, 0, 10), player,
                hp, aggro, attackSpeed, attackPower, walkingSpeed, 2);
            AddGameComponent(enemy5);

            #endregion

            #region Sounds
            Song backgroundMusic = Game.Content.Load<Song>("Sounds/Ominous");
            PlayBackgroundMusic(backgroundMusic);
            #endregion

            #region Background of the map.
            worldRenderer.SetBackground(new BasicBackground(this.Game), Color.TransparentBlack);
            #endregion

            #region Ammo
            Ammo ammo = new Ammo(this.Game, worldRenderer, this, new Rectangle(238, 400, 0, 0));
            AddGameComponent(ammo);

            Ammo ammo2 = new Ammo(this.Game, worldRenderer, this, new Rectangle(236, 426, 0, 0));
            AddGameComponent(ammo2);
            #endregion

            #region HP
            HealthPack HealthPack7786 = new HealthPack(Game, worldRenderer, this, new Rectangle(225, 55, 0, 0));
            AddGameComponent(HealthPack7786);
            HealthPack HealthPack7739 = new HealthPack(Game, worldRenderer, this, new Rectangle(25, 60, 0, 0));
            AddGameComponent(HealthPack7739);
            #endregion
        }

        public override void OnPlayerDeath()
        {
            worldRenderer.LoadMap("World2", "CastleHallway");

            base.OnPlayerDeath();
        }

        public override void RemoveAllGameComponents()
        {
            base.RemoveAllGameComponents();
        }
    }
}
