﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global;
using SavingTheWorldFromAngriness.GUI;
using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.World.Components.World2;
using SavingTheWorldFromAngriness.World.Maps;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.World.Maps.World2.ShrineSanctuaryStart
{
    class ShrineSanctuaryStart : BasicMap
    {
        private Texture2D playerAvatarTexture;
        private SoundEffect promptSoundEffect1;

        private bool stopMapLogic;

        public ShrineSanctuaryStart(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer)
        {
            stopMapLogic = false;
        }

        public override void Initialize()
        {
            base.Initialize();

            #region Background 
            worldRenderer.SetBackground(new Backgrounds.BasicBackground(this.Game), Color.Black);
            #endregion

            if (Global.Player.LAST_MAP == typeof(World2.ShrineSanctuaryHall1.ShrineSanctuaryHall1))
            {
                player.setPlayerPosition(new Vector2(277, 77));
            }
            else
            {
                player.setPlayerPosition(new Vector2(249, 593));
            }

            if (!Global.Player.INVENTORY.Contains("World2:Shrine:Key2"))
            {
                stopMapLogic = true;

                SetCameraToPlayerPosition();

                DialogPrompt prompt = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME, playerAvatarTexture,
                    "There's the door to the castle moat... But it's locked.", promptSoundEffect1);
                Game.Components.Add(prompt);
                prompt.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
                {
                    stopMapLogic = false;
                    Game.Components.Remove(prompt);
                };
            }

            worldRenderer.SetBackground(new Backgrounds.BasicBackground(Game), new Color(17, 17, 17));
        }


        public override void RemoveAllGameComponents()
        {
            SoundManager.ClearRepeatingSoundEffects();

            foreach (IGameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).RemoveAllGameComponents();
                Game.Components.Remove(gameComponent);
            }

            Game.Components.Remove(player);

            PlayerUI.ClearUI();
            Game.Components.Remove(PlayerUI);

            if (!isPlayerDead)
            {
                Global.Player.LAST_MAP = this.GetType();
            }
        }

        public override void OnPlayerDeath()
        {
            base.OnPlayerDeath();

            worldRenderer.LoadMap("World2", "ShrineSanctuaryStart");
        }

        public override void Update(GameTime gameTime)
        {
            if (stopMapLogic)
            {
                SetCameraToPlayerPosition();
                return;
            }

            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");
            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");

            if (Global.Player.LAST_MAP != typeof(World2.ShrineSanctuaryHall1.ShrineSanctuaryHall1))
            {
                Song backgroundSong = Game.Content.Load<Song>("Sounds/World2/Shrine/silentLight");
                SoundManager.PlayBackgroundMusic(backgroundSong);


            }

            if (Global.Player.INVENTORY.Contains("World2:Shrine:Key2"))
            {
                LevelDoor LevelDoor6454 = new LevelDoor(Game, worldRenderer, this, new Rectangle(22, 432, 32, 100), "World2", "CastleMoat");
                AddGameComponent(LevelDoor6454);
            }
            else
            {
                LockedLevelDoor LevelDoor6454 = new LockedLevelDoor(Game, worldRenderer, this, new Rectangle(22, 432, 32, 100));
                AddGameComponent(LevelDoor6454);
            }

            LevelDoor LevelDoor6900 = new LevelDoor(Game, worldRenderer, this, new Rectangle(269, 19, 100, 32), "World2", "ShrineSanctuaryHall1");
            AddGameComponent(LevelDoor6900);

            TorchLight TorchLight9704 = new TorchLight(Game, worldRenderer, this, new Rectangle(528, 592, 0, 0), 7, 99);
            AddGameComponent(TorchLight9704);
            TorchLight TorchLight5840 = new TorchLight(Game, worldRenderer, this, new Rectangle(26, 594, 0, 0), 7, 99);
            AddGameComponent(TorchLight5840);

            Enemy Enemy9135 = new Enemy(Game, worldRenderer, this, new Rectangle(433, 247, 0, 0), player, 221, 250, Global.GameConstants.ENEMY_ATTACKSPEED, 35, 3, 1);
            AddGameComponent(Enemy9135);
            Enemy Enemy7398 = new Enemy(Game, worldRenderer, this, new Rectangle(432, 401, 0, 0), player, 221, 250, Global.GameConstants.ENEMY_ATTACKSPEED, 35, 3, 1);
            AddGameComponent(Enemy7398);
            Enemy Enemy6132 = new Enemy(Game, worldRenderer, this, new Rectangle(122, 243, 0, 0), player, 221, 250, Global.GameConstants.ENEMY_ATTACKSPEED, 35, 3, 2);
            AddGameComponent(Enemy6132);
            Ammo Ammo2169 = new Ammo(Game, worldRenderer, this, new Rectangle(332, 30, 0, 0));
            AddGameComponent(Ammo2169);
            Ammo Ammo2170 = new Ammo(Game, worldRenderer, this, new Rectangle(270, 596, 0, 0));
            AddGameComponent(Ammo2170);
            Ammo Ammo7651 = new Ammo(Game, worldRenderer, this, new Rectangle(197, 31, 0, 0));
            AddGameComponent(Ammo7651);
            Ammo Ammo3007 = new Ammo(Game, worldRenderer, this, new Rectangle(238, 596, 0, 0));
            AddGameComponent(Ammo3007);
        }
    }
}