﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using SavingTheWorldFromAngriness.Backgrounds;
using SavingTheWorldFromAngriness.World.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavingTheWorldFromAngriness.World.Maps.World2
{
    class HypnosisWalk : BasicMap
    {
        public HypnosisWalk(Game game, WorldRenderer worldRenderer) : base(game, worldRenderer)
        { }

        public override void Initialize()
        {
            base.Initialize();

            player.setPlayerPosition(new Vector2(1181, 885));
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            #region Song
            Song backgroundMusic = Game.Content.Load<Song>("Sounds/World2/Map5");
            PlayBackgroundMusic(backgroundMusic);
            #endregion

            #region Leveldoor
            LevelDoor levelDoorMap = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(375, 360, 50, 50), "World2", "DarkAlley");
            AddGameComponent(levelDoorMap);
            #endregion

            #region Enemies
            Enemy enemy1 = new Enemy(this.Game, worldRenderer, this, new Rectangle(1160, 536, 0, 10), player, 532, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(enemy1);

            Enemy enemy2 = new Enemy(this.Game, worldRenderer, this, new Rectangle(1160, 86, 0, 10), player, 532, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(enemy2);

            Enemy enemy3 = new Enemy(this.Game, worldRenderer, this, new Rectangle(1024, 615, 0, 10), player, 532, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(enemy3);

            Enemy enemy4 = new Enemy(this.Game, worldRenderer, this, new Rectangle(332, 282, 0, 10), player, 532, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(enemy4);

            Enemy enemy5 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(308, 626, 0, 10), player, 532, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 2);
            AddGameComponent(enemy5);

            Enemy enemy7 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(462, 468, 0, 10), player, 532, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 1);
            AddGameComponent(enemy7);

            Enemy enemy8 = new Enemy(this.Game, worldRenderer,
                this, new Rectangle(617, 464, 0, 10), player, 532, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 1);
            AddGameComponent(enemy8);
            #endregion

            #region Ammo
            Ammo ammo1 = new Ammo(this.Game, worldRenderer, this, new Rectangle(1185, 852, 0, 0));
            Ammo ammo2 = new Ammo(this.Game, worldRenderer, this, new Rectangle(1184, 809, 0, 0));
            Ammo ammo3 = new Ammo(this.Game, worldRenderer, this, new Rectangle(1180, 769, 0, 0));
            Ammo ammo4 = new Ammo(this.Game, worldRenderer, this, new Rectangle(1183, 728, 0, 0));
            Ammo ammo5 = new Ammo(this.Game, worldRenderer, this, new Rectangle(1181, 684, 0, 0));
            Ammo ammo6 = new Ammo(this.Game, worldRenderer, this, new Rectangle(1181, 630, 0, 0));

            AddGameComponent(ammo1);
            AddGameComponent(ammo2);
            AddGameComponent(ammo3);
            AddGameComponent(ammo4);
            AddGameComponent(ammo5);
            AddGameComponent(ammo6);
            #endregion

            #region Background
            worldRenderer.SetBackground(new MovingLava(Game, 40, 1), Color.Red);
            #endregion
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

        }
        public override void RemoveAllGameComponents()
        {
            worldRenderer.SetBackground(new MovingClouds(this.Game, 40, 1), Color.CornflowerBlue);

            base.RemoveAllGameComponents();
        }

        public override void OnPlayerDeath()
         {
             worldRenderer.LoadMap("World2", "HypnosisWalk");
            base.OnPlayerDeath();
         }

    }
}
