﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Lighting;
using RoyT.XNA;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SavingTheWorldFromAngriness.World.Components;
using Microsoft.Xna.Framework.Media;
using SavingTheWorldFromAngriness.Backgrounds;
using SavingTheWorldFromAngriness.GUI;
using Microsoft.Xna.Framework.Audio;

namespace SavingTheWorldFromAngriness.World.Maps.World2.Kam2
{
    class Kam2 : BasicMap
    {
        private Texture2D mask;
        private SoundEffect move;
        private string flagItem;

        private Texture2D flagTexture;
        private Vector2 flagPosition;

        private SoundEffect flag;
        private SoundEffect win;

        private SoundEffect talking;
        private Texture2D avatar;

        private Rectangle flagReturnRectangle;

        private bool finished;


        public Kam2(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();

            player.setPlayerPosition(new Vector2(1807, 8440));

            finished = false;
            flagItem = "World2:flag";
            flagPosition = new Vector2(473, 163);
            flagReturnRectangle = new Rectangle(
                1717 + (Global.GameConstants.TILE_SIZE / 2),
                7828 + (Global.GameConstants.TILE_SIZE / 2),
                282,
                248);

            int windowWidth = GraphicsDevice.Viewport.Width;
            int windowHeight = GraphicsDevice.Viewport.Height;

            Vector2 newCameraPosition = -(flagPosition) + (new Vector2(windowWidth / 2, windowHeight / 2));
            int newCameraX = Convert.ToInt32(newCameraPosition.X);
            int newCameraY = Convert.ToInt32(newCameraPosition.Y);

            camera.position = new Vector2(newCameraX, newCameraY);

            finished = true;
            DialogPrompt prompt = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME, avatar,
                "You've come to the right place soldier. Go get that flag.", talking);
            Game.Components.Add(prompt);
            prompt.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
            {
                finished = false;
                Game.Components.Remove(prompt);
            };

            player.SetWalkingSpeed(6);
        }

        public override void OnPlayerDeath()
        {
            //player.setPlayerPosition(new Vector2(1807, 8440));
            Global.Player.INVENTORY.Remove(flagItem);

            worldRenderer.LoadMap("World2", "Kam2");

            base.OnPlayerDeath();
        }

        public override void Update(GameTime gameTime)
        {
            if (finished)
            {
                int windowWidth = GraphicsDevice.Viewport.Width;
                int windowHeight = GraphicsDevice.Viewport.Height;

                Vector2 newCameraPosition = -(flagPosition) + (new Vector2(windowWidth / 2, windowHeight / 2));
                int newCameraX = Convert.ToInt32(newCameraPosition.X);
                int newCameraY = Convert.ToInt32(newCameraPosition.Y);

                camera.position = new Vector2(newCameraX, newCameraY);

                return;
            }

            base.Update(gameTime);

            Rectangle playerTextureSize = player.getPlayerRectangle();
            if (playerTextureSize.Intersects(
                new Rectangle((int)flagPosition.X + (Global.GameConstants.TILE_SIZE / 2),
                (int)flagPosition.Y + (Global.GameConstants.TILE_SIZE / 2), flagTexture.Width, flagTexture.Height)))
            {
                flagPosition = new Vector2(-9999, -9999);
                Global.Player.INVENTORY.Add(flagItem);

                Global.Sounds.SoundManager.PlaySoundEffect(flag);

                List<Enemy> enemies = new List<Enemy>();
                foreach (BasicComponent component in gameComponents)
                {
                    if (component.GetType() == typeof(Enemy))
                    {
                        enemies.Add((Enemy)component);
                    }
                }

                foreach (Enemy enemy in enemies)
                {
                    lock (gameComponents)
                    {
                        RemoveGameComponent(enemy);
                    }
                }

                EnemiesAfterFlag();
            }

            if (playerTextureSize.Intersects(flagReturnRectangle))
            {
                if (Global.Player.INVENTORY.Contains(flagItem))
                {
                    Global.Sounds.SoundManager.PlaySoundEffect(win);

                    List<Enemy> enemies = new List<Enemy>();
                    foreach (BasicComponent component in gameComponents)
                    {
                        if (component.GetType() == typeof(Enemy))
                        {
                            enemies.Add((Enemy)component);
                        }
                    }

                    foreach (Enemy enemy in enemies)
                    {
                        lock (gameComponents)
                        {
                            RemoveGameComponent(enemy);
                        }
                    }

                    worldRenderer.LoadMap("World2", "Mc1");

                    finished = true;
                }
            }
        }

        public void EnemiesAfterFlag()
        {
            Ammo Ammo2028 = new Ammo(Game, worldRenderer, this, new Rectangle(235, 573, 0, 0));
            AddGameComponent(Ammo2028);
            Ammo Ammo4558 = new Ammo(Game, worldRenderer, this, new Rectangle(336, 784, 0, 0));
            AddGameComponent(Ammo4558);
            Ammo Ammo9019 = new Ammo(Game, worldRenderer, this, new Rectangle(590, 1874, 0, 0));
            AddGameComponent(Ammo9019);
            Ammo Ammo5159 = new Ammo(Game, worldRenderer, this, new Rectangle(442, 2264, 0, 0));
            AddGameComponent(Ammo5159);
            Ammo Ammo1775 = new Ammo(Game, worldRenderer, this, new Rectangle(1249, 3938, 0, 0));
            AddGameComponent(Ammo1775);
            Ammo Ammo8602 = new Ammo(Game, worldRenderer, this, new Rectangle(1720, 5327, 0, 0));
            AddGameComponent(Ammo8602);

            Enemy Enemy4977 = new Enemy(Game, worldRenderer, this, new Rectangle(1711, 7184, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 3);
            AddGameComponent(Enemy4977);
            Enemy Enemy8201 = new Enemy(Game, worldRenderer, this, new Rectangle(1679, 6096, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 3);
            AddGameComponent(Enemy8201);
            Enemy Enemy4028 = new Enemy(Game, worldRenderer, this, new Rectangle(1913, 6092, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 3);
            AddGameComponent(Enemy4028);
            Enemy Enemy9507 = new Enemy(Game, worldRenderer, this, new Rectangle(376, 1575, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 3);
            AddGameComponent(Enemy9507);
            Enemy Enemy8513 = new Enemy(Game, worldRenderer, this, new Rectangle(577, 1575, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 3);
            AddGameComponent(Enemy8513);
            Enemy Enemy4864 = new Enemy(Game, worldRenderer, this, new Rectangle(129, 428, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 3);
            AddGameComponent(Enemy4864);
            Enemy Enemy4154 = new Enemy(Game, worldRenderer, this, new Rectangle(1424, 3823, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 1);
            AddGameComponent(Enemy4154);
            Enemy Enemy2119 = new Enemy(Game, worldRenderer, this, new Rectangle(1423, 4062, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 1);
            AddGameComponent(Enemy2119);
            Enemy Enemy3066 = new Enemy(Game, worldRenderer, this, new Rectangle(1029, 3802, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 2);
            AddGameComponent(Enemy3066);
            Enemy Enemy7375 = new Enemy(Game, worldRenderer, this, new Rectangle(1051, 4053, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 2);
            AddGameComponent(Enemy7375);
        }

        public void EnemiesBeforeFlag()
        {
            Ammo Ammo4481 = new Ammo(Game, worldRenderer, this, new Rectangle(556, 2582, 0, 0));
            AddGameComponent(Ammo4481);
            Ammo Ammo1205 = new Ammo(Game, worldRenderer, this, new Rectangle(1713, 5301, 0, 0));
            AddGameComponent(Ammo1205);
            Ammo Ammo3620 = new Ammo(Game, worldRenderer, this, new Rectangle(2065, 6055, 0, 0));
            AddGameComponent(Ammo3620);
            Ammo Ammo2773 = new Ammo(Game, worldRenderer, this, new Rectangle(1920, 7713, 0, 0));
            AddGameComponent(Ammo2773);
            Ammo Ammo6210 = new Ammo(Game, worldRenderer, this, new Rectangle(1684, 7719, 0, 0));
            AddGameComponent(Ammo6210);
            Ammo Ammo5507 = new Ammo(Game, worldRenderer, this, new Rectangle(2138, 8047, 0, 0));
            AddGameComponent(Ammo5507);
            Ammo Ammo7842 = new Ammo(Game, worldRenderer, this, new Rectangle(1604, 4072, 0, 0));
            AddGameComponent(Ammo7842);

            Enemy Enemy4977 = new Enemy(Game, worldRenderer, this, new Rectangle(1711, 7184, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 0);
            AddGameComponent(Enemy4977);
            Enemy Enemy8201 = new Enemy(Game, worldRenderer, this, new Rectangle(1679, 6096, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 0);
            AddGameComponent(Enemy8201);
            Enemy Enemy4028 = new Enemy(Game, worldRenderer, this, new Rectangle(1913, 6092, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 0);
            AddGameComponent(Enemy4028);
            Enemy Enemy9507 = new Enemy(Game, worldRenderer, this, new Rectangle(376, 1575, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 0);
            AddGameComponent(Enemy9507);
            Enemy Enemy8513 = new Enemy(Game, worldRenderer, this, new Rectangle(577, 1575, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 0);
            AddGameComponent(Enemy8513);
            Enemy Enemy9824 = new Enemy(Game, worldRenderer, this, new Rectangle(133, 154, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 0);
            AddGameComponent(Enemy9824);
            Enemy Enemy4864 = new Enemy(Game, worldRenderer, this, new Rectangle(129, 428, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 0);
            AddGameComponent(Enemy4864);
            Enemy Enemy4154 = new Enemy(Game, worldRenderer, this, new Rectangle(1424, 3823, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 1);
            AddGameComponent(Enemy4154);
            Enemy Enemy2119 = new Enemy(Game, worldRenderer, this, new Rectangle(1423, 4062, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 1);
            AddGameComponent(Enemy2119);
            Enemy Enemy3066 = new Enemy(Game, worldRenderer, this, new Rectangle(1029, 3802, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 2);
            AddGameComponent(Enemy3066);
            Enemy Enemy7375 = new Enemy(Game, worldRenderer, this, new Rectangle(1051, 4053, 0, 0), player, 397, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 2);
            AddGameComponent(Enemy7375);
        }

        public override void RemoveAllGameComponents()
        {
            Global.Player.INVENTORY.Remove(flagItem);

            base.RemoveAllGameComponents();
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            mask = Game.Content.Load<Texture2D>("Textures/World2/Kam2/mask");
            move = Game.Content.Load<SoundEffect>("Sounds/World2/Kam2/move");

            flagTexture = Game.Content.Load<Texture2D>("Textures/World2/Kam2/flag");

            Song backgroundSong = Game.Content.Load<Song>("Sounds/World2/Kam2/winterMatch");
            Global.Sounds.SoundManager.PlayBackgroundMusic(backgroundSong);
            talking = Game.Content.Load<SoundEffect>("Sounds/World2/Kam2/talk");
            avatar = Game.Content.Load<Texture2D>("Textures/World2/Kam2/avatar");

            flag = Game.Content.Load<SoundEffect>("Sounds/World2/Kam2/flag");
            win = Game.Content.Load<SoundEffect>("Sounds/World2/Kam2/win");

            worldRenderer.SetBackground(new Backgrounds.BasicBackground(this.Game), Color.Black);

            Global.Sounds.SoundManager.PlaySoundEffect(move);

            worldRenderer.AddBackgroundMask(mask);

            EnemiesBeforeFlag();

            Vendor Vendor1687 = new Vendor(Game, worldRenderer, this, new Rectangle(1779, 7838, 100, 100));
            AddGameComponent(Vendor1687);
        }

        public override void DrawBehindShadows()
        {
            //base.DrawBehindShadows();
        }

        public override void Draw(GameTime gameTime)
        {
            base.DrawBehindShadows();

            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                SamplerState.LinearWrap,
                null,
                null,
                null,
                camera.TransformMatrix);
            spriteBatch.Draw(flagTexture, flagPosition, Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
