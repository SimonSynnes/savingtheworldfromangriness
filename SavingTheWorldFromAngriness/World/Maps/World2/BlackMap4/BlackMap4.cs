﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.GUI;

using Lighting;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace SavingTheWorldFromAngriness.World.Maps.World2.BlackMap4
{
    class BlackMap4 : BasicMap
    {
        private PointLight playerLight;

        public BlackMap4(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();

            Global.GameState.IS_RENDERER_MAZE = true;
        }

        public override void Update(GameTime gameTime)
        {
            playerLight.Position = player.getPlayerPosition();

            base.Update(gameTime);
        }

        public override void RemoveAllGameComponents()
        {
            Global.GameState.IS_RENDERER_MAZE = false;

            //SoundManager.StopBackgroundMusic();
            SoundManager.ClearRepeatingSoundEffects();

            foreach (IGameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).RemoveAllGameComponents();
                Game.Components.Remove(gameComponent);
            }

            Game.Components.Remove(player);

            PlayerUI.ClearUI();
            Game.Components.Remove(PlayerUI);

            if (!isPlayerDead)
            {
                Global.Player.LAST_MAP = this.GetType();
            }
        }

        protected override void LoadContent()
        {
            worldRenderer.SetBackground(new Backgrounds.BasicBackground(this.Game), Color.Black);

            playerLight = new PointLight(worldRenderer.lightingManager.lightEffect, new Vector2(0, 0), 1200f, Color.White, 1f);
            worldRenderer.lightingManager.AddLight(playerLight);

            LevelDoor LevelDoor2201 = new LevelDoor(Game, worldRenderer, this, new Rectangle(188, 255, 35, 100), "World2", "HypnosisWalk");
            AddGameComponent(LevelDoor2201);

            base.LoadContent();

            if (Global.Player.LAST_MAP == typeof(World2.BlackMap3.BlackMap3))
            {
                player.setPlayerPosition(new Vector2(1344, 1785));
            }
            else
            {
                player.setPlayerPosition(new Vector2(1344, 1785));
                Song backgroundSong = Game.Content.Load<Song>("Sounds/World1/Ink/heathen");
                SoundManager.PlayBackgroundMusic(backgroundSong);
            }

            Enemy Enemy9385 = new Enemy(Game, worldRenderer, this, new Rectangle(1249, 1346, 0, 0), player, 507, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 1);
            AddGameComponent(Enemy9385);
            Enemy Enemy1098 = new Enemy(Game, worldRenderer, this, new Rectangle(1443, 1262, 0, 0), player, 507, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 1);
            AddGameComponent(Enemy1098);
            Enemy Enemy1310 = new Enemy(Game, worldRenderer, this, new Rectangle(1643, 1189, 0, 0), player, 507, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 1);
            AddGameComponent(Enemy1310);
            Enemy Enemy9543 = new Enemy(Game, worldRenderer, this, new Rectangle(1665, 1312, 0, 0), player, 507, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 1);
            AddGameComponent(Enemy9543);
            Enemy Enemy7690 = new Enemy(Game, worldRenderer, this, new Rectangle(1152, 634, 0, 0), player, 507, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 1);
            AddGameComponent(Enemy7690);
            Enemy Enemy3264 = new Enemy(Game, worldRenderer, this, new Rectangle(551, 934, 0, 0), player, 507, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 1);
            AddGameComponent(Enemy3264);
            Enemy Enemy8325 = new Enemy(Game, worldRenderer, this, new Rectangle(471, 892, 0, 0), player, 507, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 1);
            AddGameComponent(Enemy8325);
            Enemy Enemy7452 = new Enemy(Game, worldRenderer, this, new Rectangle(773, 980, 0, 0), player, 507, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 1);
            AddGameComponent(Enemy7452);
            Enemy Enemy6369 = new Enemy(Game, worldRenderer, this, new Rectangle(811, 446, 0, 0), player, 507, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 1);
            AddGameComponent(Enemy6369);
            Enemy Enemy7793 = new Enemy(Game, worldRenderer, this, new Rectangle(505, 461, 0, 0), player, 507, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 1);
            AddGameComponent(Enemy7793);
            Enemy Enemy7139 = new Enemy(Game, worldRenderer, this, new Rectangle(1616, 778, 0, 0), player, 507, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 0);
            AddGameComponent(Enemy7139);
            Enemy Enemy7527 = new Enemy(Game, worldRenderer, this, new Rectangle(1554, 931, 0, 0), player, 507, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 0);
            AddGameComponent(Enemy7527);
            Enemy Enemy2839 = new Enemy(Game, worldRenderer, this, new Rectangle(1566, 579, 0, 0), player, 507, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 0);
            AddGameComponent(Enemy2839);
            Enemy Enemy9839 = new Enemy(Game, worldRenderer, this, new Rectangle(996, 1382, 0, 0), player, 507, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 0);
            AddGameComponent(Enemy9839);
            Enemy Enemy5195 = new Enemy(Game, worldRenderer, this, new Rectangle(1065, 872, 0, 0), player, 507, 250, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy5195);
            Enemy Enemy8173 = new Enemy(Game, worldRenderer, this, new Rectangle(1087, 1063, 0, 0), player, 507, 250, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy8173);
            Enemy Enemy2328 = new Enemy(Game, worldRenderer, this, new Rectangle(694, 1276, 0, 0), player, 507, 250, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy2328);
            Enemy Enemy7741 = new Enemy(Game, worldRenderer, this, new Rectangle(547, 1234, 0, 0), player, 507, 250, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy7741);
            Enemy Enemy9092 = new Enemy(Game, worldRenderer, this, new Rectangle(679, 662, 0, 0), player, 507, 250, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy9092);
            Enemy Enemy9532 = new Enemy(Game, worldRenderer, this, new Rectangle(280, 690, 0, 0), player, 507, 250, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy9532);
            Enemy Enemy3993 = new Enemy(Game, worldRenderer, this, new Rectangle(507, 199, 0, 0), player, 507, 250, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy3993);
            Enemy Enemy4826 = new Enemy(Game, worldRenderer, this, new Rectangle(332, 266, 0, 0), player, 507, 250, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy4826);
            Enemy Enemy8784 = new Enemy(Game, worldRenderer, this, new Rectangle(1196, 337, 0, 0), player, 507, 250, Global.GameConstants.ENEMY_ATTACKSPEED, 1, 4, 2);
            AddGameComponent(Enemy8784);

            Ammo Ammo2370 = new Ammo(Game, worldRenderer, this, new Rectangle(1389, 1640, 0, 0));
            AddGameComponent(Ammo2370);
            Ammo Ammo5087 = new Ammo(Game, worldRenderer, this, new Rectangle(1497, 1636, 0, 0));
            AddGameComponent(Ammo5087);
            Ammo Ammo6364 = new Ammo(Game, worldRenderer, this, new Rectangle(1509, 1693, 0, 0));
            AddGameComponent(Ammo6364);
            Ammo Ammo3427 = new Ammo(Game, worldRenderer, this, new Rectangle(1411, 1715, 0, 0));
            AddGameComponent(Ammo3427);
            Ammo Ammo7825 = new Ammo(Game, worldRenderer, this, new Rectangle(1365, 1749, 0, 0));
            AddGameComponent(Ammo7825);
            Ammo Ammo3044 = new Ammo(Game, worldRenderer, this, new Rectangle(1459, 1797, 0, 0));
            AddGameComponent(Ammo3044);
            Ammo Ammo5918 = new Ammo(Game, worldRenderer, this, new Rectangle(1353, 1822, 0, 0));
            AddGameComponent(Ammo5918);
            Ammo Ammo6188 = new Ammo(Game, worldRenderer, this, new Rectangle(1300, 1699, 0, 0));
            AddGameComponent(Ammo6188);
            Ammo Ammo9637 = new Ammo(Game, worldRenderer, this, new Rectangle(1252, 1768, 0, 0));
            AddGameComponent(Ammo9637);
            Ammo Ammo5052 = new Ammo(Game, worldRenderer, this, new Rectangle(1269, 1616, 0, 0));
            AddGameComponent(Ammo5052);
            Ammo Ammo6351 = new Ammo(Game, worldRenderer, this, new Rectangle(1405, 1555, 0, 0));
            AddGameComponent(Ammo6351);
            Ammo Ammo8948 = new Ammo(Game, worldRenderer, this, new Rectangle(1242, 1532, 0, 0));
            AddGameComponent(Ammo8948);
            Ammo Ammo8949 = new Ammo(Game, worldRenderer, this, new Rectangle(1210, 1510, 0, 0));
            AddGameComponent(Ammo8949);
            Ammo Ammo8950 = new Ammo(Game, worldRenderer, this, new Rectangle(1210, 1512, 0, 0));
            AddGameComponent(Ammo8950);
            Ammo Ammo8951 = new Ammo(Game, worldRenderer, this, new Rectangle(1210, 1513, 0, 0));
            AddGameComponent(Ammo8951);
            Ammo Ammo8952 = new Ammo(Game, worldRenderer, this, new Rectangle(1210, 1513, 0, 0));
            AddGameComponent(Ammo8952);
            Ammo Ammo8953 = new Ammo(Game, worldRenderer, this, new Rectangle(1210, 1517, 0, 0));
            AddGameComponent(Ammo8953);
            Ammo Ammo8954 = new Ammo(Game, worldRenderer, this, new Rectangle(1210, 1514, 0, 0));
            AddGameComponent(Ammo8954);
            Ammo Ammo8957 = new Ammo(Game, worldRenderer, this, new Rectangle(1210, 1514, 0, 0));
            AddGameComponent(Ammo8957);
            Ammo Ammo8958 = new Ammo(Game, worldRenderer, this, new Rectangle(1210, 1514, 0, 0));
            AddGameComponent(Ammo8958);
            Ammo Ammo8959 = new Ammo(Game, worldRenderer, this, new Rectangle(1210, 1514, 0, 0));
            AddGameComponent(Ammo8959);

            player.SetWalkingSpeed(2);

            Vendor Vendor1687 = new Vendor(Game, worldRenderer, this, new Rectangle(989, 222, 100, 100));
            AddGameComponent(Vendor1687);
        }

        public override void OnPlayerDeath()
        {
            worldRenderer.LoadMap("World2", "BlackMap4");
            base.OnPlayerDeath();
        }
    }
}
