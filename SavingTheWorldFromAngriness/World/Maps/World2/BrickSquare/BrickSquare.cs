﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using SavingTheWorldFromAngriness.World.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavingTheWorldFromAngriness.World.Maps.World2
{
    class BrickSquare : BasicMap
    {
        public BrickSquare(Game game, WorldRenderer worldRenderer) : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();
            player.setPlayerPosition(new Vector2(303, 720));

            worldRenderer.SetBackground(new Backgrounds.BasicBackground(this.Game), Color.Black);
        }
        protected override void LoadContent()
        {
            base.LoadContent();

            #region Song
            Song backgroundMusic = Game.Content.Load<Song>("Sounds/World2/gametime");
            PlayBackgroundMusic(backgroundMusic);
            #endregion

            #region Leveldoor
            LevelDoor LevelDoor3414 = new LevelDoor(Game, worldRenderer, this, 
                new Rectangle(328, 9, 50, 50), "World2", "Disco");
            AddGameComponent(LevelDoor3414);
            #endregion

            #region Ammo
            Ammo Ammo2638 = new Ammo(Game, worldRenderer, this, new Rectangle(167, 693, 0, 0));
            AddGameComponent(Ammo2638);
            Ammo Ammo7490 = new Ammo(Game, worldRenderer, this, new Rectangle(252, 629, 0, 0));
            AddGameComponent(Ammo7490);
            Ammo Ammo9682 = new Ammo(Game, worldRenderer, this, new Rectangle(381, 621, 0, 0));
            AddGameComponent(Ammo9682);
            Ammo Ammo9676 = new Ammo(Game, worldRenderer, this, new Rectangle(48, 693, 0, 0));
            AddGameComponent(Ammo9676);

            #endregion

            #region Enemies
            Enemy Enemy6047 = new Enemy(Game, worldRenderer, this, new Rectangle(176, 410, 0, 0), player, 417, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 2);
            AddGameComponent(Enemy6047);
            Enemy Enemy5687 = new Enemy(Game, worldRenderer, this, new Rectangle(495, 420, 0, 0), player, 417, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 2);
            AddGameComponent(Enemy5687);
            Enemy Enemy4208 = new Enemy(Game, worldRenderer, this, new Rectangle(553, 180, 0, 0), player, 417, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 2);
            AddGameComponent(Enemy4208);
            Enemy Enemy8657 = new Enemy(Game, worldRenderer, this, new Rectangle(161, 179, 0, 0), player, 417, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 2);
            AddGameComponent(Enemy8657);
            Enemy Enemy6444 = new Enemy(Game, worldRenderer, this, new Rectangle(538, 610, 0, 0), player, 417, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 2);
            AddGameComponent(Enemy6444);
            Enemy Enemy6080 = new Enemy(Game, worldRenderer, this, new Rectangle(-258, 97, 0, 0), player, 417, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 2);
            AddGameComponent(Enemy6080);
            #endregion

            #region Teleport
            Teleport Teleport7186 = new Teleport(Game, worldRenderer, this, 
                new Rectangle(107, 19, 0, 0), 303, 720);
            AddGameComponent(Teleport7186);
            #endregion

            #region HP
            HealthPack HealthPack1079 = new HealthPack(Game, worldRenderer, this, new Rectangle(710, 331, 0, 0));
            AddGameComponent(HealthPack1079);
            HealthPack HealthPack7504 = new HealthPack(Game, worldRenderer, this, new Rectangle(35, 114, 0, 0));
            AddGameComponent(HealthPack7504);
            #endregion

        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void OnPlayerDeath()
        {
            worldRenderer.LoadMap("World2", "BrickSquare");
            base.OnPlayerDeath();
        }

    }
}
