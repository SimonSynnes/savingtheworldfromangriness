﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global;
using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.World.Maps;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.World.Maps.World2.ShrineSanctuaryRoom3
{
    class ShrineSanctuaryRoom3 : BasicMap
    {
        public ShrineSanctuaryRoom3(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();

            worldRenderer.SetBackground(new Backgrounds.BasicBackground(Game), new Color(17, 17, 17));
        }

        public override void RemoveAllGameComponents()
        {
            SoundManager.ClearRepeatingSoundEffects();

            foreach (IGameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).RemoveAllGameComponents();
                Game.Components.Remove(gameComponent);
            }

            Game.Components.Remove(player);

            PlayerUI.ClearUI();
            Game.Components.Remove(PlayerUI);

            if (!isPlayerDead)
            {
                Global.Player.LAST_MAP = this.GetType();
            }
        }

        public override void OnPlayerDeath()
        {
            base.OnPlayerDeath();

            worldRenderer.LoadMap("World2", "ShrineSanctuaryRoom3");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            if (Global.Player.LAST_MAP == typeof(World2.ShrineSanctuaryRoom2.ShrineSanctuaryRoom2))
            {
                player.setPlayerPosition(new Vector2(687, 263));
            }
            else
            if (Global.Player.LAST_MAP == typeof(World2.ShrineSanctuaryHall3.ShrineSanctuaryHall3))
            {
                player.setPlayerPosition(new Vector2(105, 535));
            }
            else
            {
                player.setPlayerPosition(new Vector2(687, 263));
                Song backgroundSong = Game.Content.Load<Song>("Sounds/World2/Shrine/silentLight");
                SoundManager.PlayBackgroundMusic(backgroundSong);
            }

            LevelDoor LevelDoor6372 = new LevelDoor(Game, worldRenderer, this, new Rectangle(77, 556, 100, 32), "World2", "ShrineSanctuaryHall3");
            AddGameComponent(LevelDoor6372);
            LevelDoor LevelDoor4526 = new LevelDoor(Game, worldRenderer, this, new Rectangle(714, 236, 32, 100), "World2", "ShrineSanctuaryRoom2");
            AddGameComponent(LevelDoor4526);

            Enemy Enemy6523 = new Enemy(Game, worldRenderer, this, new Rectangle(113, 358, 0, 0), player, 282, 150, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 0);
            AddGameComponent(Enemy6523);
            Enemy Enemy7500 = new Enemy(Game, worldRenderer, this, new Rectangle(218, 361, 0, 0), player, 282, 150, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 0);
            AddGameComponent(Enemy7500);

            Enemy Enemy6559 = new Enemy(Game, worldRenderer, this, new Rectangle(302, 365, 0, 0), player, 282, 230, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 0);
            AddGameComponent(Enemy6559);
            Enemy Enemy1590 = new Enemy(Game, worldRenderer, this, new Rectangle(113, 176, 0, 0), player, 282, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 0);
            AddGameComponent(Enemy1590);
            Enemy Enemy1483 = new Enemy(Game, worldRenderer, this, new Rectangle(240, 230, 0, 0), player, 282, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 0);
            AddGameComponent(Enemy1483);
            Enemy Enemy7907 = new Enemy(Game, worldRenderer, this, new Rectangle(318, 191, 0, 0), player, 282, 150, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 0);
            AddGameComponent(Enemy7907);
            Enemy Enemy4313 = new Enemy(Game, worldRenderer, this, new Rectangle(400, 297, 0, 0), player, 282, 230, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 0);
            AddGameComponent(Enemy4313);
            Enemy Enemy6094 = new Enemy(Game, worldRenderer, this, new Rectangle(430, 209, 0, 0), player, 282, 230, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 3, 0);
            AddGameComponent(Enemy6094);
            Ammo Ammo6802 = new Ammo(Game, worldRenderer, this, new Rectangle(644, 106, 0, 0));
            AddGameComponent(Ammo6802);
            Ammo Ammo7213 = new Ammo(Game, worldRenderer, this, new Rectangle(647, 202, 0, 0));
            AddGameComponent(Ammo7213);
            Ammo Ammo6156 = new Ammo(Game, worldRenderer, this, new Rectangle(652, 279, 0, 0));
            AddGameComponent(Ammo6156);
            Ammo Ammo9121 = new Ammo(Game, worldRenderer, this, new Rectangle(654, 361, 0, 0));
            AddGameComponent(Ammo9121);
            Ammo Ammo1900 = new Ammo(Game, worldRenderer, this, new Rectangle(196, 538, 0, 0));
            AddGameComponent(Ammo1900);
            Ammo Ammo1646 = new Ammo(Game, worldRenderer, this, new Rectangle(317, 539, 0, 0));
            AddGameComponent(Ammo1646);
            Ammo Ammo5301 = new Ammo(Game, worldRenderer, this, new Rectangle(445, 532, 0, 0));
            AddGameComponent(Ammo5301);
            Ammo Ammo4511 = new Ammo(Game, worldRenderer, this, new Rectangle(536, 536, 0, 0));
            AddGameComponent(Ammo4511);
            Ammo Ammo45121 = new Ammo(Game, worldRenderer, this, new Rectangle(547, 536, 0, 0));
            AddGameComponent(Ammo45121);
            HealthPack HealthPack7749 = new HealthPack(Game, worldRenderer, this, new Rectangle(362, 528, 0, 0));
            AddGameComponent(HealthPack7749);
            HealthPack HealthPack5343 = new HealthPack(Game, worldRenderer, this, new Rectangle(264, 501, 0, 0));
            AddGameComponent(HealthPack5343);
            HealthPack HealthPack3499 = new HealthPack(Game, worldRenderer, this, new Rectangle(686, 360, 0, 0));
            AddGameComponent(HealthPack3499);
            HealthPack HealthPack9488 = new HealthPack(Game, worldRenderer, this, new Rectangle(568, 212, 0, 0));
            AddGameComponent(HealthPack9488);
            HealthPack HealthPack2230 = new HealthPack(Game, worldRenderer, this, new Rectangle(146, 63, 0, 0));
            AddGameComponent(HealthPack2230);
            HealthPack HealthPack8621 = new HealthPack(Game, worldRenderer, this, new Rectangle(87, 49, 0, 0));
            AddGameComponent(HealthPack8621);
            HealthPack HealthPack2129 = new HealthPack(Game, worldRenderer, this, new Rectangle(394, 67, 0, 0));
            AddGameComponent(HealthPack2129);
        }
    }
}