﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global;
using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.World.Maps;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SavingTheWorldFromAngriness.GUI;

namespace SavingTheWorldFromAngriness.World.Maps.World2.ShrineSanctuaryHall3
{
    class ShrineSanctuaryHall3 : BasicMap
    {
        private Texture2D keyTexture;
        private Vector2 keyPosition;
        private SoundEffect keySoundEffect;
        #region UI
        private Texture2D playerAvatarTexture;
        private SoundEffect promptSoundEffect1;
        #endregion

        public ShrineSanctuaryHall3(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();

            worldRenderer.SetBackground(new Backgrounds.BasicBackground(Game), new Color(17, 17, 17));

            if (Global.Player.INVENTORY.Contains("World2:Shrine:Key2"))
            {
                keyPosition = new Vector2(9999, 56499);
            }
            else
            {
                keyPosition = new Vector2(1381, 122);
            }
        }

        public override void RemoveAllGameComponents()
        {
            SoundManager.ClearRepeatingSoundEffects();

            foreach (IGameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).RemoveAllGameComponents();
                Game.Components.Remove(gameComponent);
            }

            Game.Components.Remove(player);

            PlayerUI.ClearUI();
            Game.Components.Remove(PlayerUI);

            if (!isPlayerDead)
            {
                Global.Player.LAST_MAP = this.GetType();
            }
        }

        public override void OnPlayerDeath()
        {
            base.OnPlayerDeath();

            worldRenderer.LoadMap("World2", "ShrineSanctuaryHall3");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (player.getPlayerRectangle().Intersects(
                new Rectangle((int)keyPosition.X + (Global.GameConstants.TILE_SIZE / 2),
                (int)keyPosition.Y + (Global.GameConstants.TILE_SIZE / 2), keyTexture.Width, keyTexture.Height)))
            {
                Global.Player.INVENTORY.Add("World2:Shrine:Key2");
                if (Global.Player.INVENTORY.Contains("World2:Shrine:Key2"))
                {
                    Global.GameState.IS_GAME_PAUSED = true;

                    DialogPrompt foundKeyMessage = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME,
                        playerAvatarTexture, "Finally I can get out of here.",
                        promptSoundEffect1);
                    Game.Components.Add(foundKeyMessage);

                    foundKeyMessage.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
                    {
                        Global.GameState.IS_GAME_PAUSED = false;

                        Game.Components.Remove(foundKeyMessage);
                    };
                }
                keyPosition = new Vector2(9999, 56499);

                Global.Sounds.SoundManager.PlaySoundEffect(keySoundEffect);
            }
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            LevelDoor LevelDoor6077 = new LevelDoor(Game, worldRenderer, this, new Rectangle(83, 19, 100, 32), "World2", "ShrineSanctuaryRoom3");
            AddGameComponent(LevelDoor6077);

            keyTexture = Game.Content.Load<Texture2D>("Textures/World2/ShrineSanctuary/key");
            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");
            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");
            keySoundEffect = Game.Content.Load<SoundEffect>("Sounds/World1/pop");

            if (Global.Player.LAST_MAP == typeof(World2.ShrineSanctuaryHall3.ShrineSanctuaryHall3))
            {
                player.setPlayerPosition(new Vector2(113, 49));
            }
            else
            {
                player.setPlayerPosition(new Vector2(113, 49));
                Song backgroundSong = Game.Content.Load<Song>("Sounds/World2/Shrine/silentLight");
                SoundManager.PlayBackgroundMusic(backgroundSong);
            }

            Enemy Enemy6633 = new Enemy(Game, worldRenderer, this, new Rectangle(667, 120, 0, 0), player, 296, 150, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 1);
            AddGameComponent(Enemy6633);
            Enemy Enemy2505 = new Enemy(Game, worldRenderer, this, new Rectangle(852, 120, 0, 0), player, 296, 150, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 2);
            AddGameComponent(Enemy2505);
            Enemy Enemy7373 = new Enemy(Game, worldRenderer, this, new Rectangle(750, 194, 0, 0), player, 296, 150, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 0);
            AddGameComponent(Enemy7373);
            Enemy Enemy1766 = new Enemy(Game, worldRenderer, this, new Rectangle(750, 50, 0, 0), player, 296, 150, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 4, 3);
            AddGameComponent(Enemy1766);

            Ammo Ammo2729 = new Ammo(Game, worldRenderer, this, new Rectangle(156, 104, 0, 0));
            AddGameComponent(Ammo2729);
            Ammo Ammo5142 = new Ammo(Game, worldRenderer, this, new Rectangle(53, 156, 0, 0));
            AddGameComponent(Ammo5142);
            Ammo Ammo5143 = new Ammo(Game, worldRenderer, this, new Rectangle(75, 180, 0, 0));
            AddGameComponent(Ammo5143);
        }

        public override void DrawBehindShadows()
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            spriteBatch.Draw(keyTexture, keyPosition, Color.White);

            spriteBatch.End();

            base.DrawBehindShadows();
        }
    }
}