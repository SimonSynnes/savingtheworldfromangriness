﻿using Microsoft.Xna.Framework;
using SavingTheWorldFromAngriness.World.Components;
using Microsoft.Xna.Framework.Media;

namespace SavingTheWorldFromAngriness.World.Maps.World2.OutsideGarden2
{
    class OutsideGarden2 : BasicMap
    {
        #region Water Rectangles
        private Rectangle waterTopLeftRectangle;
        private Rectangle waterBottomLeftRectangle;
        private Rectangle waterTopRightRectangle;
        private Rectangle waterMiddleRightRectangle;
        private Rectangle waterBottomRightRectangle;
        #endregion

        #region Vendor Rectangle
        private Rectangle vendorRectangle;
        private Rectangle vendorShoppingRectangle;
        #endregion

        public OutsideGarden2(Game game, WorldRenderer worldRenderer)
        : base(game, worldRenderer)
        {
            #region Water Rectangles
            this.waterTopLeftRectangle = new Rectangle(16, 111, 8 * 32, 4 * 32);
            this.waterTopRightRectangle = new Rectangle(656, 111, 8 * 32, 4 * 32);
            this.waterMiddleRightRectangle = new Rectangle(656, 400, 8 * 32, 4 * 32);
            this.waterBottomLeftRectangle = new Rectangle(16, 688, 8 * 32, 4 * 32);
            this.waterBottomRightRectangle = new Rectangle(656, 688, 8 * 32, 4 * 32);
            #endregion

            #region Vendor Rectangle
            this.vendorRectangle = new Rectangle(427,190,100,100);
            this.vendorShoppingRectangle = new Rectangle(443,200,70,40);
            #endregion
        }

        public override void Initialize()
        {
            base.Initialize();

            #region Background
            worldRenderer.SetBackground(new Backgrounds.MovingClouds(this.Game, 40, 1), Color.CornflowerBlue);
            #endregion

            #region Player start position
            if (Global.Player.LAST_MAP == typeof(World2.OutsideGarden3.OutsideGarden3))
            {
                player.setPlayerPosition(new Vector2(40, 460));
            }
            else
            {
                player.setPlayerPosition(new Vector2(465, 856));
            }
            #endregion

        }

        protected override void LoadContent()
        {
            base.LoadContent();

            #region Sounds
            Song backgroundMusic = Game.Content.Load<Song>("Sounds/World2/OutsideGarden/OutsideGarden");
            PlayBackgroundMusic(backgroundMusic);
            #endregion

            #region Healthpacks
            HealthPack HealthPack1606 = new HealthPack(Game, worldRenderer, this, new Rectangle(17, 838, 0, 0));
            AddGameComponent(HealthPack1606);
            HealthPack HealthPack2944 = new HealthPack(Game, worldRenderer, this, new Rectangle(872, 830, 0, 0));
            AddGameComponent(HealthPack2944);
            HealthPack HealthPack6057 = new HealthPack(Game, worldRenderer, this, new Rectangle(20, 72, 0, 0));
            AddGameComponent(HealthPack6057);
            #endregion

            #region LevelDoors
            LevelDoor EnterDoor = new LevelDoor(Game, worldRenderer, this, new Rectangle(431, 880, 64, 32), "World2", "OutsideGarden");
            AddGameComponent(EnterDoor);
            LevelDoor ExitDoor = new LevelDoor(Game, worldRenderer, this, new Rectangle(16, 438, 32, 64), "World2", "OutsideGarden3");
            AddGameComponent(ExitDoor);
            #endregion

            #region Enemies
            int hp = 327;
            int aggro = 170;
            int attackSpeed = Global.GameConstants.ENEMY_ATTACKSPEED;
            int attackPower = 10;
            int walkingSpeed = 3;

            int lookingRight = 2;
            int lookingLeft = 1;
            int lookingDown = 0;
            int lookingUp = 3;

            Enemy Enemy9797 = new Enemy(Game, worldRenderer, this, new Rectangle(303, 338, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingDown);
            AddGameComponent(Enemy9797);
            Enemy Enemy6667 = new Enemy(Game, worldRenderer, this, new Rectangle(593, 338, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingDown);
            AddGameComponent(Enemy6667);
            Enemy EnemyMiddle = new Enemy(Game, worldRenderer, this, new Rectangle(442, 350, 0, 0), player, hp, aggro + 20, attackSpeed, attackPower, walkingSpeed, lookingDown);
            AddGameComponent(EnemyMiddle);


            Enemy Enemy2185 = new Enemy(Game, worldRenderer, this, new Rectangle(290, 545, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingUp);
            AddGameComponent(Enemy2185);
            Enemy Enemy9119 = new Enemy(Game, worldRenderer, this, new Rectangle(610, 545, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingUp);
            AddGameComponent(Enemy9119);

            Enemy Enemy6561 = new Enemy(Game, worldRenderer, this, new Rectangle(280, 694, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingRight);
            AddGameComponent(Enemy6561);
            Enemy Enemy4307 = new Enemy(Game, worldRenderer, this, new Rectangle(600, 694, 0, 0), player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingLeft);
            AddGameComponent(Enemy4307);

            #endregion

            #region Vendor
            Vendor vendor = new Vendor(Game, worldRenderer, this, vendorRectangle);
            AddGameComponent(vendor);
            #endregion

            #region Ammo
            Ammo Ammo5475 = new Ammo(Game, worldRenderer, this, new Rectangle(23, 233, 0, 0));
            AddGameComponent(Ammo5475);
            Ammo Ammo4684 = new Ammo(Game, worldRenderer, this, new Rectangle(23, 524+(4*32), 0, 0));
            AddGameComponent(Ammo4684);
            Ammo Ammo3671 = new Ammo(Game, worldRenderer, this, new Rectangle(852, 362, 0, 0));
            AddGameComponent(Ammo3671);
            Ammo Ammo3672 = new Ammo(Game, worldRenderer, this, new Rectangle(440, 856, 0, 0));
            AddGameComponent(Ammo3672);
            Ammo Ammo3673 = new Ammo(Game, worldRenderer, this, new Rectangle(490, 856, 0, 0));
            AddGameComponent(Ammo3673);
            #endregion

            #region Collisions at Water Rectangles
            worldRenderer.addMiscCollision(waterTopLeftRectangle);
            worldRenderer.addMiscCollision(waterTopRightRectangle);
            worldRenderer.addMiscCollision(waterMiddleRightRectangle);
            worldRenderer.addMiscCollision(waterBottomLeftRectangle);
            worldRenderer.addMiscCollision(waterBottomRightRectangle);
            worldRenderer.addMiscCollision(vendorShoppingRectangle);



            #endregion
        }

        public override void OnPlayerDeath()
        {
            worldRenderer.LoadMap("World2", "OutsideGarden2");
            base.OnPlayerDeath();
        }
    }
}
