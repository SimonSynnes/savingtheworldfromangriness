﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using SavingTheWorldFromAngriness.Backgrounds;
using SavingTheWorldFromAngriness.World.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavingTheWorldFromAngriness.World.Maps.World2
{
    class PreBossBridge : BasicMap
    {
        public PreBossBridge(Game game, WorldRenderer worldRenderer) : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();
            player.setPlayerPosition(new Vector2(380, 725));
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            #region Background 
            worldRenderer.SetBackground(new MovingLava(Game, 40, 1), Color.Red);
            #endregion

            #region Enemies
            Enemy Enemy8337 = new Enemy(Game, worldRenderer, this, new Rectangle(290, 449, 0, 0), player, 587, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(Enemy8337);
            Enemy Enemy9510 = new Enemy(Game, worldRenderer, this, new Rectangle(444, 262, 0, 0), player, 587, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 1);
            AddGameComponent(Enemy9510);
            Enemy Enemy4580 = new Enemy(Game, worldRenderer, this, new Rectangle(461, 60, 0, 0), player, 587, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(Enemy4580);
            Enemy Enemy8907 = new Enemy(Game, worldRenderer, this, new Rectangle(265, 77, 0, 0), player, 587, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 1);
            AddGameComponent(Enemy8907);
            Enemy Enemy5146 = new Enemy(Game, worldRenderer, this, new Rectangle(292, 205, 0, 0), player, 587, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 2);
            AddGameComponent(Enemy5146);
            Enemy Enemy7164 = new Enemy(Game, worldRenderer, this, new Rectangle(-158, 55, 0, 0), player, 587, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 2, 1);
            AddGameComponent(Enemy7164);
            #endregion

            #region TorchLight
            TorchLight TorchLight2111 = new TorchLight(Game, worldRenderer, this, new Rectangle(215, 175, 0, 0), 3333, 3333);
            AddGameComponent(TorchLight2111);
            #endregion

            #region HP
            HealthPack HealthPack7847 = new HealthPack(Game, worldRenderer, this, new Rectangle(483, 233, 0, 0));
            AddGameComponent(HealthPack7847);
            #endregion

            #region Ammo
            Ammo Ammo1546 = new Ammo(Game, worldRenderer, this, new Rectangle(417, 648, 0, 0));
            AddGameComponent(Ammo1546);
            Ammo Ammo8785 = new Ammo(Game, worldRenderer, this, new Rectangle(379, 656, 0, 0));
            AddGameComponent(Ammo8785);
            Ammo Ammo2826 = new Ammo(Game, worldRenderer, this, new Rectangle(308, 665, 0, 0));
            AddGameComponent(Ammo2826);
            Ammo Ammo2827 = new Ammo(Game, worldRenderer, this, new Rectangle(316, 665, 0, 0));
            AddGameComponent(Ammo2827);
            Ammo Ammo2829 = new Ammo(Game, worldRenderer, this, new Rectangle(430, 665, 0, 0));
            AddGameComponent(Ammo2829);
            #endregion

            #region Leveldoor
            LevelDoor LevelDoor1905 = new LevelDoor(Game, worldRenderer, 
                this, new Rectangle(363, 30, 50, 50), "World3", "DragonCave");
            AddGameComponent(LevelDoor1905);
            #endregion

            #region MiscCollision
            //Prevents player from walking off map
            Rectangle OffMapBlock = new Rectangle(337, -16, 3 * 32, 2);
            worldRenderer.addMiscCollision(OffMapBlock); 
            #endregion

            #region Song
            Song backgroundMusic = Game.Content.Load<Song>("Sounds/World2/Map8");

            PlayBackgroundMusic(backgroundMusic);

            #endregion

            Vendor vendor = new Vendor(this.Game, worldRenderer, this, new Rectangle(475, 595, 100, 100));
            AddGameComponent(vendor);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
        public override void OnPlayerDeath()
        {
            worldRenderer.LoadMap("World2", "PreBossBridge");
            base.OnPlayerDeath();
        }
    }
}