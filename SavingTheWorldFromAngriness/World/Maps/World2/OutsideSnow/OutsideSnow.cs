﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.GUI;
using SavingTheWorldFromAngriness.World.Components;

namespace SavingTheWorldFromAngriness.World.Maps.World2.OutsideSnow
{
    class OutsideSnow : BasicMap
    {
        #region Key

        private Texture2D keyTexture;
        private SoundEffect keySoundEffect;
        private Vector2 keyPosition;
        private int keyNumber;

        #endregion

        #region Locked door

        private Rectangle lockedDoorRectangle;
        private Rectangle lockedDoorCollisionRectangle;
        private Texture2D lockedDoorTexture;
        private BasicComponent lockedDoorComponent;

        #endregion

        #region Sound

        private SoundEffect promptSoundEffect1;
        private SoundEffect openDoorSound;

        #endregion

        #region Texture

        private Texture2D playerAvatarTexture;

        #endregion

        #region Messages played
        private bool messagePlayed;
        private bool closedDoorMessagePlayed;
        private bool closedDoorMessagePlayed2;
        #endregion

        private bool stopMapLogic;

        public OutsideSnow(Game game, WorldRenderer worldRenderer) : base(game, worldRenderer)
        {
        }

        public override void Initialize()
        {
            #region Key

            this.keyNumber = 0;
            keyPosition = new Vector2(1360, 66);

            #endregion

            base.Initialize();

            #region Player start position

            player.setPlayerPosition(new Vector2(100, 112));

            #endregion

            #region Background

            worldRenderer.SetBackground(new Backgrounds.Snow(Game), new Color(234, 245, 255));

            #endregion

            #region Locked door rectangle

            this.lockedDoorRectangle = new Rectangle(368, 560, 3 * 36, 36);

            #endregion

            stopMapLogic = true;
            DialogPrompt prompt = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME, playerAvatarTexture,
                "*shivering* Well... I've certainly escaped the castle...", promptSoundEffect1);

            SetCameraToPlayerPosition();

            Game.Components.Add(prompt);
            prompt.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
            {
                stopMapLogic = false;
                Game.Components.Remove(prompt);
            };
        }

        public override void OnPlayerDeath()
        {
            worldRenderer.LoadMap("World2", "OutsideSnow");
            base.OnPlayerDeath();
        }
        public override void Update(GameTime gameTime)
        {
            if (stopMapLogic)
            {
                SetCameraToPlayerPosition();
                return;
            }

            base.Update(gameTime);

            #region if Player finds first key

            if (player.getPlayerRectangle().Intersects(
                new Rectangle((int)keyPosition.X + (Global.GameConstants.TILE_SIZE / 2),
                (int)keyPosition.Y + (Global.GameConstants.TILE_SIZE / 2), keyTexture.Width, keyTexture.Height)))
            {
                Global.Player.INVENTORY.Add("World2:OutsideSnow:Key");
                keyNumber++;
                keyPosition = new Vector2(1497, 1097);

                Global.Sounds.SoundManager.PlaySoundEffect(keySoundEffect);
            }
            #endregion

            #region if Player finds second key

            if (player.getPlayerRectangle().Intersects(
                new Rectangle((int)keyPosition.X + (Global.GameConstants.TILE_SIZE / 2),
                (int)keyPosition.Y + (Global.GameConstants.TILE_SIZE / 2), keyTexture.Width, keyTexture.Height)))
            {
                Global.Player.INVENTORY.Add("World2:OutsideSnow:Key");
                keyNumber++;
                keyPosition = new Vector2(1360, 55555);

                Global.Sounds.SoundManager.PlaySoundEffect(keySoundEffect);
            }
            #endregion

            #region if Player has two keys and intersects door

            if (keyNumber >= 2 && player.getPlayerRectangle().Intersects(lockedDoorRectangle) && messagePlayed == false)
            {
                Global.GameState.IS_GAME_PAUSED = true;
                DialogPrompt openDoorMessage = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME,
                        playerAvatarTexture, "Open Sesame...",
                        promptSoundEffect1);
                Game.Components.Add(openDoorMessage);
                messagePlayed = true;
                openDoorMessage.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
                {
                    Game.Components.Remove(openDoorMessage);
                    RemoveGameComponent(lockedDoorComponent);
                    worldRenderer.removeMiscCollision(lockedDoorCollisionRectangle);
                    SoundManager.PlaySoundEffect(openDoorSound);
                    Global.GameState.IS_GAME_PAUSED = false;
                };
            }
            #endregion

            #region if Player has no keys and intersects door

            if (keyNumber < 1 && player.getPlayerRectangle().Intersects(lockedDoorRectangle) && closedDoorMessagePlayed == false)
            {
                Global.GameState.IS_GAME_PAUSED = true;
                DialogPrompt lockedDoorMessage = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME,
                        playerAvatarTexture, "This door is locked...\nMaybe I can find a key to open it with.",
                        promptSoundEffect1);
                Game.Components.Add(lockedDoorMessage);
                closedDoorMessagePlayed = true;
                lockedDoorMessage.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
                {
                    Global.GameState.IS_GAME_PAUSED = false;
                    Game.Components.Remove(lockedDoorMessage);
                };
            }
            #endregion

            #region if Player has ONE keys and intersects door
            if (keyNumber == 1 && player.getPlayerRectangle().Intersects(lockedDoorRectangle) && closedDoorMessagePlayed2 == false)
            {
                Global.GameState.IS_GAME_PAUSED = true;
                DialogPrompt lockedDoorMessage = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME,
                        playerAvatarTexture, "Wrong key...\n",
                        promptSoundEffect1);
                Game.Components.Add(lockedDoorMessage);
                closedDoorMessagePlayed2 = true;
                lockedDoorMessage.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
                {
                    Global.GameState.IS_GAME_PAUSED = false;
                    Game.Components.Remove(lockedDoorMessage);
                };
            }
            #endregion

        }
        protected override void LoadContent()
        {

            #region Sounds
            Song backgroundMusic = Game.Content.Load<Song>("Sounds/World2/snowman");
            PlayBackgroundMusic(backgroundMusic);

            openDoorSound = Game.Content.Load<SoundEffect>("Sounds/World2/stoneButtonClicked");
            #endregion

            base.LoadContent();

            #region LevelDoors
            //Exit

            LevelDoor portalDoor = new LevelDoor(Game, worldRenderer, this,
                new Rectangle(560, 1300, 300, 200), "World2", "Skiing");
            AddGameComponent(portalDoor);
            #endregion

            #region Enemies

            int hp = 360;
            int aggro = 150;
            int attackSpeed = Global.GameConstants.ENEMY_ATTACKSPEED;
            int attackPower = 10;
            int walkingSpeed = 3;
            int lookingUp = 3;
            //int lookingRight = 2;
            int lookingLeft = 1;
            int lookingDown = 0;

            Enemy Enemy5048 = new Enemy(Game, worldRenderer, this, new Rectangle(674, 426, 0, 0), 
                player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingDown);
            AddGameComponent(Enemy5048);
            Enemy Enemy4490 = new Enemy(Game, worldRenderer, this, new Rectangle(675, 490, 0, 0), 
                player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingUp);
            AddGameComponent(Enemy4490);
            Enemy Enemy1938 = new Enemy(Game, worldRenderer, this, new Rectangle(1404, 871, 0, 0), 
                player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingLeft);
            AddGameComponent(Enemy1938);
            Enemy Enemy1379 = new Enemy(Game, worldRenderer, this, new Rectangle(1337, 948, 0, 0), 
                player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingUp);
            AddGameComponent(Enemy1379);
            Enemy Enemy9761 = new Enemy(Game, worldRenderer, this, new Rectangle(1281, 181, 0, 0), 
                player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingLeft);
            AddGameComponent(Enemy9761);
            Enemy Enemy4268 = new Enemy(Game, worldRenderer, this, new Rectangle(1221, 112, 0, 0), 
                player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingDown);
            AddGameComponent(Enemy4268);
            Enemy Enemy3093 = new Enemy(Game, worldRenderer, this, new Rectangle(650, 1376, 0, 0), 
                player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingLeft);
            AddGameComponent(Enemy3093);
            Enemy Enemy4726 = new Enemy(Game, worldRenderer, this, new Rectangle(650, 1441, 0, 0), 
                player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingLeft);
            AddGameComponent(Enemy4726);
            Enemy Enemy6754 = new Enemy(Game, worldRenderer, this, new Rectangle(649, 1495, 0, 0), 
                player, hp, aggro, attackSpeed, attackPower, walkingSpeed, lookingLeft);
            AddGameComponent(Enemy6754);
            #endregion

            #region Ammo
            Ammo Ammo9454 = new Ammo(Game, worldRenderer, this, new Rectangle(200, 17, 0, 0));
            AddGameComponent(Ammo9454);
            Ammo Ammo1607 = new Ammo(Game, worldRenderer, this, new Rectangle(200, 70, 0, 0));
            AddGameComponent(Ammo1607);
            Ammo Ammo2392 = new Ammo(Game, worldRenderer, this, new Rectangle(161, 40, 0, 0));
            AddGameComponent(Ammo2392);
            Ammo Ammo2393 = new Ammo(Game, worldRenderer, this, new Rectangle(167, 40, 0, 0));
            AddGameComponent(Ammo2393);
            #endregion

            #region HP
            HealthPack HealthPack1659 = new HealthPack(Game, worldRenderer, this, new Rectangle(826, 1332, 0, 0));
            AddGameComponent(HealthPack1659);
            HealthPack HealthPack5921 = new HealthPack(Game, worldRenderer, this, new Rectangle(1368, 693, 0, 0));
            AddGameComponent(HealthPack5921);
            HealthPack HealthPack9727 = new HealthPack(Game, worldRenderer, this, new Rectangle(1370, 213, 0, 0));
            AddGameComponent(HealthPack9727);
            #endregion

            #region UI
            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");
            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");

            #endregion

            #region Locked door

            lockedDoorTexture = Game.Content.Load<Texture2D>("Textures/World2/OutsideSnow/threeBlockDoor");
            lockedDoorComponent = new BasicComponent(this.Game, this.worldRenderer, this, new Rectangle(364, 590, 3 * 36, 36), lockedDoorTexture);
            lockedDoorCollisionRectangle = lockedDoorComponent.componentRectangle;

            AddGameComponent(lockedDoorComponent);
            worldRenderer.addMiscCollision(lockedDoorCollisionRectangle);

            #endregion

            #region Keys

            keyTexture = Game.Content.Load<Texture2D>("Textures/World2/ShrineSanctuary/key");
            keySoundEffect = Game.Content.Load<SoundEffect>("Sounds/World1/pop");

            #endregion
        }

        public override void DrawBehindShadows()
        {

            #region Draw Components
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                SamplerState.LinearWrap, null, null, null, camera.TransformMatrix);

            foreach (GameComponent gameComponent in gameComponents)
            {
                if (gameComponent.GetType() != typeof(Enemy))
                {
                    ((BasicComponent)gameComponent).Draw(spriteBatch);
                }
            }
            spriteBatch.End();

            #endregion
            #region Draw Keys

            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            spriteBatch.Draw(keyTexture, keyPosition, Color.White);
            spriteBatch.End();
            #endregion

            player.Draw(spriteBatch, camera);

            spriteBatch.Begin(SpriteSortMode.Immediate,
    BlendState.NonPremultiplied,
    SamplerState.LinearWrap, null, null, null, camera.TransformMatrix);

            foreach (GameComponent gameComponent in gameComponents)
            {
                if (gameComponent.GetType() == typeof(Enemy))
                {
                    ((BasicComponent)gameComponent).Draw(spriteBatch);
                }
            }
            spriteBatch.End();
            //   base.DrawBehindShadows();
        }
    }
}
