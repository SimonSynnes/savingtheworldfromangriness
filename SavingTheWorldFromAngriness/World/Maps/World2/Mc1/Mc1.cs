﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.GUI;

using GeonBit.UI;
using GeonBit.UI.Entities;
using GeonBit.UI.Entities.TextValidators;
using GeonBit.UI.DataTypes;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace SavingTheWorldFromAngriness.World.Maps.World2.flying
{
    class Mc1 : BasicMap
    {
        private bool isMoving;

        private Texture2D bikeTexture;
        private Texture2D emptyBikeTexture;
        private Vector2 emptyBikePosition;
        private Texture2D roadTexture;
        private Texture2D obstacleTexture;
        private bool iskeyDown;
        private List<Vector2> obstacles;

        private Texture2D gameTipTexture;

        private SoundEffect crashSoundEffect;
        private SoundEffect idleMCSoundEffect;
        private SoundEffect drivingSoundEffect;

        private int finishLine;
        
        public Mc1(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer)
        {
            this.isMoving = false;
            this.iskeyDown = false;
            this.finishLine = 0;
        }

        public override void Initialize()
        {
            base.Initialize();

            player.setPlayerPosition(new Vector2(100, 100));

            obstacles = new List<Vector2>();
            FillList();

            worldRenderer.SetBackground(new Backgrounds.MovingClouds(this.Game, 33, 3, new Vector2(0, -1)), Color.CornflowerBlue);
        }

        protected override void LoadContent()
        {
            bikeTexture = Game.Content.Load<Texture2D>("Textures/bike");

            base.LoadContent();
            
            roadTexture = Game.Content.Load<Texture2D>("Textures/World2/Driving/road");
            obstacleTexture = Game.Content.Load<Texture2D>("Textures/World2/Driving/obstacle");
            emptyBikeTexture = Game.Content.Load<Texture2D>("Textures/emptyBike");
            idleMCSoundEffect = Game.Content.Load<SoundEffect>("Sounds/World2/motorcycle/idleMC");
            drivingSoundEffect = Game.Content.Load<SoundEffect>("Sounds/World2/motorcycle/driving");
            crashSoundEffect = Game.Content.Load<SoundEffect>("Sounds/World2/motorcycle/crash");
            gameTipTexture = Game.Content.Load<Texture2D>("Textures/World2/Driving/GameTip");

            Global.Sounds.SoundManager.PlayRepeatingSoundEffect(idleMCSoundEffect);

            emptyBikePosition = new Vector2(worldRenderer.map_width / 2 - (emptyBikeTexture.Width / 2), worldRenderer.map_height / 2 - (emptyBikeTexture.Height / 2));

            Rectangle emptyBikeRectangle =
                new Rectangle(
                    (int)emptyBikePosition.X + 5,
                    (int)emptyBikePosition.Y + 5,
                    emptyBikeTexture.Width - 10,
                    emptyBikeTexture.Height - 10);

            worldRenderer.addMiscCollision(emptyBikeRectangle);
        }

        public override void Update(GameTime gameTime)
        {
            if (Global.GameState.IS_GAME_PAUSED)
            {
                return;
            }

            KeyboardState ks = Keyboard.GetState();

            if (isMoving)
            {
                Vector2 playerCurrentPosition = player.getPlayerPosition();
                Vector2 playerPosition = new Vector2(playerCurrentPosition.X, playerCurrentPosition.Y - 30);

                if (playerPosition.Y < finishLine)
                {
                    worldRenderer.LoadMap("World2", "BrickSquare");

                    isMoving = false;
                }

                if (playerPosition.X < -175)
                {
                    playerPosition.X = -175;
                }

                if (playerPosition.X > 190 * 3)
                {
                    playerPosition.X = 190 * 3;
                }

                foreach (Vector2 obstacle in obstacles)
                {
                    Rectangle obstacleRectangle = new Rectangle((int)obstacle.X + 41, (int)obstacle.Y + 41, 218, 117);

                    Vector2 bikePosition = player.getPlayerPosition();

                    Rectangle bikeRectangle = new Rectangle((int)bikePosition.X, (int)bikePosition.Y, bikeTexture.Width, bikeTexture.Height);

                    if (obstacleRectangle.Intersects(bikeRectangle))
                    {
                        Global.Sounds.SoundManager.PlaySoundEffect(crashSoundEffect);
                        worldRenderer.LoadMap("World2", "Mc1");

                        isMoving = false;
                    }
                }

                player.setPlayerPosition(playerPosition);
            }
            else
            {
                if (ks.IsKeyDown(Keys.Enter))
                {
                    iskeyDown = true;
                }
                else
                {
                    // Check if enter key has been pressed and then released
                    // (in order to only call functions once)
                    if (iskeyDown)
                    {
                        Rectangle emptyBikeRectangle =
                            new Rectangle(
                                (int)emptyBikePosition.X + (Global.GameConstants.TILE_SIZE / 2) - 45,
                                (int)emptyBikePosition.Y + (Global.GameConstants.TILE_SIZE / 2) - 45,
                                emptyBikeTexture.Width + 30,
                                emptyBikeTexture.Height + 30);

                        if (emptyBikeRectangle.Intersects(player.getPlayerRectangle()))
                        {
                            isMoving = true;

                            worldRenderer.SetBackground(new Backgrounds.MovingClouds(this.Game, 33, 15, new Vector2(0, -1)), Color.CornflowerBlue);
                            player.SetWalkingSpeed(15);
                            Vector2 playerPosition = player.getPlayerPosition();
                            player.setPlayerPosition(new Vector2(playerPosition.X, playerPosition.Y - 185));

                            SoundManager.StopRepeatingSoundEffect(idleMCSoundEffect);

                            SoundManager.PlayRepeatingSoundEffect(drivingSoundEffect);
                        }
                    }

                    iskeyDown = false;
                }
            }

            base.Update(gameTime);
        }

        public void FillList()
        {
            int[][] map = new int[][]
            {
                #region IntArrays for the map
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 0 },  // ----------------
                new int[] { 0, 1, 0 },  // ----------------
                new int[] { 0, 0, 0 },  // ----------------
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 1, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 1 }, // ------------------------
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 1, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 1, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 1 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 0, 0, 0 },
                new int[] { 1, 0, 1 },
                new int[] { 1, 0, 1 },
                new int[] { 1, 0, 1 },
                new int[] { 1, 0, 1 },
                new int[] { 1, 0, 1 }
	        #endregion
            };


            int height = 199;

            int onePos = -200;
            int twoPos = -200 + 270;
            int threePos = -200 + 270 * 2;

            int y = 0;
            for (int i = 0; i < map.Length; i++)
            {
                if (map[i][0] == 1)
                {
                    obstacles.Add(new Vector2(onePos, y));
                }

                if (map[i][1] == 1)
                {
                    obstacles.Add(new Vector2(twoPos, y));
                }

                if (map[i][2] == 1)
                {
                    obstacles.Add(new Vector2(threePos, y));
                }

                y -= height;
            }

            finishLine = y - 400;
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                SamplerState.LinearWrap,
                null,
                null,
                null,
                camera.TransformMatrix);

            int windowWidth = GraphicsDevice.Viewport.Width;
            int windowHeight = GraphicsDevice.Viewport.Height;

            if (isMoving)
            {
                spriteBatch.Draw(roadTexture, new Rectangle(-200, -Convert.ToInt32(camera.position.Y), 800 + 40, windowHeight), Color.ForestGreen);

                foreach (Vector2 obstacle in obstacles)
                {
                    spriteBatch.Draw(obstacleTexture, new Rectangle((int)obstacle.X, (int)obstacle.Y, 300, 199), Color.White);
                }

                spriteBatch.Draw(bikeTexture, player.getPlayerPosition(), Color.White);
            }
            else
            {
                spriteBatch.Draw(gameTipTexture, new Vector2(worldRenderer.map_width, 0), Color.White);
                spriteBatch.Draw(emptyBikeTexture, emptyBikePosition, Color.White);
            }

            spriteBatch.End();

            base.Draw(gameTime);


        }
    }
}
