﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.World.Components.World2;
using Microsoft.Xna.Framework.Media;
using SavingTheWorldFromAngriness.Backgrounds;
using Microsoft.Xna.Framework.Audio;
using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.GUI;

namespace SavingTheWorldFromAngriness.World.Maps.World2.CastleBasement
{
    class CastleBasement : BasicMap
    {
        #region Right button
        private FloorButton rightFloorButton;
        private Rectangle rightButtonRectangle;
        private Vector2 tileSize;
        private bool buttonSoundPlayed;
        private bool unlocked;
        #endregion

        #region sounds
        private SoundEffect btnClicked;
        private SoundEffect foundButton;
        #endregion

        #region Walls
        private BasicComponent wallLeft1;
        private BasicComponent wallLeft2;
        private BasicComponent wallLeft3;
        private BasicComponent wallRight1;
        private BasicComponent wallRight2;
        private BasicComponent wallRight3;
        private BasicComponent finalWall;
        private BasicComponent firstWall;


        private Texture2D roomWallTexture;
        private Rectangle wallLeft1Rectangle;
        private Rectangle wallLeft2Rectangle;
        private Rectangle wallLeft3Rectangle;
        private Rectangle wallRight1Rectangle;
        private Rectangle wallRight2Rectangle;
        private Rectangle wallRight3Rectangle;
        private Rectangle finalWallRectangle;
        private Rectangle firstWallRectangle;
        private Texture2D finalWallTexture;
        #endregion

        #region Teleport Rectangles
        private Rectangle Teleport1Rectangle;
        private Rectangle Teleport2Rectangle;
        private Rectangle Teleport3Rectangle;
        private Rectangle Teleport4Rectangle;
        private Rectangle Teleport5Rectangle;
        private Rectangle Teleport6Rectangle;
        private Rectangle Teleport7Rectangle;
        private Rectangle Teleport8Rectangle;
        private Rectangle Teleport9Rectangle;
        private Rectangle Teleport10Rectangle;
        private Rectangle Teleport11Rectangle;
        private Rectangle Teleport12Rectangle;
        private Rectangle Teleport13Rectangle;
        private Rectangle Teleport14Rectangle;
        private Rectangle Teleport15Rectangle;
        private Teleport Teleport1;
        private Teleport UnlockedTeleport;
        #endregion

        #region crackedStone
        private BasicComponent crackedStone;
        private Texture2D crackedStoneTexture;
        private bool crackedStoneRemoved;
        private Rectangle crackedStoneRectangle;
        private bool crackedStoneSoundPlayed;
        #endregion

        public CastleBasement(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer)
        {
            #region Button
            this.buttonSoundPlayed = false;
            this.rightButtonRectangle = new Rectangle(900, 1120, 32, 32);
            this.tileSize = new Vector2(32, 32);
            #endregion

            #region Walls
            this.wallLeft1Rectangle = new Rectangle(611, 944, 13, 96);
            this.wallLeft2Rectangle = new Rectangle(611, 720, 13, 96);
            this.wallLeft3Rectangle = new Rectangle(611, 496, 13, 96);

            this.wallRight1Rectangle = new Rectangle(784, 944, 13, 96);
            this.wallRight2Rectangle = new Rectangle(784, 720, 13, 96);
            this.wallRight3Rectangle = new Rectangle(784, 496, 13, 96);

            this.finalWallRectangle = new Rectangle(624, 465, 160, 13);
            this.firstWallRectangle = new Rectangle(624, 1050, 160, 13);
            #endregion

            #region Teleport Rectangles
            this.Teleport1Rectangle = new Rectangle(461, 1100, 0, 0);
            this.Teleport2Rectangle = new Rectangle(1310, 740, 0, 0);
            this.Teleport3Rectangle = new Rectangle(810, 740, 0, 0);
            this.Teleport4Rectangle = new Rectangle(47, 965, 0, 0);
            this.Teleport5Rectangle = new Rectangle(540, 965, 0, 0);
            this.Teleport6Rectangle = new Rectangle(50, 520, 0, 0);
            this.Teleport7Rectangle = new Rectangle(540, 520, 0, 0);
            this.Teleport8Rectangle = new Rectangle(1317, 965, 0, 0);
            this.Teleport9Rectangle = new Rectangle(810, 965, 0, 0);
            this.Teleport10Rectangle = new Rectangle(52, 740, 0, 0);
            this.Teleport11Rectangle = new Rectangle(540, 740, 0, 0);
            this.Teleport12Rectangle = new Rectangle(1299, 520, 0, 0);
            this.Teleport13Rectangle = new Rectangle(810, 520, 0, 0);
            this.Teleport14Rectangle = new Rectangle(670, 271, 0, 0);
            this.Teleport15Rectangle = new Rectangle(670, 534, 0, 0);

            #endregion

            #region CrackedStone
            this.crackedStoneRectangle = new Rectangle(900, 1120, 32, 32);
            this.crackedStoneRemoved = false;
            this.crackedStoneSoundPlayed = false;
            #endregion
        }

        public override void Initialize()
        {
            base.Initialize();

            player.setPlayerPosition(new Vector2(685, 1315));
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            #region Enter Room

            LevelDoor enterCastleBasement = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(656, 1392, 64, 32), "World2", "CastlePuzzle");
            AddGameComponent(enterCastleBasement);
            #endregion

            #region Exit room
            Texture2D portal = Game.Content.Load<Texture2D>("Textures/World1/CastleOutside/portal");
            LevelDoor exitCastleBasement = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(648, 96, 127, 127), "World2", "DarkTeleportRoom", portal);
            AddGameComponent(exitCastleBasement);
            #endregion

            #region Floor button right
            rightFloorButton = new FloorButton(this.Game, worldRenderer, this,
                new Rectangle(rightButtonRectangle.X, rightButtonRectangle.Y, (int)tileSize.X, (int)tileSize.Y));
            AddGameComponent(rightFloorButton);
            #endregion

            #region Sounds
            Song backgroundMusic = Game.Content.Load<Song>("Sounds/World2/CastleBasement/CastleBasement");
            PlayBackgroundMusic(backgroundMusic);


            foundButton = Game.Content.Load<SoundEffect>("Sounds/World2/CastleBasement/foundHidden");
            btnClicked = Game.Content.Load<SoundEffect>("Sounds/World2/stoneButtonClicked");


            #endregion

            #region Background
            //  worldRenderer.SetBackground(new DirtBackground(this.Game), Color.Black);
            worldRenderer.SetBackground(new Backgrounds.BasicBackground(this.Game), new Color(17, 17, 17));
            #endregion

            #region Walls
            roomWallTexture = Game.Content.Load<Texture2D>("Textures/World2/CastleBasement/door");

            wallLeft1 = new BasicComponent(this.Game, worldRenderer, this, wallLeft1Rectangle, roomWallTexture);
            AddGameComponent(wallLeft1);
            worldRenderer.addMiscCollision(wallLeft1Rectangle);

            wallLeft2 = new BasicComponent(this.Game, worldRenderer, this, wallLeft2Rectangle, roomWallTexture);
            AddGameComponent(wallLeft2);
            worldRenderer.addMiscCollision(wallLeft2Rectangle);

            wallLeft3 = new BasicComponent(this.Game, worldRenderer, this, wallLeft3Rectangle, roomWallTexture);
            AddGameComponent(wallLeft3);
            worldRenderer.addMiscCollision(wallLeft3Rectangle);

            wallRight1 = new BasicComponent(this.Game, worldRenderer, this, wallRight1Rectangle, roomWallTexture);
            AddGameComponent(wallRight1);
            worldRenderer.addMiscCollision(wallRight1Rectangle);

            wallRight2 = new BasicComponent(this.Game, worldRenderer, this, wallRight2Rectangle, roomWallTexture);
            AddGameComponent(wallRight2);
            worldRenderer.addMiscCollision(wallRight2Rectangle);

            wallRight3 = new BasicComponent(this.Game, worldRenderer, this, wallRight3Rectangle, roomWallTexture);
            AddGameComponent(wallRight3);
            worldRenderer.addMiscCollision(wallRight3Rectangle);

            finalWallTexture = Game.Content.Load<Texture2D>("Textures/World2/CastleBasement/finalDoorWall");
            finalWall = new BasicComponent(this.Game, worldRenderer, this, finalWallRectangle, finalWallTexture);
            AddGameComponent(finalWall);
            worldRenderer.addMiscCollision(finalWallRectangle);

            firstWall = new BasicComponent(this.Game, worldRenderer, this, firstWallRectangle, finalWallTexture);
            AddGameComponent(firstWall);
            worldRenderer.addMiscCollision(firstWallRectangle);
            #endregion

            #region crackedStone
            crackedStoneTexture = Game.Content.Load<Texture2D>("Textures/World2/CastleBasement/crackedStone");
            crackedStone = new BasicComponent(this.Game, worldRenderer, this, rightButtonRectangle, crackedStoneTexture);
            AddGameComponent(crackedStone);
            #endregion

            #region Healthpacks
            HealthPack HealthPack5662 = new HealthPack(Game, worldRenderer, this, new Rectangle(512, 148, 0, 0));
            AddGameComponent(HealthPack5662);
            HealthPack HealthPack6433 = new HealthPack(Game, worldRenderer, this, new Rectangle(513, 83, 0, 0));
            AddGameComponent(HealthPack6433);
            HealthPack HealthPack2381 = new HealthPack(Game, worldRenderer, this, new Rectangle(512, 211, 0, 0));
            AddGameComponent(HealthPack2381);

            HealthPack HealthPack6906 = new HealthPack(Game, worldRenderer, this, new Rectangle(725, 428, 0, 0));
            AddGameComponent(HealthPack6906);
            HealthPack HealthPack9700 = new HealthPack(Game, worldRenderer, this, new Rectangle(746, 363, 0, 0));
            AddGameComponent(HealthPack9700);
            
            //Healthpacks with teleport 6 (half way through)
            HealthPack HealthPack8192 = new HealthPack(Game, worldRenderer, this, new Rectangle(211, 546, 0, 0));
            AddGameComponent(HealthPack8192);
            HealthPack HealthPack6106 = new HealthPack(Game, worldRenderer, this, new Rectangle(179, 508, 0, 0));
            AddGameComponent(HealthPack6106);


            #endregion

            #region Ammo
            Ammo Ammo9904 = new Ammo(Game, worldRenderer, this, new Rectangle(669, 1323, 0, 0));
            AddGameComponent(Ammo9904);
            Ammo Ammo2207 = new Ammo(Game, worldRenderer, this, new Rectangle(668, 1247, 0, 0));
            AddGameComponent(Ammo2207);
            Ammo Ammo8511 = new Ammo(Game, worldRenderer, this, new Rectangle(666, 1168, 0, 0));
            AddGameComponent(Ammo8511);
            Ammo Ammo5539 = new Ammo(Game, worldRenderer, this, new Rectangle(663, 1113, 0, 0));
            AddGameComponent(Ammo5539);
            Ammo Ammo1734 = new Ammo(Game, worldRenderer, this, new Rectangle(689, 1099, 0, 0));
            AddGameComponent(Ammo1734);
            Ammo Ammo1735 = new Ammo(Game, worldRenderer, this, new Rectangle(689, 1066, 0, 0));
            AddGameComponent(Ammo1735);
            Ammo Ammo1736 = new Ammo(Game, worldRenderer, this, new Rectangle(800, 1080, 0, 0));
            AddGameComponent(Ammo1736);
            Ammo Ammo1739 = new Ammo(Game, worldRenderer, this, new Rectangle(404, 1066, 0, 0));
            AddGameComponent(Ammo1739);
            Ammo Ammo1740 = new Ammo(Game, worldRenderer, this, new Rectangle(800, 1100, 0, 0));
            AddGameComponent(Ammo1740);

            #endregion

            #region Enemies
            Enemy Enemy3534 = new Enemy(Game, worldRenderer, this, new Rectangle(321, 528, 0, 0), player, 191, 100, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 2, 1);
            AddGameComponent(Enemy3534);
            Enemy Enemy9484 = new Enemy(Game, worldRenderer, this, new Rectangle(419, 522, 0, 0), player, 191, 100, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 2, 1);
            AddGameComponent(Enemy9484);
            Enemy Enemy2404 = new Enemy(Game, worldRenderer, this, new Rectangle(907, 521, 0, 0), player, 191, 100, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 2, 2);
            AddGameComponent(Enemy2404);
            Enemy Enemy3035 = new Enemy(Game, worldRenderer, this, new Rectangle(998, 516, 0, 0), player, 191, 100, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 2, 2);
            AddGameComponent(Enemy3035);
            Enemy Enemy3924 = new Enemy(Game, worldRenderer, this, new Rectangle(332, 750, 0, 0), player, 191, 100, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 2, 1);
            AddGameComponent(Enemy3924);
            Enemy Enemy6955 = new Enemy(Game, worldRenderer, this, new Rectangle(424, 745, 0, 0), player, 191, 100, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 2, 1);
            AddGameComponent(Enemy6955);
            Enemy Enemy6355 = new Enemy(Game, worldRenderer, this, new Rectangle(916, 752, 0, 0), player, 191, 100, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 2, 2);
            AddGameComponent(Enemy6355);
            Enemy Enemy5421 = new Enemy(Game, worldRenderer, this, new Rectangle(988, 739, 0, 0), player, 191, 100, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 2, 2);
            AddGameComponent(Enemy5421);
            Enemy Enemy1061 = new Enemy(Game, worldRenderer, this, new Rectangle(349, 965, 0, 0), player, 191, 100, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 2, 1);
            AddGameComponent(Enemy1061);
            Enemy Enemy8711 = new Enemy(Game, worldRenderer, this, new Rectangle(455, 962, 0, 0), player, 191, 100, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 2, 1);
            AddGameComponent(Enemy8711);
            Enemy Enemy8493 = new Enemy(Game, worldRenderer, this, new Rectangle(907, 961, 0, 0), player, 191, 100, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 2, 2);
            AddGameComponent(Enemy8493);
            Enemy Enemy7495 = new Enemy(Game, worldRenderer, this, new Rectangle(988, 980, 0, 0), player, 191, 100, Global.GameConstants.ENEMY_ATTACKSPEED, 25, 2, 2);
            AddGameComponent(Enemy7495);
            Enemy Enemy4576 = new Enemy(Game, worldRenderer, this, new Rectangle(685, 927, 0, 0), player, 191, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 100, 3, 3);
            AddGameComponent(Enemy4576);

            #endregion

            #region Teleports

            Teleport1 = new Teleport(Game, worldRenderer, this, Teleport1Rectangle, Teleport15Rectangle.X, Teleport15Rectangle.Y);
            AddGameComponent(Teleport1);
            Teleport Teleport2 = new Teleport(Game, worldRenderer, this, Teleport2Rectangle, Teleport1Rectangle.X, Teleport1Rectangle.Y);
            AddGameComponent(Teleport2);
            Teleport Teleport3 = new Teleport(Game, worldRenderer, this, Teleport3Rectangle, Teleport4Rectangle.X, Teleport4Rectangle.Y);
            AddGameComponent(Teleport3);
            Teleport Teleport4 = new Teleport(Game, worldRenderer, this, Teleport4Rectangle, Teleport3Rectangle.X, Teleport3Rectangle.Y);
            AddGameComponent(Teleport4);
            Teleport Teleport5 = new Teleport(Game, worldRenderer, this, Teleport5Rectangle, Teleport6Rectangle.X, Teleport6Rectangle.Y);
            AddGameComponent(Teleport5);
            Teleport Teleport6 = new Teleport(Game, worldRenderer, this,Teleport6Rectangle, Teleport5Rectangle.X, Teleport5Rectangle.Y);
            AddGameComponent(Teleport6);
            Teleport Teleport7 = new Teleport(Game, worldRenderer, this, Teleport7Rectangle, Teleport8Rectangle.X, Teleport8Rectangle.Y);
            AddGameComponent(Teleport7);
            Teleport Teleport8 = new Teleport(Game, worldRenderer, this, Teleport8Rectangle, Teleport7Rectangle.X, Teleport7Rectangle.Y);
            AddGameComponent(Teleport8);
            Teleport Teleport9 = new Teleport(Game, worldRenderer, this, Teleport9Rectangle, Teleport10Rectangle.X, Teleport10Rectangle.Y);
            AddGameComponent(Teleport9);
            Teleport Teleport10 = new Teleport(Game, worldRenderer, this, Teleport10Rectangle, Teleport9Rectangle.X, Teleport9Rectangle.Y);
            AddGameComponent(Teleport10);
            Teleport Teleport11 = new Teleport(Game, worldRenderer, this, Teleport11Rectangle, Teleport12Rectangle.X, Teleport12Rectangle.Y);
            AddGameComponent(Teleport11);
            Teleport Teleport12 = new Teleport(Game, worldRenderer, this, Teleport12Rectangle, Teleport11Rectangle.X, Teleport11Rectangle.Y);
            AddGameComponent(Teleport12);
            Teleport Teleport13 = new Teleport(Game, worldRenderer, this, Teleport13Rectangle, Teleport14Rectangle.X, Teleport14Rectangle.Y);
            AddGameComponent(Teleport13);
            Teleport Teleport14 = new Teleport(Game, worldRenderer, this, Teleport14Rectangle, Teleport1Rectangle.X, Teleport1Rectangle.Y);
            AddGameComponent(Teleport14);
            Teleport Teleport15 = new Teleport(Game, worldRenderer, this, Teleport15Rectangle, Teleport1Rectangle.X, Teleport1Rectangle.Y);
            AddGameComponent(Teleport15);
            UnlockedTeleport = new Teleport(Game, worldRenderer, this, Teleport1Rectangle, Teleport2Rectangle.X, Teleport2Rectangle.Y);


            #endregion
        }

        public override void OnPlayerDeath()
        {
            base.OnPlayerDeath();

            worldRenderer.LoadMap("World2", "CastleBasement");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            #region Right Floor button

            if (rightFloorButton.IsPlayerOnRectangle == true && buttonSoundPlayed == false && crackedStoneRemoved == true)
            {
                SoundManager.PlaySoundEffect(btnClicked);
                buttonSoundPlayed = true;
            }
            if (rightFloorButton.IsPlayerOnRectangle == true && unlocked == false)
            {
                RemoveGameComponent(Teleport1);
                AddGameComponent(UnlockedTeleport);
                unlocked = true;
            }
            #endregion

            #region Cracked floor
            if (player.getPlayerRectangle().Intersects(crackedStoneRectangle) == true && crackedStoneSoundPlayed == false)
            {
                RemoveGameComponent(crackedStone);
                SoundManager.PlaySoundEffect(foundButton);
                crackedStoneSoundPlayed = true;
            }
            #endregion
        }

        public override void RemoveAllGameComponents()
        {
            SoundManager.ClearRepeatingSoundEffects();

            foreach (IGameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).RemoveAllGameComponents();
                Game.Components.Remove(gameComponent);
            }

            Game.Components.Remove(player);

            PlayerUI.ClearUI();
            Game.Components.Remove(PlayerUI);
        }

        public override void DrawBehindShadows()
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                SamplerState.LinearWrap, null, null, null, camera.TransformMatrix);

            foreach (GameComponent gameComponent in gameComponents)
            {
                if (gameComponent.GetType() != typeof(Enemy))
                {
                    ((BasicComponent)gameComponent).Draw(spriteBatch);
                }
            }

            spriteBatch.End();

            player.Draw(spriteBatch, camera);

            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                SamplerState.LinearWrap, null, null, null, camera.TransformMatrix);

            foreach (GameComponent gameComponent in gameComponents)
            {
                if (gameComponent.GetType() == typeof(Enemy))
                {
                    ((BasicComponent)gameComponent).Draw(spriteBatch);
                }
            }

            spriteBatch.End();
        }
    }
}
