﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.GUI;

using Lighting;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;


namespace SavingTheWorldFromAngriness.World.Maps.World1.Ink
{
    class Ink : BasicMap
    {
        private PointLight playerLight;

        private Texture2D inkTexture;
        private Rectangle inkRectangle;

        private bool isMapFinished;

        private SoundEffect finishSoundEffect;

        private Texture2D playerAvatarTexture;
        private SoundEffect promptSoundEffect1;

        public Ink(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();

            isMapFinished = false;
            player.setPlayerPosition(new Vector2(48, 55));
            Global.GameState.IS_RENDERER_MAZE = true;
        }

        public override void RemoveAllGameComponents()
        {
            Global.GameState.IS_RENDERER_MAZE = false;

            base.RemoveAllGameComponents();
        }

        public override void OnPlayerDeath()
        {
            worldRenderer.LoadMap("World1", "Ink");
            base.OnPlayerDeath();
        }

        public override void Update(GameTime gameTime)
        {
            if (isMapFinished)
            {
                return;
            }

            if (player.getPlayerRectangle().Intersects(inkRectangle))
            {
                // DONE!
                isMapFinished = true;

                SoundManager.PlaySoundEffect(finishSoundEffect);

                Global.Player.INVENTORY.Add("world1:ink");
                worldRenderer.LoadMap("World1", "CastleOutside");

                return;
            }

            playerLight.Position = player.getPlayerPosition();

            base.Update(gameTime);
        }

        public override void DrawBehindShadows()
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            spriteBatch.Draw(inkTexture, inkRectangle, Color.White);

            spriteBatch.End();

            base.DrawBehindShadows();
        }

        protected override void LoadContent()
        {
            worldRenderer.SetBackground(new Backgrounds.BasicBackground(this.Game), Color.Black);

            inkTexture = Game.Content.Load<Texture2D>("Textures/World1/Ink/ink");
            finishSoundEffect = Game.Content.Load<SoundEffect>("Sounds/World1/Flask/finish");
            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");
            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");

            Song backgroundSong = Game.Content.Load<Song>("Sounds/World1/Ink/heathen");
            SoundManager.PlayBackgroundMusic(backgroundSong);

            inkRectangle = new Rectangle(1154, 62, inkTexture.Width, inkTexture.Height);

            playerLight = new PointLight(worldRenderer.lightingManager.lightEffect, new Vector2(0, 0), 400f, Color.White, 1f);
            worldRenderer.lightingManager.AddLight(playerLight);

            Ammo ammo1 = new Ammo(this.Game, worldRenderer, this, new Rectangle(30, 75, 0, 0));
            Ammo ammo2 = new Ammo(this.Game, worldRenderer, this, new Rectangle(30, 125, 0, 0));
            Ammo ammo3 = new Ammo(this.Game, worldRenderer, this, new Rectangle(30, 175, 0, 0));

            AddGameComponent(ammo1);
            AddGameComponent(ammo2);
            AddGameComponent(ammo3);

            base.LoadContent();

            Enemy enemy1 = new Enemy(this.Game, worldRenderer, this, new Rectangle(1019, 199, 0, 10),
                player, 165, 200, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 4, 1);
            AddGameComponent(enemy1);

            Enemy enemy2 = new Enemy(this.Game, worldRenderer, this, new Rectangle(510, 128, 0, 10),
                player, 165, 220, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 4, 1);
            AddGameComponent(enemy2);

            Enemy enemy3 = new Enemy(this.Game, worldRenderer, this, new Rectangle(510, 443, 0, 10),
                player, 165, 180, Global.GameConstants.ENEMY_ATTACKSPEED, 10, 4, 1);
            AddGameComponent(enemy3);

            DialogPrompt prompt = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME, playerAvatarTexture, "The ink bucket should be around here somewhere...", promptSoundEffect1);
            Game.Components.Add(prompt);
            prompt.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
            {
                Game.Components.Remove(prompt);
            };

            player.SetWalkingSpeed(2);
        }
    }
}
