﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.GUI;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.World.Components.World1;
using SavingTheWorldFromAngriness.Global.Sounds;

using GeonBit.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace SavingTheWorldFromAngriness.World.Maps.World1.EndScene
{
    class DialogItem
    {
        public string title;
        public string text;
        public Texture2D avatar;
        public SoundEffect promptSoundEffect;

        public DialogItem(string title, string text, Texture2D avatar, SoundEffect promptSoundEffect)
        {
            this.title = title;
            this.text = text;
            this.avatar = avatar;
            this.promptSoundEffect = promptSoundEffect;
        }
    }

    class EndScene : BasicMap
    {
        #region dialog
        private bool inDialogScreen;
        private int dialogItemIndex;
        private Texture2D playerAvatarTexture;
        private SoundEffect promptSoundEffect1;
        private SoundEffect promptSoundEffect2;
        private List<DialogItem> dialogItems;
        #endregion

        #region character
        private bool attackPlayer;
        private string characterName;
        private Texture2D characterAvatarTexture;
        private SpriteCharacterAnimation characterAnimation;
        #endregion

        private Rectangle cutSceneRectangle;
        private Enemy enemy;
        private bool finished;

        public EndScene(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer)
        {
            this.characterName = "The King's Personal Dude";
            this.attackPlayer = false;
            this.inDialogScreen = false;
            this.dialogItems = new List<DialogItem>();
            this.dialogItemIndex = 0;
            this.finished = false;
        }

        public override void Initialize()
        {
            base.Initialize();

            player.setPlayerPosition(new Vector2(14 * Global.GameConstants.TILE_SIZE, 14 * Global.GameConstants.TILE_SIZE));
            worldRenderer.SetBackground(new Backgrounds.BasicBackground(Game), new Color(17, 17, 17));
        }

        public void DialogScene()
        {
            dialogItems.Add(new DialogItem(characterName, "Stop.", characterAvatarTexture, promptSoundEffect2));
            dialogItems.Add(new DialogItem(characterName, "I know why you're here. You've come to poison the king.", characterAvatarTexture, promptSoundEffect2));
            dialogItems.Add(new DialogItem(Global.Player.PLAYER_NAME, "...", playerAvatarTexture, promptSoundEffect1));
            dialogItems.Add(new DialogItem(characterName, "The king has fled and we have barricaded the castle.", characterAvatarTexture, promptSoundEffect2));
            dialogItems.Add(new DialogItem(characterName, "And now...", characterAvatarTexture, promptSoundEffect2));
        }

        public void NextDialogScene()
        {
            if (dialogItemIndex <= (dialogItems.Count - 1))
            {
                DialogItem item = dialogItems[dialogItemIndex];

                DialogPrompt prompt = new DialogPrompt(this.Game, item.title, item.avatar, item.text, item.promptSoundEffect);
                Game.Components.Add(prompt);
                inDialogScreen = true;

                prompt.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
                {
                    Game.Components.Remove(prompt);

                    NextDialogScene();
                };

                dialogItemIndex++;
            }
            else
            {
                inDialogScreen = false;
                AttackPlayer();
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (finished)
            {
                SetCameraToPlayerPosition();
                return;
            }

            KeyboardState ks = Keyboard.GetState();
            MouseState ms = Mouse.GetState();

            float mouseX = (camera.matrixTranslation().X - ms.X) * -1;
            float mouseY = (camera.matrixTranslation().Y - ms.Y) * -1;
            if (!inDialogScreen)
            {
                player.Update(gameTime, ks, ms, new Vector2(mouseX, mouseY));
            }

            if (player.getHealth() <= 0)
            {
                OnPlayerDeath();
            }

            if (true)
            {
                int windowWidth = GraphicsDevice.Viewport.Width;
                int windowHeight = GraphicsDevice.Viewport.Height;

                Vector2 newCameraPosition = -(player.getPlayerPosition()) + (new Vector2(windowWidth / 2, windowHeight / 2));
                int newCameraX = Convert.ToInt32(newCameraPosition.X);
                int newCameraY = Convert.ToInt32(newCameraPosition.Y);

                camera.position = new Vector2(newCameraX, newCameraY);
            }

            if (!attackPlayer && !inDialogScreen)
            {
                if (cutSceneRectangle.Intersects(player.getPlayerRectangle()))
                {
                    NextDialogScene();
                }
            }

            if (attackPlayer && !finished)
            {
                if (enemy != null)
                {
                    if (!Game.Components.Contains(enemy))
                    {
                        finished = true;

                        DialogPrompt prompt = new DialogPrompt(this.Game,
                            Global.Player.PLAYER_NAME, playerAvatarTexture,
                            "Barricaded? I need to find a way out...", promptSoundEffect1);
                        Game.Components.Add(prompt);

                        prompt.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
                        {
                            worldRenderer.LoadMap("World2", "CastleHallway");
                            Game.Components.Remove(prompt);
                        };
                    }
                }
            }
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            Texture2D maskTexture = Game.Content.Load<Texture2D>("Textures/World1/EndScene/mask");
            worldRenderer.AddBackgroundMask(maskTexture);

            int torchStrength = 15;
            TorchLight torchLight1 = new TorchLight(Game, worldRenderer, this, new Rectangle((32 * 4) + 434, (32 * 3) + 210, 0, 0), torchStrength, 150);
            AddGameComponent(torchLight1);
            TorchLight torchLight2 = new TorchLight(Game, worldRenderer, this, new Rectangle((32 * 4) + 144, (32 * 3) + 210, 0, 0), torchStrength, 150);
            AddGameComponent(torchLight2);

            Ammo ammo1 = new Ammo(this.Game, worldRenderer, this, new Rectangle((32 * 4) + 434 + 45, (32 * 3) + 200, 0, 0));
            Ammo ammo2 = new Ammo(this.Game, worldRenderer, this, new Rectangle((32 * 4) + 144 - 70, (32 * 3) + 200, 0, 0));
            AddGameComponent(ammo1);
            AddGameComponent(ammo2);

            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");
            promptSoundEffect2 = Game.Content.Load<SoundEffect>("Sounds/World1/blip2");

            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");
            characterAvatarTexture = Game.Content.Load<Texture2D>("Textures/World1/Home/manAvatar");

            characterAnimation = new SpriteCharacterAnimation(Game, worldRenderer, this, "Textures/World1/Home/characterTileSheet",
                new Rectangle[] { new Rectangle(0, 0, 32, 32), new Rectangle(32, 0, 32, 32), new Rectangle(64, 0, 32, 32) },
                new Rectangle[] { new Rectangle(0, 32, 32, 32), new Rectangle(32, 32, 32, 32), new Rectangle(64, 32, 32, 32) },
                new Rectangle[] { new Rectangle(0, 64, 32, 32), new Rectangle(32, 64, 32, 32), new Rectangle(64, 64, 32, 32) },
                new Rectangle[] { new Rectangle(0, 96, 32, 32), new Rectangle(32, 96, 32, 32), new Rectangle(64, 96, 32, 32) },
                75, 4, new Vector2((32 * 4) + 305, (32 * 3) + 45), new Vector2(32, 32), 1000, 0);
            Game.Components.Add(characterAnimation);

            DialogScene();

            this.cutSceneRectangle = new Rectangle((32 * 4), (32 * 3) + 6 * Global.GameConstants.TILE_SIZE, worldRenderer.map_width, 64);

            Song backgroundSong = Game.Content.Load<Song>("Sounds/World1/Castle/tyranCastle");
            SoundManager.PlayBackgroundMusic(backgroundSong);
        }

        public override void OnPlayerDeath()
        {
            finished = true;
            base.OnPlayerDeath();
            worldRenderer.LoadMap("World1", "EndScene");
        }

        public void AttackPlayer()
        {
            this.attackPlayer = true;

            enemy = new Enemy(this.Game, worldRenderer, this, new Rectangle((32 * 4) + 305, (32 * 3) + 45, 0, 10),
                player, 200, 180, 100, 10, 4, 1);
            AddGameComponent(enemy);

            enemy.SetSpriteCharacterAnimation(characterAnimation);
        }

        public override void DrawBehindShadows()
        {
            player.Draw(spriteBatch, camera);

            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            foreach (GameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).Draw(spriteBatch);
            }

            if (!attackPlayer)
            {
                characterAnimation.Draw(spriteBatch);
            }

            spriteBatch.End();
        }
    }
}
