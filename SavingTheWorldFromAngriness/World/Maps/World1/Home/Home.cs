﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.World.Components.World1;
using SavingTheWorldFromAngriness.GUI;

using Lighting;
using RoyT.XNA;
using GeonBit.UI;
using GeonBit.UI.Entities;
using GeonBit.UI.Entities.TextValidators;
using GeonBit.UI.DataTypes;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.World.Maps.World1.Home
{
    class DialogItem
    {
        public string title;
        public string text;
        public Texture2D avatar;
        public SoundEffect promptSoundEffect;
        public bool askForName;

        public DialogItem(string title, string text, Texture2D avatar, SoundEffect promptSoundEffect)
        {
            this.title = title;
            this.text = text;
            this.avatar = avatar;
            this.promptSoundEffect = promptSoundEffect;
            this.askForName = false;
        }

        public DialogItem(string title, string text, Texture2D avatar, SoundEffect promptSoundEffect, bool askForName)
        {
            this.title = title;
            this.text = text;
            this.avatar = avatar;
            this.promptSoundEffect = promptSoundEffect;
            this.askForName = askForName;
        }
    }

    class Home : BasicMap
    {
        #region door
        private SoundEffect soundEffectKnocking1;
        private SoundEffect soundEffectKnocking2;
        private SoundEffect soundEffectKnocking3;
        private SoundEffect soundEffectKnocking4;
        private Texture2D doorTexture;
        private Rectangle doorRectangle;
        #endregion

        #region bed
        private Texture2D bedSleepingTexture;
        private Texture2D bedTexture;
        private Rectangle bedRectangle;
        #endregion

        #region sleeping animation
        private bool isSleeping;
        private int sleepingIndex;
        private double sleepingElapsedTime;
        private Texture2D sleepingAnimationTexture;
        private Rectangle[] sleepingAnimation;
        private SoundEffect soundEffectSnoring;
        #endregion

        #region door
        private bool isKnockingEnabled;
        private bool isDoorMoveDown;
        private int doorDisplacement;
        private double doorElapsedTime;
        private double doorMoveElapsedTime;
        private double doorKnockElapsedTime;
        private bool isMoveDoor;
        private int doorKnockCount;
        private bool knockOnDoor;
        private int doorKnockingIntervalTime;
        private int doorIntervalBetweenKnockingTime;
        private int doorKnockingIntensity;
        private double doorKnockingIntensityElapsedTime;
        private double doorKnockingIntensityElapsedTimeInterval;
        private int doorDisplacementInterval;
        private int doorElapsedTimeInterval;
        private bool stopKnocking;
        private Rectangle doorOpeningEventRectangle;
        private bool isDoorOpening;
        private bool isDoorOpen;
        private Rectangle[] doorAnimation;
        private int doorAnimationIndex;
        private double doorAnimationElapsedTime;
        private PointLight doorLight;
        private SoundEffect doorOpenSoundEffect;
        #endregion

        #region gametip
        private bool isGameTip1Visible;
        private bool isGameTip2Visible;
        private Texture2D gameTipTexture1;
        private Texture2D gameTipTexture2;
        private SoundEffect gameTipSoundEffect;
        private bool showGameTip1;
        private bool showGameTip2;
        private double showGameTip1ElapsedTime;
        #endregion

        #region dialog
        private Texture2D playerAvatarTexture;
        private bool inDialogScreen;
        private SoundEffect promptSoundEffect1;
        private SoundEffect promptSoundEffect2;
        private List<DialogItem> phase2DialogItems;
        private int phase2DialogItemIndex;
        #endregion

        #region character
        private SpriteCharacterAnimation characterAnimation;
        private bool characterWithPlayer;
        private bool characterStartedTalking;
        private bool characterWalkAway;
        private bool characterEnterRoom;
        private double characterEnterRoomElapsedTime;
        private bool isCharacterVisible;
        private Texture2D characterAvatarTexture;
        private string characterName;
        #endregion

        private bool isPhase2;
        private bool isEnterKeyDown;
        // set to true in order to only play the 2nd phase.
        private bool debugPhase2;

        public Home(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer)
        {
            #region door
            this.isMoveDoor = false;
            this.isDoorMoveDown = false;
            this.doorDisplacement = 0;
            this.doorElapsedTime = 0;
            this.doorMoveElapsedTime = 0;
            this.doorKnockCount = 0;
            this.doorKnockElapsedTime = 0;
            this.knockOnDoor = false;
            this.isKnockingEnabled = true;
            this.doorKnockingIntervalTime = 12000;
            this.doorIntervalBetweenKnockingTime = 150;
            this.doorKnockingIntensity = 0;
            this.doorKnockingIntensityElapsedTime = 0;
            this.doorKnockingIntensityElapsedTimeInterval = 12000;
            this.doorDisplacementInterval = 2;
            this.doorElapsedTimeInterval = 33;
            this.stopKnocking = false;
            this.isDoorOpen = false;
            this.isDoorOpening = false;
            this.doorAnimation = new Rectangle[] { new Rectangle(0, 0, 50, 80), new Rectangle(0, 96, 50, 80),
                new Rectangle(0, 192, 50, 80), new Rectangle(0, 288, 50, 80)};
            this.doorAnimationElapsedTime = 0;
            this.doorAnimationIndex = 0;
            #endregion

            #region sleepingAnimation
            this.isSleeping = true;
            this.sleepingIndex = 0;
            this.sleepingElapsedTime = 0;
            this.sleepingAnimation = new Rectangle[] {
                new Rectangle(0, 0, 32, 88),
                new Rectangle(33, 0, 32, 88),
                new Rectangle(66, 0, 39, 88) };
            #endregion

            #region dialog
            this.isGameTip1Visible = false;
            this.showGameTip1 = false;
            this.showGameTip1ElapsedTime = 0;
            this.phase2DialogItems = new List<DialogItem>();
            this.phase2DialogItemIndex = 0;
            this.isGameTip2Visible = false;
            this.showGameTip2 = false;
            #endregion

            #region character
            this.characterWithPlayer = false;
            this.characterStartedTalking = false;
            this.characterWalkAway = false;
            this.isCharacterVisible = false;
            this.characterEnterRoom = false;
            this.characterEnterRoomElapsedTime = 0;
            this.characterName = "The King's Personal Dude";
            #endregion

            this.isPhase2 = false;
            this.debugPhase2 = false;
            this.isEnterKeyDown = false;
        }

        public override void Initialize()
        {
            base.Initialize();
            
            worldRenderer.SetBackground(new Backgrounds.Rain(Game), new Color(64, 64, 64));
        }

        protected override void LoadContent()
        {
            InitializeSpriteBatch();

            int windowWidth = GraphicsDevice.Viewport.Width;
            int windowHeight = GraphicsDevice.Viewport.Height;
            camera.position = new Vector2((windowWidth / 2) - (worldRenderer.map_width / 2),
                (windowHeight / 2) - (worldRenderer.map_height / 2));

            soundEffectKnocking1 = Game.Content.Load<SoundEffect>("Sounds/World1/Home/knocking1");
            soundEffectKnocking2 = Game.Content.Load<SoundEffect>("Sounds/World1/Home/Knocking2");
            soundEffectKnocking3 = Game.Content.Load<SoundEffect>("Sounds/World1/Home/knocking3");
            soundEffectKnocking4 = Game.Content.Load<SoundEffect>("Sounds/World1/Home/knocking4");
            doorOpenSoundEffect = Game.Content.Load<SoundEffect>("Sounds/World1/Home/dooropen");
            soundEffectSnoring = Game.Content.Load<SoundEffect>("Sounds/World1/Home/snoring");
            gameTipSoundEffect = Game.Content.Load<SoundEffect>("Sounds/World1/pop");
            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");
            promptSoundEffect2 = Game.Content.Load<SoundEffect>("Sounds/World1/blip2");

            Song songRain = Game.Content.Load<Song>("Sounds/World1/Home/raining");

            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");
            characterAvatarTexture = Game.Content.Load<Texture2D>("Textures/World1/Home/manAvatar");

            gameTipTexture1 = Game.Content.Load<Texture2D>("Textures/World1/Home/gameTip1");
            gameTipTexture2 = Game.Content.Load<Texture2D>("Textures/World1/Home/gameTip2");
            doorTexture = Game.Content.Load<Texture2D>("Textures/World1/Home/door");
            bedTexture = Game.Content.Load<Texture2D>("Textures/World1/Home/bed");
            bedSleepingTexture = Game.Content.Load<Texture2D>("Textures/World1/Home/bedSleeping");
            sleepingAnimationTexture = Game.Content.Load<Texture2D>("Textures/World1/Home/sleeping");
            Texture2D playerTexture = Game.Content.Load<Texture2D>("Textures/World1/unArmedPlayer");

            SoundManager.PlayRepeatingSoundEffect(soundEffectSnoring);
            SoundManager.PlayBackgroundMusic(songRain);

            this.bedRectangle = new Rectangle((int)camera.position.X + 350, (int)camera.position.Y + 300, 48, 81);
            this.doorRectangle = new Rectangle((int)camera.position.X + (30 * 7) - 10, (int)camera.position.Y - 58, 50, 90);

            worldRenderer.addMiscCollision(new Rectangle(350, 300, 48, 81));

            AddPlayerComponent();

            player.setPlayerPosition(new Vector2(310, 330));
            player.SetWalkingSpeed(2);
            player.SetPlayerTexture(playerTexture);

            this.doorOpeningEventRectangle = new Rectangle(0, 0, worldRenderer.map_width, 32 * 8);

            doorLight = new PointLight(worldRenderer.lightingManager.lightEffect, new Vector2(224, -9), 300f, Color.White, 0.60f);

            characterAnimation = new SpriteCharacterAnimation(Game, worldRenderer, this, "Textures/World1/Home/characterTileSheet",
                new Rectangle[] { new Rectangle(0, 0, 32, 32), new Rectangle(32, 0, 32, 32), new Rectangle(64, 0, 32, 32) },
                new Rectangle[] { new Rectangle(0, 32, 32, 32), new Rectangle(32, 32, 32, 32), new Rectangle(64, 32, 32, 32) },
                new Rectangle[] { new Rectangle(0, 64, 32, 32), new Rectangle(32, 64, 32, 32), new Rectangle(64, 64, 32, 32) },
                new Rectangle[] { new Rectangle(0, 96, 32, 32), new Rectangle(32, 96, 32, 32), new Rectangle(64, 96, 32, 32) },
                75, 1, new Vector2(30 * 7, 35), new Vector2(32, 32), 1000, 0);

            if (debugPhase2)
            {
                SoundManager.ClearRepeatingSoundEffects();
                isSleeping = false;
            }

            Texture2D transparentBackground = Game.Content.Load<Texture2D>("Textures/transparent");
            LevelDoor door = new LevelDoor(this.Game, worldRenderer, this, new Rectangle((30 * 7) - 10, 0, 50, 75), "World1", "Castle", transparentBackground);
            AddGameComponent(door);

            AddFirstSetOfDialog();
        }

        public void Phase2NextDialog()
        {
            if (phase2DialogItemIndex <= (phase2DialogItems.Count - 1))
            {
                DialogItem item = phase2DialogItems[phase2DialogItemIndex];

                DialogPrompt prompt = new DialogPrompt(this.Game, item.title, item.avatar, item.text, item.promptSoundEffect);
                Game.Components.Add(prompt);

                prompt.getOKButton().OnClick += (Entity entity) =>
                {
                    if (item.askForName)
                    {
                        AskForNameDialog();
                    }
                    else
                    {
                        Phase2NextDialog();
                    }

                    Game.Components.Remove(prompt);
                };

                phase2DialogItemIndex++;
            }
            else
            {
                characterWalkAway = true;
                this.showGameTip2 = true;
            }
        }

        public void AddFirstSetOfDialog()
        {
            phase2DialogItems.Add(new DialogItem(characterName, "Good evening. I didn't wake you up, did I?", characterAvatarTexture, promptSoundEffect2));
            phase2DialogItems.Add(new DialogItem("YOU", "...", playerAvatarTexture, promptSoundEffect1));
            phase2DialogItems.Add(new DialogItem(characterName, "What?", characterAvatarTexture, promptSoundEffect2));
            phase2DialogItems.Add(new DialogItem("YOU", "Why are you in my house?", playerAvatarTexture, promptSoundEffect1));
            phase2DialogItems.Add(new DialogItem(characterName, "The king has requested your presence at the castle.", characterAvatarTexture, promptSoundEffect2));
            phase2DialogItems.Add(new DialogItem(characterName, "He's asked for you personally.", characterAvatarTexture, promptSoundEffect2));
            phase2DialogItems.Add(new DialogItem(characterName, "From what I understand, he needs you to solve a little problem that we're having", characterAvatarTexture, promptSoundEffect2));
            phase2DialogItems.Add(new DialogItem(characterName, "...and I have come to escort you.", characterAvatarTexture, promptSoundEffect2));
            phase2DialogItems.Add(new DialogItem(characterName, "The king will explain the issue to you himself.", characterAvatarTexture, promptSoundEffect2));
            phase2DialogItems.Add(new DialogItem(characterName, "I didn't catch your name by the way. What should I call you?", characterAvatarTexture, promptSoundEffect2, true));
        }

        public void AddSecondSetOfDialog()
        {
            phase2DialogItems.Add(new DialogItem(characterName, "Well, " + Global.Player.PLAYER_NAME + ", are you ready to go?", characterAvatarTexture, promptSoundEffect2));
            phase2DialogItems.Add(new DialogItem(Global.Player.PLAYER_NAME, "Why am I being summoned? I've never served the kingship before.", playerAvatarTexture, promptSoundEffect1));
            phase2DialogItems.Add(new DialogItem(characterName, "You're far from our first pick. I'm afraid the other's turned out to be thoroughly unimpressive.", characterAvatarTexture, promptSoundEffect2));
            phase2DialogItems.Add(new DialogItem(characterName, "Even worse...", characterAvatarTexture, promptSoundEffect2));
            phase2DialogItems.Add(new DialogItem(Global.Player.PLAYER_NAME, "What?", playerAvatarTexture, promptSoundEffect1));
            phase2DialogItems.Add(new DialogItem(characterName, "Well, " + Global.Player.PLAYER_NAME + ", YOU believe in our cause, don't you?", characterAvatarTexture, promptSoundEffect2));
            phase2DialogItems.Add(new DialogItem(Global.Player.PLAYER_NAME, "What cause?", playerAvatarTexture, promptSoundEffect1));
            phase2DialogItems.Add(new DialogItem(characterName, "Nevermind.", characterAvatarTexture, promptSoundEffect2));
            phase2DialogItems.Add(new DialogItem(characterName, "As I was saying; the other men I have brought this request to, turned on us.", characterAvatarTexture, promptSoundEffect2));
            phase2DialogItems.Add(new DialogItem(characterName, "They got their just deserts.", characterAvatarTexture, promptSoundEffect2));
            phase2DialogItems.Add(new DialogItem(characterName, "There's been a few people...", characterAvatarTexture, promptSoundEffect2));
            phase2DialogItems.Add(new DialogItem(Global.Player.PLAYER_NAME, "What?", playerAvatarTexture, promptSoundEffect1));
            phase2DialogItems.Add(new DialogItem(characterName, "Some of the men had a slight moral quarrel about our situation.", characterAvatarTexture, promptSoundEffect2));
            phase2DialogItems.Add(new DialogItem(Global.Player.PLAYER_NAME, "A moral quarrel?", playerAvatarTexture, promptSoundEffect1));
            phase2DialogItems.Add(new DialogItem(characterName, "Yes.", characterAvatarTexture, promptSoundEffect2));
            phase2DialogItems.Add(new DialogItem(Global.Player.PLAYER_NAME, "...", playerAvatarTexture, promptSoundEffect1));
            phase2DialogItems.Add(new DialogItem(characterName, "You're not thinking about POISONING the king, are you?", characterAvatarTexture, promptSoundEffect2));
            phase2DialogItems.Add(new DialogItem(Global.Player.PLAYER_NAME, "What? No!", playerAvatarTexture, promptSoundEffect1));
            phase2DialogItems.Add(new DialogItem(characterName, "Good. I'll be outside. Come join me once you're ready.", characterAvatarTexture, promptSoundEffect2));
        }

        public void AskForNameDialog()
        {
            Panel panel = new Panel(new Vector2(500, 300), PanelSkin.ListBackground, Anchor.Center);

            Header header = new Header("Please enter your name", Anchor.TopCenter);
            panel.AddChild(header);

            TextInput txtName = new TextInput(false);
            txtName.PlaceholderText = "your name";
            txtName.Validators.Add(new SlugValidator(true));

            panel.AddChild(txtName);

            Button btnOK = new Button("OK", ButtonSkin.Default, Anchor.BottomRight);
            panel.AddChild(btnOK);

            btnOK.OnClick += (Entity entity) =>
            {
                Global.Player.PLAYER_NAME = txtName.Value;

                AddSecondSetOfDialog();

                UserInterface.Active.RemoveEntity(panel);

                Phase2NextDialog();
            };

            UserInterface.Active.AddEntity(panel);
        }

        public override void RemoveAllGameComponents()
        {
            SoundManager.StopBackgroundMusic();
            SoundManager.ClearRepeatingSoundEffects();

            foreach (IGameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).RemoveAllGameComponents();
                Game.Components.Remove(gameComponent);
            }

            worldRenderer.SetBackground(new Backgrounds.MovingClouds(this.Game, 40, 1), Color.CornflowerBlue);

            Game.Components.Remove(player);
            Game.Components.Remove(characterAnimation);
        }

        private void FadeOut_EnabledChanged(object sender, EventArgs e)
        {
            worldRenderer.LoadMap("World1", "Castle");
        }

        public override void Update(GameTime gameTime)
        {
            if (!isPhase2 && !debugPhase2)
            {
                Phase1(gameTime);
            }
            else
            {
                Phase2(gameTime);
            }

            if (!isSleeping)
            {
                KeyboardState ks = Keyboard.GetState();
                MouseState ms = Mouse.GetState();

                float mouseX = (camera.matrixTranslation().X - ms.X) * -1;
                float mouseY = (camera.matrixTranslation().Y - ms.Y) * -1;
                if (!Global.GameState.IS_GAME_PAUSED && !inDialogScreen)
                {
                    player.Update(gameTime, ks, ms, new Vector2(mouseX, mouseY));
                }
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                isEnterKeyDown = true;
            }
            else
            {
                // Check if enter key has been pressed and then released
                // (in order to only call functions once)
                if (isEnterKeyDown)
                {
                    foreach (GameComponent gameComponent in gameComponents)
                    {
                        if (((BasicComponent)gameComponent).checkCollision(player.getPlayerRectangle()))
                        {
                            if (((BasicComponent)gameComponent).GetType() == typeof(LevelDoor))
                            {
                                FadeOut fadeOut = new FadeOut(this.Game, worldRenderer, this, Rectangle.Empty, 15);
                                fadeOut.EnabledChanged += FadeOut_EnabledChanged;
                                AddGameComponent(fadeOut);
                            }
                            else
                            {
                                ((BasicComponent)gameComponent).InteractWithTile();
                            }

                            break;
                        }
                    }
                }

                isEnterKeyDown = false;
            }
        }

        public void Phase1(GameTime gameTime)
        {
            if (isKnockingEnabled)
            {
                DoorKnocking(gameTime);
            }

            if (stopKnocking)
            {
                isKnockingEnabled = false;

                this.doorRectangle = new Rectangle((int)camera.position.X + (30 * 7) - 10, (int)camera.position.Y - 58, 50, 90);
                this.isSleeping = false;

                SoundManager.StopRepeatingSoundEffect(soundEffectSnoring);

                stopKnocking = false;
                this.inDialogScreen = true;

                DialogPrompt prompt = new DialogPrompt(this.Game, "YOU", playerAvatarTexture, "...wha...", promptSoundEffect1);
                Game.Components.Add(prompt);

                prompt.getOKButton().OnClick += (Entity entity) =>
                {
                    DialogPrompt prompt2 = new DialogPrompt(this.Game, "YOU", playerAvatarTexture, "I better go open the door", promptSoundEffect1);
                    Game.Components.Add(prompt2);

                    prompt2.getOKButton().OnClick += (Entity entity2) =>
                    {
                        this.showGameTip1 = true;
                        this.inDialogScreen = false;
                        Game.Components.Remove(prompt2);
                    };

                    Game.Components.Remove(prompt);
                };
            }

            if (showGameTip1)
            {
                showGameTip1ElapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
                if (showGameTip1ElapsedTime >= 200)
                {
                    this.showGameTip1 = false;
                    this.isGameTip1Visible = true;
                    SoundManager.PlaySoundEffect(gameTipSoundEffect);

                    this.isPhase2 = true;

                    showGameTip1ElapsedTime = 0;
                }
            }

            if (isSleeping)
            {
                sleepingElapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
                if (sleepingElapsedTime >= 300)
                {
                    if (sleepingIndex < sleepingAnimation.Length - 1)
                    {
                        sleepingIndex++;
                    }
                    else
                    {
                        sleepingIndex = 0;
                    }

                    sleepingElapsedTime = 0;
                }
            }
        }

        public void Phase2(GameTime gameTime)
        {
            if (!isDoorOpen && !isDoorOpening)
            {
                if (player.getPlayerRectangle().Intersects(doorOpeningEventRectangle))
                {
                    isDoorOpening = true;
                    inDialogScreen = true;

                    isGameTip1Visible = false;
                }
            }

            if (isDoorOpening && !isDoorOpen)
            {
                doorAnimationElapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
                if (doorAnimationElapsedTime >= 33)
                {
                    if (doorAnimationIndex == 0)
                    {
                        SoundManager.PlaySoundEffect(doorOpenSoundEffect);
                    }

                    if (doorAnimationIndex == 1)
                    {
                        worldRenderer.lightingManager.AddLight(doorLight);
                    }

                    if (doorAnimationIndex < doorAnimation.Length - 1)
                    {
                        doorAnimationIndex++;
                    }
                    else
                    {
                        isDoorOpening = false;
                        isDoorOpen = true;

                        this.Game.Components.Add(characterAnimation);

                        characterEnterRoom = true;
                    }

                    doorAnimationElapsedTime = 0;
                }
            }

            if (characterEnterRoom)
            {
                // characterEnterRoomElapsedTime
                characterEnterRoomElapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
                if (characterEnterRoomElapsedTime >= 100)
                {
                    isCharacterVisible = true;
                    characterEnterRoom = false;
                }
            }

            if (isDoorOpen && !isDoorOpening && !characterWithPlayer && !characterEnterRoom)
            {
                Rectangle playerRectangle = player.getPlayerRectangle();
                playerRectangle.Y -= 10;
                if (characterAnimation.getCharacterRectangle().Intersects(playerRectangle))
                {
                    characterWithPlayer = true;
                    characterAnimation.SetStopMoving();
                }
                else
                {
                    characterAnimation.MoveTowardsPlayer(playerRectangle);
                }
            }

            if (characterWithPlayer)
            {
                if (!characterStartedTalking)
                {
                    Phase2NextDialog();

                    characterStartedTalking = true;
                }
            }

            if (characterWalkAway)
            {
                Rectangle exitRectangle = new Rectangle((30 * 7) - 10, -20, 32, 50);
                characterAnimation.MoveTowardsPlayer(exitRectangle);

                exitRectangle = new Rectangle((30 * 7) - 10, -20, 45, 85);
                if (characterAnimation.getCharacterRectangle().Intersects(exitRectangle))
                {
                    isCharacterVisible = false;

                    worldRenderer.addMiscCollision(new Rectangle(32 * 7, -58, 50, 80));

                    Game.Components.Remove(characterAnimation);

                    inDialogScreen = false;

                    characterWalkAway = false;
                }
            }

            if (showGameTip2)
            {
                showGameTip1ElapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
                if (showGameTip1ElapsedTime >= 1500)
                {
                    this.showGameTip2 = false;
                    this.isGameTip2Visible = true;
                    SoundManager.PlaySoundEffect(gameTipSoundEffect);
                }
            }
        }

        private void DoorKnocking(GameTime gameTime)
        {
            int doorKnocksPerRun = 4;

            if (isMoveDoor)
            {
                if (!knockOnDoor)
                {
                    doorKnockElapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
                    if (doorKnockElapsedTime >= doorIntervalBetweenKnockingTime)
                    {
                        knockOnDoor = true;

                        if (isDoorMoveDown && doorDisplacement == 0 && (doorKnockCount <= doorKnocksPerRun))
                        {
                            // Knock once - move door once
                            if (doorKnockCount == 0)
                            {
                                SoundManager.PlaySoundEffect(soundEffectKnocking1);
                            }
                            else
                            if (doorKnockCount == 1)
                            {
                                SoundManager.PlaySoundEffect(soundEffectKnocking2);
                            }
                            else
                            if (doorKnockCount == 2)
                            {
                                SoundManager.PlaySoundEffect(soundEffectKnocking3);
                            }
                            else
                            {
                                SoundManager.PlaySoundEffect(soundEffectKnocking4);
                            }
                        }

                        doorKnockElapsedTime = 0;
                    }
                }

                if (knockOnDoor)
                {
                    doorElapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
                    if (doorElapsedTime >= doorElapsedTimeInterval)
                    {
                        if (doorKnockCount <= doorKnocksPerRun)
                        {
                            if (isDoorMoveDown)
                            {
                                if (doorDisplacement < 6)
                                {
                                    doorDisplacement += doorDisplacementInterval;
                                }
                                else
                                {
                                    isDoorMoveDown = false;
                                }
                            }
                            else
                            {
                                if (doorDisplacement > 0)
                                {
                                    doorDisplacement -= doorDisplacementInterval;
                                }
                                else
                                {
                                    isDoorMoveDown = true;
                                    knockOnDoor = false;
                                    doorKnockCount++;
                                }
                            }
                        }
                        else
                        {
                            // RESET DOOR
                            this.isMoveDoor = false;
                            this.isDoorMoveDown = false;
                            this.doorDisplacement = 0;
                            this.doorElapsedTime = 0;
                            this.doorMoveElapsedTime = 0;
                            this.doorKnockCount = 0;
                            this.doorKnockElapsedTime = 0;
                            this.knockOnDoor = false;
                            this.isKnockingEnabled = true;
                        }

                        doorElapsedTime = 0;
                    }
                }
            }

            if (!isMoveDoor)
            {
                doorMoveElapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
                if (doorMoveElapsedTime >= doorKnockingIntervalTime)
                {
                    isMoveDoor = true;

                    doorMoveElapsedTime = 0;
                }
            }

            doorKnockingIntensityElapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
            if (doorKnockingIntensityElapsedTime >= doorKnockingIntensityElapsedTimeInterval)
            {
                doorKnockingIntensity++;

                if (doorKnockingIntensity == 1)
                {
                    doorKnockingIntervalTime = 4000;
                    doorIntervalBetweenKnockingTime = 120;
                    doorKnockingIntensityElapsedTimeInterval = 8000;
                }
                else
                if (doorKnockingIntensity == 2)
                {
                    doorKnockingIntervalTime = 1000;
                    doorIntervalBetweenKnockingTime = 60;
                    doorKnockingIntensityElapsedTimeInterval = 5000;
                }
                else
                if (doorKnockingIntensity == 3)
                {
                    doorKnockingIntervalTime = 1;
                    doorIntervalBetweenKnockingTime = 1;
                    doorKnockingIntensityElapsedTimeInterval = 10000;
                    doorDisplacementInterval = 3;
                }
                else
                if (doorKnockingIntensity == 4)
                {
                    doorKnockingIntervalTime = 1;
                    doorIntervalBetweenKnockingTime = 1;
                    doorDisplacementInterval = 6;
                    doorElapsedTimeInterval = 1;
                    doorKnocksPerRun = 25;
                    doorKnockingIntensityElapsedTimeInterval = 7000;
                }
                else
                if (doorKnockingIntensity == 5)
                {
                    stopKnocking = true;
                }

                doorKnockingIntensityElapsedTime = 0;
            }

            this.doorRectangle = new Rectangle((int)camera.position.X + (30 * 7) - 10 + doorDisplacement, (int)camera.position.Y - 58 + doorDisplacement, 50, 90);
        }

        public override void DrawBehindShadows()
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            foreach (GameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).Draw(spriteBatch);
            }

            if (isCharacterVisible)
            {
                characterAnimation.Draw(spriteBatch);
            }

            spriteBatch.End();

            if (!isSleeping)
            {
                player.Draw(spriteBatch, camera);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(doorTexture, doorRectangle, doorAnimation[doorAnimationIndex], Color.White);

            if (isSleeping)
            {
                spriteBatch.Draw(bedSleepingTexture, bedRectangle, Color.White);

                spriteBatch.Draw(sleepingAnimationTexture, 
                    new Rectangle((int)camera.position.X + 350, (int)camera.position.Y + 200,
                    sleepingAnimation[sleepingIndex].Width, sleepingAnimation[sleepingIndex].Height),
                    sleepingAnimation[sleepingIndex], Color.White);
            }
            else
            {
                spriteBatch.Draw(bedTexture, bedRectangle, Color.White);
            }

            if (isGameTip1Visible)
            {
                Vector2 gameTipPosition = new Vector2(camera.position.X + worldRenderer.map_width,
                    camera.position.Y - 50);
                spriteBatch.Draw(gameTipTexture1, gameTipPosition, Color.White);
            }

            if (isGameTip2Visible)
            {
                Vector2 gameTipPosition = new Vector2(camera.position.X + worldRenderer.map_width,
                    camera.position.Y - 50);
                spriteBatch.Draw(gameTipTexture2, gameTipPosition, Color.White);
            }

            spriteBatch.End();

            UserInterface.Active.Draw(spriteBatch);
            UserInterface.Active.DrawMainRenderTarget(spriteBatch);
        }
    }
}
