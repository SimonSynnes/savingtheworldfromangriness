﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components.World1;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.GUI;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;


namespace SavingTheWorldFromAngriness.World.Maps.World1.Venom
{
    class Venom : BasicMap
    {
        private Texture2D leafTexture;
        private Vector2 leafPosition;
        private bool isEnterKeyDown;
        private int leafFoundCount;

        private SoundEffect popSoundEffect;
        private Texture2D playerAvatarTexture;
        private SoundEffect promptSoundEffect1;
        private SoundEffect rightSoundEffect;

        public Venom(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer)
        {
            this.isEnterKeyDown = false;
            this.leafFoundCount = 0;
        }

        public override void Initialize()
        {
            base.Initialize();

            player.setPlayerPosition(new Vector2(75, 75));
            worldRenderer.SetBackground(new Backgrounds.Splatter(Game), Color.White);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                isEnterKeyDown = true;
            }
            else
            {
                // Check if enter key has been pressed and then released
                // (in order to only call functions once)
                if (isEnterKeyDown)
                {
                    if (player.getPlayerRectangle().Intersects(
                        new Rectangle((int)leafPosition.X + (Global.GameConstants.TILE_SIZE / 2),
                        (int)leafPosition.Y + (Global.GameConstants.TILE_SIZE / 2), leafTexture.Width, leafTexture.Height)))
                    {
                        if (leafFoundCount == 0)
                        {
                            leafPosition = new Vector2(923, 501);
                            SoundManager.PlaySoundEffect(popSoundEffect);
                        }

                        if (leafFoundCount == 1)
                        {
                            leafPosition = new Vector2(347, 182);
                            SoundManager.PlaySoundEffect(popSoundEffect);
                        }

                        if (leafFoundCount == 2)
                        {
                            // Done!
                            SoundManager.PlaySoundEffect(rightSoundEffect);
                            Global.Player.INVENTORY.Add("world1:venom");
                            worldRenderer.LoadMap("World1", "CastleOutside");
                        }

                        leafFoundCount++;
                    }
                }

                isEnterKeyDown = false;
            }
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            Song backgroundSong = Game.Content.Load<Song>("Sounds/World1/Venom/threed");
            SoundManager.PlayBackgroundMusic(backgroundSong);

            for (int i = 0; i < 13; i++)
            {
                for (int ii = 0; ii < 27; ii++)
                {
                    Leaves leaves = new Leaves(this.Game, worldRenderer, this, new Rectangle(64 + (ii * 48), 64 + (i * 48), 0, 0));
                    AddGameComponent(leaves);
                }
            }

            leafTexture = Game.Content.Load<Texture2D>("Textures/World1/Venom/venomLeaf");
            leafPosition = new Vector2(146, 595);

            rightSoundEffect = Game.Content.Load<SoundEffect>("Sounds/World1/Flask/finish");
            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");
            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");
            popSoundEffect = Game.Content.Load<SoundEffect>("Sounds/World1/pop");

            DialogPrompt prompt = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME, playerAvatarTexture, "I need three venomous leaves.", promptSoundEffect1);
            Game.Components.Add(prompt);
            prompt.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
            {
                Game.Components.Remove(prompt);
            };

            player.SetWalkingSpeed(3);
        }

        public override void DrawBehindShadows()
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            spriteBatch.Draw(leafTexture, leafPosition, Color.White);

            spriteBatch.End();

            player.Draw(spriteBatch, camera);

            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            foreach (GameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).Draw(spriteBatch);
            }

            spriteBatch.End();
        }
    }
}
