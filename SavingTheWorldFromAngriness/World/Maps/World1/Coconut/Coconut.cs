﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.GUI;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace SavingTheWorldFromAngriness.World.Maps.World1.Coconut
{
    class Tree
    {
        public int index;
        public Vector2 position;
        public Rectangle rectangle;
        public SoundEffect soundEffect;

        public Tree(int index, Vector2 treePosition, SoundEffect soundEffect)
        {
            this.index = index;
            this.position = treePosition;
            this.rectangle = new Rectangle(
                (int)treePosition.X,
                (int)treePosition.Y + 40,
                32, 20);
            this.soundEffect = soundEffect;
        }

        public Rectangle GetTreeRectangle()
        {
            return new Rectangle(
                rectangle.X + (Global.GameConstants.TILE_SIZE / 2),
                rectangle.Y + (Global.GameConstants.TILE_SIZE / 2),
                rectangle.Width, rectangle.Height);
        }
    }

    class Coconut : BasicMap
    {
        private Texture2D treeTexture;
        private List<Tree> trees;
        private Stack<Tree> treeHits;

        private SoundEffect wrongSoundEffect;
        private SoundEffect rightSoundEffect;

        private Tree ignoreTree;

        private Texture2D coconutTexture;
        private Vector2 coconutPosition;

        private Texture2D playerAvatarTexture;
        private SoundEffect promptSoundEffect1;

        private bool stopMapLogic;

        private SoundEffect popSoundEffect;

        private SpriteFont spriteFont;

        public Coconut(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer) { }

        public override void Initialize()
        {
            trees = new List<Tree>();
            treeHits = new Stack<Tree>();
            ignoreTree = null;
            stopMapLogic = false;

            base.Initialize();

            player.setPlayerPosition(new Vector2(175, 275));
            coconutPosition = new Vector2(9198, 9157);

            SoundManager.StopBackgroundMusic();

            worldRenderer.SetBackground(new Backgrounds.WaterBackground(this.Game, 1, 0), Color.Black);

            SetCameraToPlayerPosition();

            stopMapLogic = true;
            DialogPrompt prompt = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME, playerAvatarTexture, "What will happen if I walk behind one of these trees?", promptSoundEffect1);
            Game.Components.Add(prompt);
            prompt.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
            {
                stopMapLogic = false;
                Game.Components.Remove(prompt);
            };
        }

        public override void DrawBehindShadows()
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            foreach (GameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).Draw(spriteBatch);
            }

            spriteBatch.Draw(coconutTexture, coconutPosition, Color.White);

            spriteBatch.End();

            player.Draw(spriteBatch, camera);

            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            foreach (Tree tree in trees)
            {
                spriteBatch.Draw(treeTexture, tree.position, Color.White);
            }

            lock (treeHits)
            {
                int count = 0;
                foreach (Tree tree in treeHits)
                {
                    // this is as fast as you're going to get.
                    spriteBatch.DrawString(spriteFont, (treeHits.Count - count).ToString(), tree.position, Color.White);
                    count++;
                }
            }

            spriteBatch.End();
        }

        public override void Update(GameTime gameTime)
        {
            if (stopMapLogic)
            {
                SetCameraToPlayerPosition();
                return;
            }

            Rectangle playerRectangle = player.getPlayerRectangle();

            if (playerRectangle.Intersects(
                new Rectangle((int)coconutPosition.X + (Global.GameConstants.TILE_SIZE / 2),
                (int)coconutPosition.Y + (Global.GameConstants.TILE_SIZE / 2), coconutTexture.Width, coconutTexture.Height)))
            {
                SoundManager.PlaySoundEffect(rightSoundEffect);
                Global.Player.INVENTORY.Add("world1:coconut");
                worldRenderer.LoadMap("World1", "CastleOutside");
                stopMapLogic = true;

                return;
            }

            playerRectangle.Height += 20;
            bool isPlayerCollidingWithTree = false;
            foreach (Tree tree in trees)
            {
                if (tree.GetTreeRectangle().Intersects(playerRectangle))
                {
                    isPlayerCollidingWithTree = true;

                    if (ignoreTree == tree)
                    {
                        continue;
                    }

                    if (treeHits.Count == 0)
                    {
                        PushTreeHit(tree);
                        SoundManager.PlaySoundEffect(tree.soundEffect);
                    }
                    else
                    {
                        if (CurrentTreeHit() != tree)
                        {
                            PushTreeHit(tree);
                            SoundManager.PlaySoundEffect(tree.soundEffect);
                        }
                    }

                    ignoreTree = tree;
                }
            }

            if (!isPlayerCollidingWithTree)
            {
                ignoreTree = null;
            }

            if (treeHits.Count >= trees.Count)
            {
                CheckTreeHits();
            }

            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            treeTexture = Game.Content.Load<Texture2D>("Textures/World1/Coconut/tree");
            wrongSoundEffect = Game.Content.Load<SoundEffect>("Sounds/World1/Coconut/wrong");
            rightSoundEffect = Game.Content.Load<SoundEffect>("Sounds/World1/Flask/finish");
            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");
            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");
            coconutTexture = Game.Content.Load<Texture2D>("Textures/World1/Coconut/coconut");
            popSoundEffect = Game.Content.Load<SoundEffect>("Sounds/World1/pop");
            spriteFont = Game.Content.Load<SpriteFont>("Fonts/World1/CoconutFont");

            trees.Add(new Tree(1, new Vector2(482, 331), Game.Content.Load<SoundEffect>("Sounds/World1/Coconut/1")));
            trees.Add(new Tree(2, new Vector2(253, 442), Game.Content.Load<SoundEffect>("Sounds/World1/Coconut/2")));
            trees.Add(new Tree(3, new Vector2(423, 613), Game.Content.Load<SoundEffect>("Sounds/World1/Coconut/3")));
            trees.Add(new Tree(4, new Vector2(368, 115), Game.Content.Load<SoundEffect>("Sounds/World1/Coconut/4")));
            trees.Add(new Tree(5, new Vector2(268, 215), Game.Content.Load<SoundEffect>("Sounds/World1/Coconut/5")));
            trees.Add(new Tree(6, new Vector2(168, 115), Game.Content.Load<SoundEffect>("Sounds/World1/Coconut/6")));

            foreach (Tree tree in trees)
            {
                worldRenderer.addMiscCollision(tree.GetTreeRectangle());
            }

            base.LoadContent();
        }

        public void CheckTreeHits()
        {
            bool isCombinationCorrect = true;

            lock (treeHits)
            {
                for (int i = 0; i < trees.Count; i++)
                {
                    Tree tree = treeHits.Pop();

                    if (tree.index != (trees.Count - i))
                    {
                        isCombinationCorrect = false;
                    }
                }

                if (isCombinationCorrect)
                {
                    // SUCCESS!
                    coconutPosition = new Vector2(198, 257);
                    SoundManager.PlaySoundEffect(popSoundEffect);
                }
                else
                {
                    // FAILURE
                    SoundManager.PlaySoundEffect(wrongSoundEffect);
                }
            }

            treeHits.Clear();
        }

        public void PushTreeHit(Tree tree)
        {
            lock(treeHits)
            {
                treeHits.Push(tree);
            }
        }

        public Tree CurrentTreeHit()
        {
            return treeHits.Peek();
        }
    }
}
