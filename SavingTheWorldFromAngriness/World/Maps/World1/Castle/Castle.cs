﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.World.Components.World1;
using SavingTheWorldFromAngriness.GUI;

using GeonBit.UI;
using GeonBit.UI.Entities;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.World.Maps.World1.Castle
{
    class DialogItem
    {
        public string title;
        public string text;
        public Texture2D avatar;
        public SoundEffect promptSoundEffect;

        public DialogItem(string title, string text, Texture2D avatar, SoundEffect promptSoundEffect)
        {
            this.title = title;
            this.text = text;
            this.avatar = avatar;
            this.promptSoundEffect = promptSoundEffect;
        }
    }

    class Castle : BasicMap
    {
        private Texture2D maskTexture;
        private List<SpriteCharacterAnimation> spriteCharacterAnimations;
        private Texture2D kingTexture;
        private Rectangle kingPosition;
        private bool talkingToKing;
        private bool talkingToKingFinished;

        private bool inDialogScreen;
        private Texture2D playerAvatarTexture;
        private Texture2D kingAvatarTexture;
        private SoundEffect promptSoundEffect1;
        private SoundEffect promptSoundEffect2;
        private SoundEffect pickUpSoundEffect;

        private List<DialogItem> kingDialogItems;
        private int kingDialogItemsIndex;

        private bool isEnterKeyDown;

        private Texture2D questTexture;
        private bool showQuestTexture;
        private double showQuestElapsedTime;
        private float showQuestTransparancy;
        private bool removeQuestTexture;
        private bool questShownFinished;

        private Texture2D exitTexture;

        private Rectangle leaveRectangle;
        private SoundEffect questSoundEffect;

        private PlayerUI thisPlayerUI;

        public Castle(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer)
        {
            this.isEnterKeyDown = false;
            this.talkingToKing = false;
            this.talkingToKingFinished = false;
            this.kingDialogItems = new List<DialogItem>();
            this.kingDialogItemsIndex = 0;
            this.showQuestTexture = false;
            this.showQuestElapsedTime = 0;
            this.showQuestTransparancy = 0;
            this.removeQuestTexture = false;
            this.questShownFinished = false;
            this.thisPlayerUI = null;
        }

        public override void Initialize()
        {
            spriteCharacterAnimations = new List<SpriteCharacterAnimation>();

            worldRenderer.SetBackground(new Backgrounds.BasicBackground(Game), new Color(17, 17, 17));

            this.kingPosition = new Rectangle(526, 315, 32, 58);
            worldRenderer.addMiscCollision(kingPosition);

            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();
            MouseState ms = Mouse.GetState();

            int windowWidth = GraphicsDevice.Viewport.Width;
            int windowHeight = GraphicsDevice.Viewport.Height;
            float mouseX = (camera.matrixTranslation().X - ms.X) * -1;
            float mouseY = (camera.matrixTranslation().Y - ms.Y) * -1;
            if (!Global.GameState.IS_GAME_PAUSED && !inDialogScreen)
            {
                player.Update(gameTime, ks, ms, new Vector2(mouseX, mouseY));
            }

            Vector2 newCameraPosition = -(player.getPlayerPosition()) + (new Vector2(windowWidth / 2, windowHeight / 2));
            int newCameraX = Convert.ToInt32(newCameraPosition.X);
            int newCameraY = Convert.ToInt32(newCameraPosition.Y);

            camera.position = new Vector2(newCameraX, newCameraY);

            if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                isEnterKeyDown = true;
            }
            else
            {
                // Check if enter key has been pressed and then released
                // (in order to only call functions once)
                if (isEnterKeyDown)
                {
                    if (!talkingToKing)
                    {
                        if (player.getPlayerRectangle().Intersects(new Rectangle(kingPosition.X, kingPosition.Y, kingPosition.Width, kingPosition.Height + 30)))
                        {
                            KingDialog();
                            inDialogScreen = true;
                            talkingToKing = true;
                        }
                    }
                    else
                    {
                        foreach (GameComponent gameComponent in gameComponents)
                        {
                            if (((BasicComponent)gameComponent).checkCollision(player.getPlayerRectangle()))
                            {
                                ((BasicComponent)gameComponent).InteractWithTile();

                                break;
                            }
                        }
                    }
                }

                isEnterKeyDown = false;
            }

            if (talkingToKingFinished)
            {
                // showQuestTexture
                showQuestElapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
                if (showQuestElapsedTime >= 100)
                {
                    showQuestTransparancy += 0.05f;

                    if (showQuestTransparancy >= 1)
                    {
                        talkingToKingFinished = false;
                        removeQuestTexture = true;
                    }

                    showQuestElapsedTime = 0;
                }
            }

            if (removeQuestTexture)
            {
                showQuestElapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
                if (showQuestElapsedTime >= 5000)
                {
                    removeQuestTexture = false;
                    inDialogScreen = false;
                    showQuestTexture = false;
                    questShownFinished = true;
                }
            }

            if (questShownFinished)
            {
                if (leaveRectangle.Intersects(player.getPlayerRectangle()))
                {
                    questShownFinished = false;
                    inDialogScreen = true;

                    DialogPrompt prompt = new DialogPrompt(this.Game, "The King", kingAvatarTexture, "Oh, and " + Global.Player.PLAYER_NAME + "...", promptSoundEffect2);
                    Game.Components.Add(prompt);
                    prompt.getOKButton().OnClick += (Entity entity) =>
                    {
                        DialogPrompt prompt2 = new DialogPrompt(this.Game, "The King", kingAvatarTexture, "Take this. You'll need it.", promptSoundEffect2);
                        Game.Components.Add(prompt2);
                        prompt2.getOKButton().OnClick += (Entity entity2) =>
                        {
                            inDialogScreen = false;
                            Game.Components.Remove(prompt2);
                        };

                        thisPlayerUI = new PlayerUI(Game);
                        Game.Components.Add(thisPlayerUI);

                        LevelDoor exitDoor = new LevelDoor(this.Game, worldRenderer, this, new Rectangle(430, 1223, exitTexture.Width, exitTexture.Height), "World1", "CastleOutside", exitTexture);
                        AddGameComponent(exitDoor);

                        Vector2 playerPosition = player.getPlayerPosition();
                        Game.Components.Remove(player);
                        AddPlayerComponent();
                        player.setPlayerPosition(playerPosition);
                        SoundManager.PlaySoundEffect(pickUpSoundEffect);

                        Game.Components.Remove(prompt);
                    };
                }
            }
        }

        public void KingDialog()
        {
            if (kingDialogItemsIndex <= (kingDialogItems.Count - 1))
            {
                DialogItem item = kingDialogItems[kingDialogItemsIndex];

                DialogPrompt prompt = new DialogPrompt(this.Game, item.title, item.avatar, item.text, item.promptSoundEffect);
                Game.Components.Add(prompt);

                prompt.getOKButton().OnClick += (Entity entity) =>
                {
                    KingDialog();
                    Game.Components.Remove(prompt);
                };

                kingDialogItemsIndex++;
            }
            else
            {
                this.talkingToKingFinished = true;
                this.showQuestTexture = true;

                SoundManager.PlaySoundEffect(questSoundEffect);
            }
        }

        public void FillKingDialog()
        {
            kingDialogItems.Add(new DialogItem("The King", "Hello " + Global.Player.PLAYER_NAME + ". Finally you've arrived.", kingAvatarTexture, promptSoundEffect2));
            kingDialogItems.Add(new DialogItem(Global.Player.PLAYER_NAME, "You needed assistance with something?", playerAvatarTexture, promptSoundEffect1));
            kingDialogItems.Add(new DialogItem("The King", "Yes. A village in the kingdom has been acting unruly as of late.", kingAvatarTexture, promptSoundEffect2));
            kingDialogItems.Add(new DialogItem("The King", "After multiple attempts at negotiations I have concluded that drastic measures are required.", kingAvatarTexture, promptSoundEffect2));
            kingDialogItems.Add(new DialogItem("The King", Global.Player.PLAYER_NAME + ", I want you to gather the ingredients for a very particular type of poison.", kingAvatarTexture, promptSoundEffect2));
            kingDialogItems.Add(new DialogItem(Global.Player.PLAYER_NAME, "For what?", playerAvatarTexture, promptSoundEffect1));
            kingDialogItems.Add(new DialogItem("The King", "I want you to contaminate the village well with it.", kingAvatarTexture, promptSoundEffect2));
            kingDialogItems.Add(new DialogItem("The King", "That should teach them a lesson.", kingAvatarTexture, promptSoundEffect2));
            kingDialogItems.Add(new DialogItem(Global.Player.PLAYER_NAME, "You want to poison the villagers for being unruly?", playerAvatarTexture, promptSoundEffect1));
            kingDialogItems.Add(new DialogItem("The King", "Actually, it's a land dispute.", kingAvatarTexture, promptSoundEffect2));
            kingDialogItems.Add(new DialogItem(Global.Player.PLAYER_NAME, "...", playerAvatarTexture, promptSoundEffect1));
            kingDialogItems.Add(new DialogItem("The King", "Here's a list of all the ingredients that you'll need. Good luck.", kingAvatarTexture, promptSoundEffect2));
        }

        public override void RemoveAllGameComponents()
        {
            worldRenderer.SetBackground(new Backgrounds.MovingClouds(this.Game, 40, 1), Color.CornflowerBlue);

            foreach (SpriteCharacterAnimation spriteCharacter in spriteCharacterAnimations)
            {
                Game.Components.Remove(spriteCharacter);
            }

            if (thisPlayerUI != null)
            {
                thisPlayerUI.ClearUI();
                Game.Components.Remove(thisPlayerUI);
            }

            Game.Components.Remove(player);

            Global.Sounds.SoundManager.ClearRepeatingSoundEffects();

            foreach (IGameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).RemoveAllGameComponents();
                Game.Components.Remove(gameComponent);
            }
        }

        protected override void LoadContent()
        {
            InitializeSpriteBatch();

            AddPlayerComponent();

            Texture2D playerTexture = Game.Content.Load<Texture2D>("Textures/World1/unArmedPlayer");
            player.setPlayerPosition(new Vector2(519, 1233));
            player.SetWalkingSpeed(2);
            player.SetPlayerTexture(playerTexture);

            Song backgroundSong = Game.Content.Load<Song>("Sounds/World1/Castle/tyranCastle");
            SoundManager.PlayBackgroundMusic(backgroundSong);

            maskTexture = Game.Content.Load<Texture2D>("Textures/World1/Castle/mask");
            worldRenderer.AddBackgroundMask(maskTexture);

            questTexture = Game.Content.Load<Texture2D>("Textures/World1/Castle/quest");

            questSoundEffect = Game.Content.Load<SoundEffect>("Sounds/World1/Castle/quest");

            kingAvatarTexture = Game.Content.Load<Texture2D>("Textures/World1/Castle/kingAvatar");

            pickUpSoundEffect = Game.Content.Load<SoundEffect>("Sounds/World1/Castle/pickup");

            exitTexture = Game.Content.Load<Texture2D>("Textures/World1/Castle/exit");

            // 485,615 
            leaveRectangle = new Rectangle(485, 615, 70, 130);

            int torchStrength = 22;
            TorchLight torchLight = new TorchLight(Game, worldRenderer, this, new Rectangle(660 + (32 * 2), 745, 0, 0), torchStrength, 150);
            AddGameComponent(torchLight);

            TorchLight torchLight3 = new TorchLight(Game, worldRenderer, this, new Rectangle(660 + (32 * 2), 745 + (32 * 6), 0, 0), torchStrength, 150);
            AddGameComponent(torchLight3);

            TorchLight torchLight4 = new TorchLight(Game, worldRenderer, this, new Rectangle(404 - (32 * 2), 745, 0, 0), torchStrength, 150);
            AddGameComponent(torchLight4);

            TorchLight torchLight6 = new TorchLight(Game, worldRenderer, this, new Rectangle(404 - (32 * 2), 745 + (32 * 6), 0, 0), torchStrength, 150);
            AddGameComponent(torchLight6);

            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");
            promptSoundEffect2 = Game.Content.Load<SoundEffect>("Sounds/World1/blip2");
            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");

            for (int i = 0; i < 4; i++)
            {
                SpriteCharacterAnimation spriteCharacterAnimation = new SpriteCharacterAnimation(Game, worldRenderer, this, "Textures/Tileset/Knight",
                    new Rectangle[] { new Rectangle(0, 0, 48, 48), new Rectangle(48, 0, 48, 48), new Rectangle(96, 0, 48, 48) },
                    new Rectangle[] { new Rectangle(0, 48, 48, 48), new Rectangle(48, 48, 48, 48), new Rectangle(96, 48, 48, 48) },
                    new Rectangle[] { new Rectangle(0, 96, 48, 48), new Rectangle(48, 96, 48, 48), new Rectangle(96, 96, 48, 48) },
                    new Rectangle[] { new Rectangle(0, 144, 48, 48), new Rectangle(48, 144, 48, 48), new Rectangle(96, 144, 48, 48) },
                    10, 0, new Vector2(600, 715 + (75 * i)), new Vector2(48, 48), 0, 1);

                Game.Components.Add(spriteCharacterAnimation);

                Rectangle spriteCharacterRectangle = spriteCharacterAnimation.getCharacterDrawRectangle();
                spriteCharacterRectangle.X += Global.GameConstants.TILE_SIZE / 2;
                spriteCharacterRectangle.Y += Global.GameConstants.TILE_SIZE / 2;

                worldRenderer.miscCollisions.Add(spriteCharacterRectangle);

                spriteCharacterAnimations.Add(spriteCharacterAnimation);
            }

            for (int i = 0; i < 4; i++)
            {
                SpriteCharacterAnimation spriteCharacterAnimation = new SpriteCharacterAnimation(Game, worldRenderer, this, "Textures/Tileset/Knight",
                    new Rectangle[] { new Rectangle(0, 0, 48, 48), new Rectangle(48, 0, 48, 48), new Rectangle(96, 0, 48, 48) },
                    new Rectangle[] { new Rectangle(0, 48, 48, 48), new Rectangle(48, 48, 48, 48), new Rectangle(96, 48, 48, 48) },
                    new Rectangle[] { new Rectangle(0, 96, 48, 48), new Rectangle(48, 96, 48, 48), new Rectangle(96, 96, 48, 48) },
                    new Rectangle[] { new Rectangle(0, 144, 48, 48), new Rectangle(48, 144, 48, 48), new Rectangle(96, 144, 48, 48) },
                    10, 0, new Vector2(600 - (40 * 4), 715 + (75 * i)), new Vector2(48, 48), 0, 2);

                Game.Components.Add(spriteCharacterAnimation);

                Rectangle spriteCharacterRectangle = spriteCharacterAnimation.getCharacterDrawRectangle();
                spriteCharacterRectangle.X += Global.GameConstants.TILE_SIZE / 2;
                spriteCharacterRectangle.Y += Global.GameConstants.TILE_SIZE / 2;

                worldRenderer.miscCollisions.Add(spriteCharacterRectangle);

                spriteCharacterAnimations.Add(spriteCharacterAnimation);
            }

            kingTexture = Game.Content.Load<Texture2D>("Temp/oldking");

            FillKingDialog();

            DialogPrompt prompt = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME, playerAvatarTexture, "I should go and talk to the king.", promptSoundEffect1);
            Game.Components.Add(prompt);
            this.inDialogScreen = true;

            prompt.getOKButton().OnClick += (Entity entity) =>
            {
                Game.Components.Remove(prompt);
                this.inDialogScreen = false;
            };

            FadeIn fadeIn = new FadeIn(this.Game, worldRenderer, this, Rectangle.Empty, 15);
            AddGameComponent(fadeIn);
        }

        public override void DrawBehindShadows()
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            foreach (GameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).Draw(spriteBatch);
            }

            foreach (SpriteCharacterAnimation spriteCharacter in spriteCharacterAnimations)
            {
                spriteCharacter.Draw(spriteBatch);
            }

            spriteBatch.Draw(kingTexture, kingPosition, Color.White);

            spriteBatch.End();

            player.Draw(spriteBatch, camera);
        }

        public override void Draw(GameTime gameTime)
        {
            if (thisPlayerUI == null)
            {
                UserInterface.Active.Draw(spriteBatch);
                UserInterface.Active.DrawMainRenderTarget(spriteBatch);
            }
            else
            {
                thisPlayerUI.Draw(spriteBatch);
            }

            spriteBatch.Begin();

            if (showQuestTexture)
            {
                int windowWidth = GraphicsDevice.Viewport.Width;
                int windowHeight = GraphicsDevice.Viewport.Height;
                spriteBatch.Draw(questTexture, new Vector2((windowWidth / 2) - (questTexture.Width / 2), (windowHeight / 2) - (questTexture.Height / 2)), Color.White * showQuestTransparancy);
            }

            spriteBatch.End();
        }
    }
}
