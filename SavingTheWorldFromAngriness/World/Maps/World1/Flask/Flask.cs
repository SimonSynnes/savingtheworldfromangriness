﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.GUI;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;


namespace SavingTheWorldFromAngriness.World.Maps.World1.Flask
{
    class Flask : BasicMap
    {
        private double playerMovementElapsedTime;
        private float playerMovementSpeed;
        private Vector2 playerDirection;

        private Texture2D flaskTexture;
        private Vector2 flaskPosition;

        private SoundEffect waterSoundEffect;
        private SoundEffect finishSoundEffect;

        private Texture2D playerAvatarTexture;
        private SoundEffect promptSoundEffect1;

        public Flask(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer)
        {
            this.playerMovementSpeed = 0;
            this.playerMovementElapsedTime = 0;
            this.playerDirection = new Vector2(0, 0);
            this.flaskPosition = new Vector2(511, 140);
        }

        public override void Initialize()
        {
            base.Initialize();

            player.setPlayerPosition(new Vector2(175, 175));
        //    player.setPlayerPosition(new Vector2(775, 175));
            worldRenderer.SetBackground(new Backgrounds.Snow(Game), new Color(217, 245, 255));
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();
            MouseState ms = Mouse.GetState();

            float mouseX = (camera.matrixTranslation().X - ms.X) * -1;
            float mouseY = (camera.matrixTranslation().Y - ms.Y) * -1;
            if (!Global.GameState.IS_GAME_PAUSED)
            {
                Update(gameTime, ks, ms, new Vector2(mouseX, mouseY));
            }

            if (player.getHealth() <= 0)
            {
                OnPlayerDeath();
            }

            int windowWidth = GraphicsDevice.Viewport.Width;
            int windowHeight = GraphicsDevice.Viewport.Height;

            Vector2 newCameraPosition = -(player.getPlayerPosition()) + (new Vector2(windowWidth / 2, windowHeight / 2));
            int newCameraX = Convert.ToInt32(newCameraPosition.X);
            int newCameraY = Convert.ToInt32(newCameraPosition.Y);

            camera.position = new Vector2(newCameraX, newCameraY);
        }

        protected override void LoadContent()
        {
            Song backgroundSong = Game.Content.Load<Song>("Sounds/World1/Flask/wintersWhite");
            SoundManager.PlayBackgroundMusic(backgroundSong);

            waterSoundEffect = Game.Content.Load<SoundEffect>("Sounds/World1/Flask/water");
            finishSoundEffect = Game.Content.Load<SoundEffect>("Sounds/World1/Flask/finish");

            flaskTexture = Game.Content.Load<Texture2D>("Textures/World1/Flask/flask");
            Texture2D mask = Game.Content.Load<Texture2D>("Textures/World1/Flask/mask");
            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");
            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");

            worldRenderer.AddBackgroundMask(mask);

            DialogPrompt prompt = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME, playerAvatarTexture, "I better walk carefully. This ice looks very slippery.", promptSoundEffect1);
            Game.Components.Add(prompt);
            prompt.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
            {
                Game.Components.Remove(prompt);
            };

            base.LoadContent();
        }

        public override void DrawBehindShadows()
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            spriteBatch.Draw(flaskTexture, flaskPosition, Color.White);

            spriteBatch.End();

            base.DrawBehindShadows();
        }

        public void Update(GameTime gameTime, KeyboardState ks, MouseState ms, Vector2 mousePosition)
        {
            Rectangle playerTextureSize = player.getPlayerRectangle();
            Vector2 playerPosition = player.getPlayerPosition();
            Vector2 direction = mousePosition - playerPosition;
            Vector2 newPlayerDirection = Vector2.Zero;

            if (playerTextureSize.Intersects(
                new Rectangle((int)flaskPosition.X + (Global.GameConstants.TILE_SIZE / 2),
                (int)flaskPosition.Y + (Global.GameConstants.TILE_SIZE / 2), flaskTexture.Width, flaskTexture.Height)))
            {
                // DONE!
                SoundManager.PlaySoundEffect(finishSoundEffect);
                ResetPlayer();

                Global.Player.INVENTORY.Add("world1:flask");
                worldRenderer.LoadMap("World1", "CastleOutside");

                return;
            }

            float playerAngle = (float)(Math.Atan2(direction.Y, direction.X));
            player.SetPlayerAngle(playerAngle);

            playerMovementElapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
            if (playerMovementElapsedTime >= 100)
            {
                if (ks.IsKeyDown(Keys.W))
                {
                    newPlayerDirection = new Vector2(newPlayerDirection.X, -1);
                }

                if (ks.IsKeyDown(Keys.A))
                {
                    newPlayerDirection = new Vector2(-1, newPlayerDirection.Y);
                }

                if (ks.IsKeyDown(Keys.S))
                {
                    newPlayerDirection = new Vector2(newPlayerDirection.X, 1);
                }

                if (ks.IsKeyDown(Keys.D))
                {
                    newPlayerDirection = new Vector2(1, newPlayerDirection.Y);
                }

                if (newPlayerDirection != Vector2.Zero)
                {
                    playerDirection += newPlayerDirection;
                    playerMovementSpeed += 0.05f;
                }

                playerMovementElapsedTime = 0;
            }

            if (playerMovementSpeed > 0)
            {
                Vector2 newPlayerPosition = playerPosition + (playerDirection * playerMovementSpeed);

                Rectangle playerRectangleX = new Rectangle((int)newPlayerPosition.X,
                    (int)playerPosition.Y, playerTextureSize.Width, playerTextureSize.Height);

                Rectangle playerRectangleY = new Rectangle((int)playerPosition.X,
                    (int)newPlayerPosition.Y, playerTextureSize.Width, playerTextureSize.Height);

                // a18
                if (!worldRenderer.checkWallCollision(playerRectangleX) & !checkEntityCollision(playerRectangleX))
                {
                    player.setPlayerPosition(new Vector2(newPlayerPosition.X, playerPosition.Y));
                    playerPosition = new Vector2(newPlayerPosition.X, playerPosition.Y);
                }

                if (!worldRenderer.checkWallCollision(playerRectangleY) & !checkEntityCollision(playerRectangleY))
                {
                    player.setPlayerPosition(new Vector2(playerPosition.X, newPlayerPosition.Y));
                    playerPosition = new Vector2(playerPosition.X, newPlayerPosition.Y);
                }

                playerMovementSpeed -= 0.003f;

                if (playerMovementSpeed < 0)
                {
                    playerMovementSpeed = 0;
                    playerDirection = Vector2.Zero;
                }

                if (worldRenderer.checkWallTileCollision(
                    new Rectangle((int)newPlayerPosition.X,
                    (int)newPlayerPosition.Y,
                    playerTextureSize.Width,
                    playerTextureSize.Height)) == "a18W")
                {
                    ResetPlayer();
                }
            }

        }

        private void ResetPlayer()
        {
            playerMovementSpeed = 0;
            playerMovementElapsedTime = 0;
            playerDirection = new Vector2(0, 0);
            player.setPlayerPosition(new Vector2(175, 175));

            SoundManager.PlaySoundEffect(waterSoundEffect);
        }
    }
}
