﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;

namespace SavingTheWorldFromAngriness.World.Maps.World1.Beginning
{
    class levelRectangle
    {
        public string text;
        public Vector2 position;

        public levelRectangle(string text, Vector2 position)
        {
            this.text = text;
            this.position = position;
        }
    }

    class BeginningWorld1 : BasicMap
    {
        private SpriteFont levelFont;
        private List<levelRectangle> levels;
        private Texture2D levelRectangleTexture;

        public BeginningWorld1(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();

            player.setPlayerPosition(new Vector2(75, 75));
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            levelFont = Game.Content.Load<SpriteFont>("Fonts/CoinFont");
            levelRectangleTexture = Game.Content.Load<Texture2D>("Temp/world1levelrectangle");

            Texture2D headerWorld1BackgroundTexture = Game.Content.Load<Texture2D>("Temp/world1header");
            Texture2D footerWorld1BackgroundTexture = Game.Content.Load<Texture2D>("Temp/world1footer");

            BasicComponent world1Background = new BasicComponent(Game, worldRenderer, this,
                new Rectangle(177, 0, headerWorld1BackgroundTexture.Width, headerWorld1BackgroundTexture.Height), headerWorld1BackgroundTexture);
            AddGameComponent(world1Background);

            BasicComponent footer1Background = new BasicComponent(Game, worldRenderer, this,
                new Rectangle(40, 830, footerWorld1BackgroundTexture.Width, footerWorld1BackgroundTexture.Height), footerWorld1BackgroundTexture);
            AddGameComponent(footer1Background);

            levels = new List<levelRectangle>();

            levels.Add(new levelRectangle("home", new Vector2(21, 270)));
            LevelDoor homeDoor = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21, 270, levelRectangleTexture.Width,
                levelRectangleTexture.Height), "World1", "Home", levelRectangleTexture);
            AddGameComponent(homeDoor);

            levels.Add(new levelRectangle("castle", new Vector2(21 + (levelRectangleTexture.Width  * 1), 270)));
            LevelDoor homeDoor2 = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21 + (levelRectangleTexture.Width * 1), 270, levelRectangleTexture.Width,
                levelRectangleTexture.Height), "World1", "Castle", levelRectangleTexture);
            AddGameComponent(homeDoor2);

            levels.Add(new levelRectangle("castle\nouts", new Vector2(21 + (levelRectangleTexture.Width * 2), 270)));
            LevelDoor homeDoor3 = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21 + (levelRectangleTexture.Width * 2), 270, levelRectangleTexture.Width,
                levelRectangleTexture.Height), "World1", "CastleOutside", levelRectangleTexture);
            AddGameComponent(homeDoor3);

             levels.Add(new levelRectangle("flask", new Vector2(21 + (levelRectangleTexture.Width * 3), 270)));
             LevelDoor homeDoor4 = new LevelDoor(this.Game, worldRenderer, this,
                 new Rectangle(21 + (levelRectangleTexture.Width * 3), 270, levelRectangleTexture.Width,
                 levelRectangleTexture.Height), "World1", "Flask", levelRectangleTexture);
             AddGameComponent(homeDoor4);

            levels.Add(new levelRectangle("coco-\nnut", new Vector2(21, 270 + levelRectangleTexture.Height + 5)));
            LevelDoor homeDoor5 = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21, 270 + levelRectangleTexture.Height + 5, levelRectangleTexture.Width,
                levelRectangleTexture.Height), "World1", "Coconut", levelRectangleTexture);
            AddGameComponent(homeDoor5);

            levels.Add(new levelRectangle("ink", new Vector2(21 + (levelRectangleTexture.Width * 1), 270 + levelRectangleTexture.Height + 5)));
            LevelDoor homeDoor6 = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21 + (levelRectangleTexture.Width * 1), 270 + levelRectangleTexture.Height + 5, levelRectangleTexture.Width,
                levelRectangleTexture.Height), "World1", "Ink", levelRectangleTexture);
            AddGameComponent(homeDoor6);

            levels.Add(new levelRectangle("venom", new Vector2(21 + (levelRectangleTexture.Width * 2), 270 + levelRectangleTexture.Height + 5)));
            LevelDoor homeDoor7 = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21 + (levelRectangleTexture.Width * 2), 270 + levelRectangleTexture.Height + 5, levelRectangleTexture.Width,
                levelRectangleTexture.Height), "World1", "Venom", levelRectangleTexture);
            AddGameComponent(homeDoor7);

            levels.Add(new levelRectangle("end", new Vector2(21 + (levelRectangleTexture.Width * 3), 270 + levelRectangleTexture.Height + 5)));
            LevelDoor homeDoor8 = new LevelDoor(this.Game, worldRenderer, this,
                new Rectangle(21 + (levelRectangleTexture.Width * 3), 270 + levelRectangleTexture.Height + 5, levelRectangleTexture.Width,
                levelRectangleTexture.Height), "World1", "EndScene", levelRectangleTexture);
            AddGameComponent(homeDoor8);
        }

        public override void DrawBehindShadows()
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            foreach (GameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).Draw(spriteBatch);
            }

            foreach (levelRectangle level in levels)
            {
                Vector2 levelFontSize = levelFont.MeasureString(level.text);
                spriteBatch.DrawString(levelFont, level.text,
                    new Vector2(level.position.X + (levelRectangleTexture.Width / 2) - (levelFontSize.X / 2),
                    level.position.Y + (levelRectangleTexture.Height / 2) - (levelFontSize.Y / 2)), Color.Black);
            }

            spriteBatch.End();

            player.Draw(spriteBatch, camera);
        }
    }
}
