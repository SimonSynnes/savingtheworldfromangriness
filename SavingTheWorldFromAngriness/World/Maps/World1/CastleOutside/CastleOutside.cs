﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.GUI;

using GeonBit.UI;
using GeonBit.UI.Entities;
using GeonBit.UI.Entities.TextValidators;
using GeonBit.UI.DataTypes;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.World.Maps.World1.CastleOutside
{
    class CastleOutside : BasicMap
    {
        private Texture2D maskTexture1;
        private Texture2D caveTexture;
        private Texture2D playerAvatarTexture;
        private Texture2D portalTexture;
        private SoundEffect promptSoundEffect1;

        public CastleOutside(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer)
        {
            // Uncomment to skip portals
            /*Global.Player.INVENTORY.Add("world1:coconut");
            Global.Player.INVENTORY.Add("world1:flask");
            Global.Player.INVENTORY.Add("world1:ink");
            Global.Player.INVENTORY.Add("world1:venom");*/
        }

        public override void Initialize()
        {
            base.Initialize();

            player.setPlayerPosition(new Vector2(715, 100));
            worldRenderer.SetBackground(new Backgrounds.MovingClouds(this.Game, 40, 1), Color.CornflowerBlue);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void RemoveAllGameComponents()
        {
            base.RemoveAllGameComponents();
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            int ingredientsCount = 0;

            maskTexture1 = Game.Content.Load<Texture2D>("Textures/World1/CastleOutside/mask1");
            caveTexture = Game.Content.Load<Texture2D>("Textures/World1/CastleOutside/cave");
            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");
            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");

            portalTexture = Game.Content.Load<Texture2D>("Textures/World1/CastleOutside/portal");

            Song song = Game.Content.Load<Song>("Sounds/World1/CastleOutside/fga");
            SoundManager.PlayBackgroundMusic(song);

            Rectangle sideCaveRectangleLeft = new Rectangle(17 * Global.GameConstants.TILE_SIZE, -1 * Global.GameConstants.TILE_SIZE, Global.GameConstants.TILE_SIZE, Global.GameConstants.TILE_SIZE * 2);
            worldRenderer.addMiscCollision(sideCaveRectangleLeft);

            Rectangle sideCaveRectangleRight = new Rectangle(-16 + 24 * Global.GameConstants.TILE_SIZE, -1 * Global.GameConstants.TILE_SIZE, Global.GameConstants.TILE_SIZE, Global.GameConstants.TILE_SIZE * 2);
            worldRenderer.addMiscCollision(sideCaveRectangleRight);

            Rectangle sideCaveRectangleTop = new Rectangle(17 * Global.GameConstants.TILE_SIZE, -3 * Global.GameConstants.TILE_SIZE, Global.GameConstants.TILE_SIZE * 7, Global.GameConstants.TILE_SIZE);
            worldRenderer.addMiscCollision(sideCaveRectangleTop);

            if (!Global.Player.INVENTORY.Contains("world1:coconut"))
            {
                LevelDoor mapCoconut = new LevelDoor(this.Game, worldRenderer, this, new Rectangle(192, 350, portalTexture.Width, portalTexture.Height), "World1", "Coconut", portalTexture);
                AddGameComponent(mapCoconut);
            }
            else
            {
                ingredientsCount++;
            }

            if (!Global.Player.INVENTORY.Contains("world1:flask"))
            {
                LevelDoor mapFlask = new LevelDoor(this.Game, worldRenderer, this, new Rectangle(464, 350, portalTexture.Width, portalTexture.Height), "World1", "Flask", portalTexture);
                AddGameComponent(mapFlask);
            }
            else
            {
                ingredientsCount++;
            }

            if (!Global.Player.INVENTORY.Contains("world1:ink"))
            {
                LevelDoor mapInk = new LevelDoor(this.Game, worldRenderer, this, new Rectangle(700, 140, portalTexture.Width, portalTexture.Height), "World1", "Ink", portalTexture);
                AddGameComponent(mapInk);
            }
            else
            {
                ingredientsCount++;
            }

            if (!Global.Player.INVENTORY.Contains("world1:venom"))
            {
                LevelDoor mapVenom = new LevelDoor(this.Game, worldRenderer, this, new Rectangle(347, 241, portalTexture.Width, portalTexture.Height), "World1", "Venom", portalTexture);
                AddGameComponent(mapVenom);
            }
            else
            {
                ingredientsCount++;
            }

            if (ingredientsCount == 4)
            {
                DialogPrompt prompt = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME, playerAvatarTexture, "Finally I have all the ingredients I need. Now I can go and give it to the king!", promptSoundEffect1);
                Game.Components.Add(prompt);
                prompt.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
                {
                    Game.Components.Remove(prompt);
                };

                LevelDoor levelDoor = new LevelDoor(this.Game, worldRenderer,
                    this, new Rectangle(
                        17 * Global.GameConstants.TILE_SIZE,
                        -2 * Global.GameConstants.TILE_SIZE,
                        Global.GameConstants.TILE_SIZE * 6,
                        Global.GameConstants.TILE_SIZE * 2), "World1", "EndScene");
                AddGameComponent(levelDoor);
            }
            else
            {
                if (ingredientsCount == 0)
                {
                    DialogPrompt prompt = new DialogPrompt(this.Game, Global.Player.PLAYER_NAME, playerAvatarTexture, "These portals should take me to where the ingredients are.", promptSoundEffect1);
                    Game.Components.Add(prompt);
                    prompt.getOKButton().OnClick += (GeonBit.UI.Entities.Entity entity) =>
                    {
                        Game.Components.Remove(prompt);
                    };
                }

                LevelDoor levelDoor = new LevelDoor(this.Game, worldRenderer,
                    this, new Rectangle(
                        17 * Global.GameConstants.TILE_SIZE,
                        -2 * Global.GameConstants.TILE_SIZE,
                        Global.GameConstants.TILE_SIZE * 6,
                        Global.GameConstants.TILE_SIZE * 2), "World1", "CastleWaiting");
                AddGameComponent(levelDoor);
            }

            DrawQuestProgress();
        }
        
        public void DrawQuestProgress()
        {
            Panel panel = new Panel(new Vector2(450, 300), PanelSkin.None, Anchor.TopRight, new Vector2(10));

            // checkboxes example
            panel.AddChild(new Header("Quest Progression"));
            panel.AddChild(new HorizontalLine());
            panel.AddChild(new Paragraph("Poison ingredients:"));

            CheckBox checkPotionFlask = new CheckBox("Potion Flask");
            checkPotionFlask.Scale = 0.7f;
            checkPotionFlask.Locked = true;
            checkPotionFlask.FillColor = Color.MediumVioletRed;
            if (Global.Player.INVENTORY.Contains("world1:flask"))
            {
                checkPotionFlask.Checked = true;
                checkPotionFlask.FillColor = Color.Green;
            }

            CheckBox checkCoconutMilk = new CheckBox("Coconut Milk");
            checkCoconutMilk.Scale = 0.7f;
            checkCoconutMilk.Locked = true;
            checkCoconutMilk.FillColor = Color.MediumVioletRed;
            if (Global.Player.INVENTORY.Contains("world1:coconut"))
            {
                checkCoconutMilk.Checked = true;
                checkCoconutMilk.FillColor = Color.Green;
            }

            CheckBox checkVenomousLeaf = new CheckBox("Venomous Leaf");
            checkVenomousLeaf.Scale = 0.7f;
            checkVenomousLeaf.Locked = true;
            checkVenomousLeaf.FillColor = Color.MediumVioletRed;
            if (Global.Player.INVENTORY.Contains("world1:venom"))
            {
                checkVenomousLeaf.Checked = true;
                checkVenomousLeaf.FillColor = Color.Green;
            }

            CheckBox checkCephalopodInk = new CheckBox("Cephalopod Ink");
            checkCephalopodInk.Scale = 0.7f;
            checkCephalopodInk.Locked = true;
            checkCephalopodInk.FillColor = Color.MediumVioletRed;
            if (Global.Player.INVENTORY.Contains("world1:ink"))
            {
                checkCephalopodInk.Checked = true;
                checkCephalopodInk.FillColor = Color.Green;
            }

            panel.AddChild(checkPotionFlask);
            panel.AddChild(checkCoconutMilk);
            panel.AddChild(checkVenomousLeaf);
            panel.AddChild(checkCephalopodInk);

            UserInterface.Active.AddEntity(panel);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            spriteBatch.Draw(maskTexture1, new Vector2(-16, -16), Color.White);
            spriteBatch.Draw(caveTexture, new Vector2(300, -347 + 20), Color.White);

            spriteBatch.End();

            player.Draw(spriteBatch, camera);

            base.Draw(gameTime);
        }

        public override void DrawBehindShadows()
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            foreach (GameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).Draw(spriteBatch);
            }

            spriteBatch.End();
        }
    }
}
