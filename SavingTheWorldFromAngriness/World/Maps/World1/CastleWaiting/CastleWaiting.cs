﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.GUI;

using GeonBit.UI;
using GeonBit.UI.Entities;
using GeonBit.UI.Entities.TextValidators;
using GeonBit.UI.DataTypes;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.World.Maps.World1.CastleWaiting
{
    class CastleWaiting : BasicMap
    {
        private List<SpriteCharacterAnimation> spriteCharacterAnimations;
        private Texture2D kingAvatarTexture;
        private Texture2D kingTexture;
        private Texture2D exitTexture;
        private Rectangle kingPosition;
        private Texture2D maskTexture;
        private SoundEffect promptSoundEffect2;
        private bool isEnterKeyDown;

        public CastleWaiting(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer)
        {
            this.spriteCharacterAnimations = new List<SpriteCharacterAnimation>();
            this.isEnterKeyDown = false;
        }

        public override void Initialize()
        {
            base.Initialize();

            player.setPlayerPosition(new Vector2(519, 1233));
            kingPosition = new Rectangle(526, 315, 32, 58);
            worldRenderer.addMiscCollision(kingPosition);
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            Song backgroundSong = Game.Content.Load<Song>("Sounds/World1/Castle/tyranCastle");
            SoundManager.PlayBackgroundMusic(backgroundSong);

            worldRenderer.SetBackground(new Backgrounds.BasicBackground(Game), new Color(17, 17, 17));
            maskTexture = Game.Content.Load<Texture2D>("Textures/World1/Castle/mask");
            worldRenderer.AddBackgroundMask(maskTexture);

            promptSoundEffect2 = Game.Content.Load<SoundEffect>("Sounds/World1/blip2");

            kingAvatarTexture = Game.Content.Load<Texture2D>("Textures/World1/Castle/kingAvatar");
            kingTexture = Game.Content.Load<Texture2D>("Temp/oldking");

            exitTexture = Game.Content.Load<Texture2D>("Textures/World1/Castle/exit");

            int torchStrength = 22;
            TorchLight torchLight = new TorchLight(Game, worldRenderer, this, new Rectangle(660 + (32 * 2), 745, 0, 0), torchStrength, 150);
            AddGameComponent(torchLight);

            TorchLight torchLight3 = new TorchLight(Game, worldRenderer, this, new Rectangle(660 + (32 * 2), 745 + (32 * 6), 0, 0), torchStrength, 150);
            AddGameComponent(torchLight3);

            TorchLight torchLight4 = new TorchLight(Game, worldRenderer, this, new Rectangle(404 - (32 * 2), 745, 0, 0), torchStrength, 150);
            AddGameComponent(torchLight4);

            TorchLight torchLight6 = new TorchLight(Game, worldRenderer, this, new Rectangle(404 - (32 * 2), 745 + (32 * 6), 0, 0), torchStrength, 150);
            AddGameComponent(torchLight6);

            for (int i = 0; i < 4; i++)
            {
                SpriteCharacterAnimation spriteCharacterAnimation = new SpriteCharacterAnimation(Game, worldRenderer, this, "Textures/Tileset/Knight",
                    new Rectangle[] { new Rectangle(0, 0, 48, 48), new Rectangle(48, 0, 48, 48), new Rectangle(96, 0, 48, 48) },
                    new Rectangle[] { new Rectangle(0, 48, 48, 48), new Rectangle(48, 48, 48, 48), new Rectangle(96, 48, 48, 48) },
                    new Rectangle[] { new Rectangle(0, 96, 48, 48), new Rectangle(48, 96, 48, 48), new Rectangle(96, 96, 48, 48) },
                    new Rectangle[] { new Rectangle(0, 144, 48, 48), new Rectangle(48, 144, 48, 48), new Rectangle(96, 144, 48, 48) },
                    10, 0, new Vector2(600, 715 + (75 * i)), new Vector2(48, 48), 0, 1);

                Game.Components.Add(spriteCharacterAnimation);

                Rectangle spriteCharacterRectangle = spriteCharacterAnimation.getCharacterDrawRectangle();
                spriteCharacterRectangle.X += Global.GameConstants.TILE_SIZE / 2;
                spriteCharacterRectangle.Y += Global.GameConstants.TILE_SIZE / 2;

                worldRenderer.miscCollisions.Add(spriteCharacterRectangle);

                spriteCharacterAnimations.Add(spriteCharacterAnimation);
            }

            for (int i = 0; i < 4; i++)
            {
                SpriteCharacterAnimation spriteCharacterAnimation = new SpriteCharacterAnimation(Game, worldRenderer, this, "Textures/Tileset/Knight",
                    new Rectangle[] { new Rectangle(0, 0, 48, 48), new Rectangle(48, 0, 48, 48), new Rectangle(96, 0, 48, 48) },
                    new Rectangle[] { new Rectangle(0, 48, 48, 48), new Rectangle(48, 48, 48, 48), new Rectangle(96, 48, 48, 48) },
                    new Rectangle[] { new Rectangle(0, 96, 48, 48), new Rectangle(48, 96, 48, 48), new Rectangle(96, 96, 48, 48) },
                    new Rectangle[] { new Rectangle(0, 144, 48, 48), new Rectangle(48, 144, 48, 48), new Rectangle(96, 144, 48, 48) },
                    10, 0, new Vector2(600 - (40 * 4), 715 + (75 * i)), new Vector2(48, 48), 0, 2);

                Game.Components.Add(spriteCharacterAnimation);

                Rectangle spriteCharacterRectangle = spriteCharacterAnimation.getCharacterDrawRectangle();
                spriteCharacterRectangle.X += Global.GameConstants.TILE_SIZE / 2;
                spriteCharacterRectangle.Y += Global.GameConstants.TILE_SIZE / 2;

                worldRenderer.miscCollisions.Add(spriteCharacterRectangle);

                spriteCharacterAnimations.Add(spriteCharacterAnimation);
            }

            LevelDoor exitDoor = new LevelDoor(this.Game, worldRenderer, this, new Rectangle(430, 1223, exitTexture.Width, exitTexture.Height), "World1", "CastleOutside", exitTexture);
            AddGameComponent(exitDoor);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                isEnterKeyDown = true;
            }
            else
            {
                // Check if enter key has been pressed and then released
                // (in order to only call functions once)
                if (isEnterKeyDown)
                {
                    if (player.getPlayerRectangle().Intersects(new Rectangle(kingPosition.X, kingPosition.Y, kingPosition.Width, kingPosition.Height + 30)))
                    {
                        Global.GameState.IS_GAME_PAUSED = true;
                        DialogPrompt prompt = new DialogPrompt(this.Game, "The King", kingAvatarTexture, "How is it going " + Global.Player.PLAYER_NAME + "?", promptSoundEffect2);
                        Game.Components.Add(prompt);
                        prompt.getOKButton().OnClick += (Entity entity) =>
                        {
                            Global.GameState.IS_GAME_PAUSED = false;
                            Game.Components.Remove(prompt);
                        };
                    }
                }

                isEnterKeyDown = false;
            }
        }

        public override void DrawBehindShadows()
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                null, null, null, null, camera.TransformMatrix);

            foreach (GameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).Draw(spriteBatch);
            }

            foreach (SpriteCharacterAnimation spriteCharacter in spriteCharacterAnimations)
            {
                spriteCharacter.Draw(spriteBatch);
            }

            spriteBatch.Draw(kingTexture, kingPosition, Color.White);

            spriteBatch.End();

            player.Draw(spriteBatch, camera);
        }
    }
}
