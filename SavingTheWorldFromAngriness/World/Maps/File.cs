﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Reflection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SavingTheWorldFromAngriness.World.Maps
{
    public static class File
    {
        private static string Read(string world, string map)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = String.Format("SavingTheWorldFromAngriness.World.Maps.{0}.{1}.{1}.txt", world, map);

            string result = "";
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
            }

            return result;
        }

        public static List<Object[]> Load(string world, string mapName, out int width, out int height, out List<float[]> lights, out float darkness, out string txtData)
        {
            List<Object[]> level = new List<Object[]>();
            lights = new List<float[]>();

            darkness = 0.65f;
            string map = Read(world, mapName);
            txtData = map;

            int y = 0;
            int x = 0;
            width = 0;
            height = 0;

            string[] lines = map.Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].StartsWith("//") || lines[i].Length < 3)
                {
                    continue;
                }

                if (lines[i].StartsWith("|"))
                {
                    lines[i] = lines[i].Substring(1, lines[i].Length - 1);
                    lines[i] = lines[i].Trim();
                    lines[i] = lines[i].Replace("\n", "");

                    string[] tiles = lines[i].Split(',');
                    for (int ii = 0; ii < tiles.Length - 1; ii++) // tile lines end with ","
                    {
                        level.Add(new Object[] { tiles[ii], new Vector2(x, y) });

                        x += Global.GameConstants.TILE_SIZE;

                        if (x > width)
                        {
                            width = x;
                        }
                    }

                    x = 0;
                    y += Global.GameConstants.TILE_SIZE;
                }

                if (lines[i].StartsWith("Light:"))
                {
                    // X, Y, radius, R, G, B, power

                    lines[i] = lines[i].Substring(6, lines[i].Length - 6);
                    lines[i] = lines[i].Trim();
                    lines[i] = lines[i].Replace("\n", "");

                    string[] tiles = lines[i].Split('#');

                    float lightX = float.Parse(tiles[0]);
                    float lightY = float.Parse(tiles[1]);
                    float radius = float.Parse(tiles[2]);

                    float R = float.Parse(tiles[3]);
                    float G = float.Parse(tiles[4]);
                    float B = float.Parse(tiles[5]);

                    float power = float.Parse(tiles[6], System.Globalization.CultureInfo.InvariantCulture.NumberFormat);

                    lights.Add(new float[] { lightX, lightY, radius, R, G, B, power });
                }


                if (lines[i].StartsWith("Darkness:"))
                {
                    lines[i] = lines[i].Substring(9, lines[i].Length - 9);
                    lines[i] = lines[i].Trim();
                    lines[i] = lines[i].Replace("\n", "");

                    darkness = float.Parse(lines[i], System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                }

            }

            height = y;

            return level;
        }

        public static List<Object[]> Load(out int width, out int height, out List<float[]> lights, out float darkness, string worldmap)
        {
            List<Object[]> level = new List<Object[]>();
            lights = new List<float[]>();

            darkness = 0.65f;
            string map = worldmap;

            int y = 0;
            int x = 0;
            width = 0;
            height = 0;

            string[] lines = map.Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].StartsWith("//") || lines[i].Length < 3)
                {
                    continue;
                }

                if (lines[i].StartsWith("|"))
                {
                    lines[i] = lines[i].Substring(1, lines[i].Length - 1);
                    lines[i] = lines[i].Trim();
                    lines[i] = lines[i].Replace("\n", "");

                    string[] tiles = lines[i].Split(',');
                    for (int ii = 0; ii < tiles.Length - 1; ii++) // tile lines end with ","
                    {
                        level.Add(new Object[] { tiles[ii], new Vector2(x, y) });

                        x += Global.GameConstants.TILE_SIZE;

                        if (x > width)
                        {
                            width = x;
                        }
                    }

                    x = 0;
                    y += Global.GameConstants.TILE_SIZE;
                }

                if (lines[i].StartsWith("Light:"))
                {
                    // X, Y, radius, R, G, B, power

                    lines[i] = lines[i].Substring(6, lines[i].Length - 6);
                    lines[i] = lines[i].Trim();
                    lines[i] = lines[i].Replace("\n", "");

                    string[] tiles = lines[i].Split('#');

                    float lightX = float.Parse(tiles[0]);
                    float lightY = float.Parse(tiles[1]);
                    float radius = float.Parse(tiles[2]);

                    float R = float.Parse(tiles[3]);
                    float G = float.Parse(tiles[4]);
                    float B = float.Parse(tiles[5]);

                    float power = float.Parse(tiles[6], System.Globalization.CultureInfo.InvariantCulture.NumberFormat);

                    lights.Add(new float[] { lightX, lightY, radius, R, G, B, power });
                }


                if (lines[i].StartsWith("Darkness:"))
                {
                    lines[i] = lines[i].Substring(9, lines[i].Length - 9);
                    lines[i] = lines[i].Trim();
                    lines[i] = lines[i].Replace("\n", "");

                    darkness = float.Parse(lines[i], System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                }

            }

            height = y;

            return level;
        }
    }
}