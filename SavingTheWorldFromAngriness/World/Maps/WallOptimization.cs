﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

using SavingTheWorldFromAngriness.World.Tiles;

namespace SavingTheWorldFromAngriness.World.Maps
{
    class WallOptimization
    {
        public static List<Tile> optimizeWalls(string map, TileManager tileManager)
        {
            List<Tile> wallHorizontalTiles;
            MaximizeTileHorizontal(map, tileManager, out wallHorizontalTiles);

            List<Tile> wallVerticalTiles;
            MaximizeTileVertical(map, tileManager, out wallVerticalTiles);

            List<Tile> wallSingleTiles;
            MaximizeTileSingles(map, tileManager, wallHorizontalTiles.Concat(wallVerticalTiles).ToList(), out wallSingleTiles);

            List<Tile> combined = wallHorizontalTiles.Concat(wallVerticalTiles).ToList().Concat(wallSingleTiles).ToList();

            return combined;
        }

        private static void MaximizeTileVertical(string map, TileManager tileManager, out List<Tile> wallVerticalTiles)
        {
            wallVerticalTiles = new List<Tile>();

            string[][] tilesMap = null;

            string[] lines = map.Split('\n');

            int count = 0;
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].StartsWith("|"))
                {
                    count++;
                }
            }

            tilesMap = new string[count][];

            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].StartsWith("|"))
                {
                    lines[i] = lines[i].Substring(1, lines[i].Length - 1);
                    lines[i] = lines[i].Trim();
                    lines[i] = lines[i].Replace("\n", "");

                    string[] tiles = lines[i].Split(',');
                    tilesMap[i] = tiles;
                }
            }

            List<List<Tile>> walls = new List<List<Tile>>();
            for (int i = 0; i <= tilesMap.Length; i++)
            {
                List<Tile> tileList = new List<Tile>();
                Tile beginTile = null;

                for (int ii = 0; ii < tilesMap.Length; ii++)
                {
                    if (tilesMap.Length <= ii)
                    {
                        continue;
                    }

                    if (tilesMap[ii].Length <= i)
                    {
                        continue;
                    }

                    string tileCode = tilesMap[ii][i];
                    Tile currentTile = tileManager.CreateNewTile(tileCode, new Vector2(i * Global.GameConstants.TILE_SIZE, ii * Global.GameConstants.TILE_SIZE));

                    if (currentTile != null && currentTile.type == Tile.Type.wall)
                    {
                        if (beginTile != null)
                        {
                            if (beginTile.name == currentTile.name)
                            {
                                // There is a beginner tile
                                if (tileList.Count == 0)
                                {
                                    // The tiles have not been added yet
                                    tileList.Add(beginTile);
                                    tileList.Add(currentTile);
                                }
                                else
                                {
                                    // The list already contains the beginTile
                                    tileList.Add(currentTile);
                                }
                            }
                            else
                            {
                                if (tileList.Count != 0)
                                {
                                    walls.Add(new List<Tile>(tileList));

                                    tileList = new List<Tile>();
                                    beginTile = null;
                                }

                                beginTile = currentTile;
                            }
                        }
                        else
                        {
                            beginTile = currentTile;
                        }
                    }
                    else
                    {
                        if (tileList.Count != 0)
                        {
                            walls.Add(new List<Tile>(tileList));
                        }

                        tileList = new List<Tile>();
                        beginTile = null;
                    }
                }

                if (tileList.Count != 0)
                {
                    walls.Add(new List<Tile>(tileList));
                }
            }

            foreach (List<Tile> wall in walls)
            {
                Tile wallTile = wall[0];
                Rectangle wallRectangle = new Rectangle((int)wall[0].position.X, (int)wall[0].position.Y, 0, 0);

                foreach (Tile tile in wall)
                {
                    wallRectangle = new Rectangle(
                        wallRectangle.X,
                        wallRectangle.Y,
                        Global.GameConstants.TILE_SIZE,
                        wallRectangle.Height + Global.GameConstants.TILE_SIZE);
                }

                Tile wallVerticalTile = new Tile(wallTile.position, wallTile.texture, wallTile.crop, wallTile.type, wallTile.name, wallRectangle);
                wallVerticalTiles.Add(wallVerticalTile);
            }
        }

        private static void MaximizeTileHorizontal(string map, TileManager tileManager, out List<Tile> wallHorizontalTiles)
        {
            wallHorizontalTiles = new List<Tile>();

            List<List<Tile>> walls = new List<List<Tile>>();

            string[] lines = map.Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                List<Tile> tileList = new List<Tile>();
                Tile beginTile = null;

                if (lines[i].StartsWith("//") || lines[i].Length < 3)
                {
                    continue;
                }

                if (lines[i].StartsWith("|"))
                {
                    lines[i] = lines[i].Substring(1, lines[i].Length - 1);
                    lines[i] = lines[i].Trim();
                    lines[i] = lines[i].Replace("\n", "");

                    string[] tiles = lines[i].Split(',');
                    for (int ii = 0; ii < tiles.Length - 1; ii++) // tile lines end with ","
                    {
                        string tileCode = tiles[ii].ToString();
                        Tile currentTile = tileManager.CreateNewTile(tileCode, new Vector2(ii * Global.GameConstants.TILE_SIZE, i * Global.GameConstants.TILE_SIZE));

                        if (currentTile != null && currentTile.type == Tile.Type.wall)
                        {
                            if (beginTile != null)
                            {
                                if (beginTile.name == currentTile.name)
                                {
                                    // There is a beginner tile
                                    if (tileList.Count == 0)
                                    {
                                        // The tiles have not been added yet
                                        tileList.Add(beginTile);
                                        tileList.Add(currentTile);
                                    }
                                    else
                                    {
                                        // The list already contains the beginTile
                                        tileList.Add(currentTile);
                                    }
                                }
                                else
                                {
                                    if (tileList.Count != 0)
                                    {
                                        walls.Add(new List<Tile>(tileList));

                                        tileList = new List<Tile>();
                                        beginTile = null;
                                    }

                                    beginTile = currentTile;
                                }
                            }
                            else
                            {
                                beginTile = currentTile;
                            }
                        }
                        else
                        {
                            if (tileList.Count != 0)
                            {
                                walls.Add(new List<Tile>(tileList));
                            }

                            tileList = new List<Tile>();
                            beginTile = null;
                        }
                    }
                }

                if (tileList.Count != 0)
                {
                    walls.Add(new List<Tile>(tileList));
                }
            }

            foreach (List<Tile> wall in walls)
            {
                Tile wallTile = wall[0];
                Rectangle wallRectangle = new Rectangle((int)wall[0].position.X, (int)wall[0].position.Y, 0, 0);

                foreach (Tile tile in wall)
                {
                    wallRectangle = new Rectangle(wallRectangle.X,
                        wallRectangle.Y,
                        wallRectangle.Width + Global.GameConstants.TILE_SIZE,
                        Global.GameConstants.TILE_SIZE);
                }

                Tile wallHorizontalTile = new Tile(wallTile.position, wallTile.texture, wallTile.crop, wallTile.type, wallTile.name, wallRectangle);
                wallHorizontalTiles.Add(wallHorizontalTile);
            }
        }

        private static void MaximizeTileSingles(string map, TileManager tileManager, List<Tile> ignoreTiles, out List<Tile> wallSingleTiles)
        {
            wallSingleTiles = new List<Tile>();

            string[] lines = map.Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].StartsWith("//") || lines[i].Length < 3)
                {
                    continue;
                }

                if (lines[i].StartsWith("|"))
                {
                    lines[i] = lines[i].Substring(1, lines[i].Length - 1);
                    lines[i] = lines[i].Trim();
                    lines[i] = lines[i].Replace("\n", "");

                    string[] tiles = lines[i].Split(',');
                    for (int ii = 0; ii < tiles.Length - 1; ii++) // tile lines end with ","
                    {
                        bool addTileToIgnoreList = true;
                        string tileCode = tiles[ii].ToString();
                        Tile currentTile = tileManager.CreateNewTile(tileCode, new Vector2(ii * Global.GameConstants.TILE_SIZE, i * Global.GameConstants.TILE_SIZE));

                        if (currentTile != null && currentTile.type == Tile.Type.wall)
                        {
                            foreach (Tile ignoreTile in ignoreTiles)
                            {
                                if (ignoreTile.rectangle.Intersects(currentTile.rectangle))
                                {
                                    if (currentTile.rectangle.Intersects(ignoreTile.rectangle))
                                    {
                                        addTileToIgnoreList = false;
                                    }
                                }
                            }

                            if (addTileToIgnoreList)
                            {
                                wallSingleTiles.Add(currentTile);
                            }
                        }
                    }
                }
            }
        }
    }
}
