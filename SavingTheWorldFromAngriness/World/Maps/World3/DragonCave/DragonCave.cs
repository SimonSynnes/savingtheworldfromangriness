﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.World.Components.World3;
using SavingTheWorldFromAngriness.GUI;

using GeonBit.UI;
using GeonBit.UI.Entities;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace SavingTheWorldFromAngriness.World.Maps.World3.DragonCave
{
    class DialogItem
    {
        public string title;
        public string text;
        public Texture2D avatar;
        public SoundEffect promptSoundEffect;
        public bool showDragon;

        public DialogItem(string title, string text, Texture2D avatar, SoundEffect promptSoundEffect, bool showDragon)
        {
            this.title = title;
            this.text = text;
            this.avatar = avatar;
            this.promptSoundEffect = promptSoundEffect;
            this.showDragon = showDragon;
        }
    }

    class DragonCave : BasicMap
    {
        #region beginning dialog
        private bool showKingTexture;
        private Vector2 kingTexturePosition;
        private Texture2D kingTexture;
        private Texture2D playerAvatarTexture;
        private Texture2D kingAvatarTexture;
        private SoundEffect promptSoundEffect1;
        private SoundEffect promptSoundEffect2;
        private List<DialogItem> kingDialogItems;
        private int kingDialogItemsIndex;
        private bool beginKingDialog;
        #endregion

        private bool updateDragon;
        private Dragon dragon;
        private SoundEffect lightning;

        public DragonCave(Game game, WorldRenderer worldRenderer)
            : base(game, worldRenderer)
        {
            #region beginning dialog
            this.kingDialogItems = new List<DialogItem>();
            this.kingDialogItemsIndex = 0;
            this.beginKingDialog = true;
            this.showKingTexture = true;
            this.kingTexturePosition = new Vector2(781, 578);
            #endregion

            this.updateDragon = false;
        }

        public override void Initialize()
        {
            base.Initialize();

            player.setPlayerPosition(new Vector2(225, 248));
            SetCameraFollowPlayer(false);
        }

        public override void Update(GameTime gameTime)
        {
            if (!Global.GameState.IS_GAME_PAUSED)
            {
                base.Update(gameTime);
            }


            int windowWidth = GraphicsDevice.Viewport.Width;
            int windowHeight = GraphicsDevice.Viewport.Height;

            #region beginning dialog
            if (beginKingDialog)
            {
                KingDialog();
                beginKingDialog = false;
            }

            if (showKingTexture)
            {
                camera.position = new Vector2((windowWidth / 2) - (worldRenderer.map_width / 2),
                    (windowHeight / 2) - (worldRenderer.map_height / 2));
            }
            #endregion

            if (updateDragon)
            {
                dragon.Update(gameTime);

                if (dragon.getHealth() <= 0)
                {
                    Global.GameState.IS_GAME_PAUSED = true;

                    EndGame();

                    updateDragon = false;
                }
            }
        }

        public void EndGame()
        {
            Global.Sounds.SoundManager.StopBackgroundMusic();

            Panel panel = new Panel(new Vector2(800, 600), PanelSkin.Simple);

            Label text = new Label("Congratulations. You have successfully saved the world from angriness.");
            text.AddHyphenWhenBreakWord = true;
            text.BreakWordsIfMust = true;
            text.FillColor = Color.White;
            text.Scale = 1.5f;

            Button btnRestart = new Button("Re-start game with same items", ButtonSkin.Fancy, Anchor.BottomCenter, null, new Vector2(0, 100));
            btnRestart.OnClick += (Entity entity) =>
            {
                Global.Player.INVENTORY.Clear();

                worldRenderer.LoadMap("World1", "Home");
            };

            Button btnExit = new Button("Exit to main menu", ButtonSkin.Fancy, Anchor.BottomCenter);
            btnExit.OnClick += (Entity entity) =>
            {
                UserInterface.Active.RemoveEntity(panel);
                worldRenderer.ClearMap();

                MainMenu startMenu = new MainMenu(this.Game, worldRenderer);
                Game.Components.Add(startMenu);

                Global.GameState.IS_GAME_PAUSED = false;
            };

            panel.AddChild(text);
            panel.AddChild(btnRestart);
            panel.AddChild(btnExit);

            UserInterface.Active.AddEntity(panel);
        }

        public override bool checkBulletCollision(Rectangle collisionRectangle)
        {
            bool returnValue = false;

            foreach (GameComponent gameComponent in gameComponents)
            {
                if (gameComponent.GetType() == typeof(Dragon))
                {
                    Rectangle enemyRectangle = ((Dragon)gameComponent).getEntityRectangle();

                    if (enemyRectangle.Intersects(collisionRectangle))
                    {
                        ((Dragon)gameComponent).AttackEnemy(player.getAttackPower());

                        returnValue = true;
                        break;
                    }
                }
            }

            return returnValue;
        }

        protected override void LoadContent()
        {
            Song backgroundSong = Game.Content.Load<Song>("Sounds/World3/faul");
            SoundManager.PlayBackgroundMusic(backgroundSong);

            lightning = Game.Content.Load<SoundEffect>("Sounds/World3/lightning");

            worldRenderer.SetBackground(new Backgrounds.FlyingSparks(this.Game), Color.DarkRed);

            Texture2D maskTexture = Game.Content.Load<Texture2D>("Textures/World3/mask");
            worldRenderer.AddBackgroundMask(maskTexture);

            TorchLight TorchLight3678 = new TorchLight(Game, worldRenderer, this, new Rectangle(163 + 32 * 5, 228, 0, 0), 8, 99);
            AddGameComponent(TorchLight3678);

            LevelDoor LevelDoor7537 = new LevelDoor(Game, worldRenderer, this, new Rectangle(149, 218, 32, 75), "World2", "PreBossBridge");
            AddGameComponent(LevelDoor7537);

            #region beginning dialog
            kingAvatarTexture = Game.Content.Load<Texture2D>("Textures/World1/Castle/kingAvatar");
            promptSoundEffect1 = Game.Content.Load<SoundEffect>("Sounds/World1/blip1");
            promptSoundEffect2 = Game.Content.Load<SoundEffect>("Sounds/World1/blip2");
            playerAvatarTexture = Game.Content.Load<Texture2D>("Textures/playerAvatar");
            kingTexture = Game.Content.Load<Texture2D>("Temp/oldking");

            FillKingDialog();
            #endregion


            Ammo Ammo7119 = new Ammo(Game, worldRenderer, this, new Rectangle(318, 834, 0, 0));
            AddGameComponent(Ammo7119);
            Ammo Ammo8738 = new Ammo(Game, worldRenderer, this, new Rectangle(305, 850, 0, 0));
            AddGameComponent(Ammo8738);
            Ammo Ammo7736 = new Ammo(Game, worldRenderer, this, new Rectangle(374, 862, 0, 0));
            AddGameComponent(Ammo7736);
            Ammo Ammo4384 = new Ammo(Game, worldRenderer, this, new Rectangle(369, 841, 0, 0));
            AddGameComponent(Ammo4384);
            Ammo Ammo7565 = new Ammo(Game, worldRenderer, this, new Rectangle(1273, 944, 0, 0));
            AddGameComponent(Ammo7565);
            Ammo Ammo7676 = new Ammo(Game, worldRenderer, this, new Rectangle(1256, 964, 0, 0));
            AddGameComponent(Ammo7676);
            Ammo Ammo5738 = new Ammo(Game, worldRenderer, this, new Rectangle(1255, 943, 0, 0));
            AddGameComponent(Ammo5738);
            Ammo Ammo9341 = new Ammo(Game, worldRenderer, this, new Rectangle(1289, 922, 0, 0));
            AddGameComponent(Ammo9341);
            Ammo Ammo9215 = new Ammo(Game, worldRenderer, this, new Rectangle(1191, 265, 0, 0));
            AddGameComponent(Ammo9215);
            Ammo Ammo9843 = new Ammo(Game, worldRenderer, this, new Rectangle(1177, 211, 0, 0));
            AddGameComponent(Ammo9843);
            Ammo Ammo1494 = new Ammo(Game, worldRenderer, this, new Rectangle(1157, 199, 0, 0));
            AddGameComponent(Ammo1494);
            Ammo Ammo2770 = new Ammo(Game, worldRenderer, this, new Rectangle(1174, 255, 0, 0));
            AddGameComponent(Ammo2770);
            Ammo Ammo6608 = new Ammo(Game, worldRenderer, this, new Rectangle(415, 230, 0, 0));
            AddGameComponent(Ammo6608);
            Ammo Ammo7293 = new Ammo(Game, worldRenderer, this, new Rectangle(447, 226, 0, 0));
            AddGameComponent(Ammo7293);
            Ammo Ammo9761 = new Ammo(Game, worldRenderer, this, new Rectangle(445, 221, 0, 0));
            AddGameComponent(Ammo9761);
            Ammo Ammo4323 = new Ammo(Game, worldRenderer, this, new Rectangle(439, 219, 0, 0));
            AddGameComponent(Ammo4323);
            Ammo Ammo3047 = new Ammo(Game, worldRenderer, this, new Rectangle(448, 1126, 0, 0));
            AddGameComponent(Ammo3047);
            Ammo Ammo2502 = new Ammo(Game, worldRenderer, this, new Rectangle(1878, 196, 0, 0));
            AddGameComponent(Ammo2502);
            Ammo Ammo8194 = new Ammo(Game, worldRenderer, this, new Rectangle(1619, 511, 0, 0));
            AddGameComponent(Ammo8194);
            Ammo Ammo6139 = new Ammo(Game, worldRenderer, this, new Rectangle(1720, 638, 0, 0));
            AddGameComponent(Ammo6139);
            Ammo Ammo9395 = new Ammo(Game, worldRenderer, this, new Rectangle(538, 1136, 0, 0));
            AddGameComponent(Ammo9395);
            Ammo Ammo7203 = new Ammo(Game, worldRenderer, this, new Rectangle(500, 1131, 0, 0));
            AddGameComponent(Ammo7203);
            HealthPack HealthPack3515 = new HealthPack(Game, worldRenderer, this, new Rectangle(505, 225, 0, 0));
            AddGameComponent(HealthPack3515);
            HealthPack HealthPack9985 = new HealthPack(Game, worldRenderer, this, new Rectangle(518, 218, 0, 0));
            AddGameComponent(HealthPack9985);
            HealthPack HealthPack5065 = new HealthPack(Game, worldRenderer, this, new Rectangle(547, 208, 0, 0));
            AddGameComponent(HealthPack5065);
            HealthPack HealthPack6155 = new HealthPack(Game, worldRenderer, this, new Rectangle(1253, 300, 0, 0));
            AddGameComponent(HealthPack6155);
            HealthPack HealthPack4847 = new HealthPack(Game, worldRenderer, this, new Rectangle(1252, 296, 0, 0));
            AddGameComponent(HealthPack4847);
            HealthPack HealthPack1686 = new HealthPack(Game, worldRenderer, this, new Rectangle(1244, 288, 0, 0));
            AddGameComponent(HealthPack1686);
            HealthPack HealthPack8675 = new HealthPack(Game, worldRenderer, this, new Rectangle(1269, 917, 0, 0));
            AddGameComponent(HealthPack8675);
            HealthPack HealthPack1463 = new HealthPack(Game, worldRenderer, this, new Rectangle(1263, 929, 0, 0));
            AddGameComponent(HealthPack1463);
            HealthPack HealthPack1926 = new HealthPack(Game, worldRenderer, this, new Rectangle(1252, 948, 0, 0));
            AddGameComponent(HealthPack1926);
            HealthPack HealthPack9071 = new HealthPack(Game, worldRenderer, this, new Rectangle(350, 908, 0, 0));
            AddGameComponent(HealthPack9071);
            HealthPack HealthPack1544 = new HealthPack(Game, worldRenderer, this, new Rectangle(342, 916, 0, 0));
            AddGameComponent(HealthPack1544);
            HealthPack HealthPack5187 = new HealthPack(Game, worldRenderer, this, new Rectangle(332, 905, 0, 0));
            AddGameComponent(HealthPack5187);
            HealthPack HealthPack2366 = new HealthPack(Game, worldRenderer, this, new Rectangle(506, 998, 0, 0));
            AddGameComponent(HealthPack2366);
            HealthPack HealthPack4148 = new HealthPack(Game, worldRenderer, this, new Rectangle(773, 995, 0, 0));
            AddGameComponent(HealthPack4148);
            HealthPack HealthPack8938 = new HealthPack(Game, worldRenderer, this, new Rectangle(1285, 553, 0, 0));
            AddGameComponent(HealthPack8938);
            HealthPack HealthPack1076 = new HealthPack(Game, worldRenderer, this, new Rectangle(1043, 204, 0, 0));
            AddGameComponent(HealthPack1076);
            HealthPack HealthPack1968 = new HealthPack(Game, worldRenderer, this, new Rectangle(806, 202, 0, 0));
            AddGameComponent(HealthPack1968);
            
            base.LoadContent();
        }

        public override void DrawBehindShadows()
        {
            player.Draw(spriteBatch, camera);

            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                SamplerState.LinearWrap,
                null,
                null,
                null,
                camera.TransformMatrix);

            if (showKingTexture)
            {
                spriteBatch.Draw(kingTexture, kingTexturePosition, Color.White);
            }

            foreach (GameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).Draw(spriteBatch);
            }

            spriteBatch.End();
        }

        #region begin dialog
        public void KingDialog()
        {
            Global.GameState.IS_GAME_PAUSED = true;

            if (kingDialogItemsIndex <= (kingDialogItems.Count - 1))
            {
                DialogItem item = kingDialogItems[kingDialogItemsIndex];

                if (item.showDragon)
                {
                    SoundManager.PlaySoundEffect(lightning);
                    dragon = new Dragon(this.Game, worldRenderer, this, new Rectangle(325 + 324 + 90, 248 + 217 + 90, 0, 0));
                    AddGameComponent(dragon);

                    showKingTexture = false;
                }

                DialogPrompt prompt = new DialogPrompt(this.Game, item.title, item.avatar, item.text, item.promptSoundEffect);
                Game.Components.Add(prompt);

                prompt.getOKButton().OnClick += (Entity entity) =>
                {
                    KingDialog();
                    Game.Components.Remove(prompt);
                };

                kingDialogItemsIndex++;
            }
            else
            {
                updateDragon = true;
                SetCameraFollowPlayer(true);
                Global.GameState.IS_GAME_PAUSED = false;
            }
        }

        public override void OnPlayerDeath()
        {
            worldRenderer.LoadMap("World3", "DragonCave");

            base.OnPlayerDeath();
        }

        public void FillKingDialog()
        {
            kingDialogItems.Add(new DialogItem("The King", "...", kingAvatarTexture, promptSoundEffect2, false));
            kingDialogItems.Add(new DialogItem(Global.Player.PLAYER_NAME, "!!???", playerAvatarTexture, promptSoundEffect1, true));
        }
        #endregion
    }
}
