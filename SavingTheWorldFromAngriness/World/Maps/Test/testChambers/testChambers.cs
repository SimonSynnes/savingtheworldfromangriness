﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.World.Components;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace SavingTheWorldFromAngriness.World.Maps.Test.testChambers
{
    class testChambers : BasicMap
    {
        public testChambers(Game game, WorldRenderer worldRenderer) 
            : base(game, worldRenderer) { }

        public override void Initialize()
        {
            base.Initialize();

            player.setPlayerPosition(new Vector2(32 * 10, 32 * 8));
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            player.SetWalkingSpeed(9);
            Texture2D textChamberBackgroundTexture = Game.Content.Load<Texture2D>("Temp/TestingChambers2");

            Texture2D World1 = Game.Content.Load<Texture2D>("Temp/World1");
            Texture2D World2 = Game.Content.Load<Texture2D>("Temp/World2");
            Texture2D World3 = Game.Content.Load<Texture2D>("Temp/World3");

            LevelDoor levelDoorWorld1 = new LevelDoor(this.Game, worldRenderer, this, new Rectangle(34, 450, 170, 108), "World1", "BeginningWorld1", World1);
            AddGameComponent(levelDoorWorld1);

            LevelDoor levelDoorWorld2 = new LevelDoor(this.Game, worldRenderer, this, new Rectangle(220, 450, 170, 108), "World2", "BeginningWorld2", World2);
            AddGameComponent(levelDoorWorld2);

            LevelDoor levelDoorWorld3 = new LevelDoor(this.Game, worldRenderer, this, new Rectangle(406, 450, 170, 108), "World3", "BeginningWorld3", World3);
            AddGameComponent(levelDoorWorld3);

            BasicComponent testChamberBackground = new BasicComponent(Game, worldRenderer, this, new Rectangle(177, 100, 285, 259), textChamberBackgroundTexture);
            AddGameComponent(testChamberBackground);

        }
    }
}
