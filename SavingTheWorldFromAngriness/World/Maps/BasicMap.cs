﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.GUI;
using SavingTheWorldFromAngriness.World.Components;
using SavingTheWorldFromAngriness.Global.Sounds;

using GeonBit.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SavingTheWorldFromAngriness.World.Maps
{
    public class BasicMap : DrawableGameComponent
    {
        public SpriteBatch spriteBatch { get; private set; }

        public Player player { get; private set; }
        public Camera camera { get; private set; }
        public WorldRenderer worldRenderer { get; private set; }
        public PlayerUI PlayerUI { get; private set; }

        public List<IGameComponent> gameComponents;

        private bool isEnterKeyDown;
        private bool cameraFollowPlayer;
        public bool isPlayerDead { get; private set; }

        public BasicMap(Game game, WorldRenderer worldRenderer)
            : base(game)
        {
            this.camera = worldRenderer.getCamera();
            this.worldRenderer = worldRenderer;
            this.isEnterKeyDown = false;
            this.cameraFollowPlayer = true;
            this.gameComponents = new List<IGameComponent>();
            this.isPlayerDead = false;
        }

        public virtual void RemoveAllGameComponents()
        {
            SoundManager.StopBackgroundMusic();
            SoundManager.ClearRepeatingSoundEffects();

            foreach (IGameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).RemoveAllGameComponents();
                Game.Components.Remove(gameComponent);
            }

            Game.Components.Remove(player);

            PlayerUI.ClearUI();
            Game.Components.Remove(PlayerUI);

            if (!isPlayerDead)
            {
                Global.Player.LAST_MAP = this.GetType();
            }
        }

        public void SetCamera(Camera camera)
        {
            this.camera = camera;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public void SetCameraFollowPlayer(bool value)
        {
            cameraFollowPlayer = value;
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            this.player = new Player(this.Game, worldRenderer, this);
            Game.Components.Add(player);

            this.PlayerUI = new PlayerUI(Game);
            Game.Components.Add(PlayerUI);

            base.LoadContent();
        }

        public void InitializeSpriteBatch()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        public void AddPlayerComponent()
        {
            this.player = new Player(this.Game, worldRenderer, this);
            Game.Components.Add(player);
        }

        public virtual bool checkBulletCollision(Rectangle collisionRectangle)
        {
            bool returnValue = false;

            foreach (GameComponent gameComponent in gameComponents)
            {
                if (gameComponent.GetType() == typeof(Enemy))
                {
                    Rectangle enemyRectangle = ((Enemy)gameComponent).getEntityRectangle();

                    if (enemyRectangle.Intersects(collisionRectangle))
                    {
                        ((Enemy)gameComponent).AttackEnemy(player.getAttackPower());

                        returnValue = true;
                        break;
                    }
                }
            }

            return returnValue;
        }

        public bool checkEntityCollision(Rectangle collisionRectangle)
        {
            bool returnValue = false;

            foreach (GameComponent gameComponent in gameComponents)
            {
                if (gameComponent.GetType().BaseType == typeof(MovingEntity))
                {
                    Rectangle enemyRectangle = ((MovingEntity)gameComponent).getEntityRectangle();

                    if (enemyRectangle.Intersects(collisionRectangle))
                    {
                        returnValue = true;
                        break;
                    }
                }
            }

            return returnValue;
        }

        public bool checkEntityCollision(Rectangle collisionRectangle, Rectangle ignoreCollisionRectangle)
        {
            bool returnValue = false;

            foreach (GameComponent gameComponent in gameComponents)
            {
                if (gameComponent.GetType().BaseType == typeof(MovingEntity))
                {
                    Rectangle enemyRectangle = ((MovingEntity)gameComponent).getEntityRectangle();

                    if (enemyRectangle == ignoreCollisionRectangle)
                    {
                        continue;
                    }

                    if (enemyRectangle.Intersects(collisionRectangle))
                    {
                        returnValue = true;
                        break;
                    }
                }
            }

            return returnValue;
        }

        public void SetCameraToPlayerPosition()
        {
            int windowWidth = GraphicsDevice.Viewport.Width;
            int windowHeight = GraphicsDevice.Viewport.Height;

            Vector2 newCameraPosition = -(player.getPlayerPosition()) + (new Vector2(windowWidth / 2, windowHeight / 2));
            int newCameraX = Convert.ToInt32(newCameraPosition.X);
            int newCameraY = Convert.ToInt32(newCameraPosition.Y);

            camera.position = new Vector2(newCameraX, newCameraY);
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();
            MouseState ms = Mouse.GetState();

            float mouseX = (camera.matrixTranslation().X - ms.X) * -1;
            float mouseY = (camera.matrixTranslation().Y - ms.Y) * -1;
            if (!Global.GameState.IS_GAME_PAUSED)
            {
                player.Update(gameTime, ks, ms, new Vector2(mouseX, mouseY));
            }

            if (player.getHealth() <= 0)
            {
                OnPlayerDeath();
            }

            if (cameraFollowPlayer)
            {
                SetCameraToPlayerPosition();
            }

            if (ks.IsKeyDown(Keys.Enter))
            {
                isEnterKeyDown = true;
            }
            else
            {
                // Check if enter key has been pressed and then released
                // (in order to only call functions once)
                if (isEnterKeyDown)
                {
                    foreach (GameComponent gameComponent in gameComponents)
                    {
                        if (((BasicComponent)gameComponent).checkCollision(player.getPlayerRectangle()))
                        {
                            ((BasicComponent)gameComponent).InteractWithTile();

                            break;
                        }
                    }
                }

                isEnterKeyDown = false;
            }

            base.Update(gameTime);
        }

        /**
         * The WorldRenderer calls this method BEFORE drawing the lighting over the world map.
         * Used to draw most all components since most all components should have lighting.
         */
        public virtual void DrawBehindShadows()
        {
            player.Draw(spriteBatch, camera);

            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                SamplerState.LinearWrap,
                null,
                null,
                null,
                camera.TransformMatrix);

            foreach (GameComponent gameComponent in gameComponents)
            {
                ((BasicComponent)gameComponent).Draw(spriteBatch);
            }

            spriteBatch.End();
        }

        /**
         * Method called when player has 0 health.
         */
        public virtual void OnPlayerDeath()
        {
            isPlayerDead = true;
            Global.Player.HEALTH = 100;
            Global.Player.COINS = 0;
            Global.Player.AMMO = 0;
            Global.Player.AMMO_IN_MAGAZINE = 0;
        }

        public void AddGameComponent(BasicComponent gameComponent)
        {
            lock (gameComponents)
            {
                gameComponents.Add(gameComponent);
                Game.Components.Add(gameComponent);
            }
        }

        public void RemoveGameComponent(BasicComponent gameComponent)
        {
            lock (gameComponents)
            {
                gameComponent.RemoveAllGameComponents();
                gameComponents.Remove(gameComponent);
                Game.Components.Remove(gameComponent);
            }
        }

        public void PlayBackgroundMusic(Song song)
        {
            SoundManager.PlayBackgroundMusic(song);
        }

        public override void Draw(GameTime gameTime)
        {
            PlayerUI.Draw(spriteBatch);

            base.Draw(gameTime);
        }
    }
}
