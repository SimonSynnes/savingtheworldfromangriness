﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SavingTheWorldFromAngriness.World.Tiles
{
    // Retrieve tile from code
    public class TileTranslate
    {
        private static Object[][] tiles = new Object[][]
        {
            //row 1
            new Object[] { new Vector2(0, 0), new Vector2(32, 32), Tile.Type.floor, "000" },
            new Object[] { new Vector2(32, 0), new Vector2(32, 32), Tile.Type.floor, "001" },
            new Object[] { new Vector2(64, 0), new Vector2(32, 32), Tile.Type.wall, "002" },
            new Object[] { new Vector2(96, 0), new Vector2(32, 32), Tile.Type.wall, "019" },
            new Object[] { new Vector2(128, 0), new Vector2(32, 32), Tile.Type.floor, "020" },
            new Object[] { new Vector2(160, 0), new Vector2(32, 32), Tile.Type.wall, "021" },
            new Object[] { new Vector2(192, 0), new Vector2(32, 32), Tile.Type.floor, "026" }, //white
            new Object[] { new Vector2(224, 0), new Vector2(32, 32), Tile.Type.wall, "027" }, //black
            new Object[] { new Vector2(256, 0), new Vector2(32, 32), Tile.Type.floor, "a27" },
            new Object[] { new Vector2(288, 0), new Vector2(32, 32), Tile.Type.floor, "b27" },
            new Object[] { new Vector2(320, 0), new Vector2(32, 32), Tile.Type.floor, "c27" },
            new Object[] { new Vector2(352, 0), new Vector2(32, 32), Tile.Type.floor, "d27" },
            //row 2
            new Object[] { new Vector2(0, 32), new Vector2(32, 32), Tile.Type.floor, "003" },
            new Object[] { new Vector2(32, 32), new Vector2(32, 32), Tile.Type.floor, "005" },
            new Object[] { new Vector2(64, 32), new Vector2(32, 32), Tile.Type.floor, "007" },
            new Object[] { new Vector2(96, 32), new Vector2(32, 32), Tile.Type.floor, "009" },
            new Object[] { new Vector2(128, 32), new Vector2(32, 32), Tile.Type.floor, "011" },
            new Object[] { new Vector2(128, 32), new Vector2(32, 32), Tile.Type.wall, "F011" },
            new Object[] { new Vector2(160, 32), new Vector2(32, 32), Tile.Type.floor, "013" },
            new Object[] { new Vector2(192, 32), new Vector2(32, 32), Tile.Type.floor, "015" },
            new Object[] { new Vector2(192, 32), new Vector2(32, 32), Tile.Type.wall, "F015" },
            new Object[] { new Vector2(224, 32), new Vector2(32, 32), Tile.Type.floor, "017" },// water
            new Object[] { new Vector2(256, 32), new Vector2(32, 32), Tile.Type.wall, "a17" },// water
            new Object[] { new Vector2(288, 32), new Vector2(32, 32), Tile.Type.wall, "b17" },// water
            new Object[] { new Vector2(320, 32), new Vector2(32, 32), Tile.Type.floor, "c17" },// water
            new Object[] { new Vector2(352, 32), new Vector2(32, 32), Tile.Type.floor, "d17" },// water
            //row 3
            new Object[] { new Vector2(0, 64), new Vector2(32, 32), Tile.Type.wall, "004" },
            new Object[] { new Vector2(32, 64), new Vector2(32, 32), Tile.Type.wall, "006" },
            new Object[] { new Vector2(64, 64), new Vector2(32, 32), Tile.Type.wall, "008" },
            new Object[] { new Vector2(96, 64), new Vector2(32, 32), Tile.Type.wall, "010" },
            new Object[] { new Vector2(128, 64), new Vector2(32, 32), Tile.Type.wall, "012" },
            new Object[] { new Vector2(160, 64), new Vector2(32, 32), Tile.Type.wall, "014" },
            new Object[] { new Vector2(192, 64), new Vector2(32, 32), Tile.Type.wall, "016" },
            new Object[] { new Vector2(224, 64), new Vector2(32, 32), Tile.Type.wall, "018" },
            new Object[] { new Vector2(256, 64), new Vector2(32, 32), Tile.Type.floor, "a18" }, // water
            new Object[] { new Vector2(256, 64), new Vector2(32, 32), Tile.Type.wall, "a18W" }, // water
            new Object[] { new Vector2(288, 64), new Vector2(32, 32), Tile.Type.floor, "b18" },
            new Object[] { new Vector2(320, 64), new Vector2(32, 32), Tile.Type.floor, "c18" },
            new Object[] { new Vector2(352, 64), new Vector2(32, 32), Tile.Type.floor, "d18" },
            //row 4
            new Object[] { new Vector2(0, 96), new Vector2(32, 32), Tile.Type.floor, "022" },
            new Object[] { new Vector2(32, 96), new Vector2(32, 32), Tile.Type.floor, "024" },
            new Object[] { new Vector2(64, 96), new Vector2(32, 32), Tile.Type.floor, "028" },
            new Object[] { new Vector2(96, 96), new Vector2(32, 32), Tile.Type.floor, "029" },
            new Object[] { new Vector2(128, 96), new Vector2(32, 32), Tile.Type.floor, "030" },
            new Object[] { new Vector2(160, 96), new Vector2(32, 32), Tile.Type.floor, "031" },
            new Object[] { new Vector2(192, 96), new Vector2(32, 32), Tile.Type.floor, "032" },
            new Object[] { new Vector2(224, 96), new Vector2(32, 32), Tile.Type.floor, "a32" },
            new Object[] { new Vector2(256, 96), new Vector2(32, 32), Tile.Type.floor, "b32" },
            new Object[] { new Vector2(288, 96), new Vector2(32, 32), Tile.Type.floor, "c32" },
            new Object[] { new Vector2(320, 96), new Vector2(32, 32), Tile.Type.floor, "d32" },
            new Object[] { new Vector2(352, 96), new Vector2(32, 32), Tile.Type.floor, "e32" },
            //row 5
            new Object[] { new Vector2(0, 128), new Vector2(32, 32), Tile.Type.wall, "023" },
            new Object[] { new Vector2(32, 128), new Vector2(32, 32), Tile.Type.wall, "025" },
            new Object[] { new Vector2(64, 128), new Vector2(32, 32), Tile.Type.floor, "033" },
            new Object[] { new Vector2(96, 128), new Vector2(32, 32), Tile.Type.floor, "034" },
            new Object[] { new Vector2(128, 128), new Vector2(32, 32), Tile.Type.floor, "035" },
            new Object[] { new Vector2(160, 128), new Vector2(32, 32), Tile.Type.floor, "036" },
            new Object[] { new Vector2(192, 128), new Vector2(32, 32), Tile.Type.wall, "037" },
            new Object[] { new Vector2(224, 128), new Vector2(32, 32), Tile.Type.floor, "038" },
            new Object[] { new Vector2(256, 128), new Vector2(32, 32), Tile.Type.wall, "039" },
            new Object[] { new Vector2(288, 128), new Vector2(32, 32), Tile.Type.wall, "040" },
            new Object[] { new Vector2(320, 128), new Vector2(32, 32), Tile.Type.wall, "041" },
            new Object[] { new Vector2(352, 128), new Vector2(32, 32), Tile.Type.wall, "042" },
            //row 6
            new Object[] { new Vector2(0, 160), new Vector2(32, 32), Tile.Type.wall, "043" },
            new Object[] { new Vector2(32, 160), new Vector2(32, 32), Tile.Type.wall, "044" },
            new Object[] { new Vector2(64, 160), new Vector2(32, 32), Tile.Type.floor, "045" },
            new Object[] { new Vector2(96, 160), new Vector2(32, 32), Tile.Type.floor, "046" },
            new Object[] { new Vector2(128, 160), new Vector2(32, 32), Tile.Type.floor, "047" },
            new Object[] { new Vector2(160, 160), new Vector2(32, 32), Tile.Type.floor, "048" },
            new Object[] { new Vector2(192, 160), new Vector2(32, 32), Tile.Type.floor, "049" },
            new Object[] { new Vector2(224, 160), new Vector2(32, 32), Tile.Type.floor, "050" },
            new Object[] { new Vector2(256, 160), new Vector2(32, 32), Tile.Type.floor, "051" },
            new Object[] { new Vector2(288, 160), new Vector2(32, 32), Tile.Type.floor, "052" },
            new Object[] { new Vector2(320, 160), new Vector2(32, 32), Tile.Type.floor, "053" },
            new Object[] { new Vector2(352, 160), new Vector2(32, 32), Tile.Type.floor, "054" },

            //row 7
            new Object[] { new Vector2(128, 192), new Vector2(32, 32), Tile.Type.floor, "059" }, //lava
            new Object[] { new Vector2(160, 192), new Vector2(32, 32), Tile.Type.floor, "b33" },
            new Object[] { new Vector2(192, 192), new Vector2(32, 32), Tile.Type.floor, "b34" },
            new Object[] { new Vector2(224, 192), new Vector2(32, 32), Tile.Type.floor, "060" },
            new Object[] { new Vector2(256, 192), new Vector2(32, 32), Tile.Type.floor, "061" },
            new Object[] { new Vector2(288, 192), new Vector2(32, 32), Tile.Type.floor, "062" },
            new Object[] { new Vector2(320, 192), new Vector2(32, 32), Tile.Type.floor, "063" },
            new Object[] { new Vector2(352, 192), new Vector2(32, 32), Tile.Type.wall, "064" },
            //rad 8
            //rad 9
            //rad 10
            //rad 11
            //rad 12
            //rad 13
            //rad 14
            new Object[] { new Vector2(0, 416), new Vector2(32, 32), Tile.Type.floor, "132" },
            new Object[] { new Vector2(32, 416), new Vector2(32, 32), Tile.Type.floor, "133" },
            new Object[] { new Vector2(64, 416), new Vector2(32, 32), Tile.Type.floor, "134" },
            new Object[] { new Vector2(96, 416), new Vector2(32, 32), Tile.Type.floor, "135" },
            new Object[] { new Vector2(128, 416), new Vector2(32, 32), Tile.Type.floor, "136" },
            new Object[] { new Vector2(160, 416), new Vector2(32, 32), Tile.Type.floor, "137" },
            new Object[] { new Vector2(192, 416), new Vector2(32, 32), Tile.Type.floor, "138" },
            new Object[] { new Vector2(224, 416), new Vector2(32, 32), Tile.Type.floor, "139" },
            new Object[] { new Vector2(256, 416), new Vector2(32, 32), Tile.Type.floor, "140" },
            new Object[] { new Vector2(288, 416), new Vector2(32, 32), Tile.Type.floor, "141" },
            new Object[] { new Vector2(320, 416), new Vector2(32, 32), Tile.Type.floor, "142" },
            new Object[] { new Vector2(352, 416), new Vector2(32, 32), Tile.Type.floor, "143" },
            //rad 15
            new Object[] { new Vector2(0, 448), new Vector2(32, 32), Tile.Type.floor, "144" },
            new Object[] { new Vector2(32, 448), new Vector2(32, 32), Tile.Type.floor, "145" },
            new Object[] { new Vector2(64, 448), new Vector2(32, 32), Tile.Type.floor, "146" },
            // rad 16
            new Object[] { new Vector2(0, 480), new Vector2(32, 32), Tile.Type.wall, "156" },
            new Object[] { new Vector2(32, 480), new Vector2(32, 32), Tile.Type.floor, "157" },
            new Object[] { new Vector2(64, 480), new Vector2(32, 32), Tile.Type.floor, "158" },
            new Object[] { new Vector2(96, 480), new Vector2(32, 32), Tile.Type.floor, "159" },
        };

        public static Object[] translate(string tileCode)
        {
            Object[] returnValue = null;

            // returnValue[0] = position in tilesheet
            // returnValue[1] = tile size
            // returnValue[2] = tile type
            // returnValue[3] = tile name

            for (int i = 0; i < tiles.Length; i++)
            {
                if (tileCode == tiles[i][3].ToString())
                {
                    returnValue = tiles[i];
                    break;
                }
            }

            return returnValue;
        }

        public static Object[][] getTilesArray()
        {
            return tiles;
        }
    }
}
