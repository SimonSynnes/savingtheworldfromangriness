﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SavingTheWorldFromAngriness.World.Tiles
{
    public class Tile
    {
        public Texture2D texture { get; set; }
        public Vector2 position { get; set; }
        public Vector2 crop { get; set; }
        public Type type { get; set; }
        public string name { get; set; }
        public Rectangle rectangle { get; set; }

        public Tile(Vector2 position, Texture2D texture, Vector2 crop, Type type, string name, Rectangle rectangle)
        {
            this.texture = texture;
            this.position = position;
            this.crop = crop;
            this.type = type;
            this.name = name;
            this.rectangle = rectangle;
        }

        public Texture2D Crop(Rectangle source)
        {
            Texture2D croppedTexture2d = new Texture2D(texture.GraphicsDevice, source.Width, source.Height);
            Color[] imageData = new Color[texture.Width * texture.Height];
            Color[] croppedImageData = new Color[source.Width * source.Height];

            texture.GetData<Color>(imageData);
            
            int index = 0;

            for (int y = source.Y; y < source.Y + source.Height; y++)
            {
                for (int x = source.X; x < source.X + source.Width; x++)
                {
                    croppedImageData[index] = imageData[y * texture.Width + x];
                    index++;
                }
            }

            croppedTexture2d.SetData<Color>(croppedImageData);
            return croppedTexture2d;
        }

        public enum Type : ushort
        {
            wall = 0,
            floor = 1,
            mechanism = 2
        };
    }
}
