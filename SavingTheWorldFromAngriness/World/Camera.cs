﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SavingTheWorldFromAngriness.World
{
    public class Camera
    {
        public Vector2 position { get; set; }
        public float rotation { get; set; }
        public float zoom { get; set; }

        public Camera()
        {
            position = Vector2.Zero;
            rotation = 0f;
            zoom = 1f;
        }

        public Matrix TransformMatrix
        {
            get
            {
                return Matrix.CreateRotationZ(rotation) * Matrix.CreateScale(zoom) *
                       Matrix.CreateTranslation(position.X, position.Y, 0);
            }
        }

        public Vector2 matrixTranslation()
        {
            return new Vector2(
                TransformMatrix.Translation.X,
                TransformMatrix.Translation.Y);
        }
    }
}
