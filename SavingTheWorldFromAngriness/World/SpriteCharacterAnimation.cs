﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.World.Maps;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.World
{
    public class SpriteCharacterAnimation : DrawableGameComponent
    {
        private enum SpriteWalkingDirection
        {
            DOWN = 0,
            LEFT = 1,
            RIGHT = 2,
            UP = 3
        }

        private Rectangle[][] tileAnimationCrops = new Rectangle[][]
        {
            new Rectangle[] { Rectangle.Empty, Rectangle.Empty },   // tileAnimationCrops[0] = DOWN
            new Rectangle[] { Rectangle.Empty, Rectangle.Empty },   // tileAnimationCrops[1] = LEFT
            new Rectangle[] { Rectangle.Empty, Rectangle.Empty },   // tileAnimationCrops[2] = RIGHT
            new Rectangle[] { Rectangle.Empty, Rectangle.Empty }    // tileAnimationCrops[3] = UP
        };

        WorldRenderer worldRenderer;

        private bool isMoving;
        private int animationSpeed;
        private double elapsedTime;

        private Vector2 characterPosition;
        private Vector2 characterSize;

        private Rectangle characterRectangle;
        private float characterMovementSpeed;

        private Texture2D tileSheet;
        private string tileSheetFileName;
        private int currentWalkingRectangleIndex;
        private SpriteWalkingDirection currentWalkingDirection;

        private BasicMap basicMap;
        private int aggroDistance;

        public SpriteCharacterAnimation(Game game, WorldRenderer worldRenderer, BasicMap basicMap, string tileSheet, Rectangle[] tileAnimationDown, Rectangle[] tileAnimationLeft,
            Rectangle[] tileAnimationRight, Rectangle[] tileAnimationUp, int animationSpeed, int characterSpeed, Vector2 characterPosition, Vector2 characterSize, int aggroDistance, int enemyStartDirection) : base(game)
        {
            tileAnimationCrops[(int)SpriteWalkingDirection.UP] = tileAnimationUp;
            tileAnimationCrops[(int)SpriteWalkingDirection.DOWN] = tileAnimationDown;
            tileAnimationCrops[(int)SpriteWalkingDirection.LEFT] = tileAnimationLeft;
            tileAnimationCrops[(int)SpriteWalkingDirection.RIGHT] = tileAnimationRight;

            this.elapsedTime = 0;
            this.currentWalkingRectangleIndex = 0;
            this.tileSheetFileName = tileSheet;
            this.isMoving = false;
            this.animationSpeed = animationSpeed;
            this.characterMovementSpeed = characterSpeed;
            this.characterSize = characterSize;
            this.worldRenderer = worldRenderer;
            this.characterPosition = characterPosition;
            this.currentWalkingDirection = (SpriteWalkingDirection)enemyStartDirection;
            this.characterRectangle = new Rectangle((int)characterPosition.X, (int)characterPosition.Y, (int)characterSize.X, (int)characterSize.Y);
            this.basicMap = basicMap;
            this.aggroDistance = aggroDistance;
        }

        protected override void LoadContent()
        {
            tileSheet = Game.Content.Load<Texture2D>(tileSheetFileName);

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            elapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
            if (elapsedTime >= animationSpeed)
            {
                if (isMoving)
                {
                    if (currentWalkingRectangleIndex < tileAnimationCrops[(int)currentWalkingDirection].Length - 1)
                    {
                        currentWalkingRectangleIndex++;
                    }
                    else
                    {
                        currentWalkingRectangleIndex = 0;
                    }
                }
                else
                {
                    currentWalkingRectangleIndex = 1;
                }

                elapsedTime = 0;
            }

            base.Update(gameTime);
        }

        public bool AttackPlayer(Rectangle playerRectangle)
        {
            bool returnValue = false;

            if (playerRectangle.Intersects(characterRectangle))
            {
                returnValue = true;
            }

            return returnValue;
        }

        public void SetStopMoving()
        {
            isMoving = false;
        }

        public void SetAggroDistance(int value)
        {
            this.aggroDistance = value;
        }

        public void MoveTowardsPlayer(Rectangle playerRectangle)
        {
            if (Vector2.Distance(characterPosition, new Vector2(playerRectangle.X, playerRectangle.Y)) < aggroDistance)
            {
                isMoving = true;
            }

            if (isMoving & playerRectangle != Rectangle.Empty)
            {
                Rectangle characterRectangle = new Rectangle((int)characterPosition.X, (int)characterPosition.Y, (int)characterSize.X, (int)characterSize.Y);

                if (!playerRectangle.Intersects(characterRectangle))
                {
                    Vector2 direction = new Vector2(playerRectangle.X, playerRectangle.Y) - characterPosition;
                    direction.Normalize();

                    int xDirection = Convert.ToInt32(direction.X);
                    int yDirection = Convert.ToInt32(direction.Y);

                    if (xDirection == 0 & yDirection == 1)
                    {
                        // DOWN
                        currentWalkingDirection = SpriteWalkingDirection.DOWN;
                    }

                    if (xDirection == 0 & yDirection == -1)
                    {
                        // UP
                        currentWalkingDirection = SpriteWalkingDirection.UP;
                    }

                    if (xDirection == 1 & yDirection == 0)
                    {
                        // RIGHT
                        currentWalkingDirection = SpriteWalkingDirection.RIGHT;
                    }

                    if (xDirection == -1 & yDirection == 0)
                    {
                        // LEFT
                        currentWalkingDirection = SpriteWalkingDirection.LEFT;
                    }

                    if (xDirection == -1 & yDirection == -1)
                    {
                        // UP
                        currentWalkingDirection = SpriteWalkingDirection.UP;
                    }

                    if (xDirection == 1 & yDirection == 1)
                    {
                        // UP
                        currentWalkingDirection = SpriteWalkingDirection.DOWN;
                    }

                    if (xDirection == -1 & yDirection == 1)
                    {
                        // DOWN
                        currentWalkingDirection = SpriteWalkingDirection.DOWN;
                    }

                    if (xDirection == 1 & yDirection == -1)
                    {
                        // UP
                        currentWalkingDirection = SpriteWalkingDirection.UP;
                    }

                    Vector2 newCharacterPosition = characterPosition + (direction * characterMovementSpeed);

                    Rectangle playerRectangleX = new Rectangle((int)newCharacterPosition.X + (Global.GameConstants.TILE_SIZE / 2),
                        (int)characterPosition.Y + (Global.GameConstants.TILE_SIZE / 2), (int)characterSize.X, (int)characterSize.Y);

                    Rectangle playerRectangleY = new Rectangle((int)characterPosition.X + (Global.GameConstants.TILE_SIZE / 2),
                        (int)newCharacterPosition.Y + (Global.GameConstants.TILE_SIZE / 2), (int)characterSize.X, (int)characterSize.Y);

                    if (!worldRenderer.checkWallCollision(playerRectangleX) & !basicMap.checkEntityCollision(playerRectangleX, getCharacterRectangle()))
                    {
                        characterPosition = new Vector2(newCharacterPosition.X, characterPosition.Y);
                    }

                    if (!worldRenderer.checkWallCollision(playerRectangleY) & !basicMap.checkEntityCollision(playerRectangleY, getCharacterRectangle()))
                    {
                        characterPosition = new Vector2(characterPosition.X, newCharacterPosition.Y);
                    }

                    this.characterRectangle = new Rectangle((int)characterPosition.X, (int)characterPosition.Y, (int)characterSize.X, (int)characterSize.Y);
                }
                else
                {
                    isMoving = false;
                }
            }
        }

        public Rectangle getCharacterRectangle()
        {
            return new Rectangle(Convert.ToInt32(characterPosition.X) + (Global.GameConstants.TILE_SIZE / 2),
               Convert.ToInt32(characterPosition.Y) + (Global.GameConstants.TILE_SIZE / 2), Convert.ToInt32(characterSize.X), Convert.ToInt32(characterSize.Y));
        }

        public Rectangle getCharacterDrawRectangle()
        {
            return new Rectangle(Convert.ToInt32(characterPosition.X),
               Convert.ToInt32(characterPosition.Y), Convert.ToInt32(characterSize.X), Convert.ToInt32(characterSize.Y));
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(tileSheet, getCharacterDrawRectangle(), tileAnimationCrops[(int)currentWalkingDirection][currentWalkingRectangleIndex], Color.White);
        }
    }
}
