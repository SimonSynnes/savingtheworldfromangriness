﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SavingTheWorldFromAngriness.World.Maps;

namespace SavingTheWorldFromAngriness.World.Components.World1
{
    class Leaf
    {
        public Vector2 originPosition;
        public Vector2 currentPosition;
        public Color color;
        public float radius;
        public float speed;

        public Leaf(Vector2 originPosition, Vector2 currentPosition, Color color, float radius, float speed)
        {
            this.originPosition = originPosition;
            this.currentPosition = currentPosition;
            this.color = color;
            this.radius = radius;
            this.speed = speed;
        }
    }

    class Leaves : BasicComponent
    {
        Texture2D leafTexture;
        List<Leaf> leaves;
        double elapsedTime;

        public Leaves(Game game, WorldRenderer worldRenderer, BasicMap basicMap, Rectangle componentRectangle)
            : base(game, worldRenderer, basicMap, componentRectangle)
        {
            this.leaves = new List<Leaf>();
            this.elapsedTime = 0;
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            leafTexture = Game.Content.Load<Texture2D>("Textures/Components/leaf");

            AddLeafPile(new Vector2(componentRectangle.X, componentRectangle.Y));
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            elapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
            if (elapsedTime >= 33)
            {
                foreach (Leaf leaf in leaves)
                {
                    Rectangle leafRectangle = new Rectangle(
                        (int)leaf.currentPosition.X,
                        (int)leaf.currentPosition.Y,
                        leafTexture.Width, leafTexture.Height);

                    if (!leafRectangle.Intersects(basicMap.player.getPlayerRectangle()))
                    {
                        continue;
                    }

                    float time = (float)gameTime.TotalGameTime.TotalSeconds;
                    float speed = MathHelper.PiOver2 * leaf.speed;
                    float radius = leaf.radius;
                    Vector2 origin = leaf.originPosition;

                    leaf.currentPosition.X = (float)(Math.Cos(time * speed) * radius + origin.X);
                    leaf.currentPosition.Y = (float)(Math.Sin(time * speed) * radius + origin.Y);
                }

                elapsedTime = 0;
            }
        }

        public void AddLeafPile(Vector2 location)
        {
            int points = 7;
            double radius = 52;

            double slice = 4 * Math.PI / points;
            for (int i = 0; i <= points; i++)
            {
                double angle = slice * i;

                int xCoord = (int)(location.X + radius * Math.Cos(angle));
                int yCoord = (int)(location.Y + radius * Math.Sin(angle));
                
                Color randomColor = new Color(Global.GameConstants.RNDM.Next(150, 256),
                    Global.GameConstants.RNDM.Next(256), Global.GameConstants.RNDM.Next(256));

                leaves.Add(new Leaf(new Vector2(xCoord, yCoord), new Vector2(xCoord, yCoord), randomColor, leafTexture.Width / 2, MathHelper.PiOver2 * (3 + (i * 2))));
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            foreach (Leaf leaf in leaves)
            {
                spriteBatch.Draw(leafTexture, leaf.currentPosition, leaf.color);
            }
        }
    }
}
