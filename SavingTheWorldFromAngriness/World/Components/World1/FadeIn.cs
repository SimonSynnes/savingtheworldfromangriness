﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.World.Maps;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.World.Components.World1
{
    public class FadeIn : BasicComponent
    {
        private SpriteBatch spriteBatch;
        private float fadeIntensity;
        private double fadeIntensityElapsedTime;
        private double fadeSpeed;

        public FadeIn(Game game, WorldRenderer worldRenderer, BasicMap basicMap, Rectangle componentRectangle, double fadeSpeed) 
            : base(game, worldRenderer, basicMap, componentRectangle)
        {
            this.fadeIntensity = 1f;
            this.fadeIntensityElapsedTime = 0;
            this.fadeSpeed = fadeSpeed;
        }

        public override void Update(GameTime gameTime)
        {
            fadeIntensityElapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
            if (fadeIntensityElapsedTime >= fadeSpeed)
            {
                fadeIntensity -= 0.05f;

                fadeIntensityElapsedTime = 0;
            }

            if (fadeIntensity <= 0)
            {
                this.Enabled = false;
                basicMap.RemoveGameComponent(this);
            }
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            base.LoadContent();
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            int windowWidth = GraphicsDevice.Viewport.Width;
            int windowHeight = GraphicsDevice.Viewport.Height;
            spriteBatch.Draw(componentTexture, new Rectangle(0, 0, windowWidth, windowHeight), Color.Black * fadeIntensity);

            spriteBatch.End();
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            // Do nothing
        }
    }
}
