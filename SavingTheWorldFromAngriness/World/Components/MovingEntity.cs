﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SavingTheWorldFromAngriness.World.Maps;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.World.Components
{
    public class MovingEntity : BasicComponent
    {
        public MovingEntity(Game game, WorldRenderer worldRenderer, BasicMap basicMap, Rectangle componentRectangle) 
            : base(game, worldRenderer, basicMap, componentRectangle) { }

        public virtual Rectangle getEntityRectangle()
        {
            return componentRectangle;
        }
   }
}
