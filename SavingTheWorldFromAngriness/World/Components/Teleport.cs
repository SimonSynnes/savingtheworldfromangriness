﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using SavingTheWorldFromAngriness.World.Maps;

namespace SavingTheWorldFromAngriness.World.Components
{
    public class Teleport : BasicComponent
    {
        private Texture2D teleportTexture;
        private Rectangle teleportRectangle;
        private SoundEffect soundEffect;
        private Vector2 teleportToPosition;

        private double elapsedTime;
        private int animationIndex;
        private Rectangle[] animation;

        public Teleport(Game game, WorldRenderer worldRenderer, BasicMap basicMap, Rectangle componentRectangle, int xCoord, int yCoord)
            : base(game, worldRenderer, basicMap, componentRectangle)
        {
            this.teleportToPosition = new Vector2(xCoord, yCoord);
            this.animationIndex = 0;
            this.elapsedTime = 0;
            this.animation = new Rectangle[]
            { new Rectangle(0, 160, 80, 80), new Rectangle(80, 160, 80, 80), new Rectangle(160, 160, 80, 80) };
        }

        protected override void LoadContent()
        {
            teleportTexture = Game.Content.Load<Texture2D>("Textures/Tileset/teleport");
            teleportRectangle = new Rectangle(componentRectangle.X, componentRectangle.Y, 80, 80);

            soundEffect = Game.Content.Load<SoundEffect>("Sounds/teleport");

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            if (basicMap.player.getPlayerRectangle().Intersects(
                new Rectangle(teleportRectangle.X + (Global.GameConstants.TILE_SIZE / 2) + 35,
                teleportRectangle.Y + (Global.GameConstants.TILE_SIZE / 2) + 35, teleportRectangle.Width - 70, teleportRectangle.Height - 70)))
            {
                basicMap.player.setPlayerPosition(teleportToPosition);

                Global.Sounds.SoundManager.PlaySoundEffect(soundEffect);
            }

            elapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
            if (elapsedTime >= 66)
            {
                if (animationIndex < animation.Length - 1)
                {
                    animationIndex++;
                }
                else
                {
                    animationIndex = 0;
                }

                elapsedTime = 0;
            }

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(teleportTexture, teleportRectangle, animation[animationIndex], Color.White);
        }
    }
}
