﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.World.Maps;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.World.Components
{
    public class BasicComponent : DrawableGameComponent
    {
        public WorldRenderer worldRenderer { get; private set; }
        public BasicMap basicMap { get; private set; }
        public Rectangle componentRectangle { get; private set; }
        public Texture2D componentTexture { get; private set; }

        public BasicComponent(Game game, WorldRenderer worldRenderer, BasicMap basicMap, Rectangle componentRectangle) 
            : base(game)
        {
            this.worldRenderer = worldRenderer;
            this.componentRectangle = componentRectangle;
            this.basicMap = basicMap;
            this.componentTexture = null;
        }

        public BasicComponent(Game game, WorldRenderer worldRenderer, BasicMap basicMap, Rectangle componentRectangle, Texture2D componentTexture)
            : base(game)
        {
            this.worldRenderer = worldRenderer;
            this.componentRectangle = componentRectangle;
            this.basicMap = basicMap;
            this.componentTexture = componentTexture;
        }

        public virtual void RemoveAllGameComponents()
        {
            //
        }

        public virtual void InteractWithTile()
        {
            // Do something
        }

        public bool checkCollision(Rectangle rectangle)
        {
            bool returnValue = false;

            if (componentRectangle.Intersects(rectangle))
            {
                returnValue = true;
            }

            return returnValue;
        }

        protected override void LoadContent()
        {
            if (componentTexture == null)
            {
                componentTexture = Game.Content.Load<Texture2D>("Textures/Rectangle");
            }

            base.LoadContent();
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(componentTexture, componentRectangle, Color.White);
        }
    }
}
