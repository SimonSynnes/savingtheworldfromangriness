﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using SavingTheWorldFromAngriness.World.Maps;

namespace SavingTheWorldFromAngriness.World.Components
{
    public class Ammo : BasicComponent
    {
        private Texture2D ammoTexture;
        private Rectangle ammoRectangle;
        private SoundEffect soundEffect;

        public Ammo(Game game, WorldRenderer worldRenderer, BasicMap basicMap, Rectangle componentRectangle)
            : base(game, worldRenderer, basicMap, componentRectangle)
        {

        }

        protected override void LoadContent()
        {
            ammoTexture = Game.Content.Load<Texture2D>("Textures/AmmoPack");
            ammoRectangle = new Rectangle(componentRectangle.X, componentRectangle.Y, ammoTexture.Width, ammoTexture.Height);

            soundEffect = Game.Content.Load<SoundEffect>("Sounds/ammo");

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            if (basicMap.player.getPlayerRectangle().Intersects(
                new Rectangle(ammoRectangle.X + (Global.GameConstants.TILE_SIZE / 2),
                ammoRectangle.Y + (Global.GameConstants.TILE_SIZE / 2), ammoRectangle.Width, ammoRectangle.Height)))
            {
                Global.Player.AMMO += 20;

                Global.Sounds.SoundManager.PlaySoundEffect(soundEffect);

                this.Enabled = false;
                basicMap.RemoveGameComponent(this);
            }

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(ammoTexture, ammoRectangle, Color.White);
        }
    }
}
