﻿using GeonBit.UI;
using GeonBit.UI.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using SavingTheWorldFromAngriness.GUI;
using SavingTheWorldFromAngriness.World.Maps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavingTheWorldFromAngriness.World.Components
{
    class Vendor : BasicComponent
    {
        private SoundEffect soundEffect;
        private SoundEffect promptSoundEffect;
        private Texture2D vendorTexture;
        private Rectangle vendorRectangle;
        private bool startDialog;
        private bool isPlayerOnVendor;

        public Vendor(Game game, WorldRenderer worldRenderer, BasicMap basicMap, Rectangle componentRectangle) :
            base(game, worldRenderer, basicMap, componentRectangle)
        {
            this.isPlayerOnVendor = false;
            this.startDialog = false;
        }
        protected override void LoadContent()
        {
            vendorTexture = Game.Content.Load<Texture2D>("Textures/Components/vendor");
            soundEffect = Game.Content.Load<SoundEffect>("Sounds/teleport");
            vendorRectangle = new Rectangle(componentRectangle.X, componentRectangle.Y, 80, 80);
            promptSoundEffect = Game.Content.Load<SoundEffect>("Sounds/World1/blip2");

            worldRenderer.addMiscCollision(new Rectangle(vendorRectangle.X, vendorRectangle.Y, vendorRectangle.Width, vendorRectangle.Height - 25));

            base.LoadContent();
        }
        public override void InteractWithTile()
        {
            VendorMenu vendormenu = new VendorMenu(Game, worldRenderer);
            Game.Components.Add(vendormenu);

        }
        public override void Update(GameTime gameTime)
        {
            if (basicMap.player.getPlayerRectangle().Intersects(
                new Rectangle(componentRectangle.X + (Global.GameConstants.TILE_SIZE / 2),
                componentRectangle.Y + (Global.GameConstants.TILE_SIZE / 2), componentRectangle.Width, componentRectangle.Height)))
            {
                isPlayerOnVendor = true;
            }
            else
            {
                isPlayerOnVendor = false;
            }

            base.Update(gameTime);
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(vendorTexture, vendorRectangle, Color.White);
        }
    }
}
