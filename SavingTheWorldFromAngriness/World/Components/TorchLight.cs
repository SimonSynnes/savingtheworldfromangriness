﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.World.Maps;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Lighting;

namespace SavingTheWorldFromAngriness.World.Components
{
    public class TorchLight : BasicComponent
    {
        private Texture2D torchTileSheet;
        private PointLight outerTorchLight;
        private PointLight innerTorchLight;
        private Rectangle torchPosition;

        private bool shrinkLightRadius;
        private int shrinkLightRadiusMax;
        private int shrinkLightRadiusMin;
        private int shrinkLightRadiusSpeed;

        private int animationSpeed;
        private int currentAnimationIndex;
        private int lightSpeedTick;
        private double elapsedAnimationTime;
        private double elapsedLightTime;

        private Rectangle[] torchTileAnimationCrops;

        public TorchLight(Game game, WorldRenderer worldRenderer, BasicMap basicMap, Rectangle componentRectangle, int lightPower, int lightSpeed)
            : base(game, worldRenderer, basicMap, componentRectangle)
        {
            this.outerTorchLight = new PointLight(worldRenderer.lightingManager.lightEffect, new Vector2(componentRectangle.X + 8, componentRectangle.Y + 12), 200, Color.White * 0.50f, 0.75f);
            worldRenderer.lightingManager.AddLight(outerTorchLight);

            this.innerTorchLight = new PointLight(worldRenderer.lightingManager.lightEffect, new Vector2(componentRectangle.X + 8, componentRectangle.Y + 12), 50, Color.White, 0.85f);
            worldRenderer.lightingManager.AddLight(innerTorchLight);

            this.torchPosition = new Rectangle(componentRectangle.X, componentRectangle.Y, 16, 24);
            worldRenderer.addMiscCollision(torchPosition);

            this.shrinkLightRadius = true;
            this.shrinkLightRadiusMax = 20 * lightPower;
            this.shrinkLightRadiusMin = 15 * lightPower;
            this.shrinkLightRadiusSpeed = lightSpeed;
            this.currentAnimationIndex = 0;
            this.elapsedAnimationTime = 0;
            this.animationSpeed = 150;
            this.elapsedLightTime = 0;
            this.lightSpeedTick = 10;
        }

        public override void Initialize()
        {
            base.Initialize();

            torchTileAnimationCrops = new Rectangle[] { new Rectangle(0, 0, 16, 24), new Rectangle(0, 32, 16, 24),
                new Rectangle(0, 64, 16, 24), new Rectangle(0, 96, 16, 24)};
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            torchTileSheet = Game.Content.Load<Texture2D>("Textures/Tileset/Torch");
        }

        public override void Update(GameTime gameTime)
        {
            elapsedLightTime += gameTime.ElapsedGameTime.TotalMilliseconds;
            if (elapsedLightTime >= shrinkLightRadiusSpeed)
            {
                if (shrinkLightRadius)
                {
                    outerTorchLight.Radius -= lightSpeedTick;
                    innerTorchLight.Radius -= lightSpeedTick / 10;

                    if (outerTorchLight.Radius < shrinkLightRadiusMin)
                    {
                        shrinkLightRadius = false;
                    }
                }
                else
                {
                    outerTorchLight.Radius += lightSpeedTick;
                    innerTorchLight.Radius += lightSpeedTick / 10;

                    if (outerTorchLight.Radius > shrinkLightRadiusMax)
                    {
                        shrinkLightRadius = true;
                    }
                }

                elapsedLightTime = 0;
            }

            elapsedAnimationTime += gameTime.ElapsedGameTime.TotalMilliseconds;
            if (elapsedAnimationTime >= animationSpeed)
            {
                if (currentAnimationIndex < torchTileAnimationCrops.Length - 1)
                {
                    currentAnimationIndex++;
                }
                else
                {
                    currentAnimationIndex = 0;
                }

                elapsedAnimationTime = 0;
            }

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(torchTileSheet, torchPosition, torchTileAnimationCrops[currentAnimationIndex], Color.White);
        }
    }
}