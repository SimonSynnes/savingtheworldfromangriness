﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.World.Maps;

using GeonBit.UI;
using GeonBit.UI.Entities;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace SavingTheWorldFromAngriness.World.Components.World3
{
    class FireBall
    {
        public Vector2 position;
        public Rectangle rectanglePosition;
        public Vector2 velocity;

        public FireBall(Rectangle rectanglePosition, Vector2 velocity)
        {
            this.rectanglePosition = rectanglePosition;
            this.position = new Vector2(rectanglePosition.X, rectanglePosition.Y);
            this.velocity = velocity;
        }

        public Rectangle getRectangle()
        {
            return new Rectangle(
                (int)position.X + (Global.GameConstants.TILE_SIZE / 2),
                (int)position.Y + (Global.GameConstants.TILE_SIZE / 2),
                rectanglePosition.Width, rectanglePosition.Height);
        }
    }

    class Dragon : MovingEntity
    {
        private Texture2D texture;
        private Vector2 position;
        private float rotation;
        private double elapsedTime;
        private int health;

        ProgressBar dragonHealthBar;

        Vector2 fireballOriginPosition;
        List<FireBall> fireBalls;
        private double fireballElapsedTime;

        private Texture2D fireballTexture;

        public Dragon(Game game, WorldRenderer worldRenderer, BasicMap basicMap, Rectangle componentRectangle) 
            : base(game, worldRenderer, basicMap, componentRectangle)
        {
            this.health = 1000;
            this.rotation = 0;
            this.elapsedTime = 0;
            this.fireballElapsedTime = 0;
            this.fireBalls = new List<FireBall>();
            int windowWidth = GraphicsDevice.Viewport.Width;
            int windowHeight = GraphicsDevice.Viewport.Height;
            this.dragonHealthBar = new ProgressBar(0, (uint)health, new Vector2(windowWidth / 2, 45), Anchor.TopCenter, new Vector2(0, 5));
            this.dragonHealthBar.Value = health;
            this.dragonHealthBar.ProgressFill.FillColor = Color.Red;
            UserInterface.Active.AddEntity(dragonHealthBar);
        }

        public void Update(GameTime gameTime)
        {
           // base.Update(gameTime);

            elapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
            if (elapsedTime >= 2)
            {
                rotation += 0.16f;

                if (rotation >= 6.2f)
                {
                    rotation = 0;
                }

                fireballElapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
                if (fireballElapsedTime >= 33)
                {
                    fireBalls.Add(new FireBall(new Rectangle(
                    (int)position.X + (texture.Width / 2),
                    (int)position.Y + (texture.Height / 2),
                    fireballTexture.Width, fireballTexture.Height), new Vector2((float)Math.Cos(rotation), (float)Math.Sin(rotation)) * 5f));

                    fireballElapsedTime = 0;
                }

                elapsedTime = 0;
            }

           // FireBall removeFireBall = null;
            List<FireBall> removeFireBalls = new List<FireBall>();
            foreach (FireBall fireball in fireBalls)
            {
                fireball.position += fireball.velocity * 0.5f;

                if (basicMap.player.getPlayerRectangle().Intersects(fireball.getRectangle()))
                {
                    // HIT
                    basicMap.player.attackPlayer(10);

                    removeFireBalls.Add(fireball);
                }

                float length = Vector2.Distance(fireballOriginPosition, fireball.position);

                if (length > 400)
                {
                    removeFireBalls.Add(fireball);
                }
            }

            foreach (FireBall fireball in removeFireBalls)
            {
                lock (fireBalls)
                {
                    fireBalls.Remove(fireball);
                }
            }

            this.dragonHealthBar.Value = health;
        }

        public int getHealth()
        {
            return health;
        }

        public void AttackEnemy(int attackPower)
        {
            health -= 10;
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            texture = Game.Content.Load<Texture2D>("Textures/World3/dragon");
            fireballTexture = Game.Content.Load<Texture2D>("Textures/World3/fireball");
            position = new Vector2(componentRectangle.X, componentRectangle.Y);
            fireballOriginPosition = new Vector2((int)position.X + (texture.Width / 2), (int)position.Y + (texture.Height / 2));
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, new Rectangle(Convert.ToInt32(position.X) + Convert.ToInt32(texture.Width / 2),
                Convert.ToInt32(position.Y) + Convert.ToInt32(texture.Height / 2), texture.Width, texture.Height), null,
                Color.White, rotation, new Vector2(Convert.ToInt32(texture.Width / 2),
                Convert.ToInt32(texture.Height / 2)), SpriteEffects.None, 0);

            foreach (FireBall fireball in fireBalls)
            {
                spriteBatch.Draw(fireballTexture, fireball.position, Color.White);
            }
        }

        public override Rectangle getEntityRectangle()
        {
            return new Rectangle(
                Convert.ToInt32(position.X) + (Global.GameConstants.TILE_SIZE / 2),
                Convert.ToInt32(position.Y) + (Global.GameConstants.TILE_SIZE / 2),
                Convert.ToInt32(texture.Width),
                Convert.ToInt32(texture.Height));
        }
    }
}
