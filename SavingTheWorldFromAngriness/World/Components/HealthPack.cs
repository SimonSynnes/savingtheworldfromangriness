﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using SavingTheWorldFromAngriness.World.Maps;

namespace SavingTheWorldFromAngriness.World.Components
{
    public class HealthPack : BasicComponent
    {
        private Texture2D healthPackTexture;
        private Rectangle healthPackRectangle;
        private SoundEffect soundEffect;

        public HealthPack(Game game, WorldRenderer worldRenderer, BasicMap basicMap, Rectangle componentRectangle)
            : base(game, worldRenderer, basicMap, componentRectangle)
        {

        }

        protected override void LoadContent()
        {
            healthPackTexture = Game.Content.Load<Texture2D>("Textures/HealthPack");
            healthPackRectangle = new Rectangle(componentRectangle.X, componentRectangle.Y, healthPackTexture.Width, healthPackTexture.Height);

            soundEffect = Game.Content.Load<SoundEffect>("Sounds/health");

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            if (basicMap.player.getPlayerRectangle().Intersects(
                new Rectangle(healthPackRectangle.X + (Global.GameConstants.TILE_SIZE / 2),
                healthPackRectangle.Y + (Global.GameConstants.TILE_SIZE / 2), healthPackRectangle.Width, healthPackRectangle.Height)))
            {
                Global.Player.HEALTH += 35;

                if (Global.Player.HEALTH > 100)
                {
                    Global.Player.HEALTH = 100;
                }

                Global.Sounds.SoundManager.PlaySoundEffect(soundEffect);

                this.Enabled = false;
                basicMap.RemoveGameComponent(this);
            }

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(healthPackTexture, healthPackRectangle, Color.White);
        }
    }
}
