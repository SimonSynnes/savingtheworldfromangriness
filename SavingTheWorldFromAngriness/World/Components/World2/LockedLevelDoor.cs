﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.World.Maps;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.World.Components.World2
{
    // Textures/doorLocked
    class LockedLevelDoor : BasicComponent
    {
        private SpriteBatch spriteBatch;
        private Texture2D levelDoorTexture;
        private Texture2D doorTextTexture;
        private bool isPlayerOnLevelDoor;

        public LockedLevelDoor(Game game, WorldRenderer worldRenderer, BasicMap basicMap, Rectangle componentRectangle) 
            : base(game, worldRenderer, basicMap, componentRectangle)
        {
            this.isPlayerOnLevelDoor = false;
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            spriteBatch = new SpriteBatch(GraphicsDevice);

            levelDoorTexture = Game.Content.Load<Texture2D>("Textures/levelDoor");
            doorTextTexture = Game.Content.Load<Texture2D>("Textures/doorLocked");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (basicMap.player.getPlayerRectangle().Intersects(
                new Rectangle(componentRectangle.X + (Global.GameConstants.TILE_SIZE / 2),
                componentRectangle.Y + (Global.GameConstants.TILE_SIZE / 2), componentRectangle.Width, componentRectangle.Height)))
            {
                isPlayerOnLevelDoor = true;
            }
            else
            {
                isPlayerOnLevelDoor = false;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(levelDoorTexture,
                new Vector2(componentRectangle.X, componentRectangle.Y),
                componentRectangle, Color.DarkGray, 0, Vector2.Zero, 1, SpriteEffects.None, 0);
        }

        public override void Draw(GameTime gameTime)
        {
            if (isPlayerOnLevelDoor)
            {
                spriteBatch.Begin(SpriteSortMode.Texture,
                    BlendState.NonPremultiplied,
                    null,
                    null,
                    null,
                    null,
                    basicMap.camera.TransformMatrix);

                spriteBatch.Draw(doorTextTexture,
                    new Vector2(
                        componentRectangle.X + (componentRectangle.Width / 2) - (doorTextTexture.Width / 2),
                        componentRectangle.Y + (componentRectangle.Height / 2) - (doorTextTexture.Height / 2)), Color.White);

                spriteBatch.End();
            }

            base.Draw(gameTime);
        }
    }
}
