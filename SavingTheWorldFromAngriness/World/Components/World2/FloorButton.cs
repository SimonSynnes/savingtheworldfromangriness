﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SavingTheWorldFromAngriness.World.Maps;

namespace SavingTheWorldFromAngriness.World.Components.World2
{
    class FloorButton : BasicComponent
    {
        private Texture2D floorButtonUnpressedTexture;
        private Rectangle floorButtonRectangle;
        private Texture2D floorButtonPressedTexture;
        public bool IsPlayerOnRectangle { get; private set; }


        public FloorButton(Game game, WorldRenderer worldRenderer, BasicMap basicMap, Rectangle componentRectangle)
            : base(game, worldRenderer, basicMap, componentRectangle)
        {
            IsPlayerOnRectangle = false;
        }

        public override void Initialize()
        {
            base.Initialize();

            floorButtonRectangle = new Rectangle
                (componentRectangle.X,
               componentRectangle.Y,
               componentRectangle.Width, componentRectangle.Height);
        }
        protected override void LoadContent()
        {
            floorButtonPressedTexture = Game.Content.Load<Texture2D>("Textures/Buttons/buttonPressed");
            floorButtonUnpressedTexture = Game.Content.Load<Texture2D>("Textures/Buttons/buttonUnpressed");

            base.LoadContent();
        }

        public Rectangle GetFloorButtonRectangle()
        {
            return new Rectangle(floorButtonRectangle.X + (Global.GameConstants.TILE_SIZE / 2), floorButtonRectangle.Y + (Global.GameConstants.TILE_SIZE / 2), floorButtonRectangle.Width, floorButtonRectangle.Height);
        }

        public override void InteractWithTile()
        {
            base.InteractWithTile();
        }

        public override void RemoveAllGameComponents()
        {
            base.RemoveAllGameComponents();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (GetFloorButtonRectangle().Intersects(basicMap.player.getPlayerRectangle()))
            {
                IsPlayerOnRectangle = true;
            }
            else
            {
                IsPlayerOnRectangle = false;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (IsPlayerOnRectangle)
            {
                spriteBatch.Draw(floorButtonPressedTexture, floorButtonRectangle, Color.White);
            }
            else
            {
                spriteBatch.Draw(floorButtonUnpressedTexture, floorButtonRectangle, Color.White);
            }
        }

    }
}
