﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.World.Maps;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace SavingTheWorldFromAngriness.World.Components
{
    public class Enemy : MovingEntity
    {
        private SpriteCharacterAnimation spriteCharacterAnimation;

        private Player player;
        private int attackSpeed;
        private int attackPower;
        private double elapsedTime;

        private bool alive;
        private int health;
        private int aggroDistance;
        private int walkingSpeed;
        private int enemyStartDirection;

        private SoundEffect bulletImpactSoundEffect;

        public Enemy(Game game, WorldRenderer worldRenderer, BasicMap basicMap, Rectangle componentRectangle,
            Player player, int health, int aggroDistance, int attackSpeed, int attackPower, int walkingSpeed, int enemyStartDirection) 
            : base(game, worldRenderer, basicMap, componentRectangle)
        {
            this.player = player;
            this.attackSpeed = attackSpeed;
            this.attackPower = attackPower;
            this.elapsedTime = 0;
            this.alive = true;
            this.health = health;
            this.aggroDistance = aggroDistance;
            this.walkingSpeed = walkingSpeed;
            this.enemyStartDirection = enemyStartDirection;
        }

        public void AttackEnemy(int attackPower)
        {
            spriteCharacterAnimation.SetAggroDistance(9000);
            health -= attackPower;
            Global.Sounds.SoundManager.PlaySoundEffect(bulletImpactSoundEffect);
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            int enemyModelRow = Global.GameConstants.RNDM.Next(0, 2);
            int enemyModelColumn = Global.GameConstants.RNDM.Next(0, 4);
            spriteCharacterAnimation = new SpriteCharacterAnimation(Game, worldRenderer, basicMap, "Textures/Tileset/Knight",
                new Rectangle[] { new Rectangle(0 + (enemyModelColumn * (48 * 3)), 0 + (enemyModelRow * (48 * 4)), 48, 48), new Rectangle(48 + (enemyModelColumn * (48 * 3)), 0 + (enemyModelRow * (48 * 4)), 48, 48), new Rectangle(96 + (enemyModelColumn * (48 * 3)), 0 + (enemyModelRow * (48 * 4)), 48, 48) },
                new Rectangle[] { new Rectangle(0 + (enemyModelColumn * (48 * 3)), 48 + (enemyModelRow * (48 * 4)), 48, 48), new Rectangle(48 + (enemyModelColumn * (48 * 3)), 48 + (enemyModelRow * (48 * 4)), 48, 48), new Rectangle(96 + (enemyModelColumn * (48 * 3)), 48 + (enemyModelRow * (48 * 4)), 48, 48) },
                new Rectangle[] { new Rectangle(0 + (enemyModelColumn * (48 * 3)), 96 + (enemyModelRow * (48 * 4)), 48, 48), new Rectangle(48 + (enemyModelColumn * (48 * 3)), 96 + (enemyModelRow * (48 * 4)), 48, 48), new Rectangle(96 + (enemyModelColumn * (48 * 3)), 96 + (enemyModelRow * (48 * 4)), 48, 48) },
                new Rectangle[] { new Rectangle(0 + (enemyModelColumn * (48 * 3)), 144 + (enemyModelRow * (48 * 4)), 48, 48), new Rectangle(48 + (enemyModelColumn * (48 * 3)), 144 + (enemyModelRow * (48 * 4)), 48, 48), new Rectangle(96 + (enemyModelColumn * (48 * 3)), 144 + (enemyModelRow * (48 * 4)), 48, 48) },
                10, walkingSpeed, new Vector2(componentRectangle.X, componentRectangle.Y), new Vector2(48, 48), aggroDistance, enemyStartDirection);

            Game.Components.Add(spriteCharacterAnimation);
            bulletImpactSoundEffect = Game.Content.Load<SoundEffect>("Sounds/Misc/impactEnemy");
        }

        public override void RemoveAllGameComponents()
        {
            Game.Components.Remove(spriteCharacterAnimation);

            base.RemoveAllGameComponents();
        }

        public void Die()
        {
            this.Enabled = false;


            if (Global.Player.WEAPON.GetType() == typeof(Weapons.BeginnerGun))
            {
                Global.Player.COINS += 2;
            }

            if (Global.Player.WEAPON.GetType() == typeof(Weapons.MediumLevelGun))
            {
                Global.Player.COINS += 20;
            }

            if (Global.Player.WEAPON.GetType() == typeof(Weapons.HighLevelGun))
            {
                Global.Player.COINS += 60;
            }

            if (Global.Player.WEAPON.GetType() == typeof(Weapons.GodlikeGun))
            {
                Global.Player.COINS += 60;
            }

            basicMap.RemoveGameComponent(this);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (Global.GameState.IS_GAME_PAUSED)
            {
                return;
            }

            if (health <= 0)
            {
                Die();
                return;
            }

            spriteCharacterAnimation.MoveTowardsPlayer(player.getPlayerRectangle());

            elapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
            if (elapsedTime >= attackSpeed)
            {
                if (spriteCharacterAnimation.AttackPlayer(player.getPlayerRectangle()))
                {
                    // Damage player...
                    player.attackPlayer(attackPower);
                }

                elapsedTime = 0;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (!alive)
            {
                return;
            }

            spriteCharacterAnimation.Draw(spriteBatch);
        }

        public override Rectangle getEntityRectangle()
        {
            if (alive)
            {
                return spriteCharacterAnimation.getCharacterRectangle();
            }
            else
            {
                return Rectangle.Empty;
            }
        }

        public void SetSpriteCharacterAnimation(SpriteCharacterAnimation spriteCharacterAnimation)
        {
            lock (this.spriteCharacterAnimation)
            {
                Game.Components.Remove(this.spriteCharacterAnimation);

                this.spriteCharacterAnimation = spriteCharacterAnimation;
            }
        }
    }
}
