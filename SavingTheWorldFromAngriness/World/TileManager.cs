﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using SavingTheWorldFromAngriness.World.Tiles;

namespace SavingTheWorldFromAngriness.World
{
    public class TileManager : DrawableGameComponent
    {
        private Texture2D tileSet01;

        public TileManager(Game game)
            : base(game)
        {
            tileSet01 = null;
        }

        protected override void LoadContent()
        {
            tileSet01 = Game.Content.Load<Texture2D>("Textures/Tileset/TileSet01");

            base.LoadContent();
        }

        public Tile CreateNewTile(string tileCode, Vector2 position)
        {
            Tile returnValue = null;

            Object[] newTileData = TileTranslate.translate(tileCode);

            if (newTileData != null)
            {
                returnValue = new Tile(position, tileSet01, (Vector2)newTileData[0],
                    (Tile.Type)newTileData[2], newTileData[3].ToString(),
                    new Rectangle((int)position.X, (int)position.Y, Global.GameConstants.TILE_SIZE, Global.GameConstants.TILE_SIZE));
            }

            return returnValue;
        }

        public Texture2D getTileSetTexture()
        {
            return tileSet01;
        }
    }
}
