﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.Backgrounds;
using SavingTheWorldFromAngriness.GUI;

using GeonBit.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using SavingTheWorldFromAngriness.World.Tiles;

namespace SavingTheWorldFromAngriness.World
{
    public class WorldRenderer : DrawableGameComponent
    {
        SpriteBatch spriteBatch;

        public LightingManager lightingManager;
        public TileManager tileManager;

        public Tile[] lowerLevel; // Floor and background
        public Tile[] upperLevel; // Characters and misc
        public List<Rectangle> miscCollisions;

        Texture2D rectangleTexture;

        Camera camera;

        public int map_width = 2;
        public int map_height = 2;

        public float worldDarkness;
        public bool isWorldCreator;

        private List<IGameComponent> gameComponents;

        private BasicBackground background;
        private Color backgroundColor;

        private Texture2D backgroundTexture;
        public List<Texture2D> backgroundMasks;

        private Matrix m;
        private AlphaTestEffect a;
        private DepthStencilState s1;
        private DepthStencilState s2;

        private GameMenu gameMenu;

        public WorldRenderer(Game game)
            : base(game)
        {
            camera = new Camera();
            camera.position = new Vector2(500, 500);

            lightingManager = new LightingManager(Game, camera);
            Game.Components.Add(lightingManager);

            tileManager = new TileManager(Game);
            Game.Components.Add(tileManager);

            backgroundColor = Color.CornflowerBlue;
            background = new BasicBackground(this.Game);
            game.Components.Add(background);

            SetBackground(new BasicBackground(this.Game), new Color(17, 17, 17));

            lowerLevel = new Tile[0];
            upperLevel = new Tile[0];
            miscCollisions = new List<Rectangle>();

            worldDarkness = 0.65f;
            gameComponents = new List<IGameComponent>();

            backgroundMasks = new List<Texture2D>();

            isWorldCreator = false;

            gameMenu = null;
        }

        public void AddBackgroundMask(Texture2D texture)
        {
            lock (backgroundMasks)
            {
                backgroundMasks.Add(texture);
            }
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            rectangleTexture = this.Game.Content.Load<Texture2D>("Textures/Rectangle");
            RenderMapMaskTarget();
            RenderBackgroundTexture(spriteBatch);

            m = Matrix.CreateOrthographicOffCenter(0,
                GraphicsDevice.PresentationParameters.BackBufferWidth,
                GraphicsDevice.PresentationParameters.BackBufferHeight,
                0, 0, 1
                );

            a = new AlphaTestEffect(GraphicsDevice)
            {
                Projection = m
            };

            s1 = new DepthStencilState
            {
                StencilEnable = true,
                StencilFunction = CompareFunction.Always,
                StencilPass = StencilOperation.Replace,
                ReferenceStencil = 1,
                DepthBufferEnable = false,
            };

            s2 = new DepthStencilState
            {
                StencilEnable = true,
                StencilFunction = CompareFunction.LessEqual,
                StencilPass = StencilOperation.Keep,
                ReferenceStencil = 1,
                DepthBufferEnable = false,
            };

            base.LoadContent();
        }

        public void SetBackground(BasicBackground background, Color color)
        {
            Game.Components.Remove(this.background);

            this.background = background;
            Game.Components.Add(background);

            backgroundColor = color;
        }

        public void Draw()
        {
            int windowWidth = GraphicsDevice.Viewport.Width;
            int windowHeight = GraphicsDevice.Viewport.Height;

            lightingManager.DrawTiles(lowerLevel);

            foreach (GameComponent gameComponent in gameComponents)
            {
                ((Maps.BasicMap)gameComponent).DrawBehindShadows();
            }

            lightingManager.DrawLighting(worldDarkness);

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.LinearWrap, s1, null, a);

            for (int i = 0; i < backgroundMasks.Count; i++)
            {
                int index = (backgroundMasks.Count - 1) - i;

                spriteBatch.Draw(backgroundMasks[index], new Vector2(
                    Convert.ToInt32((int)camera.position.X) - (Global.GameConstants.TILE_SIZE / 2),
                    Convert.ToInt32((int)camera.position.Y) - (Global.GameConstants.TILE_SIZE / 2)),
                    Color.White);
            }

            spriteBatch.Draw(backgroundTexture, new Vector2(0, 0), new Rectangle(0, 0, windowWidth, (int)camera.position.Y - Global.GameConstants.TILE_SIZE / 2), backgroundColor);
            spriteBatch.Draw(backgroundTexture, new Vector2(0, ((int)camera.position.Y + map_height) - Global.GameConstants.TILE_SIZE / 2), new Rectangle(0, 0, windowWidth, windowHeight - (int)camera.position.Y), backgroundColor);
            spriteBatch.Draw(backgroundTexture, new Vector2(0, 0), new Rectangle(0, 0, (int)camera.position.X - Global.GameConstants.TILE_SIZE / 2, windowHeight), backgroundColor);
            spriteBatch.Draw(backgroundTexture, new Vector2((int)camera.position.X + map_width - Global.GameConstants.TILE_SIZE / 2, 0), new Rectangle(0, 0, windowWidth - ((int)camera.position.X + map_width) + Global.GameConstants.TILE_SIZE / 2, windowHeight), backgroundColor);

            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.LinearWrap, s2, null, null);

            background.Draw(spriteBatch);

            spriteBatch.End();
        }

        private void AddNewGameComponent(string className)
        {
            object gameComponent = NewClassInstance(className, this.Game, this);
            this.Game.Components.Add((IGameComponent)gameComponent);

            gameComponents.Add((IGameComponent)gameComponent);
        }

        private object NewClassInstance(string className, object argument1, object argument2)
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();

            var type = assembly.GetTypes()
                .First(t => t.Name == className);

            object[] arguments = { argument1, argument2 };

            return Activator.CreateInstance(type, arguments);
        }

        public Camera getCamera()
        {
            return camera;
        }

        public void LoadMap(string world, string map)
        {
            Global.Player.CURRENT_WORLD = world;
            Global.Player.CURRENT_MAP = map;
            Global.GameState.IS_GAME_PAUSED = true;
            ClearMap();

            gameMenu = new GameMenu(this.Game, this);
            Game.Components.Add(gameMenu);

            List<Tile> listLowerLevel = new List<Tile>();
            List<float[]> listLights = new List<float[]>();

            string txtData = "";
            List<Object[]> mapData = new List<Object[]>();
            int width = 0;
            int height = 0;
            if (!isWorldCreator)
            {
                mapData = World.Maps.File.Load(world, map, out width, out height, out listLights, out worldDarkness, out txtData);
            }
            else
            {
                mapData = World.Maps.File.Load(out width, out height, out listLights, out worldDarkness, world);
            }

            map_width = width;
            map_height = height;

            for (int i = 0; i < mapData.Count; i++)
            {
                string tileCode = ((Object[])mapData[i])[0].ToString();
                Vector2 tilePosition = (Vector2)((Object[])mapData[i])[1];

                Tile tile = tileManager.CreateNewTile(tileCode, tilePosition);

                if (isWorldCreator & tile == null)
                {
                    tile = tileManager.CreateNewTile("000", tilePosition);
                }

                if (tile == null)
                {
                    continue;
                }

                if (tile.type == Tile.Type.floor)
                {
                    listLowerLevel.Add(tile);
                }

                if (tile.type == Tile.Type.wall)
                {
                    if (isWorldCreator)
                    {
                        Texture2D tileTexture = tile.Crop(new Rectangle((int)tile.crop.X, (int)tile.crop.Y, Global.GameConstants.TILE_SIZE, Global.GameConstants.TILE_SIZE));
                        tileTexture.Tag = tileCode;

                        listLowerLevel.Add(null);

                        lock (lightingManager.blocks)
                        {
                            lightingManager.AddVisualBlock(tileTexture, tile.position, 0, tileTexture, i);
                        }
                    }
                }
            }

            if (!isWorldCreator)
            {
                List<Tile> listMiddleLevel = World.Maps.WallOptimization.optimizeWalls(txtData, tileManager);

                lock (lightingManager.blocks)
                {
                    foreach (Tile tile in listMiddleLevel)
                    {
                        Texture2D tileTexture = tile.Crop(new Rectangle((int)tile.crop.X, (int)tile.crop.Y, Global.GameConstants.TILE_SIZE, Global.GameConstants.TILE_SIZE));
                        tileTexture.Tag = tile.name;

                        lightingManager.AddVisualBlock(tileTexture, tile.position, 0, tileTexture, tile.rectangle);
                    }
                }
            }

            if (!isWorldCreator)
            {
                AddNewGameComponent(map);
            }

            foreach (float[] light in listLights)
            {
                lock (lightingManager.lights)
                {
                    lightingManager.AddLight(new Vector2(light[0], light[1]), light[2], new Color((int)light[3], (int)light[4], (int)light[5]), light[6]);
                }
            }

            lock (lowerLevel)
                lock (upperLevel)
                    lock (lightingManager.blocks)
                    {
                        lowerLevel = listLowerLevel.ToArray();
                    }

            if (!isWorldCreator)
            {
                RenderMapMaskTarget();
                RenderBackgroundTexture(spriteBatch);
            }

            Global.GameState.IS_GAME_PAUSED = false;
        }

        public void RenderMapMaskTarget()
        {
            RenderTarget2D renderTarget = new RenderTarget2D(GraphicsDevice, map_width, map_height, false, SurfaceFormat.Color, DepthFormat.None);

            GraphicsDevice.SetRenderTarget(renderTarget);
            GraphicsDevice.Clear(Color.Transparent);

            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, SamplerState.LinearWrap, DepthStencilState.DepthRead, RasterizerState.CullNone);
            for (int i = 0; i < (renderTarget.Height / Global.GameConstants.TILE_SIZE); i++)
            {
                for (int ii = 0; ii < (renderTarget.Width / Global.GameConstants.TILE_SIZE); ii++)
                {
                    int x = Global.GameConstants.TILE_SIZE * ii;
                    int y = Global.GameConstants.TILE_SIZE * i;

                    Rectangle rectangle = new Rectangle(x, y, Global.GameConstants.TILE_SIZE, Global.GameConstants.TILE_SIZE);

                    bool addRectangleToMask = true;

                    if (checkWallCollision(rectangle))
                    {
                        addRectangleToMask = false;
                    }
                    else
                    {
                        for (int iii = 0; iii < lowerLevel.Length; iii++)
                        {
                            if (lowerLevel[iii].rectangle.Intersects(rectangle))
                            {
                                addRectangleToMask = false;
                                break;
                            }
                        }
                    }

                    if (addRectangleToMask)
                    {
                        spriteBatch.Draw(rectangleTexture, rectangle, backgroundColor);
                    }
                }
            }

            spriteBatch.End();

            GraphicsDevice.SetRenderTarget(null);
            GraphicsDevice.Clear(Color.Transparent);

            Color[] texdata = new Color[renderTarget.Width * renderTarget.Height];
            renderTarget.GetData(texdata);
            Texture2D mapMaskTexture = new Texture2D(GraphicsDevice, renderTarget.Width, renderTarget.Height);
            mapMaskTexture.SetData(texdata);

            backgroundMasks.Add(mapMaskTexture);
        }

        public void RenderBackgroundTexture(SpriteBatch spriteBatch)
        {
            RenderTarget2D mapMaskTarget = new RenderTarget2D(GraphicsDevice, 5, 5, false, SurfaceFormat.Color, DepthFormat.None);

            GraphicsDevice.SetRenderTarget(mapMaskTarget);
            GraphicsDevice.Clear(Color.Transparent);

            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, SamplerState.LinearWrap, DepthStencilState.DepthRead, RasterizerState.CullNone);
            spriteBatch.Draw(rectangleTexture, new Rectangle(0, 0, 5, 5), backgroundColor);
            spriteBatch.End();

            GraphicsDevice.SetRenderTarget(null);
            GraphicsDevice.Clear(Color.Transparent);

            Color[] texdata = new Color[mapMaskTarget.Width * mapMaskTarget.Height];
            mapMaskTarget.GetData(texdata);
            backgroundTexture = new Texture2D(GraphicsDevice, mapMaskTarget.Width, mapMaskTarget.Height);
            backgroundTexture.SetData(texdata);
        }

        public void ClearMap()
        {
            lock (lightingManager.blocks)
                lock (lightingManager.lights)
                {
                    lightingManager.ClearBlocks();
                    lightingManager.ClearLights();
                }

            lock (lowerLevel)
                lock (upperLevel)
                {
                    lowerLevel = new Tile[0];
                }

            Game.Components.Remove(gameMenu);

            foreach (IGameComponent gameComponent in gameComponents)
            {
                ((World.Maps.BasicMap)gameComponent).RemoveAllGameComponents();
                this.Game.Components.Remove(gameComponent);
            }

            gameComponents.Clear();
            miscCollisions.Clear();
            backgroundMasks.Clear();

            map_width = 0;
            map_height = 0;

            if (!isWorldCreator)
            {
                UserInterface.Active.Clear();
            }
        }

        public void LoadMapWorldCreator(int width, int height)
        {
            lowerLevel = new Tile[width * height];
            upperLevel = new Tile[width * height];

            Tile tile = tileManager.CreateNewTile("000", new Vector2(0, 0));

            int mapWidth = 0;
            int mapHeight = 0;

            int count = 0;
            for (int y = 0; y < height; y++)
            {
                for (int i = 0; i < width; i++)
                {
                    lowerLevel[count] = new Tile(new Vector2(i * Global.GameConstants.TILE_SIZE, y * Global.GameConstants.TILE_SIZE),
                        tile.texture, tile.crop, tile.type, tile.name, Rectangle.Empty);

                    if ((i * Global.GameConstants.TILE_SIZE) > mapWidth)
                    {
                        mapWidth = (i * Global.GameConstants.TILE_SIZE);
                    }

                    count++;
                }

                mapHeight = y * Global.GameConstants.TILE_SIZE;
            }

            map_width = mapWidth + Global.GameConstants.TILE_SIZE;
            map_height = mapHeight + Global.GameConstants.TILE_SIZE;
        }

        public void addMiscCollision(Rectangle rectangle)
        {
            lock (miscCollisions)
            {
                miscCollisions.Add(new Rectangle(rectangle.X + (Global.GameConstants.TILE_SIZE / 2),
                    rectangle.Y + (Global.GameConstants.TILE_SIZE / 2), rectangle.Width, rectangle.Height));
            }
        }

        public void removeMiscCollision(Rectangle rectangle)
        {
            lock (miscCollisions)
            {
                Rectangle removeRectangle = new Rectangle(rectangle.X + (Global.GameConstants.TILE_SIZE / 2),
                    rectangle.Y + (Global.GameConstants.TILE_SIZE / 2), rectangle.Width, rectangle.Height);
                miscCollisions.Remove(removeRectangle);
            }
        }

        public bool checkWallCollision(Rectangle rectangle)
        {
            bool returnValue = false;

            for (int i = 0; i < lightingManager.blocks.Count; i++)
            {
                Rectangle collisionRectangle =
                    new Rectangle(
                        (int)lightingManager.blocks[i].Pose.Position.X,
                        (int)lightingManager.blocks[i].Pose.Position.Y,
                    lightingManager.blocks[i].rectangle.Width,
                    lightingManager.blocks[i].rectangle.Height);

                if (rectangle.Intersects(collisionRectangle))
                {
                    returnValue = true;
                    break;
                }
            }

            if (returnValue == false)
            {
                foreach (Rectangle miscCollisionRectangle in miscCollisions)
                {
                    if (miscCollisionRectangle.Intersects(rectangle))
                    {
                        returnValue = true;
                        break;
                    }
                }
            }

            return returnValue;
        }

        public string checkWallTileCollision(Rectangle rectangle)
        {
            string returnValue = "";

            for (int i = 0; i < lightingManager.blocks.Count; i++)
            {
                Rectangle collisionRectangle =
                    new Rectangle(
                        (int)lightingManager.blocks[i].Pose.Position.X,
                        (int)lightingManager.blocks[i].Pose.Position.Y,
                    lightingManager.blocks[i].rectangle.Width,
                    lightingManager.blocks[i].rectangle.Height);

                if (rectangle.Intersects(collisionRectangle))
                {
                    returnValue = lightingManager.blocks[i].Texture.Tag.ToString();
                    break;
                }
            }

            return returnValue;
        }

        private bool isF6Down = false;
        public override void Update(GameTime gameTime)
        {
            if (Game1.DEBUG_MODE)
            {
                KeyboardState ks = Keyboard.GetState();

                if (!Global.GameState.IS_GAME_PAUSED)
                {
                    if (ks.IsKeyDown(Keys.F6))
                    {
                        isF6Down = true;
                    }
                    else
                    {
                        // Check if enter key has been pressed and then released
                        // (in order to only call functions once)
                        if (isF6Down)
                        {
                            LoadNextMap();
                        }

                        isF6Down = false;
                    }
                }
            }

            base.Update(gameTime);
        }

        public void LoadNextMap()
        {
            Global.GameState.IS_GAME_PAUSED = true;

            string currentMap = Global.Player.CURRENT_WORLD + "," + Global.Player.CURRENT_MAP;

            Dictionary<string, string> nextMap = new Dictionary<string, string>();
            nextMap.Add("World1,Home", "World1,Castle");
            nextMap.Add("World1,Castle", "World1,CastleOutside");
            nextMap.Add("World1,CastleOutside", "World1,CastleWaiting");
            nextMap.Add("World1,CastleWaiting", "World1,Coconut");
            nextMap.Add("World1,Coconut", "World1,Flask");
            nextMap.Add("World1,Flask", "World1,Venom");
            nextMap.Add("World1,Venom", "World1,Ink");
            nextMap.Add("World1,Ink", "World1,EndScene");
            nextMap.Add("World1,EndScene", "World2,CastleHallway");
            nextMap.Add("World2,CastleHallway", "World2,CastleBanquetHall");
            nextMap.Add("World2,CastleBanquetHall", "World2,CastlePuzzle");
            nextMap.Add("World2,CastlePuzzle", "World2,CastleBasement");
            nextMap.Add("World2,CastleBasement", "World2,DarkTeleportRoom");
            nextMap.Add("World2,DarkTeleportRoom", "World2,ShrineEntrance");
            nextMap.Add("World2,ShrineEntrance", "World2,ShrineSanctuaryStart");
            nextMap.Add("World2,ShrineSanctuaryStart", "World2,ShrineSanctuaryHall1");
            nextMap.Add("World2,ShrineSanctuaryHall1", "World2,ShrineSanctuaryOutside1");
            nextMap.Add("World2,ShrineSanctuaryOutside1", "World2,ShrineSanctuaryHall2");
            nextMap.Add("World2,ShrineSanctuaryHall2", "World2,ShrineSanctuaryRoom1");
            nextMap.Add("World2,ShrineSanctuaryRoom1", "World2,ShrineSanctuaryRoom2");
            nextMap.Add("World2,ShrineSanctuaryRoom2", "World2,ShrineSanctuaryRoom3");
            nextMap.Add("World2,ShrineSanctuaryRoom3", "World2,ShrineSanctuaryHall3");
            nextMap.Add("World2,ShrineSanctuaryHall3", "World2,CastleMoat");
            nextMap.Add("World2,CastleMoat", "World2,OutsideGarden");
            nextMap.Add("World2,OutsideGarden", "World2,OutsideGarden2");
            nextMap.Add("World2,OutsideGarden2", "World2,OutsideGarden3");
            nextMap.Add("World2,OutsideGarden3", "World2,OutsideSnow");
            nextMap.Add("World2,OutsideSnow", "World2,Skiing");
            nextMap.Add("World2,Skiing", "World2,MilitaryBase");
            nextMap.Add("World2,MilitaryBase", "World2,Kam2");
            nextMap.Add("World2,Kam2", "World2,Mc1");
            nextMap.Add("World2,Mc1", "World2,BrickSquare");
            nextMap.Add("World2,BrickSquare", "World2,Disco");
            nextMap.Add("World2,Disco", "World2,BlackMap1");
            nextMap.Add("World2,BlackMap1", "World2,BlackMap2");
            nextMap.Add("World2,BlackMap2", "World2,BlackMap3");
            nextMap.Add("World2,BlackMap3", "World2,BlackMap4");
            nextMap.Add("World2,BlackMap4", "World2,HypnosisWalk");
            nextMap.Add("World2,HypnosisWalk", "World2,DarkAlley");
            nextMap.Add("World2,DarkAlley", "World2,PreBossBridge");
            nextMap.Add("World2,PreBossBridge", "World3,DragonCave");

            string mapName;
            if (!nextMap.TryGetValue(currentMap, out mapName))
            {
                mapName = "World1,Home";
            }

            LoadMap(mapName.Split(',')[0], mapName.Split(',')[1]);
        }
    }
}