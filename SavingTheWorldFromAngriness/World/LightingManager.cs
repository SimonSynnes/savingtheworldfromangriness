﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Lighting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SavingTheWorldFromAngriness.World.Tiles;

namespace SavingTheWorldFromAngriness.World
{
    public class LightingManager : DrawableGameComponent
    {
        SpriteBatch spriteBatch;

        RenderTarget2D colorMap;
        RenderTarget2D lightMap;
        RenderTarget2D blurMap;

        public Effect lightEffect;
        Effect combineEffect;

        Quad quad;

        public List<Visual> blocks;
        public List<PointLight> lights;

        Camera camera;

        public LightingManager(Game game, Camera camera)
            : base(game)
        {
            int windowWidth = GraphicsDevice.Viewport.Width;
            int windowHeight = GraphicsDevice.Viewport.Height;

            // Set up all render targets, the blur map doesn't need a depth buffer
            colorMap = new RenderTarget2D(GraphicsDevice, windowWidth, windowHeight, false, SurfaceFormat.Color, DepthFormat.None);
            lightMap = new RenderTarget2D(GraphicsDevice, windowWidth, windowHeight, false, SurfaceFormat.Color, DepthFormat.None);
            blurMap = new RenderTarget2D(GraphicsDevice, windowWidth, windowHeight, false, SurfaceFormat.Color, DepthFormat.None);

            quad = new Quad();
            blocks = new List<Visual>();
            lights = new List<PointLight>();
            
            this.camera = camera;
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            combineEffect = Game.Content.Load<Effect>("Effects/Combine");
            lightEffect = Game.Content.Load<Effect>("Effects/Light");

            base.LoadContent();
        }

        public void AddVisualBlock(Texture2D texture, Vector2 position, float rotation, Texture2D glow, Rectangle rectangle)
        {
            blocks.Add(new Visual(texture, position, rotation, glow, 0, rectangle));
        }

        public void AddVisualBlock(Texture2D texture, Vector2 position, float rotation, Texture2D glow, int index)
        {
            blocks.Add(new Visual(texture, position, rotation, glow, index, new Rectangle((int)position.X, (int)position.Y, Global.GameConstants.TILE_SIZE, Global.GameConstants.TILE_SIZE)));
        }

        public void AddLight(Vector2 position, float radius, Color color, float power)
        {
             lights.Add(new PointLight(lightEffect, position, radius, color, power));
        }

        public void AddLight(PointLight light)
        {
            lock (lights)
            {
                lights.Add(light);
            }
        }

        public void RemoveLight(Vector2 position)
        {
            PointLight removeLight = null;

            foreach (PointLight light in lights)
            {
                if (light.Position == position)
                {
                    removeLight = light;
                    break;
                }
            }

            if (removeLight != null)
            {
                lock (lights)
                {
                    lights.Remove(removeLight);
                }
            }
        }

        public void RemoveVisualBlock(Vector2 position)
        {
            Visual removeBlock = null;

            foreach (Visual visual in blocks)
            {
                if (visual.Pose.Position == position)
                {
                    removeBlock = visual;
                    break;
                }
            }

            if (removeBlock != null)
            {
                lock (blocks)
                {
                    blocks.Remove(removeBlock);
                }
            }
        }

        public bool isBlockInList(int index, string visualBlock)
        {
            bool returnValue = false;


            foreach (Visual visual in blocks)
            {
                if (visual.Index == index && visual.Texture.Tag.ToString() == visualBlock)
                {
                    returnValue = true;
                    break;
                }
            }


            return returnValue;
        }

        public bool isBlockInList(int index)
        {
            bool returnValue = false;
            foreach (Visual visual in blocks)
            {
                if (visual.Index == index)
                {
                    returnValue = true;
                    break;
                }
            }
            return returnValue;
        }

        public Visual getBlock(int index)
        {
            Visual returnValue = null;
            foreach (Visual visual in blocks)
            {
                if (visual.Index == index)
                {
                    returnValue = visual;
                    break;
                }
            }

            return returnValue;
        }

        public void RemoveVisualBlock(int index)
        {
            Visual removeBlock = null;

            foreach (Visual visual in blocks)
            {
                if (visual.Index == index)
                {
                    removeBlock = visual;
                    break;
                }
            }

            if (removeBlock != null)
            {
                lock (blocks)
                {
                    blocks.Remove(removeBlock);
                }
            }
        }

        public void ClearLights()
        {
            lock (lights)
            {
                lights.Clear();
            }
        }

        public void ClearBlocks()
        {
            lock (blocks)
            {
                blocks.Clear();
            }
        }

        public void DrawLighting(float ambientLightStrength)
        {
            // Draw the lights
            DrawLightMap(ambientLightStrength);

            // Combine
            CombineAndDraw();
        }

        private void CombineAndDraw()
        {
            GraphicsDevice.SetRenderTarget(null);

            GraphicsDevice.BlendState = BlendState.Opaque;

            // Samplers states are set by the shader itself            
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;

            combineEffect.Parameters["colorMap"].SetValue(colorMap);
            combineEffect.Parameters["lightMap"].SetValue(lightMap);

            combineEffect.Techniques[0].Passes[0].Apply();
            quad.Render(GraphicsDevice, Vector2.One * -1.0f, Vector2.One);
        }

        /// <summary>
        /// Draws everything that emits light to a seperate render target
        /// </summary>
        private void DrawLightMap(float ambientLightStrength)
        {
            GraphicsDevice.SetRenderTarget(lightMap);
            GraphicsDevice.Clear(Color.White * ambientLightStrength);

            GraphicsDevice.BlendState = BlendState.Additive;

            // Samplers states are set by the shader itself            
            GraphicsDevice.DepthStencilState = DepthStencilState.None;
            GraphicsDevice.RasterizerState = RasterizerState.CullNone;

            lock (lights)
            {
                foreach (PointLight l in lights)
                {
                    l.Render(GraphicsDevice, blocks, camera.position);
                }
            }

            spriteBatch.Begin(SpriteSortMode.BackToFront,
                BlendState.AlphaBlend,
                SamplerState.LinearWrap,
                DepthStencilState.Default,
                RasterizerState.CullCounterClockwise,
                null,
                camera.TransformMatrix);

            lock (blocks)
            {
                foreach (Visual v in blocks)
                {
                    Vector2 origin = new Vector2(v.Glow.Width / 2.0f, v.Glow.Height / 2.0f);
                    spriteBatch.Draw(v.Glow, v.Pose.Position, v.rectangle, Color.White, v.Pose.Rotation, origin, v.Pose.Scale, SpriteEffects.None, 0);
                }
            }

            spriteBatch.End();
        }

        /// <summary>
        /// Draws all normal objects
        /// </summary>
        public void DrawTiles(Tile[] tiles)
        {
            GraphicsDevice.SetRenderTarget(colorMap);

            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin(SpriteSortMode.BackToFront,
                BlendState.NonPremultiplied,
                SamplerState.LinearWrap,
                DepthStencilState.Default,
                RasterizerState.CullCounterClockwise,
                null,
                camera.TransformMatrix);

            lock (blocks)
            {
                foreach (Visual v in blocks)
                {
                    Vector2 origin = new Vector2(v.Texture.Width / 2.0f, v.Texture.Height / 2.0f);

                    spriteBatch.Draw(v.Texture, v.Pose.Position, v.rectangle, Color.White, v.Pose.Rotation, origin, v.Pose.Scale, SpriteEffects.None, 0);
                }
            }

            for (int i = 0; i < tiles.Length; i++)
            {
                if (tiles[i] != null)
                {
                   spriteBatch.Draw(tiles[i].texture, tiles[i].position - new Vector2(Global.GameConstants.TILE_SIZE / 2, Global.GameConstants.TILE_SIZE / 2),
                        new Rectangle((int)tiles[i].crop.X, (int)tiles[i].crop.Y, Global.GameConstants.TILE_SIZE, Global.GameConstants.TILE_SIZE), Color.White, 0,
                        Vector2.Zero, 1, SpriteEffects.None, 0);
                }
            }

            spriteBatch.End();
        }
    }
}
