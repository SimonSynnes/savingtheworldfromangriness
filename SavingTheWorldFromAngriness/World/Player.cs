﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using SavingTheWorldFromAngriness.World.Maps;
using SavingTheWorldFromAngriness.Global.Sounds;

using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace SavingTheWorldFromAngriness.World
{
    public class Player : DrawableGameComponent
    {
        private Texture2D playerTexture;
        private Vector2 playerSize;
        private Vector2 playerPosition;
        private float playerAngle;
        private float playerMovementSpeed;
        private WorldRenderer worldRenderer;
        private List<Bullet> bullets;
        private Texture2D bulletTexture;
        private BasicMap basicMap;
        private bool isLeftMouseDown;
        private SoundEffect paintBallSoundEffect;
        private SoundEffect paintBallReloadSoundEffect;
        private SoundEffect bulletImpactSoundEffect;
        private SoundEffect bulletOutOfAmmo;
        private SoundEffect bulletLandSoundEffect;
        private bool isReloading;
        private double isReloadingElapsedTime;
        private double reloadSpeed;
        private List<Vector2> splatPositions;
        private Texture2D splatTexture;

        public Player(Game game, WorldRenderer worldRenderer, BasicMap basicMap) : base(game)
        {
            this.playerPosition = new Vector2(12 * 32, 4 * 32);
            this.playerMovementSpeed = 4f;
            this.playerAngle = 0;
            this.isLeftMouseDown = false;
            this.worldRenderer = worldRenderer;
            this.basicMap = basicMap;
            this.bullets = new List<Bullet>();
            this.isReloading = false;
            this.isReloadingElapsedTime = 0;
            this.reloadSpeed = 1170;
            this.playerSize = new Vector2(47, 41);
            this.splatPositions = new List<Vector2>();
        }

        protected override void LoadContent()
        {
            playerTexture = Game.Content.Load<Texture2D>("Textures/PlayerTexture");
            bulletTexture = Game.Content.Load<Texture2D>("Textures/Bullet");
            paintBallSoundEffect = Game.Content.Load<SoundEffect>("Sounds/bullet");
            paintBallReloadSoundEffect = Game.Content.Load<SoundEffect>("Sounds/Reloading");
            bulletImpactSoundEffect = Game.Content.Load<SoundEffect>("Sounds/hitsound");
            bulletOutOfAmmo = Game.Content.Load<SoundEffect>("Sounds/Misc/noAmmo");
            splatTexture = Game.Content.Load<Texture2D>("Textures/splat");
            bulletLandSoundEffect = Game.Content.Load<SoundEffect>("Sounds/Misc/impact");

            base.LoadContent();
        }

        public List<Vector2> getSplats()
        {
            return splatPositions;
        }

        public void AddSplat(Vector2 splat)
        {
            lock (splatPositions)
            {
                splatPositions.Add(splat);
            }
        }

        public int getHealth()
        {
            return Global.Player.HEALTH;
        }

        public int getAttackPower()
        {
            return Global.Player.WEAPON.attackPower;
        }
        
        public void SetWalkingSpeed(int speed)
        {
            this.playerMovementSpeed = speed;
        }

        public void SetPlayerSize(Vector2 size)
        {
            this.playerSize = size;
        }

        public void SetPlayerTexture(Texture2D texture)
        {
            this.playerTexture = texture;
        }

        public void SetPlayerAngle(float angle)
        {
            this.playerAngle = angle;
        }

        public void attackPlayer(int attackPower)
        {
            Global.Player.HEALTH -= attackPower;
            Global.Sounds.SoundManager.PlaySoundEffect(bulletImpactSoundEffect);
        }
        
        public void Update(GameTime gameTime, KeyboardState ks, MouseState ms, Vector2 mousePosition)
        {
            Vector2 direction = mousePosition - playerPosition;

            if (ms.LeftButton == ButtonState.Pressed)
            {
                isLeftMouseDown = true;
            }
            else
            {
                // Check if left mouse has been pressed and then released
                // (in order to only call functions once)
                if (isLeftMouseDown)
                {
                    Shoot();
                }

                isLeftMouseDown = false;
            }

            if (isReloading)
            {
                isReloadingElapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
                if (isReloadingElapsedTime >= reloadSpeed)
                {
                    Global.Player.WEAPON.Reload();

                    isReloading = false;
                    isReloadingElapsedTime = 0;
                }

            }

            Vector2 newPlayerDirection = Vector2.Zero;
            playerAngle = (float)(Math.Atan2(direction.Y, direction.X));

            if (ks.IsKeyDown(Keys.W) || (ks.IsKeyDown(Keys.Up)))
            {
                newPlayerDirection = new Vector2(newPlayerDirection.X, -1);
            }

            if (ks.IsKeyDown(Keys.A) || (ks.IsKeyDown(Keys.Left)))
            {
                newPlayerDirection = new Vector2(-1, newPlayerDirection.Y);
            }

            if (ks.IsKeyDown(Keys.S) || (ks.IsKeyDown(Keys.Down)))
            {
                newPlayerDirection = new Vector2(newPlayerDirection.X, 1);
            }

            if (ks.IsKeyDown(Keys.D) || (ks.IsKeyDown(Keys.Right)))
            {
                newPlayerDirection = new Vector2(1, newPlayerDirection.Y);
            }

            if (newPlayerDirection != Vector2.Zero)
            {
                direction.Normalize();

                Vector2 newPlayerPosition = playerPosition + (newPlayerDirection * playerMovementSpeed);

                Rectangle playerRectangleX = new Rectangle((int)newPlayerPosition.X,
                    (int)playerPosition.Y, playerTexture.Width, playerTexture.Height);

                Rectangle playerRectangleY = new Rectangle((int)playerPosition.X,
                    (int)newPlayerPosition.Y, playerTexture.Width, playerTexture.Height);

                if (!worldRenderer.checkWallCollision(playerRectangleX) & !basicMap.checkEntityCollision(playerRectangleX))
                {
                    playerPosition = new Vector2(newPlayerPosition.X, playerPosition.Y);
                }

                if (!worldRenderer.checkWallCollision(playerRectangleY) & !basicMap.checkEntityCollision(playerRectangleY))
                {
                    playerPosition = new Vector2(playerPosition.X, newPlayerPosition.Y);
                }
            }

            UpdateBullets();
        }

        public void UpdateBullets()
        {
            foreach (Bullet bullet in bullets)
            {
                if (bullet.isVisible)
                {
                    bullet.position += bullet.velocity;

                    if (Vector2.Distance(bullet.position, playerPosition) > Global.Player.WEAPON.bulletRange)
                    {
                        bullet.isVisible = false;
                    }
                    else
                    {
                        Rectangle bulletRectangle = new Rectangle(
                            (int)bullet.position.X + (Global.GameConstants.TILE_SIZE / 2),
                            (int)bullet.position.Y + (Global.GameConstants.TILE_SIZE / 2),
                            bullet.texture.Width, bullet.texture.Height);
                        if (worldRenderer.checkWallCollision(bulletRectangle))
                        {
                            bullet.isVisible = false;
                        }
                        else
                        {
                            if (basicMap.checkBulletCollision(bulletRectangle))
                            {
                                bullet.isVisible = false;
                                bullet.hitEnemy = true;
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < bullets.Count; i++)
            {
                if (!bullets[i].isVisible)
                {
                    lock (bullets)
                    {
                        splatPositions.Add(bullets[i].position - (new Vector2((int)splatTexture.Width / 2, (int)splatTexture.Height / 2)));
                        if (!bullets[i].hitEnemy)
                        {
                            Global.Sounds.SoundManager.PlaySoundEffect(bulletLandSoundEffect);
                        }

                        bullets.Remove(bullets[i]);
                    }
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch, Camera camera)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred,
                BlendState.NonPremultiplied,
                SamplerState.AnisotropicClamp,
                DepthStencilState.Default,
                RasterizerState.CullCounterClockwise,
                null,
                camera.TransformMatrix);

            foreach (Bullet bullet in bullets)
            {
                bullet.Draw(spriteBatch);
            }

            foreach (Vector2 position in splatPositions)
            {
                spriteBatch.Draw(splatTexture, position, Color.White);
            }

            spriteBatch.Draw(playerTexture, new Rectangle(Convert.ToInt32(playerPosition.X),
                Convert.ToInt32(playerPosition.Y), playerTexture.Width, playerTexture.Height), null,
                Color.White, playerAngle, new Vector2(playerTexture.Width / 2, playerTexture.Height / 2), SpriteEffects.None, 0);

            spriteBatch.End();
        }

        public void setPlayerPosition(Vector2 newPosition)
        {
            playerPosition = newPosition;
        }

        public Vector2 getPlayerPosition()
        {
            return playerPosition;
        }

        public Rectangle getPlayerRectangle()
        {
            Vector2 newPlayerAngle = Vector2.Transform(playerSize / 14, Matrix.CreateRotationZ(playerAngle));

            return new Rectangle(
                (int)playerPosition.X - (Global.GameConstants.TILE_SIZE / 2) - (int)newPlayerAngle.X,
                (int)playerPosition.Y - (Global.GameConstants.TILE_SIZE / 2) - (int)newPlayerAngle.Y,
                (int)playerSize.X - 10, (int)playerSize.Y - 10);
        }

        public void Shoot()
        {
            if (!isReloading)
            {
                if (Global.Player.AMMO_IN_MAGAZINE <= 0)
                {
                    // Needs to reload...
                    if (Global.Player.AMMO > 0)
                    {
                        isReloading = true;
                        Global.Sounds.SoundManager.PlaySoundEffect(paintBallReloadSoundEffect);
                    }

                    Global.Sounds.SoundManager.PlaySoundEffect(bulletOutOfAmmo);
                }
                else
                {
                    SoundManager.PlaySoundEffect(paintBallSoundEffect);

                    Bullet newBullet = new Bullet(bulletTexture);
                    newBullet.velocity = new Vector2((float)Math.Cos(playerAngle), (float)Math.Sin(playerAngle)) * 5f;
                    newBullet.position = playerPosition + newBullet.velocity * 5 - new Vector2(9, 8);
                    newBullet.isVisible = true;

                    if (bullets.Count() < 30)
                    {
                        bullets.Add(newBullet);
                    }

                    Global.Player.AMMO_IN_MAGAZINE--;

                    if (Global.Player.AMMO_IN_MAGAZINE == 0)
                    {
                        isReloading = true;
                        Global.Sounds.SoundManager.PlaySoundEffect(paintBallReloadSoundEffect);
                    }
                }
            }
        }
    }
}
