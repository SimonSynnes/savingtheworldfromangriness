﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness
{
    class IntroSlide
    {
        public Texture2D texture;
        public int slideLength;
        public bool skipFade;

        public IntroSlide(Texture2D texture, int slideLength, bool skipFade)
        {
            this.texture = texture;
            this.slideLength = slideLength;
            this.skipFade = skipFade;
        }
    }

    class IntroScreen : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private Game1 game1;

        private double elapsedTimeUntilSkip;
        private bool isSkipEnabled;

        private Texture2D skipTexture;
        private Texture2D titleTexture;

        private double colorOpacityTimeElapsed;
        private float colorOpacity;

        private double beginIntroTimeElapsed;
        private bool beginIntro;

        private int slideIndex;
        private List<IntroSlide> slides;

        private double nextSlideTimeElapsed;
        private double waitTimeForSlide;

        public IntroScreen(Game game) : base (game)
        {
            this.game1 = (Game1)game;
            this.elapsedTimeUntilSkip = 0;
            this.isSkipEnabled = false;
            this.beginIntro = false;
            this.colorOpacity = 0;
            this.colorOpacityTimeElapsed = 0;
            this.beginIntroTimeElapsed = 0;
            this.slideIndex = 0;
            this.waitTimeForSlide = 0;
            this.nextSlideTimeElapsed = 0;
            this.slides = new List<IntroSlide>();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);


            Song song = Game.Content.Load<Song>("Sounds/Intro/intro");
            Global.Sounds.SoundManager.PlayBackgroundMusic(song);

            skipTexture = Game.Content.Load<Texture2D>("Textures/Intro/skip");
            titleTexture = Game.Content.Load<Texture2D>("Textures/Intro/title");

            slides.Add(new IntroSlide(titleTexture, 2000, false));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/titleColor"), 6000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/monogame"), 100, false));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/geonbit"), 100, false));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/jonoie"), 100, false));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/text1"), 8000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/text2"), 8000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/madeInVS"), 5000, false));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/mapMaking"), 5000, false));

            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l1"), 2000, false));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l2"), 15000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l3"), 5000, false));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l4"), 5000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l6"), 7000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l7"), 5000, false));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l8"), 7000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l9"), 3000, false));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l10"), 8000, false));

            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/dotdot"), 7000, false));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l11"), 8000, false));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l12"), 8000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l13"), 6000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l14"), 5000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l15"), 5000, false));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l16"), 6000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l17"), 3000, false));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l18"), 5000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l19"), 8000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l20"), 3000, false));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l21"), 8000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l22"), 15000, false));

            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l23"), 5000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l24"), 5000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l25"), 5000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l26"), 5000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l27"), 5000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l28"), 5000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l29"), 10000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l30"), 2000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l31"), 5000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l32"), 5000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l33"), 5000, true));
            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/l34"), 15000, true));

            slides.Add(new IntroSlide(Game.Content.Load<Texture2D>("Textures/Intro/stwfa"), 5000, false));
            waitTimeForSlide = slides[0].slideLength;

            base.LoadContent();
        }
        // Textures/Intro/text3
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (!isSkipEnabled)
            {
                elapsedTimeUntilSkip += gameTime.ElapsedGameTime.TotalMilliseconds;
                if (elapsedTimeUntilSkip >= 7500)
                {
                    isSkipEnabled = true;

                    elapsedTimeUntilSkip = 0;
                }
            }

            if (!beginIntro)
            { 
                beginIntroTimeElapsed += gameTime.ElapsedGameTime.TotalMilliseconds;
                if (beginIntroTimeElapsed >= 2000)
                {
                    beginIntro = true;

                    beginIntroTimeElapsed = 0;
                }

                return;
            }

            if (isSkipEnabled)
            {
                MouseState ms = Mouse.GetState();

                if (ms.LeftButton == ButtonState.Pressed)
                {
                    Global.Sounds.SoundManager.StopBackgroundMusic();

                    game1.OpenStartMenu();
                    this.Game.Components.Remove(this);
                }
            }

            if (colorOpacity <= 1)
            {
                colorOpacityTimeElapsed += gameTime.ElapsedGameTime.TotalMilliseconds;
                if (colorOpacityTimeElapsed >= 66)
                {
                    colorOpacity += 0.02f;
                    colorOpacityTimeElapsed = 0;
                }
            }
            else
            {
                nextSlideTimeElapsed += gameTime.ElapsedGameTime.TotalMilliseconds;
                if (nextSlideTimeElapsed >= waitTimeForSlide)
                {
                    if (slideIndex < (slides.Count - 1))
                    {
                        slideIndex++;

                        waitTimeForSlide = slides[slideIndex].slideLength;

                        if (!slides[slideIndex].skipFade)
                        {
                            colorOpacity = 0;
                        }
                    }

                    nextSlideTimeElapsed = 0;
                }
            }
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(new Color(35, 35, 35));

            int windowWidth = GraphicsDevice.Viewport.Width;
            int windowHeight = GraphicsDevice.Viewport.Height;

            spriteBatch.Begin();
            if (isSkipEnabled)
            {
                spriteBatch.Draw(skipTexture, 
                    new Vector2(
                        windowWidth - skipTexture.Width - 35,
                        windowHeight - skipTexture.Height - 35), Color.White);
            }

            if (slides[slideIndex].texture.Width > windowWidth && slides[slideIndex].texture.Height > windowHeight)
            {
                spriteBatch.Draw(slides[slideIndex].texture,
                    new Rectangle(
                        0, 0,
                        windowWidth, windowHeight),
                    Color.White * colorOpacity);
            }
            else
            if (slides[slideIndex].texture.Width > windowWidth)
            {
                spriteBatch.Draw(slides[slideIndex].texture,
                    new Rectangle(
                        0,
                        (windowHeight / 2) - (slides[slideIndex].texture.Height / 2),
                        windowWidth, slides[slideIndex].texture.Height),
                    Color.White * colorOpacity);
            }
            else
            if (slides[slideIndex].texture.Height > windowHeight)
            {
                spriteBatch.Draw(slides[slideIndex].texture,
                    new Rectangle(
                        (windowWidth / 2) - (slides[slideIndex].texture.Width / 2),
                        0,
                        slides[slideIndex].texture.Width, windowHeight),
                    Color.White * colorOpacity);
            }
            else
            {
                spriteBatch.Draw(slides[slideIndex].texture,
                    new Vector2(
                        (windowWidth / 2) - (slides[slideIndex].texture.Width / 2),
                        (windowHeight / 2) - (slides[slideIndex].texture.Height / 2)), Color.White * colorOpacity);
            }

            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
