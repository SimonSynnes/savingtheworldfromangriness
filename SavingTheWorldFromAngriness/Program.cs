﻿using System;
using System.Reflection;
using System.Linq;

namespace SavingTheWorldFromAngriness
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(String[] args)
        {
            if (args.Length == 0)
            {
                using (var game = new Game1())
                {
                    game.Run();
                }
            }
            else
            {
                if (args[0].Trim().ToLower() == "test")
                {
                    Test.ConsoleUI.BeginTesting();
                }
            }
        }

        public static Type[] GetComponentsClasses()
        {
            return GetTypesInNamespace(Assembly.GetExecutingAssembly(), "SavingTheWorldFromAngriness.World.Components");
        }

        private static Type[] GetTypesInNamespace(Assembly assembly, string nameSpace)
        {
            return
              assembly.GetTypes()
                      .Where(t => String.Equals(t.Namespace, nameSpace, StringComparison.Ordinal))
                      .ToArray();
        }
    }
#endif
}
