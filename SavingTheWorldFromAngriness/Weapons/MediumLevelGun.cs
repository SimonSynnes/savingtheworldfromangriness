﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavingTheWorldFromAngriness.Weapons
{
    [Serializable]
    class MediumLevelGun : Weapon
    {
        public MediumLevelGun()
        {
            this.attackPower = 35;
            this.bulletRange = 170;
            this.magazineSize = 13;
        }
    }
}