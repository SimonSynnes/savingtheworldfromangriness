﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavingTheWorldFromAngriness.Weapons
{
    [Serializable]
    class BeginnerGun : Weapon
    {
        public BeginnerGun()
        {
            this.attackPower = 25;
            this.bulletRange = 150;
            this.magazineSize = 10;
        }
    }
}
