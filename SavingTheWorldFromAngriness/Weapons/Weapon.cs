﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavingTheWorldFromAngriness.Weapons
{
    [Serializable]
    public class Weapon
    {
        public int attackPower;
        public int bulletRange;
        public int magazineSize;

        public Weapon()
        {
        }

        public void Reload()
        {
            if (Global.Player.AMMO < (magazineSize - Global.Player.AMMO_IN_MAGAZINE))
            {
                Global.Player.AMMO_IN_MAGAZINE = Global.Player.AMMO;
                Global.Player.AMMO = 0;
            }
            else
            {
                Global.Player.AMMO -= magazineSize;
                Global.Player.AMMO_IN_MAGAZINE = (magazineSize - Global.Player.AMMO_IN_MAGAZINE);
            }

        }
    }
}
