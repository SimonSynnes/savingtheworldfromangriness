﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavingTheWorldFromAngriness.Weapons
{
    [Serializable]
    class HighLevelGun : Weapon
    {
        public HighLevelGun()
        {
            this.attackPower = 45;
            this.bulletRange = 200;
            this.magazineSize = 15;
        }
    }
}