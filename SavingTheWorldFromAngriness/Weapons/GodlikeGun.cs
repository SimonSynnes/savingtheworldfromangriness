﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavingTheWorldFromAngriness.Weapons
{
    [Serializable]
    class GodlikeGun : Weapon
    {
        public GodlikeGun()
        {
            this.attackPower = 50;
            this.bulletRange = 250;
            this.magazineSize = 17;
        }
    }
}