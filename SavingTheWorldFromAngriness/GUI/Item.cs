﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GeonBit.UI;
using GeonBit.UI.Entities;
using GeonBit.UI.Entities.TextValidators;
using GeonBit.UI.DataTypes;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using SavingTheWorldFromAngriness.World;

namespace SavingTheWorldFromAngriness.GUI
{
    public static class Item
    {
        public static Button DialogPanel(string name, Texture2D avatar, out Paragraph paragraph)
        {
            Panel panel = new Panel(new Vector2(600, 200), PanelSkin.Simple, Anchor.BottomCenter, new Vector2(0, 50));
            panel.Padding = Vector2.Zero;

            Panel entitiesGroup = new Panel(new Vector2(600, 200), PanelSkin.None, Anchor.AutoCenter);
            entitiesGroup.Padding = Vector2.Zero;
            panel.AddChild(entitiesGroup);

            Panel avatarPanel = new Panel(new Vector2(120, 120), PanelSkin.None, Anchor.CenterLeft);
            avatarPanel.Padding = Vector2.Zero;
            entitiesGroup.AddChild(avatarPanel);

            Panel titlePanel = new Panel(new Vector2(480, 55), PanelSkin.None, Anchor.TopRight, new Vector2(0, 15));
            titlePanel.Padding = Vector2.Zero;
            entitiesGroup.AddChild(titlePanel);

            Label title = new Label(name, Anchor.Center, null, new Vector2(10, 0));
            title.FillColor = Color.Red;
            title.Scale = 1f;

            titlePanel.AddChild(title);

            Panel textPanel = new Panel(new Vector2(480, 145), PanelSkin.None, Anchor.BottomRight);
            textPanel.Padding = Vector2.Zero;

            paragraph = new Paragraph("", Anchor.TopLeft, new Vector2(440, 145), new Vector2(35, 15));
            paragraph.Padding = Vector2.Zero;
            textPanel.AddChild(paragraph);

            Image avatarPanelImage = new Image(avatar, new Vector2(120, 0), ImageDrawMode.Stretch, Anchor.CenterLeft, new Vector2(15, 0));
            avatarPanel.AddChild(avatarPanelImage);

            entitiesGroup.AddChild(textPanel);

            Button btnOK = new Button("OK", ButtonSkin.Default, Anchor.TopRight, new Vector2(150, 55), new Vector2(-152, 0));
            panel.AddChild(btnOK);

            btnOK.OnClick += (Entity entity) =>
            {
                UserInterface.Active.RemoveEntity(panel);
            };

            UserInterface.Active.AddEntity(panel);

            return btnOK;
        }
        public static Button OptionsPanel()
        {
            Panel optionsPanel = new Panel(new Vector2(600, 500));
            UserInterface.Active.AddEntity(optionsPanel);

            optionsPanel.AddChild(new Header("Options menu"));
            optionsPanel.AddChild(new LineSpace());
            optionsPanel.AddChild(new Paragraph("Music volume"));
            optionsPanel.AddChild(new LineSpace());
            float musicValue = MediaPlayer.Volume * 10;
            Slider songSlider = new Slider(min: 0, max: 10, skin: SliderSkin.Fancy, anchor: Anchor.Auto, offset: Vector2.Zero);
            songSlider.Value = (int)musicValue;
            optionsPanel.AddChild(songSlider);
            songSlider.OnValueChange = (Entity entity) =>
            {
                Global.Sounds.SoundManager.VOLUME_SONG = songSlider.GetValueAsPercent();
                MediaPlayer.Volume = songSlider.GetValueAsPercent();
            };

            optionsPanel.AddChild(new LineSpace());
            optionsPanel.AddChild(new Paragraph("Sound effect volume"));
            optionsPanel.AddChild(new LineSpace());
            float effectValue = Global.Sounds.SoundManager.VOLUME_EFFECT * 10;
            Slider soundEffectSlider = new Slider(min: 0, max: 10, skin: SliderSkin.Fancy, anchor: Anchor.Auto, offset: Vector2.Zero);
            soundEffectSlider.Value = (int)effectValue;
            optionsPanel.AddChild(soundEffectSlider);
            soundEffectSlider.OnValueChange = (Entity entity) =>
            {
                Global.Sounds.SoundManager.VOLUME_EFFECT = soundEffectSlider.GetValueAsPercent();
                SoundEffect.MasterVolume = soundEffectSlider.GetValueAsPercent();
            };

            Button btn = new Button("OK", ButtonSkin.Default, Anchor.BottomCenter, new Vector2(150, 55), new Vector2(-152, 0));
            optionsPanel.AddChild(btn);

            btn.OnClick += (Entity entity) =>
            {
                UserInterface.Active.RemoveEntity(optionsPanel);
            };

            return btn;
        }

    }
}
