﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SavingTheWorldFromAngriness.World.Maps;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

using GeonBit.UI;
using GeonBit.UI.Entities;
using GeonBit.UI.Entities.TextValidators;
using GeonBit.UI.DataTypes;

namespace SavingTheWorldFromAngriness.GUI
{
    class DialogPrompt : GameComponent
    {
        private string title;
        private Texture2D avatar;
        private Paragraph paragraph;
        private string text;
        private double elapsedTime;
        private bool isPromptFinished;
        private Button btnOK;
        private SoundEffect promptSoundEffect;

        public DialogPrompt(Game game, string title, Texture2D avatar, string text, SoundEffect promptSoundEffect)
            : base(game)
        {
            this.title = title;
            this.avatar = avatar;
            this.text = text;
            this.elapsedTime = 0;
            this.isPromptFinished = false;
            this.promptSoundEffect = promptSoundEffect;
        }

        public override void Initialize()
        {
            btnOK = Item.DialogPanel(title, avatar, out paragraph);

            btnOK.OnClick += (Entity entity) =>
            {
                isPromptFinished = true;
            };
        }

        public Button getOKButton()
        {
            return btnOK;
        }

        public override void Update(GameTime gameTime)
        {
            if (!isPromptFinished)
            {
                elapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
                if (elapsedTime >= 66)
                {
                    int paragraphLength = paragraph.Text.Length;
                    int textLength = text.Length;

                    if (paragraphLength < textLength)
                    {
                        paragraph.Text += text[paragraphLength];
                        Global.Sounds.SoundManager.PlaySoundEffect(promptSoundEffect);
                    }
                    else
                    {
                        isPromptFinished = true;
                    }

                    elapsedTime = 0;
                }
            }
        }
    }
}
