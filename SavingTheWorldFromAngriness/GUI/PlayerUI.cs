﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GeonBit.UI;
using GeonBit.UI.Entities;
using GeonBit.UI.Entities.TextValidators;
using GeonBit.UI.DataTypes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SavingTheWorldFromAngriness.Weapons;

namespace SavingTheWorldFromAngriness.GUI
{
    public class PlayerUI : DrawableGameComponent
    {
        private ProgressBar healthBar;
        private List<Entity> UIEntities;
        private SpriteFont digitalFont;
        private Texture2D heartTexture;
        private Texture2D coinTexture;
        private SpriteFont ammoFont;
        private Texture2D gunTexture;
        private Texture2D ammoSeperatorTexture;
        private Texture2D medTexture;
        private Texture2D highTexture;
        private Texture2D godlikeTexure;

        public Texture2D GunTexture
        {
            get
            {
                return gunTexture;
            }

            set
            {
                gunTexture = value;
            }
        }

        public PlayerUI(Game game)
            : base(game)
        {
            UIEntities = new List<Entity>();
        }

        protected override void LoadContent()
        {
            digitalFont = Game.Content.Load<SpriteFont>("Fonts/CoinFont");
            heartTexture = Game.Content.Load<Texture2D>("Textures/heart");
            coinTexture = Game.Content.Load<Texture2D>("Textures/bitcoin");
            ammoFont = Game.Content.Load<SpriteFont>("Fonts/AmmoFont");
            ammoFont.Spacing = -10;
            
            gunTexture = Game.Content.Load<Texture2D>("Textures/PaintBallGun1");
            medTexture = Game.Content.Load<Texture2D>("Textures/mediumGun");
            highTexture = Game.Content.Load<Texture2D>("Textures/highGun");
            godlikeTexure = Game.Content.Load<Texture2D>("Textures/godlikeGun");

            ammoSeperatorTexture = Game.Content.Load<Texture2D>("Textures/AmmoSeperator");

            AddStatsPanel();

            base.LoadContent();
        }

        /**
         * This function shows a player health bar
         */
        private void AddStatsPanel()
        {
            Panel statsPanel = new Panel(new Vector2(300, 170), PanelSkin.None, Anchor.TopLeft, new Vector2(5, 5));
            statsPanel.Opacity = 200;

            Panel entitiesGroup = new Panel(new Vector2(0, 240), PanelSkin.None, Anchor.AutoCenter);
            entitiesGroup.Padding = Vector2.Zero;
            statsPanel.AddChild(entitiesGroup);

            Panel leftPanel = new Panel(new Vector2(810, 0), PanelSkin.None, Anchor.TopLeft);
            leftPanel.Padding = Vector2.Zero;
            entitiesGroup.AddChild(leftPanel);

            Panel rightPanel = new Panel(new Vector2(190, 0), PanelSkin.None, Anchor.TopRight, new Vector2(-20, 0));
            rightPanel.Padding = Vector2.Zero;
            entitiesGroup.AddChild(rightPanel);

            healthBar = new ProgressBar(0, 100, Anchor.Auto, new Vector2(0, 0));
            healthBar.Locked = true;
            healthBar.Value = 100;
            healthBar.Size = new Vector2(rightPanel.Size.X, 30);
            healthBar.ProgressFill.FillColor = Color.Red;
            healthBar.Padding = new Vector2(2, 0);

            Image heart = new Image(heartTexture, new Vector2(heartTexture.Width, heartTexture.Height), ImageDrawMode.Stretch, Anchor.Auto);
            Image coin = new Image(coinTexture, new Vector2(coinTexture.Width, coinTexture.Height), ImageDrawMode.Stretch, Anchor.Auto, new Vector2(0, 25));

            leftPanel.AddChild(heart);
            leftPanel.AddChild(coin);

            rightPanel.AddChild(healthBar);

            UserInterface.Active.AddEntity(statsPanel);

            UIEntities.Add(statsPanel);
        }

        public override void Update(GameTime gameTime)
        {
            SetHealth(Global.Player.HEALTH);

            base.Update(gameTime);
        }

        public void ClearUI()
        {
            foreach (Entity entity in UIEntities)
            {
                UserInterface.Active.RemoveEntity(entity);
            }
        }

        public void SetHealth(int value)
        {
            healthBar.Value = value;
        }

        public void ShowDialogScreen(Texture2D avatar, string message)
        {
            Panel panel = new Panel(new Vector2(400, 500), PanelSkin.Simple);

            Image img = new Image(avatar, new Vector2(240, 190), ImageDrawMode.Stretch, Anchor.TopCenter);

            Label text = new Label(message);
            text.AddHyphenWhenBreakWord = true;
            text.BreakWordsIfMust = true;
            text.FillColor = Color.White;
            text.Scale = 1.5f;

            Button btnExit = new Button("Ok", ButtonSkin.Fancy, Anchor.BottomCenter);
            btnExit.OnClick += (Entity entity) =>
            {
                UserInterface.Active.RemoveEntity(panel);
            };

            panel.AddChild(img);
            panel.AddChild(text);
            panel.AddChild(btnExit);

            UserInterface.Active.AddEntity(panel);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            UserInterface.Active.Draw(spriteBatch);
            UserInterface.Active.DrawMainRenderTarget(spriteBatch);

            int windowWidth = GraphicsDevice.Viewport.Width;
            int windowHeight = GraphicsDevice.Viewport.Height;

            // AMMO not functional in game yet. Only graphics.

            spriteBatch.Begin();
            spriteBatch.DrawString(digitalFont, Global.Player.COINS.ToString(), new Vector2(108, 104), Color.Yellow);
            if (Global.Player.WEAPON is BeginnerGun) { 
            spriteBatch.Draw(gunTexture, new Vector2((windowWidth - gunTexture.Width) - 16, (windowHeight - gunTexture.Height) - 16), Color.White);
            }
            if (Global.Player.WEAPON is MediumLevelGun)
            {
                spriteBatch.Draw(medTexture, new Vector2((windowWidth - gunTexture.Width) - 16, (windowHeight - gunTexture.Height) - 16), Color.White);
            }
            if (Global.Player.WEAPON is HighLevelGun)
            {
                spriteBatch.Draw(highTexture, new Vector2((windowWidth - gunTexture.Width) - 16, (windowHeight - gunTexture.Height) - 16), Color.White);
            }
            if (Global.Player.WEAPON is GodlikeGun)
            {
                spriteBatch.Draw(godlikeTexure, new Vector2((windowWidth - gunTexture.Width) - 16, (windowHeight - gunTexture.Height) - 16), Color.White);
            }
            Vector2 ammoMeasureString = ammoFont.MeasureString(Global.Player.AMMO.ToString());
            Vector2 magazineMeasureString = ammoFont.MeasureString(Global.Player.AMMO_IN_MAGAZINE.ToString());

            spriteBatch.DrawString(ammoFont, Global.Player.AMMO.ToString(), 
                new Vector2(
                    (windowWidth - gunTexture.Width) - ammoMeasureString.X + 20,
                    (windowHeight - gunTexture.Height) - 16 + 40),
                Color.White);

            spriteBatch.DrawString(ammoFont, Global.Player.AMMO_IN_MAGAZINE.ToString(),
                new Vector2(
                    ((windowWidth - gunTexture.Width) - ammoMeasureString.X - 5) - magazineMeasureString.X + 10,
                    ((windowHeight - gunTexture.Height) - 16 + 30) - 22),
                Color.White);

            spriteBatch.Draw(ammoSeperatorTexture, new Vector2(
                    (windowWidth - gunTexture.Width) - ammoMeasureString.X - 5,
                    (windowHeight - gunTexture.Height) - 16 + 30),
                    Color.DarkRed);

            spriteBatch.End();
        }
    }
}
