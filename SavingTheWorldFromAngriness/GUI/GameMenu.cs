using System;
using GeonBit.UI;
using GeonBit.UI.Entities;
using SavingTheWorldFromAngriness.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using SavingTheWorldFromAngriness.Weapons;
using Microsoft.Xna.Framework.Audio;
using SavingTheWorldFromAngriness.Global.Sounds;

namespace SavingTheWorldFromAngriness.GUI
{
    public class GameMenu : DrawableGameComponent
    {
        #region Panel
        private Panel interfaceGameMenu;
        #endregion

        #region Esc bools
        private bool isEscapeKeyDown;
        private bool isGameMenuOpen;
        #endregion

        #region WorldRenderer

        private WorldRenderer worldRenderer;
        #endregion

        #region Sounds
        private SoundEffect openMenuSound;
        private SoundEffect closeMenuSound;
        private SoundEffect buttonClickSound;
        private SoundEffect hoverButtonSound;
        #endregion

        /*Constructor*/
        public GameMenu(Game game, WorldRenderer worldRenderer) : base(game)
        {
            #region Esc bools

            isEscapeKeyDown = false;
            isGameMenuOpen = false;
            interfaceGameMenu = null;
            #endregion

            #region WorldRenderer

            this.worldRenderer = worldRenderer;
            #endregion
        }

        public override void Update(GameTime gameTime)
        {
            #region If Esc is pressed
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                isEscapeKeyDown = true;
            }
            else
            {
                // Check if escape key has been pressed and then released
                // (in order to only call functions once)
                if (isEscapeKeyDown)
                {
                    if (isGameMenuOpen)
                    {
                        isGameMenuOpen = false;
                        Global.GameState.IS_GAME_PAUSED = false;
                        CloseStartMenu();
                    }
                    else
                    {
                        isGameMenuOpen = true;
                        Global.GameState.IS_GAME_PAUSED = true;
                        OpenStartMenu();
                    }
                }

                isEscapeKeyDown = false;
            }
            #endregion

            base.Update(gameTime);
        }

        /*
        * This function shows the game menu when the key "Esc" is pushed. 
        */
        public void OpenStartMenu()
        {
            #region Panel; Game Menu

            SoundManager.PlaySoundEffect(openMenuSound);
            Panel panel = new Panel(size: new Vector2(500, 500), skin: PanelSkin.Default);
            Header header = new Header("Game Menu");
            panel.AddChild(header);

            interfaceGameMenu = panel;
            UserInterface.Active.AddEntity(interfaceGameMenu);

            #endregion

            #region Resume Button
            /*Button that starts the game?*/
            var btnPlay = new Button("Resume", ButtonSkin.Default);
            PlaySounds(btnPlay);
            btnPlay.OnClick += (Entity entity) =>
            {
                CloseStartMenu();
                Global.GameState.IS_GAME_PAUSED = false;
            };
            
            #endregion

            #region Save Button

            var btnSave = new Button("Save/Load Game", ButtonSkin.Default);
            PlaySounds(btnSave);
            btnSave.OnClick += (Entity entity) =>
            {
                string directory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Saves");
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                Panel saveBiggerPanel = new Panel(new Vector2(600, 650));
                UserInterface.Active.AddEntity(saveBiggerPanel);
                Panel savePanel = new Panel(new Vector2(600, 620));
                savePanel.Skin = PanelSkin.None;
                saveBiggerPanel.AddChild(savePanel);
                savePanel.AddChild(new Header("Save/Load game"));
                savePanel.AddChild(new LineSpace());

                Button newSaveButton = new Button("Press this button to save!");
                PlaySounds(newSaveButton);

                if (Directory.GetFiles(directory).Length >= 5)
                {
                    newSaveButton.Disabled = true;
                    newSaveButton.ButtonParagraph.Text = "Max amount of saves reached";
                }

                newSaveButton.FillColor = Color.SpringGreen;
                savePanel.AddChild(newSaveButton);

                newSaveButton.OnClick += (Entity entity2) =>
                {
                    SaveGame saveGame = new SaveGame(Global.Player.PLAYER_NAME, Global.Player.HEALTH, Global.Player.AMMO, Global.Player.AMMO_IN_MAGAZINE,
                        Global.Player.CURRENT_WORLD, Global.Player.CURRENT_MAP, Global.Player.COINS, Global.Player.INVENTORY, Global.Player.WEAPON);
                    saveGame.Save();
                    newSaveButton.ButtonParagraph.Text = "GAME SAVED!";
                    newSaveButton.Disabled = true;
                };


                foreach (String file in Directory.GetFiles(directory, "*.save*", SearchOption.AllDirectories).OrderByDescending(d => new FileInfo(d).LastWriteTime.TimeOfDay))
                {
                    Button button = new Button(Path.GetFileNameWithoutExtension(file));
                    PlaySounds(button);
                    savePanel.AddChild(button);

                    button.OnClick += (Entity entity2) =>
                    {
                        Panel buttonPanel = new Panel(size: new Vector2(500, 350), skin: PanelSkin.Golden, anchor: Anchor.Center, offset: null);
                        UserInterface.Active.AddEntity(buttonPanel);
                        Button loadButton = new Button("Load game");
                        Button overwriteButton = new Button("Overwrite game");
                        Button deleteButton = new Button("Delete games");
                        Button cancelButton = new Button("Cancel", ButtonSkin.Default, Anchor.BottomCenter, new Vector2(200, 55));

                        buttonPanel.AddChild(loadButton);
                        buttonPanel.AddChild(deleteButton);
                        buttonPanel.AddChild(overwriteButton);
                        buttonPanel.AddChild(cancelButton);

                        PlaySounds(loadButton);
                        PlaySounds(deleteButton);
                        PlaySounds(overwriteButton);
                        PlaySounds(cancelButton);

                        overwriteButton.OnClick += (Entity entity8) =>
                        {
                            string[] lines = { SaveGame.Base64Encode(Global.Player.PLAYER_NAME), SaveGame.Base64Encode(Global.Player.HEALTH.ToString()),SaveGame.Base64Encode(Global.Player.AMMO.ToString()),
                                SaveGame.Base64Encode(Global.Player.AMMO_IN_MAGAZINE.ToString()),
                                SaveGame.Base64Encode(Global.Player.CURRENT_WORLD), SaveGame.Base64Encode(Global.Player.CURRENT_MAP), SaveGame.Base64Encode(Global.Player.COINS.ToString()),
                                SaveGame.Base64Encode(string.Join(",", Global.Player.INVENTORY)), SaveGame.ObjectToString(Global.Player.WEAPON) };

                            string overWriteFileName = Path.Combine(directory, Path.GetFileName(file));
                            File.WriteAllLines(overWriteFileName, lines);
                            //File.Move(overWriteFileName, "SAVE" + $@"{DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss")}.save");

                            button.ButtonParagraph.Text = Path.GetFileNameWithoutExtension($@"{DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss")}.save");

                            UserInterface.Active.RemoveEntity(buttonPanel);
                        };

                        cancelButton.OnClick += (Entity entity6) =>
                        {
                            UserInterface.Active.RemoveEntity(buttonPanel);
                        };

                        loadButton.OnClick += (Entity entity3) =>
                        {
                            string[] lines = File.ReadAllLines(file);
                            Global.Player.PLAYER_NAME = SaveGame.Base64Decode(lines[0]);
                            Global.Player.HEALTH = int.Parse(SaveGame.Base64Decode(lines[1]));
                            Global.Player.AMMO = int.Parse(SaveGame.Base64Decode(lines[2]));
                            Global.Player.AMMO_IN_MAGAZINE = int.Parse(SaveGame.Base64Decode(lines[3]));
                            Global.Player.CURRENT_WORLD = SaveGame.Base64Decode(lines[4]);
                            Global.Player.CURRENT_MAP = SaveGame.Base64Decode(lines[5]);
                            Global.Player.COINS = int.Parse(SaveGame.Base64Decode(lines[6]));

                            if (lines[7].Length != 0)
                            {
                                Global.Player.INVENTORY = SaveGame.Base64Decode(lines[7]).Split(',').ToList();
                            }
                            else
                            {
                                Global.Player.INVENTORY.Clear();
                            }

                            byte[] bytes = Convert.FromBase64String(lines[8]);
                            using (MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length))
                            {
                                ms.Write(bytes, 0, bytes.Length);
                                ms.Position = 0;
                                Global.Player.WEAPON = (Weapon)new BinaryFormatter().Deserialize(ms);
                            }

                            UserInterface.Active.RemoveEntity(buttonPanel);
                            UserInterface.Active.RemoveEntity(saveBiggerPanel);
                            worldRenderer.LoadMap(Global.Player.CURRENT_WORLD, Global.Player.CURRENT_MAP);
                        };

                        deleteButton.OnClick += (Entity entity4) =>
                        {
                            File.Delete(file);
                            UserInterface.Active.RemoveEntity(buttonPanel);
                            savePanel.RemoveChild(button);
                        };
                    };
                }
                Button btn = new Button("OK", ButtonSkin.Default, Anchor.BottomCenter, new Vector2(150, 55), new Vector2(-152, 0));
                saveBiggerPanel.AddChild(btn);
                PlaySounds(btn);

                btn.OnClick += (Entity entity6) =>
                {
                    UserInterface.Active.RemoveEntity(saveBiggerPanel);
                };
            };
            #endregion

            #region Option Button

            var btnOptions = new Button("Options", ButtonSkin.Default);
            PlaySounds(btnOptions);
            btnOptions.OnClick += (Entity entity) =>
            {
                Button okBtn = Item.OptionsPanel();
                PlaySounds(okBtn);
            };
            #endregion

            #region Restart level Button

            var btnRestartLevel = new Button("Restart Level", ButtonSkin.Default);
            PlaySounds(btnRestartLevel);

            btnRestartLevel.OnClick += (Entity entity) =>
            {
                worldRenderer.LoadMap(Global.Player.CURRENT_WORLD, Global.Player.CURRENT_MAP);
                Global.Player.HEALTH = 100;
                Global.Player.COINS = 0;
                Global.Player.AMMO = 0;
                Global.Player.AMMO_IN_MAGAZINE = 0;
            };
            #endregion

            #region Exit Button

            /*Button that exits the game*/
            var btnExit = new Button("Exit to Main Menu", ButtonSkin.Default);
            PlaySounds(btnExit);

            btnExit.OnClick += (Entity entity) =>
            {
                CloseStartMenu();
                worldRenderer.ClearMap();

                MainMenu startMenu = new MainMenu(this.Game, worldRenderer);
                Game.Components.Add(startMenu);

                Global.Player.LAST_MAP = null;

                Global.GameState.IS_GAME_PAUSED = false;
            };
            #endregion

            #region Adding buttons to panel

            /*Adding buttons to the panel/menu */
            panel.AddChild(btnPlay);
            panel.AddChild(btnSave);
            panel.AddChild(btnOptions);
            panel.AddChild(btnRestartLevel);
            panel.AddChild(btnExit);

            #endregion

        }

        private void CloseStartMenu()
        {
            if (interfaceGameMenu != null)
            {
                SoundManager.PlaySoundEffect(closeMenuSound);
                UserInterface.Active.RemoveEntity(interfaceGameMenu);

                interfaceGameMenu = null;
            }
        }
        protected override void LoadContent()
        {
            #region Sounds
            buttonClickSound = Game.Content.Load<SoundEffect>("Sounds/Misc/buttonClickSound");
            closeMenuSound = Game.Content.Load<SoundEffect>("Sounds/Misc/closeMenu");
            openMenuSound = Game.Content.Load<SoundEffect>("Sounds/Misc/openMenu");
            hoverButtonSound = Game.Content.Load<SoundEffect>("Sounds/Misc/buttonHoverSound");
            #endregion

            base.LoadContent();
        }

        private void PlaySounds(Button btn)
        {
            #region OnMouseEnter

            btn.OnMouseEnter += (Entity entity) =>
            {
                SoundManager.PlaySoundEffect(hoverButtonSound);
            };
            #endregion

            #region OnClick

            btn.OnClick += (Entity entity) =>
            {
                SoundManager.PlaySoundEffect(buttonClickSound);
            };
            #endregion
        }
    }
}
