﻿using GeonBit.UI;
using GeonBit.UI.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using SavingTheWorldFromAngriness.Global.Sounds;
using SavingTheWorldFromAngriness.Weapons;
using SavingTheWorldFromAngriness.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SavingTheWorldFromAngriness.GUI
{
    public class VendorMenu : DrawableGameComponent
    {
        private Panel vendorMenu;
        private WorldRenderer worldRenderer;
        private int mediumGunCost;
        private int highGunCost;
        private int godlikeGunCost;
        private SoundEffect openVendorSound;
        private SoundEffect closeVendorSound;
        private SoundEffect buyVendorSound;
        private int ammoCost;

        public VendorMenu(Game game, WorldRenderer worldRenderer) : base(game) {
            this.worldRenderer = worldRenderer;
            this.vendorMenu = null;

            mediumGunCost = 20;
            highGunCost = 500;
            godlikeGunCost = 2500;
            ammoCost = 250;
        }

        public override void Initialize()
        {
            //Pause the game when interacting with vendor
            Global.GameState.IS_GAME_PAUSED = true;
            byte activeOpacity = 140;
            byte inActiveOpacity = 0;
            openVendorSound = Game.Content.Load<SoundEffect>("Sounds/Vendor/Open");
            closeVendorSound = Game.Content.Load<SoundEffect>("Sounds/Vendor/Close");
            buyVendorSound = Game.Content.Load<SoundEffect>("Sounds/Vendor/Buy");

            SoundManager.PlaySoundEffect(openVendorSound);
            Panel panel = new Panel(new Vector2(900, 690));
            vendorMenu = panel;
            UserInterface.Active.AddEntity(vendorMenu);

            panel.AddChild(new Header("Vendor"));
            panel.AddChild(new LineSpace());
            panel.AddChild(new LineSpace());
            panel.AddChild(new HorizontalLine());
            Panel medPanel = new Panel(new Vector2(840, 100), PanelSkin.Simple, Anchor.AutoInline);
            Panel highPanel = new Panel(new Vector2(840, 100), PanelSkin.Simple, Anchor.AutoInline);
            Panel godPanel = new Panel(new Vector2(840, 100), PanelSkin.Simple, Anchor.AutoInline);
            Panel ammoPanel = new Panel(new Vector2(840, 100), PanelSkin.Simple, Anchor.AutoInline);

            Header medHeader = new Header("Medium tier gun - " + mediumGunCost.ToString() + " coins");
            medPanel.AddChild(medHeader);
            Icon medIcon = new Icon(IconType.None, Anchor.TopRight, 1, true, new Vector2(35, 2));
            medIcon.Texture = Game.Content.Load<Texture2D>("Textures/mediumGun");
            medPanel.AddChild(medIcon);
            medPanel.AddChild(new LineSpace());
            medPanel.AddChild(new LineSpace());
            medPanel.FillColor = Color.Black;
            medPanel.Opacity = inActiveOpacity;

            medPanel.OnMouseEnter += (Entity entity) =>
            {
                medPanel.Opacity = activeOpacity;
                highPanel.Opacity = inActiveOpacity;
                godPanel.Opacity = inActiveOpacity;
            };

            medPanel.OnMouseLeave += (Entity entity) =>
            {
                medPanel.Opacity = inActiveOpacity;
            };

            medPanel.OnClick += (Entity entity) =>
            {
                BuyMediumGun();
            };

            medIcon.OnClick += (Entity entity) =>
            {
                BuyMediumGun();
            };

            medHeader.OnClick += (Entity entity) =>
            {
                BuyMediumGun();
            };
            panel.AddChild(medPanel);
            panel.AddChild(new HorizontalLine());
            Header highHeader = new Header("High tier gun - " + highGunCost.ToString() + " coins");
            highPanel.AddChild(highHeader);
            Icon highIcon = new Icon(IconType.None, Anchor.TopRight, 1, true, new Vector2(35, 2));
            highIcon.Texture = Game.Content.Load<Texture2D>("Textures/highGun");
            highPanel.AddChild(highIcon);
            highPanel.AddChild(new LineSpace());
            highPanel.AddChild(new LineSpace());
            highPanel.FillColor = Color.Black;
            highPanel.Opacity = inActiveOpacity;

            highPanel.OnMouseEnter += (Entity entity) =>
            {
                highPanel.Opacity = activeOpacity;
                godPanel.Opacity = inActiveOpacity;
                medPanel.Opacity = inActiveOpacity;
                
            };

            highPanel.OnMouseLeave += (Entity entity) =>
            {
                highPanel.Opacity = inActiveOpacity;
            };

            highPanel.OnClick += (Entity entity) =>
            {
                BuyHighGun();
            };

            highIcon.OnClick += (Entity entity) =>
            {
                BuyHighGun();
            };

            highHeader.OnClick += (Entity entity) =>
            {
                BuyHighGun();
            };
            panel.AddChild(highPanel);
            panel.AddChild(new HorizontalLine());
            Header godHeader = new Header("Godlike tier gun - " + godlikeGunCost.ToString() + " coins");
            godPanel.AddChild(godHeader);
            Icon godlikeIcon = new Icon(IconType.None, Anchor.TopRight, 1, true, new Vector2(35, 2));
            godlikeIcon.Texture = Game.Content.Load<Texture2D>("Textures/godlikeGun");
            godPanel.AddChild(godlikeIcon);
            godPanel.AddChild(new LineSpace());
            godPanel.AddChild(new LineSpace());
            godPanel.FillColor = Color.Black;
            godPanel.Opacity = inActiveOpacity;

            godPanel.OnMouseEnter += (Entity entity) =>
            {
                godPanel.Opacity = activeOpacity;
                medPanel.Opacity = inActiveOpacity;
                highPanel.Opacity = inActiveOpacity;
            };

            godPanel.OnMouseLeave += (Entity entity) =>
            {
                godPanel.Opacity = inActiveOpacity;
            };

            godPanel.OnClick += (Entity entity) =>
            {
                BuyGodGun();
            };

            godlikeIcon.OnClick += (Entity entity) =>
            {
                BuyGodGun();
            };

            godHeader.OnClick += (Entity entity) =>
            {
                BuyGodGun();
            };
            panel.AddChild(godPanel);
            panel.AddChild(new HorizontalLine());
            Header ammoHeader = new Header("Paintball ammo - " + ammoCost.ToString() + " coins");
            ammoPanel.AddChild(ammoHeader);
            Icon ammoIcon = new Icon(IconType.None, Anchor.TopRight, 1, true, new Vector2(35, 2));
            ammoIcon.Texture = Game.Content.Load<Texture2D>("Textures/AmmoPack");
            ammoPanel.AddChild(ammoIcon);
            ammoPanel.AddChild(new LineSpace());
            ammoPanel.AddChild(new LineSpace());
            ammoPanel.FillColor = Color.Black;
            ammoPanel.Opacity = inActiveOpacity;

            ammoPanel.OnMouseEnter += (Entity entity) =>
            {
                ammoPanel.Opacity = activeOpacity;
                medPanel.Opacity = inActiveOpacity;
                highPanel.Opacity = inActiveOpacity;
                godPanel.Opacity = inActiveOpacity;
            };

            ammoPanel.OnMouseLeave += (Entity entity) =>
            {
                ammoPanel.Opacity = inActiveOpacity;
            };

            ammoPanel.OnClick += (Entity entity) =>
            {
                buyAmmo();
            };

            ammoIcon.OnClick += (Entity entity) =>
            {
                buyAmmo();
            };

            ammoHeader.OnClick += (Entity entity) =>
            {
                buyAmmo();
            };
            panel.AddChild(ammoPanel);
            /*Button to exit the menu*/
            var btnExit = new Button("OK", ButtonSkin.Default, anchor: Anchor.BottomCenter);
                btnExit.OnClick += (Entity entity) =>
                {
                    closeMenu();
                    Global.GameState.IS_GAME_PAUSED = false;
                };
                panel.AddChild(btnExit);
            base.Initialize();
            }

        private void BuyGodGun()
        {
            if (Global.Player.COINS >= godlikeGunCost && Global.Player.WEAPON.GetType() == typeof(HighLevelGun))
            {
                Global.Player.WEAPON = new GodlikeGun();
                Global.Player.COINS -= godlikeGunCost;
                SoundManager.PlaySoundEffect(buyVendorSound);
            }

            else if (Global.Player.WEAPON.GetType() == typeof(GodlikeGun))
            {
                AlreadyOwnedMessage();
            }
            else
            {
                NotEnoughMoneyMessage();
            }
        }

        private void BuyHighGun()
        {
            if (Global.Player.COINS >= highGunCost && Global.Player.WEAPON.GetType() == typeof(MediumLevelGun))
            {
                Global.Player.WEAPON = new HighLevelGun();
                Global.Player.COINS -= highGunCost;
                SoundManager.PlaySoundEffect(buyVendorSound);
            }
            else if (Global.Player.WEAPON.GetType() == typeof(GodlikeGun))
            {
                BetterWeaponMessage();
            }
            else if (Global.Player.WEAPON.GetType() == typeof(HighLevelGun))
            {
                AlreadyOwnedMessage();
            }
            else
            {
                NotEnoughMoneyMessage();
            }
        }

        private void BuyMediumGun()
        {
            if (Global.Player.COINS >= mediumGunCost && Global.Player.WEAPON.GetType() == typeof(BeginnerGun))
            {
                Global.Player.WEAPON = new MediumLevelGun();
                Global.Player.COINS -= mediumGunCost;
                SoundManager.PlaySoundEffect(buyVendorSound);
            }
            else if (Global.Player.WEAPON.GetType() == typeof(HighLevelGun) || Global.Player.WEAPON.GetType() == typeof(GodlikeGun))
            {
                BetterWeaponMessage();
            }
            else if (Global.Player.WEAPON.GetType() == typeof(MediumLevelGun))
            {
                AlreadyOwnedMessage();
            }
            else
            {
                NotEnoughMoneyMessage();
            }
        }

        private void buyAmmo()
        {
            if (Global.Player.COINS >= ammoCost)
            {
                Global.Player.AMMO += 20;
                Global.Player.COINS -= ammoCost;
                SoundManager.PlaySoundEffect(buyVendorSound);
            }
            else
            {
                Panel warningPanel = new Panel(new Vector2(430, 240));
                warningPanel.AddChild(new Paragraph("Insufficient funds!"));
                UserInterface.Active.AddEntity(warningPanel);
                var btnExit1 = new Button("OK", ButtonSkin.Default, Anchor.BottomCenter);
                warningPanel.AddChild(btnExit1);
                btnExit1.OnClick += (Entity entity1) =>
                {
                    UserInterface.Active.RemoveEntity(warningPanel);
                };
            }
        }

        private void closeMenu()
        {
            if (vendorMenu != null)
            {
                UserInterface.Active.RemoveEntity(vendorMenu);
                SoundManager.PlaySoundEffect(closeVendorSound);

                vendorMenu = null;
            }
        }

        private void NotEnoughMoneyMessage()
        {
            Panel warningPanel = new Panel(new Vector2(430, 240));
            warningPanel.AddChild(new Paragraph("Insufficient funds, or you have not bought the cheaper paintballguns yet!"));
            UserInterface.Active.AddEntity(warningPanel);
            var btnExit1 = new Button("OK", ButtonSkin.Default, Anchor.BottomCenter);
            warningPanel.AddChild(btnExit1);
            btnExit1.OnClick += (Entity entity1) =>
            {
                UserInterface.Active.RemoveEntity(warningPanel);
            };
        }
        
        private void BetterWeaponMessage()
        {
            Panel warningPanel = new Panel(new Vector2(310, 200));
            warningPanel.AddChild(new Paragraph("You have no use for that now!"));
            UserInterface.Active.AddEntity(warningPanel);
            var btnExit1 = new Button("OK", ButtonSkin.Default, Anchor.BottomCenter);
            warningPanel.AddChild(btnExit1);
            btnExit1.OnClick += (Entity entity1) =>
            {
                UserInterface.Active.RemoveEntity(warningPanel);
            };
        }

        private void AlreadyOwnedMessage()
        {
            Panel warningPanel = new Panel(new Vector2(310, 200));
            warningPanel.AddChild(new Paragraph("You already own that weapon!"));
            UserInterface.Active.AddEntity(warningPanel);
            var btnExit1 = new Button("OK", ButtonSkin.Default, Anchor.BottomCenter);
            warningPanel.AddChild(btnExit1);
            btnExit1.OnClick += (Entity entity1) =>
            {
                UserInterface.Active.RemoveEntity(warningPanel);
            };
        }


    }
}

