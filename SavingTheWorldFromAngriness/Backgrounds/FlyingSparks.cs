﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.Backgrounds
{
    class FlyingSparks : BasicBackground
    {
        private float spawnWidth;
        private float density;
        private int speed;
        private List<RainDrop> raindrops;
        private float timer;
        private Random rand1, rand2;
        private Texture2D texture;

        public FlyingSparks(Game game) : base(game)
        {
            raindrops = new List<RainDrop>();

            this.spawnWidth = this.GraphicsDevice.Viewport.Width + 200;
            this.density = 800;
            this.speed = 35;

            rand1 = new Random();
            rand2 = new Random();
        }

        protected override void LoadContent()
        {
            texture = Game.Content.Load<Texture2D>("Textures/Backgrounds/spark");

            base.LoadContent();
        }

        public void createParticle()
        {
            double anything = rand1.Next();

            raindrops.Add(new RainDrop(texture, new Vector2(
            -50 + (float)rand1.NextDouble() * spawnWidth, GraphicsDevice.Viewport.Height),
            new Vector2(1, rand2.Next(speed, speed + 5) * -1)));
        }

        public override void Update(GameTime gameTime)
        {
            timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

            while (timer > 0)
            {
                timer -= 1f / density;
                createParticle();
            }

            for (int i = 0; i < raindrops.Count; i++)
            {
                raindrops[i].Update();

                if (raindrops[i].position.Y < 0)
                {
                    raindrops.RemoveAt(i);
                    i--;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            foreach (RainDrop raindrop in raindrops)
            {
                raindrop.Draw(spriteBatch);
            }
        }
    }
}
