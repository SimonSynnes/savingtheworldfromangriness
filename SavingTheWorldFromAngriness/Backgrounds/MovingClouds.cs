﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.Backgrounds
{
    class MovingClouds : BasicBackground
    {
        private List<Background> backgrounds;
        private double elapsedTime;
        private int moveSpeed;
        private int moveAmount;
        private Vector2 moveDirection;

        public MovingClouds(Game game, int moveSpeed, int moveAmount)
            : base(game)
        {
            this.backgrounds = new List<Background>();
            this.elapsedTime = 0;
            this.moveSpeed = moveSpeed;
            this.moveAmount = moveAmount;
            this.moveDirection = new Vector2(1, 1);
        }

        public MovingClouds(Game game, int moveSpeed, int moveAmount, Vector2 direction) 
            : base(game)
        {
            this.backgrounds = new List<Background>();
            this.elapsedTime = 0;
            this.moveSpeed = moveSpeed;
            this.moveAmount = moveAmount;
            this.moveDirection = direction;
        }

        protected override void LoadContent()
        {
            backgrounds = new List<Background>();
            backgrounds.Add(new Background(Game.Content.Load<Texture2D>("Textures/Backgrounds/Clouds1"), new Vector2(300, 300), 0.6f));
            backgrounds.Add(new Background(Game.Content.Load<Texture2D>("Textures/Backgrounds/Clouds2"), new Vector2(500, 500), 0.8f));
            backgrounds.Add(new Background(Game.Content.Load<Texture2D>("Textures/Backgrounds/Clouds3"), new Vector2(700, 700), 1.1f));

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            Vector2 direction = Vector2.Zero;
            elapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
            if (elapsedTime >= moveSpeed)
            {
                direction = new Vector2(moveAmount, moveAmount) * moveDirection;

                elapsedTime = 0;
            }

            foreach (Background bg in backgrounds)
            {
                bg.Update(gameTime, direction, GraphicsDevice.Viewport);
            }

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            foreach (Background bg in backgrounds)
            {
                bg.Draw(spriteBatch);
            }

            base.Draw(spriteBatch);
        }
    }
}
