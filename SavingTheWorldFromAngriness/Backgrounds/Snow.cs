﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.Backgrounds
{
    public class SnowDrop
    {
        private Texture2D texture;
        private Vector2 velocity;
        public Vector2 position;

        public SnowDrop(Texture2D texture, Vector2 position, Vector2 velocity)
        {
            this.texture = texture;
            this.position = position;
            this.velocity = velocity;
        }

        public void Update()
        {
            position += velocity;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
        }
    }

    class Snow : BasicBackground
    {
        private float spawnWidth;
        private float density;
        private int speed;
        private List<SnowDrop> raindrops;
        private float timer;
        private Random rand1, rand2;
        private Texture2D texture;

        public Snow(Game game) : base(game)
        {
            raindrops = new List<SnowDrop>();

            this.spawnWidth = this.GraphicsDevice.Viewport.Width ;
            this.density = 35;
            this.speed = 2;

            rand1 = new Random();
            rand2 = new Random();
        }

        protected override void LoadContent()
        {
            texture = Game.Content.Load<Texture2D>("Textures/Backgrounds/snow");

            base.LoadContent();
        }

        public void createParticle()
        {
            double anything = rand1.Next();

            raindrops.Add(new SnowDrop(texture, new Vector2(
            -50 + (float)rand1.NextDouble() * spawnWidth, 0),
            new Vector2(1, rand2.Next(speed, speed + 5))));
        }

        public override void Update(GameTime gameTime)
        {
            timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

            while (timer > 0)
            {
                timer -= 1f / density;
                createParticle();
            }

            for (int i = 0; i < raindrops.Count; i++)
            {
                raindrops[i].Update();

                if (raindrops[i].position.Y > GraphicsDevice.Viewport.Height)
                {
                    raindrops.RemoveAt(i);
                    i--;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            foreach (SnowDrop raindrop in raindrops)
            {
                raindrop.Draw(spriteBatch);
            }
        }
    }
}
