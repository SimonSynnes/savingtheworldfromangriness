﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.Backgrounds
{
    public class BasicBackground : DrawableGameComponent
    {
        public BasicBackground(Game game) 
            : base(game)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {

        }
    }
}
