﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SavingTheWorldFromAngriness.Backgrounds
{
    public class Background
    {
        private Texture2D texture;
        private Vector2 offset;
        public Vector2 speed;
        public float zoom;

        private Viewport viewport;
        
        private Rectangle rectangle
        {
            get { return new Rectangle((int)(offset.X), (int)(offset.Y), (int)(viewport.Width / zoom), (int)(viewport.Height / zoom)); }
        }

        public Background(Texture2D texture, Vector2 speed, float zoom)
        {
            this.texture = texture;
            this.offset = Vector2.Zero;
            this.speed = speed;
            this.zoom = zoom;
        }

        public void Update(GameTime gametime, Vector2 direction, Viewport viewport)
        {
            if (Global.GameState.IS_GAME_PAUSED)
            {
                return;
            }

            float elapsed = (float)gametime.ElapsedGameTime.TotalSeconds;

            this.viewport = viewport;

            Vector2 distance = direction * speed * elapsed;

            offset += distance;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, new Vector2(viewport.X, viewport.Y), rectangle, Color.White, 0, Vector2.Zero, zoom, SpriteEffects.None, 1);
        }
    }
}
