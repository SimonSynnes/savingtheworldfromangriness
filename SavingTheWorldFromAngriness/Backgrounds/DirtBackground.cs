﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SavingTheWorldFromAngriness.Backgrounds
{
    class DirtBackground : BasicBackground
    {
        private List<Background> backgrounds;

        public DirtBackground(Game game)
            : base(game)
        {
            this.backgrounds = new List<Background>();
        }

        protected override void LoadContent()
        {
            backgrounds = new List<Background>();
            backgrounds.Add(new Background(Game.Content.Load<Texture2D>("Textures/Backgrounds/dirt"), new Vector2(0, 0), 1f));
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            Vector2 direction = Vector2.Zero;

            foreach (Background bg in backgrounds)
            {
                bg.Update(gameTime, direction, GraphicsDevice.Viewport);
            }

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            foreach (Background bg in backgrounds)
            {
                bg.Draw(spriteBatch);
            }

            base.Draw(spriteBatch);
        }
    }
}
