﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using System.Collections.Generic;
using GeonBit.UI.Entities;
using GeonBit.UI.Entities.TextValidators;
using GeonBit.UI.DataTypes;

using SavingTheWorldFromAngriness.GUI;
using SavingTheWorldFromAngriness.World;
using SavingTheWorldFromAngriness.Debug;

using System.Net;
using GeonBit.UI;
using System.Threading;

namespace SavingTheWorldFromAngriness
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        public static bool DEBUG_MODE = false;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        WorldRenderer worldRenderer;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this)
            {
                PreferredDepthStencilFormat = DepthFormat.Depth24Stencil8
            };

            Content.RootDirectory = "Content";

            graphics.GraphicsProfile = GraphicsProfile.HiDef;
            graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
			graphics.IsFullScreen = true;
            graphics.HardwareModeSwitch = false;

            graphics.ApplyChanges();
        }

        protected override void Initialize()
        {
            UserInterface.Initialize(Content, BuiltinThemes.hd);

            // Add world renderer
            worldRenderer = new WorldRenderer(this);
            Components.Add(worldRenderer);
            if (!DEBUG_MODE)
            {
                IntroScreen intro = new IntroScreen(this);
                Components.Add(intro);
            }
            else
            {
                OpenStartMenu();
            }

            if (DEBUG_MODE)
            {
                FrameRateCounter frameRateCounter = new FrameRateCounter(this);
                Components.Add(frameRateCounter);

                DebugString debugString = new DebugString(this);
                Components.Add(debugString);
            }

            base.Initialize();
        }

        public void OpenStartMenu()
        {
            MainMenu startMenu = new MainMenu(this, worldRenderer);
            Components.Add(startMenu);
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);


            // TODO: use this.Content to load your game content here
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            // TODO: Add your update logic here
            UserInterface.Active.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

            worldRenderer.Draw();

            base.Draw(gameTime);
        }
    }
}