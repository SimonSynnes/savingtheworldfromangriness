﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace CrossPlatformProjectGenerator
{
    class GenerateProject
    {
        static string PATH_MAIN_CSPROJ;
        static string PATH_MAIN_CONTENT;

        static string PATH_TEMPLATE_CSPROJ;
        static string PATH_TEMPLATE_CONTENT;

        static string PATH_MAIN_DIRECTORY;

        static void Main(string[] args)
        {
            // Assume we are working from the CrossPlatform's root directory.

            string configText = File.ReadAllText("GenerateProject.config");
            string[] config = configText.Split('\n');

            PATH_MAIN_DIRECTORY = config[0].Trim();
            PATH_MAIN_CSPROJ = config[1].Trim();
            PATH_TEMPLATE_CSPROJ = config[2].Trim();
            PATH_MAIN_CONTENT = config[3].Trim();
            PATH_TEMPLATE_CONTENT = config[4].Trim();

            GenerateProjectFile();
            GenerateContentFile();
        }

        private static void GenerateContentFile()
        {
            if (File.Exists(@"Content\Content.mgcb"))
            {
                File.Delete(@"Content\Content.mgcb");
            }

            string contentOutput = File.ReadAllText(PATH_TEMPLATE_CONTENT);

            string content = "";

            string mainContentText = File.ReadAllText(PATH_MAIN_CONTENT);
            string[] linesInMainContent = mainContentText.Split('\n');

            bool beginReading = false;
            foreach (string line in linesInMainContent)
            {
                if (line.Contains("#---------------------------------- Content ---------------------------------#"))
                {
                    beginReading = true;
                    continue;
                }

                if (beginReading)
                {
                    if (line.Contains("Combine.fx"))
                    {
                        content += line + "\n";
                    }
                    else
                    if (line.Contains("Light.fx"))
                    {
                        content += line + "\n";
                    }
                    else
                    if (line.StartsWith("#begin"))
                    {
                        string path = line.Replace("#begin ", "").Trim();

                        content += "#begin " + "../" + PATH_MAIN_DIRECTORY.Replace(@"\", "/") + "Content/" + path + "\n";
                    }
                    else
                    if (line.StartsWith("/build:"))
                    {
                        string path = line.Replace("/build:", "").Trim();

                        content += "/build:" + "../" + PATH_MAIN_DIRECTORY.Replace(@"\", "/") + "Content/" + path + "\n";
                    }
                    else
                    if (line.StartsWith("/copy:"))
                    {
                        string path = line.Replace("/copy:", "").Trim();

                        content += "/copy:" + "../" + PATH_MAIN_DIRECTORY.Replace(@"\", "/") + "Content/" + path + "\n";
                    }
                    else
                    {
                        content += line + "\n";
                    }
                }
            }


            contentOutput = contentOutput.Replace("{CONTENTS}", content);

            File.WriteAllText(@"Content\Content.mgcb", contentOutput);
        }

        private static void GenerateProjectFile()
        {
            if (File.Exists("SavingTheWorldFromAngriness.csproj"))
            {
                File.Delete("SavingTheWorldFromAngriness.csproj");
            }

            string crossPlatformProjectOutput = File.ReadAllText(PATH_TEMPLATE_CSPROJ);

            string mainProjectText = File.ReadAllText(PATH_MAIN_CSPROJ);

            string compileIncludes = "";
            string embeddedResources = "";

            string[] linesInMainProject = mainProjectText.Split('\n');
            foreach (string line in linesInMainProject)
            {
                if (line.Trim().StartsWith("<Compile Include="))
                {
                    string includeLine = line.Split(new string[] { "<Compile Include=\"" }, StringSplitOptions.None)[1].Split
                        (new string[] { "\" />" }, StringSplitOptions.None)[0].Trim();

                    compileIncludes += String.Format("\n    <Compile Include=\"{0}\">\n      <Link>{1}</Link>\n    </Compile>", PATH_MAIN_DIRECTORY + includeLine, includeLine);
                }

                if (line.Trim().StartsWith("<EmbeddedResource Include="))
                {
                    string embeddedLine = line.Split(new string[] { "<EmbeddedResource Include=\"" }, StringSplitOptions.None)[1].Split
                        (new string[] { "\" />" }, StringSplitOptions.None)[0].Trim();

                    embeddedResources += String.Format("\n    <EmbeddedResource Include=\"{0}\">\n      <Link>{1}</Link>\n    </EmbeddedResource>", PATH_MAIN_DIRECTORY + embeddedLine, embeddedLine);
                }
            }

            crossPlatformProjectOutput = crossPlatformProjectOutput.Replace("{COMPILE_INCLUDE}", compileIncludes.Trim());
            crossPlatformProjectOutput = crossPlatformProjectOutput.Replace("{EMBEDDED_RESOURCE}", embeddedResources.Trim());

            File.WriteAllText("SavingTheWorldFromAngriness.csproj", crossPlatformProjectOutput);
        }
    }
}
